#include "sheet_doc.hpp"

#include <list>
#include <sstream>

LAX_DISABLE_WARNINGS()
#include <wx/filedlg.h>
#include <wx/filesys.h>
#include <wx/log.h>
#include <wx/msgdlg.h>
#include <wx/numdlg.h>
#include <wx/stdpaths.h>
#include <wx/stdstream.h>
#include <wx/textdlg.h>
#include <wx/wfstream.h>
#include <wx/zipstrm.h>
LAX_REENABLE_WARNINGS()

#include <nlohmann/json.hpp>

#include "charts/barchart.hpp"
#include "charts/line_graph.hpp"
#include "charts/chart_setup.hpp"
#include "compiler/importer/stdlib_finder.hpp"
#include "main.hpp"
#include "main_window.hpp"
#include "sheet_view.hpp"
#include "util/scope_exit.hpp"

namespace lax
{
    bool PageData::recompile(const nlohmann::json* j)
    {
        using nlohmann::json;

        ScopeExit raii([this, oldTbls = std::move(tables)]() mutable
        {
            oldTbls.swap(tables);
        });

        ValueSystem* sys;

        try
        {
            std::istringstream istr(code);
            sys = &comp.recompile(istr);
        }
        catch (CompilationFailure& cf)
        {
            wxGetApp().getWindow().logFailure(name, cf);
            return false;
        }

        for (const auto& pair: comp.getTableMap())
        {
            const ICaseString first(pair.first);

            if (auto itr = tblCache.find(first);
                itr != tblCache.end())
            {
                tables.push_back(itr->second);
                tables.back()->update(pair.second, *sys);
            }
            else
            {
                auto tbl = std::make_shared<SheetDataTable>(pair.second, *sys);
                tblCache.emplace(first, tbl);
                tables.push_back(tbl);
                tables.back()->update(pair.second, *sys);

                if (!j)
                {
                    // if loading from a file, don't add default rows
                    tbl->InsertRows(0, 8);
                }
            }
        }

        for (const auto& [n, chart]: comp.getChartMap())
        {
            ICaseString name = chart->getTable()->getTableName();
            tblCache.at(name)->addObserver(chart);
        }

        if (j)
        {
            for (const auto& [name, data]: j->at("tables").items())
            {
                from_json(data, *tblCache.at(static_cast<ICaseString>(name)));
            }

            for (const auto& [name, data]: j->at("charts").items())
            {
                std::string type;
                data.at("type").get_to(type);
                from_json(data, *comp.findChart(name, type));
            }
        }

        raii.release();
        return true;
    }

    void to_json(nlohmann::json& j, const PageData& pg)
    {
        using nlohmann::json;

        json tbls;

        for (const auto& tbl: pg.tables)
        {
            ICaseString name(tbl->getImpl().getTableName());
            tbls[*name] = *tbl;
        }

        json charts;

        for (const auto& ch: pg.comp.getChartMap())
        {
            charts[static_cast<std::string>(*ch.first)] = *ch.second;
        }

        j = json {
            {"tables", tbls},
            {"charts", charts},
            {"code", pg.code},
            {"name", pg.name},
        };
    }

    void from_json(const nlohmann::json& j, PageData& pg)
    {
        j.at("name").get_to(pg.name);
        j.at("code").get_to(pg.code);
        pg.recompile(&j);
    }

    wxIMPLEMENT_DYNAMIC_CLASS(SheetDoc, wxDocument)

LAX_DISABLE_WARNINGS()
    wxBEGIN_EVENT_TABLE(SheetDoc, wxDocument)
        EVT_MENU(MainWindow::kNewPage, SheetDoc::OnNewPage)
        EVT_MENU(wxID_UNDO, SheetDoc::OnUndo)
        EVT_MENU(wxID_REDO, SheetDoc::OnRedo)
        EVT_MENU(MainWindow::kRecalc, SheetDoc::OnRecalc)
        EVT_MENU(MainWindow::kDeletePage, SheetDoc::OnDeletePage)
        EVT_MENU(MainWindow::kRenamePage, SheetDoc::OnRenamePage)
        EVT_MENU(MainWindow::kSaveTable, SheetDoc::OnSaveTable)
        EVT_AUINOTEBOOK_PAGE_CHANGED(MainWindow::kSheetNotebook, SheetDoc::OnChangePage)
        EVT_GRID_CELL_CHANGING(SheetDoc::OnEditCell)
        EVT_TIMER(wxID_ANY, SheetDoc::OnBackup)
    wxEND_EVENT_TABLE()
LAX_REENABLE_WARNINGS()

    SheetDoc::SheetDoc()
    {
        m_importer = std::make_shared<CompileInfo>();

        auto fileFinder = std::make_shared<PathFinder>();
        std::shared_ptr<Importer> importer(m_importer, &m_importer->imp);
        m_importer->imp.addImportType("std", StdlibFinder::get());
        fileFinder->addLoader(std::make_shared<CompileLoader>(importer));

        m_importer->builders["line"_is] = std::make_unique<LineGraphBuilder>();
        m_importer->builders["bar"_is] = std::make_unique<BarChartBuilder>();

        m_autoSave.Start(60000); // autosave every minute
        m_autoSave.SetOwner(this);
    }

    bool SheetDoc::DeleteContents()
    {
        m_pages.clear();
        m_active = -1;
        return true;
    }

    bool SheetDoc::OnNewDocument()
    {
        if (!wxDocument::OnNewDocument())
        {
            return false;
        }

        m_pageNum = 1;
        wxCommandEvent evt;
        OnNewPage(evt);
        UpdateAllViews();

        return true;
    }

    bool SheetDoc::DoSaveDocument(const wxString& filename)
    {
        using nlohmann::json;

        if (m_dir != wxFileName(filename).GetPath())
        {
            // we want to check to make sure that it can be compiled in its new
            // location

            if (!changeFilename(filename) && // compilation failed
                wxMessageBox(_("Failed compilation in the new directory.\nDo "
                               "you want to save here anyway?"),
                        wxMessageBoxCaptionStr, wxYES_NO) == wxNO) // user changed mind
            {
                return false;
            }
        }

        wxTempFileOutputStream temp(filename);
        wxZipOutputStream zout(temp);

        json pages;

        for (const PageData::Ptr& page: m_pages)
        {
            pages.push_back(page->path);
            zout.PutNextEntry(wxS("Sheets/") + page->path);

            if (!writeSheet(zout, *page))
            {
                wxLogDebug(wxS("failed to write sheet %s"), page->name);
                return false;
            }
        }

        json manifest {
            {"version", 0},
            {"pages", pages},
        };

        zout.PutNextEntry(wxS("index.json"));
        wxStdOutputStream ostr(zout);

        if (!(ostr << manifest))
        {
            wxLogDebug(wxS("failed to save index of file"));
            return false;
        }

        zout.Close(); // must have zout write ending before commit temp
                      // otherwise temp will be closed when zout's dtor writes
                      // the ending
        return temp.Commit();
    }

    bool SheetDoc::writeSheet(wxOutputStream& out, const PageData& page)
    {
        // TODO (Max#1#): Save & load charts from file
        using nlohmann::json;
        json sheet = page;
        wxStdOutputStream ostr(out);
        return (ostr << sheet).good();
    } // SheetDoc::writeSheet

    bool SheetDoc::DoOpenDocument(const wxString& filename)
    {
        using nlohmann::json;

        wxFileSystem sys;
        std::unique_ptr<wxFSFile> indexFile(sys.OpenFile(filename + "#zip:index.json"));
        m_dir = wxFileName(filename).GetPath().ToStdString();
        m_filename = filename;

        if (!indexFile)
        {
            wxLogDebug(wxS("could not find index.json in file"));
            wxLogError(wxS("Failed to load spreadsheet."));
            return false;
        }

        wxInputStream* indexStrm = indexFile->GetStream();
        wxStdInputStream istr(*indexStrm);

        bool problem = false;

        try
        {
            json doc;
            istr >> doc;

            if (doc.at("version").get<int>() > 0)
            {
                wxMessageDialog dlg(wxGetApp().GetTopWindow(),
                        _("Version of file is higher than can be loaded.\n"
                          "Do you want to try loading it anyway?"),
                        wxMessageBoxCaptionStr, wxYES_NO);

                if (dlg.ShowModal() == wxNO)
                {
                    return false;
                }
            }

            json pages = doc.at("pages");

            for (const auto& pg: pages)
            {
                wxString file;
                pg.get_to(file);

                std::unique_ptr<wxFSFile> sheet(sys.OpenFile(filename +
                        wxS("#zip:Sheets/") + file));

                if (!sheet || !readSheet(*sheet->GetStream(), file))
                {
                    wxLogError(_("Could not load sheet %s"), file);
                    problem = true;
                    continue;
                }
            }
        }
        catch (nlohmann::detail::exception& ex)
        {
            wxLogDebug("problem with JSON: %s", ex.what());
            problem = true;
        }
        catch (std::exception& ex)
        {
            wxLogDebug("generic problem loading file: %s", ex.what());
            problem = true;
        }

        if (problem)
        {
            if (wxMessageBox(_("There were problems loading the spreadsheet.\n"
                           "Do you want to continue anyway?"),
                    wxMessageBoxCaptionStr, wxYES_NO | wxCENTER) == wxNO)
            {
                return false;
            }
        }

        return true;
    }

    bool SheetDoc::readSheet(wxInputStream& in, const wxString& file)
    {
        using nlohmann::json;
        wxStdInputStream istr(in);

        try
        {
            json doc;
            istr >> doc;

            m_pages.push_back(std::make_shared<PageData>("", file, m_filename,
                    m_dir, m_importer));
            doc.get_to(*m_pages.back());
        }
        catch (nlohmann::detail::exception& ex)
        {
            wxLogDebug("JSON error: %s", ex.what());
            return false;
        }
        catch (std::exception& ex)
        {
            wxLogDebug("generic error: %s", ex.what());
            return false;
        }

        return true;
    } // SheetDoc::readSheet

    void SheetDoc::OnUndo(wxCommandEvent& evt)
    {
        if (!m_commandProcessor)
        {
            evt.Skip();
            return;
        }

        m_commandProcessor->Undo();
        Modify(m_commandProcessor->IsDirty());
    }

    void SheetDoc::OnRedo(wxCommandEvent& evt)
    {
        if (!m_commandProcessor)
        {
            evt.Skip();
            return;
        }

        m_commandProcessor->Redo();
        Modify(m_commandProcessor->IsDirty());
    }

    void SheetDoc::OnNewPage(wxCommandEvent& evt)
    {
        wxString name = _("Sheet") + std::to_string(m_pageNum++);
        auto page =
        std::make_shared<PageData>(name, name, m_filename, m_dir, m_importer);
        int pgNo = m_pages.size(); // this is before we add the new page

        GetCommandProcessor()->Submit(new SheetCommand(getAddPage(pgNo, page),
                getRemovePage(pgNo, page), _("New page")));
        Modify(true);

        evt.Skip();
    }

    std::function<bool()> SheetDoc::getAddPage(int pgNo, PageData::Ptr page)
    {
        return [this, page, pgNo]
        {
            m_pages.insert(m_pages.begin() + pgNo, page);

            UpdateInfo info(UpdateInfo::kAddPage, page.get(), pgNo);
            UpdateAllViews(nullptr, &info);

            return true;
        };
    }

    std::function<bool()> SheetDoc::getRemovePage(int pgNo, PageData::Ptr page)
    {
        return [this, page, pgNo]
        {
            m_pages.erase(m_pages.begin() + pgNo);

            UpdateInfo info(UpdateInfo::kRemovePage, page.get(), pgNo);
            UpdateAllViews(nullptr, &info);

            return true;
        };
    }

    void SheetDoc::OnRenamePage(wxCommandEvent&)
    {
        PageData::Ptr page = m_pages.at(m_active);

        wxString newName = wxGetTextFromUser(_("New name for sheet:"),
                wxGetTextFromUserPromptStr, page->name);

        if (!newName.empty())
        {
            auto setName = [this, active = m_active, page](const wxString& newName)
            {
                page->name = newName;

                UpdateInfo info(UpdateInfo::kRenamePage, page.get(), active);
                UpdateAllViews(nullptr, &info);

                return true;
            };

            GetCommandProcessor()->Submit(new SheetCommand(
                    std::bind(setName, newName), std::bind(setName, page->name),
                    _("Rename sheet")));
            Modify(true);
        }
    }

    void SheetDoc::OnDeletePage(wxCommandEvent&)
    {
        auto page = m_pages.at(m_active);
        GetCommandProcessor()->Submit(new SheetCommand(
                getRemovePage(m_active, page), getAddPage(m_active, page),
                _("Delete sheet")));
        Modify(true);
    }

    void SheetDoc::OnChangePage(wxAuiNotebookEvent& evt)
    {
        if (!m_pages.empty())
        {
            m_active = evt.GetSelection();
        }

        evt.Skip();
    }

    namespace
    {
        class EditCellCommand:
            public wxCommand
        {
        public:
            EditCellCommand(Object::Ptr old, int row, int col,
                    SheetDataTable& tbl):
                wxCommand(true, _("Enter data")), m_old(old), m_row(row),
                m_col(col), m_table(tbl) {}

            bool Do() override
            {
                auto tmp = m_old;
                m_old = m_table.getObject(m_row, m_col);
                m_table.setObject(m_row, m_col, tmp);
                m_table.GetView()->ForceRefresh();
                return true;
            }

            bool Undo() override
            {
                return Do();
            }
        private:
            Object::Ptr m_old;
            int m_row;
            int m_col;
            SheetDataTable& m_table;
        };
    }

    void SheetDoc::OnEditCell(wxGridEvent& evt)
    {
        wxGrid* grid = static_cast<wxGrid*>(evt.GetEventObject());
        SheetDataTable* tbl = dynamic_cast<SheetDataTable*>(grid->GetTable());
        wxCHECK_RET(tbl, wxS("grid must have SheetDataTable data"));

        //  is not scalar
        if (tbl->isReadOnly(evt.GetRow(), evt.GetCol()))
        {
            evt.Veto(); // produced by formula; read-only
            return;
        }

        int row = evt.GetRow();
        int col = evt.GetCol();

        GetCommandProcessor()->Store(
                new EditCellCommand(tbl->getObject(row, col), row, col, *tbl));
        Modify(true);

        evt.Skip();
    }

    void SheetDoc::OnRecalc(wxCommandEvent&)
    {
        PageData& pg = *m_pages.at(m_active);

        for (const SheetDataTable::Ptr& tbl: pg.tables)
        {
            try
            {
                tbl->recalc();
            }
            catch (std::runtime_error& ex)
            {
                wxLogError(_("Could not recalculate:\n%s"), ex.what());
            }
        }

        Modify(true);
    }

    void SheetDoc::OnSaveTable(wxCommandEvent& evt)
    {
        TableBase* base = static_cast<TableBase*>(evt.GetClientData());
        SheetDataTable* tbl = static_cast<SheetDataTable*>(base);

        wxFileDialog dlg(nullptr, wxFileSelectorPromptStr, wxEmptyString,
                wxEmptyString, SheetDataTable::getWildcard(),
                wxFD_SAVE | wxFD_OVERWRITE_PROMPT);

        if (dlg.ShowModal() == wxID_OK)
        {
            tbl->saveTable(dlg.GetPath(), dlg.GetFilterIndex());
        }
    }

    bool SheetDoc::changeFilename(const wxString& newFile)
    {
        std::string dir = wxFileName(newFile).GetPath().ToStdString();
        std::string file = newFile.ToStdString();
        dir.swap(m_dir);
        file.swap(m_filename);

        ScopeExit guard([this, &dir, &file]
        {
            dir.swap(m_dir);
            file.swap(m_filename);

            for (const PageData::Ptr& page: m_pages)
            {
                page->comp.setFilename(m_filename);
                page->comp.setDir(m_dir);
            }
        });

        for (const PageData::Ptr& page: m_pages)
        {
            page->comp.setFilename(m_filename);
            page->comp.setDir(m_dir);
        }

        for (const PageData::Ptr& page: m_pages)
        {
            if (!page->recompile())
            {
                return false;
            }
        }

        guard.release();
        return true;
    }

    void SheetDoc::OnBackup(wxTimerEvent&)
    {
        if (IsModified() && !m_filename.empty())
        {
            Save();
        }
    }

    wxDEFINE_EVENT(LAX_TABLE_MOVE, wxCommandEvent);

    bool SheetCommand::Do()
    {
        return static_cast<bool>(m_doFunc) && m_doFunc();
    }

    bool SheetCommand::Undo()
    {
        return static_cast<bool>(m_undoFunc) && m_undoFunc();
    }
}
