#include "axis_chart.hpp"

LAX_DISABLE_WARNINGS()
#include <wx/control.h>
#include <wx/dcclient.h>
#include <wx/dcscreen.h>
LAX_REENABLE_WARNINGS()

#include "util/function_iterator.hpp"

namespace lax
{
LAX_DISABLE_WARNINGS()
    wxBEGIN_EVENT_TABLE(AxisChart, wxWindow)
        EVT_PAINT(AxisChart::OnPaint)
        EVT_SIZE(AxisChart::OnResize)
    wxEND_EVENT_TABLE()
LAX_REENABLE_WARNINGS()

    AxisChart::AxisChart(const ChartBase::Ptr& cd, wxWindow* parent,
            wxWindowID id, const wxPoint& pos, const wxSize& size):
        wxWindow(parent, id, pos, size), m_data(cd)
    {
        SetBackgroundColour(*wxWHITE);
        SetBackgroundStyle(wxBG_STYLE_PAINT);
    }

    wxSize AxisChart::DoGetBestClientSize() const
    {
        wxScreenDC dc;
        dc.SetFont(GetFont());

        const auto& cd = m_data->getChartData();
        const wxSize vertSize = dc.GetTextExtent(cd.verTitle);
        const wxSize horSize = dc.GetTextExtent(cd.horTitle);
        const wxSize gridSize = getGridSize();

        const int width = gridSize.x + vertSize.y + getVerLblWidth() + 25;
        updateLegendRect(dc, width);
        const int height = gridSize.y + horSize.y + getLblHeight() + 30 +
                           m_legendRect.GetBox().height;

        return wxSize(width, height);
    }

    void AxisChart::updateRects(wxDC& dc)
    {
        const auto& cd = m_data->getChartData();
        const wxSize horSize = dc.GetTextExtent(cd.horTitle);
        const wxSize verSize = dc.GetTextExtent(cd.verTitle);
        const wxSize mySize = GetSize();
        const int lblHeight = getLblHeight();

        updateLegendRect(dc, GetSize().x);

        // update chart rectangle
        const wxRect legend = m_legendRect.GetBox();

        m_chartRect.x = getChartLeft(dc);
        m_chartRect.y = legend.GetBottom() + 5;
        m_chartRect.width = mySize.x - m_chartRect.x - 10;
        m_chartRect.height = mySize.y - 20 - lblHeight - horSize.y -
                             legend.height;
        RefreshRect(m_chartRect);

        // update horizontal axis rectangle
        m_horAxisRect.x = m_chartRect.x;
        m_horAxisRect.y = m_chartRect.GetBottom() + 5 + lblHeight;
        m_horAxisRect.width = m_chartRect.width;
        m_horAxisRect.height = horSize.y;
        RefreshRect(m_horAxisRect);

        // update vertical axis rectangle
        const wxRect vertTotal(5, m_chartRect.y, verSize.y, m_chartRect.height);

        m_verAxisRect.x = vertTotal.x;
        m_verAxisRect.width = verSize.y;
        m_verAxisRect.y = vertTotal.y;
        m_verAxisRect.height = std::min(verSize.x, vertTotal.height);
        m_verAxisRect = m_verAxisRect.CenterIn(vertTotal);
        RefreshRect(m_verAxisRect);
    }

    void AxisChart::updateLegendRect(wxDC& dc, int width) const
    {
        m_legendRect.Clear();

        const auto tbl = m_data->getTable();
        const int left = getChartLeft(dc);
        const wxRect center(left, 0, width - left, 1000000);
        int height = GetFont().GetPixelSize().y + 2;
        const int squareWidth = height;
        const int maxWidth = width - left - 10;
        wxRect currentRect(0, 5, 0, height);

        for (int s = 0; s < m_data->getNumSeries(); ++s)
        {
            wxString str = getLabel(s);
            const wxSize size = dc.GetTextExtent(str);
            height = std::max(currentRect.height, height);
            currentRect.height = height;
            int width = currentRect.width + size.x + squareWidth + 4;

            if (width > maxWidth) // can't fit another one
            {
                if (currentRect.width != 0) // already fit some into row
                {
                    currentRect = currentRect.CenterIn(center, wxHORIZONTAL);
                    m_legendRect.Union(currentRect);
                }
                else // this is the first one in this row; we'll truncate it
                {
                    m_legendRect.Union(left + 5, 5, maxWidth, height);
                }

                // reset currentRect, making sure to shift it down
                currentRect = wxRect(0, currentRect.y + height + 2, 0, height);
            }
            else // can fit it; just add it to the current width
            {
                currentRect.width = width;
            }
        }

        currentRect = currentRect.CenterIn(center, wxHORIZONTAL);
        m_legendRect.Union(currentRect);
    }

    int AxisChart::getChartLeft(wxDC& dc) const
    {
        const auto& cd = m_data->getChartData();
        wxSize verSize = dc.GetTextExtent(cd.verTitle);
        return verSize.y + getVerLblWidth() + 20;
    }

    void AxisChart::OnResize(wxSizeEvent& evt)
    {
        auto newBuf = std::make_unique<wxBitmap>(evt.GetSize());
        m_dc.SelectObject(*newBuf);
        m_buffer = std::move(newBuf);
        m_redraw = true;
    }

    void AxisChart::OnPaint(wxPaintEvent&)
    {
        // TODO (Max#1#): Prevent flickering

        if (m_redraw)
        {
            redraw(m_dc);
        }

        wxPaintDC dc(this);
        dc.Blit(wxPoint(0, 0), GetSize(), &m_dc, wxPoint(0, 0));
    }

    void AxisChart::redraw(wxDC& dc)
    {
        dc.SetFont(GetFont());

        // clear background
        dc.SetPen(GetBackgroundColour());
        dc.SetBrush(GetBackgroundColour());
        dc.DrawRectangle(0, 0, m_buffer->GetWidth(), m_buffer->GetHeight());

        updateRects(dc);

        const wxRect horLblRect(m_chartRect.GetBottomLeft(),
                                wxSize(m_chartRect.width, getLblHeight()));

        const wxPoint verLblStart(m_verAxisRect.GetRight() + 5, m_chartRect.y);
        const wxPoint verLblEnd(m_chartRect.x - 5, m_chartRect.GetBottom());
        const wxRect verLblRect(verLblStart, verLblEnd);

        drawTitles(dc);
        drawGrid(dc, m_chartRect);
        drawChartBody(dc, m_chartRect);
        drawLabels(dc, horLblRect, verLblRect);
        drawLegend(dc);

        m_redraw = false;
    }

    void AxisChart::drawTitles(wxDC& dc)
    {
        // draw horizontal title
        const auto& cd = m_data->getChartData();
        wxString lbl = wxControl::Ellipsize(cd.horTitle, dc,
                            wxELLIPSIZE_MIDDLE, m_horAxisRect.width);
        dc.DrawLabel(lbl, m_horAxisRect, wxALIGN_CENTER);

        // draw vertical title
        lbl = wxControl::Ellipsize(cd.verTitle, dc,
                            wxELLIPSIZE_MIDDLE, m_verAxisRect.height);
        dc.DrawRotatedText(lbl, m_verAxisRect.GetBottomLeft(), 90.0);
    }

    void AxisChart::drawLegend(wxDC& dc)
    {
        wxRegionIterator rect(m_legendRect);
        const auto tbl = m_data->getTable();
        int remaining = rect.GetW();
        int start = rect.GetX();

        dc.SetPen(*wxBLACK_PEN);

        for (int ser = 0; ser < m_data->getNumSeries(); ++ser)
        {
            wxCHECK2_MSG(rect, break, "failed to draw legend");
            const int squareWidth = rect.GetH();
            wxString lbl = getLabel(ser);
            int width = squareWidth + dc.GetTextExtent(lbl).x;

            if (width > remaining)
            {
                // either truncate or go to the next rectangle
                if (remaining == rect.GetW() || width > (++rect).GetW())
                {
                    // either nothing has been done in this row or the next row
                    // is still too small, so we will truncate
                    lbl = wxControl::Ellipsize(lbl, dc, wxELLIPSIZE_MIDDLE,
                                               rect.GetW() - squareWidth);
                }

                // reset just in case we went to the next rectangle
                start = rect.GetX();
                remaining = rect.GetW();
            }

            wxRect square(start, rect.GetY(), squareWidth, squareWidth);
            drawLegendSquare(dc, square, ser);
            dc.DrawText(lbl, start + squareWidth + 2, rect.GetY());
            start += width + 4;
            remaining -= width + 4;
        }
    }

    wxSize AxisChart::getGridSize() const
    {
        constexpr int height = 200;
        constexpr int width = 350;
        return wxSize(width, height);
    }

    void ScaledAxis::updateRange(const ChartBase& chart, const wxFont& font,
            int maxWidth)
    {
        const auto tbl = chart.getTable();
        resetRange();

        for (int i = 0; i < chart.getNumSeries(); ++i)
        {
            auto arr = std::dynamic_pointer_cast<ArrayObject>(chart.getObject(i));

            if (!arr)
            {
                continue;
            }

            doUpdateRange(arr);
        }

        finishUpdate(font, maxWidth);
    }

    void ScaledAxis::updateRange(const Table& tbl, int series, const wxFont& f,
            int maxWidth)
    {
        resetRange();
        auto arr = std::dynamic_pointer_cast<ArrayObject>(tbl.getArray(series));
        doUpdateRange(arr);
        finishUpdate(f, maxWidth);
    }

    void ScaledAxis::doUpdateRange(const std::shared_ptr<ArrayObject>& arr)
    {
        assert(arr);

        for (const auto& obj: *arr)
        {
            auto n = std::dynamic_pointer_cast<NumberObject>(obj);

            if (!n)
            {
                continue;
            }

            double d = n->getValue();
            m_max = std::max(m_max, d);
            m_min = std::min(m_min, d);
        }
    }

    namespace
    {
        inline double numDigits(double n)
        {
            if (n == 0.0)
            {
                // avoid the NaN that would result
                return std::numeric_limits<double>::min_exponent10;
            }
            else
            {
                return std::floor(std::log10(n));
            }
        }
    }

    void ScaledAxis::finishUpdate(const wxFont& font, int maxWidth)
    {
        double maxLargestDigit = numDigits(m_max);
        double minLargestDigit = numDigits(-m_min);
        double largestDigit = std::max(maxLargestDigit, minLargestDigit);
        double mult = std::pow(10.0, largestDigit);
        m_max = std::ceil(m_max / mult) * mult;
        m_min = std::floor(m_min / mult) * mult;

        if (m_min != 0.0 && m_max != 0.0)
        {
            if (std::abs(m_min) < std::abs(m_max))
            {
                m_min = -m_max;
            }
            else
            {
                m_max = -m_min;
            }
        }

        wxSize newMaxWidth = wxDefaultSize;
        wxScreenDC dc;
        dc.SetFont(font);
        wxString str;

        for (int i = 0; i <= 10; ++i)
        {
            formatFor(str, i);
            newMaxWidth.IncTo(dc.GetTextExtent(str));
        }

        m_maxSize = newMaxWidth;

        if (maxWidth > 0)
        {
            m_drawer.setMaxWidth(maxWidth / 10);
            m_drawer.setMaxSize(m_maxSize);
            m_maxSize = m_drawer.getMaxSize();
        }
    }

    void ScaledAxis::drawGrid(wxDC& dc, const wxRect& r, wxOrientation orient) const
    {
        int lineStep, current, end;
        const int* startX;
        const int* endX;
        const int* startY;
        const int* endY;

        switch (orient)
        {
        case wxBOTH:
            // recurse for vertical, then fallthru to horizontal
            drawGrid(dc, r, wxVERTICAL);
            [[fallthrough]];
        case wxHORIZONTAL:
            lineStep = r.GetWidth() / 10;
            current = r.x;          // start at the left; end at the right
            startX = &current;      // start and end are the same
            endX = &current;        // (i.e. the position on the x-axis)
            startY = &r.y;          // y starts at the top
            end = r.GetBottom();    // and ends at the bottom
            endY = &end;
            break;
        case wxVERTICAL:
            lineStep = r.GetHeight() / 10;  // step based on height
            current = r.y;                  // go along the y-axis
            startX = &r.x;                  // x starts at the left
            end = r.GetRight();             // and ends at the right
            endX = &end;
            startY = &current;              // start and end are the same for y
            endY = &current;                // (i.e. the position on the y-axis)
            break;
        default: // illegal value for orientation
            wxFAIL;
            return;
        }

        dc.SetPen(*wxGREY_PEN);
        dc.SetBrush(*wxGREY_BRUSH);

        const int startLineX = r.GetLeft();
        const int endLineY = r.GetBottom();
        const wxPoint startVerLine(startLineX, r.GetTop());
        const wxPoint endVerLine(startLineX, endLineY);
        dc.DrawLine(startVerLine, endVerLine);

        const wxPoint startHorLine(startLineX, endLineY);
        const wxPoint endHorLine(r.GetRight(), endLineY);
        dc.DrawLine(startHorLine, endHorLine);

        if (m_min != m_max)
        {
            for (int i = 0; i < 10; ++i)
            {
                dc.DrawLine(*startX, *startY, *endX, *endY);
                current += lineStep;
            }
        }
        else
        {
            // just draw a line down the middle
            current += lineStep * 5;
            dc.DrawLine(*startX, *startY, *endX, *endY);
        }
    }

    void ScaledAxis::drawLabels(wxDC& dc, const wxRect& r, wxOrientation orient) const
    {
        wxString lbl;

        dc.SetPen(*wxGREY_PEN);
        dc.SetBrush(*wxGREY_BRUSH);

        switch (orient)
        {
        case wxHORIZONTAL:
        {
            FunctionIterator first(0, [this, &lbl](int i)->const wxString&
            {
                formatFor(lbl, i);
                return lbl;
            });

            decltype(first) last(10);

            m_drawer.setMaxWidth(r.width / 10);
            m_drawer.drawLabels(dc, first, last, r, wxALIGN_TOP | wxALIGN_LEFT);

            break;
        }
        case wxVERTICAL:
        {
            // center around the line
            wxRect drawIn(r.x, r.GetBottom() - m_maxSize.y / 2,
                          r.width, m_maxSize.y);
            const int lineStep = -r.height / 10; // negative because we go up
            constexpr int flags = wxALIGN_RIGHT | wxALIGN_TOP;

            if (m_min != m_max)
            {
                wxString lbl;

                for (int i = 0; i <= 10; ++i)
                {
                    formatFor(lbl, i);
                    dc.DrawLabel(lbl, drawIn, flags);
                    drawIn.y += lineStep;
                }
            }
            else
            {
                drawIn.y += lineStep * 5; // draw 0 in middle
                dc.DrawLabel(wxS("0"), drawIn, flags);
            }

            break;
        }
        case wxBOTH:
            // doesn't make much sense as we would be overwriting in at least
            // the top left corner; so fail
        default:
            wxFAIL;
        }
    }

    int ScaledAxis::getPos(double val, int begin, int end) const noexcept
    {
        if (m_max == m_min)
        {
            // avoid producing NaN
            return (end - begin) / 2 + begin;
        }

        // If m_min == 0.0, then fraction is in [0.0, 1.0]
        // Elif m_max == 0.0, then fraction is in [-1.0, 0.0]
        // Else fraction is in [-0.5, 0.5]
        double fraction = val / (m_max - m_min);

        // shift fraction to [0, 1]
        if (m_max == 0.0)
        {
            // we want the smallest (i.e. most negative) to be at the bottom
            // (and thus at zero)
            fraction += 1.0;
        }
        else if (m_min != 0.0)
        {
            // ditto above
            fraction += 0.5;
        }

        return fraction * (end - begin) + begin;
    }

    void ScaledAxis::formatFor(wxString& lbl, int i) const
    {
        const double lblStep = (m_max - m_min) / 10.0;

        if ((i == 5 && m_min != 0.0 && m_max != 0.0) ||
            (i == 10 && m_max == 0.0))
        {
            // generally we don't hit zero exactly, so just readjust
            lbl = wxS("0");
        }
        else
        {
            lbl.Printf("%g", std::fma(i, lblStep, m_min));
        }
    }

    namespace
    {
        constexpr double sin60 = 0.866025404;

        // Calculates the point to pass to DrawRotatedText such that what would
        // be the top right corner is at topRight. Essentially rotates 60
        // degrees about topRight.
        inline wxPoint
        getTextStart(wxDC& dc, const wxString& str, const wxPoint& topRight)
        {
            const int width = dc.GetTextExtent(str).x;
            const wxPoint pt(-width / 2, width * sin60);
            return pt + topRight;
        }
    }

    bool LabelDrawer::doUpdate(const wxSize& sz)
    {
        m_size.IncTo(sz);

        if (m_size.x > m_maxWidth)
        {
            m_size = wxSize(m_slantLength / 2, m_slantLength * sin60);
            return true;
        }

        return false;
    }

    void LabelDrawer::doDraw(wxDC& dc, const wxString& lbl, const wxRect& r,
                             int align) const
    {
        if (m_diagonal)
        {
            int middle;

            if (align & wxALIGN_RIGHT)
            {
                middle = r.GetRight();
            }
            else if (align & wxALIGN_CENTER_HORIZONTAL)
            {
                middle = (r.GetLeft() + r.GetRight()) / 2;
            }
            else
            {
                // aligned to the left
                middle = r.GetLeft();
            }

            const wxString str =
            wxControl::Ellipsize(lbl, dc, wxELLIPSIZE_MIDDLE, m_slantLength);
            const wxPoint start = getTextStart(dc, str, wxPoint(middle, r.y));
            dc.DrawRotatedText(str, start, 60.0);
        }
        else
        {
            dc.DrawLabel(lbl, r, align);
        }
    }
}
