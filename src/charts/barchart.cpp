#include "barchart.hpp"

#include <unordered_map>

LAX_DISABLE_WARNINGS()
#include <wx/config.h>
#include <wx/dcbuffer.h>
#include <wx/dcclient.h>
#include <wx/dcscreen.h>
#include <wx/log.h>
#include <wx/propgrid/propgrid.h>
#include <wx/propgrid/advprops.h>
#include <wx/propgrid/propgridiface.h>
#include <wx/sizer.h>
LAX_REENABLE_WARNINGS()

#include <nlohmann/json.hpp>

#include "axis_chart.hpp"
#include "chart_props.hpp"
#include "graphics_defaults.hpp"
#include "main.hpp"
#include "util/function_iterator.hpp"

namespace lax
{
    class BarChartCtrl:
        public AxisChart
    {
    public:
        explicit BarChartCtrl(wxWindow* parent, BarChart* bc);

        void informChange(wxPropertyGridEvent& prop);
        void updateSeries();
    private:
        int getLblHeight() const override;
        void drawLabels(wxDC& dc, const wxRect& horiz, const wxRect& vert) const override;
        void drawChartBody(wxDC& dc, const wxRect& r) const override;
        void drawGrid(wxDC& dc, const wxRect& r) const override;
        wxSize getGridSize() const override;
        int getVerLblWidth() const override;
        void drawLegendSquare(wxDC& dc, const wxRect& r, int seriesIndex) const override;

        int getChartLeft(wxDC& dc) const
        {
            const auto& cd = getChartData()->getChartData();
            wxSize verSize = dc.GetTextExtent(cd.verTitle);
            return verSize.y + m_vertAxis.getMaxSize().x + 20;
        }

        int getMaxLblWidth() const noexcept
        {
            return getChartData()->getNumSeries() * (m_barWidth + m_barSpace);
        }

        auto getLblIters() const
        {
            const int col = getChartData()->getIndex();
            const auto tbl = getChartData()->getTable();
            const auto arr = std::dynamic_pointer_cast<ArrayObject>(tbl->getArray(col));
            assert(arr);

            FunctionIterator first(0, [arr](int row)
            {
                using namespace std::literals::string_literals;

                if (auto obj = (*arr)[row])
                {
                    return obj->stringize();
                }
                else
                {
                    return ""s;
                }
            });

            decltype(first) last(tbl->getNumRows());

            return std::pair(std::move(first), std::move(last));
        }

        ScaledAxis m_vertAxis;
        mutable LabelDrawer m_horAxis;
        BarChart* m_bars;

        constexpr static int m_barWidth = 30;
        constexpr static int m_barSpace = 5;
        constexpr static int m_indexGap = 10;
        constexpr static int m_slantHeight = 60;
    };

    BarChartCtrl::BarChartCtrl(wxWindow* parent, BarChart* bc):
        AxisChart(staticWeakCast<ChartBase>(bc->getWeakRef()),
                parent, wxID_ANY), m_horAxis(0, m_slantHeight), m_bars(bc)
    {
        SetBackgroundColour(*wxWHITE);
        SetBackgroundStyle(wxBG_STYLE_PAINT);
        updateSeries();
    }

    int BarChartCtrl::getLblHeight() const
    {
        const auto tbl = getChartData()->getTable();
        if (tbl->getNumRows() == 0)
        {
            return 0;
        }

        auto [first, last] = getLblIters();
        m_horAxis.setMaxWidth(getGridSize().x / tbl->getNumRows());
        m_horAxis.updateSize(first, last);
        return m_horAxis.getMaxSize().y;
    }

    namespace
    {
        // Calculates the point to pass to DrawRotatedText such that what would
        // be the top right corner is at topRight. Essentially rotates 60
        // degrees about topRight.
        inline wxPoint
        getTextStart(wxDC& dc, const wxString& str, const wxPoint& topRight)
        {
            constexpr double sin60 = 0.866025404;

            const int width = dc.GetTextExtent(str).x;
            const wxPoint pt(-width / 2, width * sin60);
            return pt + topRight;
        }
    }

    void BarChartCtrl::drawLabels(wxDC& dc, const wxRect& horiz, const wxRect& vert) const
    {
        // draw horizontal labels
        auto [first, last] = getLblIters();
        m_horAxis.drawLabels(dc, first, last, horiz);

        // draw vertical labels
        m_vertAxis.drawLabels(dc, vert, wxVERTICAL);
    }

    void BarChartCtrl::drawGrid(wxDC& dc, const wxRect& r) const
    {
        m_vertAxis.drawGrid(dc, r, wxVERTICAL);
    }

    namespace
    {
        void makeLegal(wxRect& r) noexcept
        {
            if (r.width < 0)
            {
                r.x += r.width;
                r.width = -r.width;
            }

            if (r.height < 0)
            {
                r.y += r.height;
                r.height = -r.height;
            }
        }
    }

    void BarChartCtrl::drawChartBody(wxDC& dc, const wxRect& r) const
    {
        dc.SetPen(*wxBLACK_PEN);

        const int begin = r.GetBottom();
        const int end = r.y;

        const auto& tbl = *getChartData()->getTable();
        const int numGroups = tbl.getNumRows();
        wxPoint onAxis(r.x + m_indexGap, m_vertAxis.getPos(0.0, begin, end));
        const int numSeries = getChartData()->getNumSeries();

        for (int group = 0; group < numGroups; ++group)
        {
            for (int ser = 0; ser < numSeries; ++ser)
            {
                dc.SetBrush(*m_bars->m_brushes[ser]);

                const auto col = getChartData()->getObject(ser);
                const auto g = std::dynamic_pointer_cast<ArrayObject>(col);
                const auto num = std::dynamic_pointer_cast<NumberObject>((*g)[group]);

                if (num)
                {
                    const int offX = onAxis.x + m_barWidth;
                    const int offY = m_vertAxis.getPos(num->getValue(), begin, end);
                    wxRect barRect(onAxis, wxPoint(offX, offY));
                    makeLegal(barRect);

                    dc.DrawRectangle(barRect);
                }

                onAxis.x += m_barSpace + m_barWidth;
            }

            onAxis.x += m_indexGap - m_barSpace;
        }
    }

    void BarChartCtrl::updateSeries()
    {
        m_vertAxis.updateRange(*getChartData(), GetFont());

        InvalidateBestSize();
        wxSize newSize = GetBestSize();
        SetSize(newSize);
        GetParent()->Fit();

        const int size = getChartData()->getNumSeries();
        m_bars->m_brushes.resize(size, nullptr);

        for (int i = 0; i < size; ++i)
        {
            ICaseString name(getChartData()->getSeriesName(i));
            m_bars->m_brushes[i] = m_bars->getBrushFor(name);
        }
    }

    void BarChartCtrl::informChange(wxPropertyGridEvent& evt)
    {
        auto prop = ChartSetup::findProperty(evt.GetPropertyName());
        const wxVariant v = evt.GetValue();
        bool doFit = false;

        // Note that some changes only happen when !evt.CanVeto().
        // This is because that indicates that this is an EVT_PG_CHANGED,
        // meaning that changes in the chart's data (upon which these actions
        // rely) have already taken place.

        switch (prop)
        {
        case ChartSetup::Property::kHorTitleName:
            if (!evt.CanVeto()) // is changed event
            {
                InvalidateBestSize();
                doFit = true;
                repaint();
            }

            break;
        case ChartSetup::Property::kVertTitleName:
            if (!evt.CanVeto())
            {
                InvalidateBestSize();
                doFit = true;
                repaint();
            }

            break;
        case ChartSetup::Property::kTitleName:
            break;
        case ChartSetup::Property::kNotFound:
        case ChartSetup::Property::kNumProps:
        {
            long index;

            if (!evt.GetPropertyName().ToLong(&index))
            {
                wxLogDebug("not implemented");
            }
            else if (!evt.CanVeto()) // is series brush and is done
            {
                *m_bars->m_brushes.at(index) << evt.GetPropertyValue();
                repaint();
            }

            break;
        }
        }

        if (doFit)
        {
            wxSize newSize = GetBestSize();
            SetSize(newSize);
            GetParent()->Fit();
        }
    }

    wxSize BarChartCtrl::getGridSize() const
    {
        const int height = AxisChart::getGridSize().y;

        // find width
        const auto tbl = getChartData()->getTable();
        const int numGroups = tbl->getNumRows();
        const int numSeries = getChartData()->getNumSeries();
        const int groupWidth = m_barWidth * numSeries + m_barSpace * (numSeries - 1);
        const int barsWidth = groupWidth * numGroups + (numGroups + 1) * m_indexGap;

        return wxSize(barsWidth, height);
    }

    int BarChartCtrl::getVerLblWidth() const
    {
        return m_vertAxis.getMaxSize().x;
    }

    void BarChartCtrl::drawLegendSquare(wxDC& dc, const wxRect& r, int seriesIndex) const
    {
        dc.SetBrush(*m_bars->m_brushes[seriesIndex]);
        dc.DrawRectangle(r);
    }

    class BarChartSetup:
        public ChartSetup
    {
    public:
        BarChartSetup(wxWindow* parent, BarChart* data);
    private:
        void OnChangeProperty(wxPropertyGridEvent& evt);

        wxDECLARE_EVENT_TABLE();
    };

LAX_DISABLE_WARNINGS()
    wxBEGIN_EVENT_TABLE(BarChartSetup, ChartSetup)
        EVT_PG_CHANGING(wxID_ANY, BarChartSetup::OnChangeProperty)
        EVT_PG_CHANGED(wxID_ANY, BarChartSetup::OnChangeProperty)
    wxEND_EVENT_TABLE()
LAX_REENABLE_WARNINGS()

    BarChartSetup::BarChartSetup(wxWindow* parent, BarChart* data):
        ChartSetup(parent, staticWeakCast<ChartBase>(data->getWeakRef()))
    {
        wxString str;

        data->m_customize = nullptr;
        wxPropertyGridInterface* pgi = getPropGrid();

        for (int i = 0; i < data->getNumSeries(); ++i)
        {
            str.Printf("%d", i);
            ICaseString name(getChart()->getSeriesName(i));
            wxBrush b = *data->getBrushFor(name);
            wxString lbl = getChart()->getSeriesLabel(i);

            auto prop = pgi->Append(new BrushProperty(lbl, str, b));
            pgi->Expand(prop);

            if (!data->m_customize)
            {
                data->m_customize = prop->GetParent();
            }
        }
    }

    void BarChartSetup::OnChangeProperty(wxPropertyGridEvent& evt)
    {
        auto bars = staticWeakCast<BarChart>(getChart());

        if (bars->m_ctrl.get())
        {
            bars->m_ctrl->informChange(evt);
        }

        if (!evt.WasVetoed())
        {
            evt.Skip();
        }
    }

    wxWindow* BarChart::makeChart(wxWindow* parent)
    {
        m_ctrl = new BarChartCtrl(parent, this);
        return m_ctrl.get();
    }

    ChartSetup* BarChart::createSetup(wxWindow* parent)
    {
        return new BarChartSetup(parent, this);
    }

    bool BarChart::updateIndexAndSeries()
    {
        if (auto ctrl = m_ctrl.get())
        {
            ctrl->updateSeries();
        }

        return true;
    }

    void BarChart::doUpdate(int)
    {
        if (auto ctrl = m_ctrl.get())
        {
            ctrl->updateSeries();
            ctrl->repaint();
        }
    }

    void BarChart::onColRename(const SheetDataTable::Ptr& tbl, int c)
    {
        const int idx = getColumnIndex(c);

        if (idx < 0)
        {
            // couldn't find column; ignore it
            return;
        }

        doUpdate(-1);
        m_customize->Item(idx)->SetLabel(tbl->GetColLabelValue(c));
    }

    wxBrush* BarChart::getBrushFor(const ICaseString& name)
    {
        if (auto itr = m_data->brushMap.find(name); itr != m_data->brushMap.end())
        {
            return &itr->second;
        }
        else
        {
            wxBrush b = GraphicsDefaults::get().getDefaultBrush(m_data->currentColor++);
            return &m_data->brushMap.emplace(name, b).first->second;
        }
    }

    void BarChart::serialize(nlohmann::json& j) const
    {
        for (wxBrush* b: m_brushes)
        {
            nlohmann::json brush {
                {"color", b->GetColour().GetRGBA()},
                {"style", brushStyleToString(b->GetStyle())},
            };

            j.push_back(brush);
        }
    }

    void BarChart::deserialize(const nlohmann::json& j)
    {
        if (j.size() > static_cast<std::size_t>(getNumSeries()))
        {
            throw std::runtime_error("too many brushes for bar chart");
        }

        std::size_t i = 0;

        for (const auto& b: j)
        {
            wxColour c(b.at("color").get<wxUint32>());
            int style = stringToBrushStyle(b.at("style").get<std::string>());
            wxBrush brush = *wxTheBrushList->FindOrCreateBrush(c, style);

            ICaseString name(getSeriesName(i));
            wxBrush& myBrush = m_data->brushMap[name];
            myBrush = brush;
            m_brushes.push_back(&myBrush);
            ++i;
        }
    }

namespace
{
    struct BarChartPData
    {
        BarChartPData(ICaseStringView name)
        {
            data.cd.title = wxString(name->begin(), name->end());
        }

        wxPoint pos;
        BarChart::BarData data;
    };
}

    std::any BarChartBuilder::makePData() const
    {
        return std::make_any<BarChartPData>(m_name);
    }

    ChartBuilder::Ptr BarChartBuilder::clone(const std::shared_ptr<Table>& tbl,
            ICaseStringView name, PDataManager& mgr, ValueSystem& sys)
    {
        auto ret = std::make_unique<BarChartBuilder>();
        ret->m_name = name;
        BarChartPData* bcpd = std::any_cast<BarChartPData>(&ret->getPData(mgr));
        ret->m_chart = makeValue<BarChart>(sys, bcpd->data);
        ret->m_chart->setTable(tbl);
        ret->m_chart->setPosition(bcpd->pos);
        return ret;
    }

    WeakRef<ChartBase> BarChartBuilder::build()
    {
        if (m_chart->getNumSeries() == 0)
        {
            throw Failure("bar chart needs at least one series element");
        }

        m_chart->addDependencies();
        return m_chart;
    }

    void BarChartBuilder::setIndex(const Table&, int idx)
    {
        m_chart->setIndex(idx);
    }

    void BarChartBuilder::addSeriesArray(const Table& tbl, int arrIndex)
    {
        auto expect = ArrayObject::theType(NumberObject::theType());

        if (tbl.getNode(arrIndex)->getType() != expect)
        {
            throw Failure("series for bar chart must be array of Number");
        }

        m_chart->addArraySeries(arrIndex);
    }

    void BarChartBuilder::addSeriesFunc(const Table&, int)
    {
        throw Failure("functions may not be used for bar chart's series");
    }
}
