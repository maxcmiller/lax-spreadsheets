#include "chart_setup.hpp"

#include <memory>

LAX_DISABLE_WARNINGS()
#include <wx/choicdlg.h>
#include <wx/choicebk.h>
#include <wx/log.h>
#include <wx/propgrid/propgrid.h>
#include <wx/propgrid/advprops.h>
#include <wx/propgrid/manager.h>
#include <wx/sizer.h>
LAX_REENABLE_WARNINGS()

#include <nlohmann/json.hpp>

#include "main.hpp"
#include "table.hpp"

using ChartBasePtr = std::shared_ptr<lax::ChartBase>;
WX_PG_DECLARE_VARIANT_DATA(ChartBasePtr)
WX_PG_IMPLEMENT_VARIANT_DATA(ChartBasePtr)

namespace lax
{
    void ChartBase::setIndex(int idx)
    {
        addDependency(m_tbl->getNode(idx), true);
        m_index = idx;
    }

    void ChartBase::addArraySeries(int s)
    {
        addDependency(m_tbl->getNode(s), true);
        m_series.push_back(s);
    }

    void ChartBase::addScalarSeries(int s)
    {
        addDependency(m_tbl->getScalar(s).node, false);
        m_series.push_back(-s - 1);
    }

    int ChartBase::getColumnIndex(int tblCol) const
    {
        for (std::size_t i = 0; i < m_series.size(); ++i)
        {
            if (m_series[i] == tblCol)
            {
                return i;
            }
        }

        return -1;
    }

    wxString ChartBase::getSeriesLabel(int s) const
    {
        int ser = m_series.at(s);

        if (ser < 0)
        {
            ser = -ser - 1;

            const auto& sd = m_tbl->getScalarData(ser);
            const auto* info = std::any_cast<SheetDataTable::ScalarInfo>(&sd);

            return info? info->label:wxString();
        }
        else
        {
            const auto& cd = m_tbl->getColData(ser);
            const auto* info = std::any_cast<SheetDataTable::ColumnInfo>(&cd);
            return info? info->label:wxString();
        }
    }

    ICaseStringView ChartBase::getSeriesName(int s) const
    {
        int ser = m_series.at(s);

        if (ser < 0)
        {
            ser = -ser - 1;
            return m_tbl->getScalar(ser).name;
        }
        else
        {
            return m_tbl->getName(ser);
        }
    }

    ValueNode::Ptr ChartBase::getNode(int s) const
    {
        int ser = m_series.at(s);

        if (ser < 0)
        {
            ser = -ser - 1;
            return m_tbl->getScalar(ser).node;
        }
        else
        {
            return m_tbl->getNode(ser);
        }
    }

LAX_DISABLE_WARNINGS()
    wxBEGIN_EVENT_TABLE(ChartSetup, wxPanel)
        EVT_PG_CHANGING(wxID_ANY, ChartSetup::OnChange)
    wxEND_EVENT_TABLE()
LAX_REENABLE_WARNINGS()

    ChartSetup::ChartSetup(wxWindow* parent, const ChartBase::Ptr& data):
        wxPanel(parent), m_chart(data)
    {
        const auto& cd = data->getChartData();
        auto sizer = std::make_unique<wxBoxSizer>(wxVERTICAL);
        wxPropertyGrid* setup = new wxPropertyGrid(this);
        m_propGrid = setup;

        wxPGProperty* titles =
        setup->Append(new wxPropertyCategory(_("Chart and Axis Titles"),
                    wxS("titleCat")));

        titles->AppendChild(new wxStringProperty(_("Title"), wxS("title"),
                cd.title));
        titles->AppendChild(new wxStringProperty(_("Horizontal title"),
                wxS("horTitle"), cd.horTitle));
        titles->AppendChild(new wxStringProperty(_("Vertical title"),
                wxS("vertTitle"), cd.verTitle));

        m_customize = new wxPropertyCategory(_("Customize"), wxS("customizeCat"));
        setup->Append(m_customize);
        sizer->Add(setup, wxSizerFlags(1).Expand());
        SetSizer(sizer.release());

        m_chart->setSetup(this);
    }

    void ChartSetup::OnChange(wxPropertyGridEvent& evt)
    {
        wxVariant v = evt.GetValue();
        auto& cd = m_chart->getChartData();

        switch (findProperty(evt.GetPropertyName()))
        {
        case Property::kTitleName:
            cd.title = v.GetString();
            break;
        case Property::kHorTitleName:
            cd.horTitle = v.GetString();
            break;
        case Property::kVertTitleName:
            cd.verTitle = v.GetString();
            break;
        case Property::kNotFound:
        case Property::kNumProps:
            break;
        }

        evt.Skip();
    }

    ChartSetup::Property ChartSetup::findProperty(const wxString& name)
    {
        static std::unordered_map<wxString, Property, wxStringHash> map
        {
            std::pair(wxS("title"), Property::kTitleName),
            std::pair(wxS("horTitle"), Property::kHorTitleName),
            std::pair(wxS("vertTitle"), Property::kVertTitleName),
        };

        if (auto itr = map.find(name); itr != map.end())
        {
            return itr->second;
        }
        else
        {
            return Property::kNotFound;
        }
    }

    void to_json(nlohmann::json& j, const ChartBase& cd)
    {
        using nlohmann::json;

        json extra;
        cd.serialize(extra);

        const auto& data = cd.getChartData();

        j = json {
            {"name", data.title},
            {"horTitle", data.horTitle},
            {"verTitle", data.verTitle},
            {"xPos", cd.getPosition().x},
            {"yPos", cd.getPosition().y},
            {"type", cd.getId()},
            {"chartSpecific", extra}
        };
    }

    void from_json(const nlohmann::json& j, ChartBase& cd)
    {
        wxPoint pos;
        std::string type;
        auto& data = cd.getChartData();

        j.at("name").get_to(data.title);
        j.at("horTitle").get_to(data.horTitle);
        j.at("verTitle").get_to(data.verTitle);
        j.at("xPos").get_to(pos.x);
        j.at("yPos").get_to(pos.y);
        j.at("type").get_to(type);

        cd.setPosition(pos);
        cd.deserialize(j.at("chartSpecific"));
    }
}
