#ifndef LAX_SRC_CHARTS_GRAPHICS_DEFAULTS_HPP_
#define LAX_SRC_CHARTS_GRAPHICS_DEFAULTS_HPP_

#include "disable_warnings.h"

#include <unordered_map>
#include <vector>

LAX_DISABLE_WARNINGS()
#include <wx/brush.h>
#include <wx/colour.h>
#include <wx/pen.h>
LAX_REENABLE_WARNINGS()

namespace lax
{
    class GraphicsSettingsDlg;

    class GraphicsDefaults
    {
    public:
        GraphicsDefaults(const GraphicsDefaults&) = delete;

        static GraphicsDefaults& get()
        {
            static GraphicsDefaults gd;
            return gd;
        }

        wxBrush getDefaultBrush(int n) const;
        wxPen getDefaultPen(int n) const;
        void showDialog();

        static const std::unordered_map<int, wxString>& brushNames();
        static const std::unordered_map<int, wxString>& penNames();
    private:
        GraphicsDefaults()
        {
            readConfig();
        }

        void readConfig();
        void writeConfig();

        std::vector<wxColour> m_colors;
        std::vector<int> m_brushStyles;
        std::vector<int> m_penStyles;
        int m_penWidth;

        friend class GraphicsSettingsDlg;
    };

    int stringToBrushStyle(std::string_view str);
    const char* brushStyleToString(int style);

    int stringToPenStyle(std::string_view str);
    const char* penStyleToString(int style);
}

#endif // LAX_SRC_CHARTS_GRAPHICS_DEFAULTS_HPP_
