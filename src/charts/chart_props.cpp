#include "chart_props.hpp"

LAX_DISABLE_WARNINGS()
#include <wx/dc.h>
#include <wx/log.h>
#include <wx/propgrid/advprops.h>
#include <wx/propgrid/propgrid.h>
#include <wx/propgrid/props.h>
LAX_REENABLE_WARNINGS()

namespace lax
{
    BrushProperty::BrushProperty(const wxString& label, const wxString& name,
            const wxBrush& b):
        wxPGProperty(label, name), m_brush(b)
    {
        AddPrivateChild(new wxColourProperty(_("Color"), wxS("color"), b.GetColour()));
        AddPrivateChild(new wxEnumProperty(_("Style"), wxS("style"),
                getStyles()));
        SetValue(m_value << b);
    }

    void BrushProperty::OnCustomPaint(wxDC& dc, const wxRect& rect, wxPGPaintData&)
    {
        dc.SetBrush(m_brush);
        dc.DrawRectangle(rect);
    }

    wxVariant BrushProperty::ChildChanged(wxVariant& thisValue, int childIndex,
            wxVariant& childValue) const
    {
        wxBrush b;
        b << thisValue;

        switch (childIndex)
        {
        case 0: // color
        {
            wxColour c;
            c << childValue;
            b = *wxTheBrushList->FindOrCreateBrush(c, b.GetStyle());
            break;
        }
        case 1: // style
        {
            int i = childValue.GetInteger();
            b = *wxTheBrushList->FindOrCreateBrush(b.GetColour(), m_styles[i]);
            break;
        }
        default:
            wxFAIL_MSG("unhandled case in BrushProperty");
        }

        thisValue << b;
        return thisValue;
    }

    void BrushProperty::OnSetValue()
    {
        m_brush << m_value;

        wxVariant color;
        color << m_brush.GetColour();

        Item(0)->SetValue(color);
        Item(1)->SetValue(findStyle(m_brush.GetStyle()));
    }

    wxPGChoices& BrushProperty::getStyles()
    {
        static wxPGChoices ch;

        if (ch.GetCount() == 0)
        {
            wxArrayString names;

            names.push_back(_("Solid"));
            names.push_back(_("Back diagonal hatch"));
            names.push_back(_("Cross diagonal hatch"));
            names.push_back(_("Forward diagonal hatch"));
            names.push_back(_("Cross hatch"));
            names.push_back(_("Horizontal hatch"));
            names.push_back(_("Vertical hatch"));

            ch.Add(names);
        }

        return ch;
    }

    constexpr int BrushProperty::findStyle(int style)
    {
        for (std::size_t i = 0; i < sizeof(m_styles) / sizeof(m_styles[0]); ++i)
        {
            if (style == m_styles[i])
            {
                return i;
            }
        }

        return -1;
    }

    PenProperty::PenProperty(const wxString& title, const wxString& name,
            const wxPen& p):
        wxPGProperty(title, name), m_pen(p)
    {
        AddPrivateChild(new wxColourProperty(_("Color"), wxS("color"),
                p.GetColour()));
        AddPrivateChild(new wxEnumProperty(_("Style"), wxS("style"),
                getStyles()));

        wxPGProperty* width = new wxIntProperty(_("Width"), wxS("width"), 2);
        AddPrivateChild(width);

        width->SetAttribute("Min", 0);
        width->SetAttribute("Max", 10);
        width->SetAttribute("Wrap", false);
        width->SetEditor(wxPGEditor_SpinCtrl);

        SetValue(m_value << p);
    }

    void PenProperty::OnCustomPaint(wxDC& dc, const wxRect& rect, wxPGPaintData&)
    {
        const int y = (rect.GetTop() + rect.GetBottom()) / 2;
        dc.SetPen(m_pen);
        dc.DrawLine(rect.GetLeft(), y, rect.GetRight(), y);
    }

    wxVariant PenProperty::ChildChanged(wxVariant& thisValue, int childIndex,
            wxVariant& childValue) const
    {
        wxPen p;
        p << thisValue;

        switch (childIndex)
        {
        case 0: // color
        {
           wxColour c;
           c << childValue;
           p = *wxThePenList->FindOrCreatePen(c, p.GetWidth(), p.GetStyle());
           break;
        }
        case 1: // style
        {
            int style = childValue.GetInteger();
            p = *wxThePenList->FindOrCreatePen(p.GetColour(), p.GetWidth(), style);
            break;
        }
        case 2: // width
        {
            int width = childValue.GetInteger();
            p = *wxThePenList->FindOrCreatePen(p.GetColour(), width, p.GetStyle());
            break;
        }
        default:
            wxFAIL_MSG("unhandled case in PenProperty::ChildChanged");
        }

        thisValue << p;
        return thisValue;
    }

    void PenProperty::OnSetValue()
    {
        m_pen << m_value;

        wxVariant color;
        color << m_pen.GetColour();

        int style = m_pen.GetStyle();
        int index = getStyles().Index(style);

        Item(0)->SetValue(color);
        Item(1)->SetValueFromInt(index);
        Item(2)->SetValue(m_pen.GetWidth());
    }

    wxPGChoices& PenProperty::getStyles()
    {
        static wxPGChoices ch;

        if (ch.GetCount() == 0)
        {
            ch.Add(_("Solid"), wxPENSTYLE_SOLID);
            ch.Add(_("Dotted"), wxPENSTYLE_DOT);
            ch.Add(_("Long dashed"), wxPENSTYLE_LONG_DASH);
            ch.Add(_("Short dashed"), wxPENSTYLE_SHORT_DASH);
            ch.Add(_("Dot-dash"), wxPENSTYLE_DOT_DASH);
        }

        return ch;
    }
}

WX_PG_IMPLEMENT_VARIANT_DATA_DUMMY_EQ(wxBrush)
WX_PG_IMPLEMENT_VARIANT_DATA_DUMMY_EQ(wxPen)
