#ifndef LAX_SRC_CHARTS_LINE_GRAPH_HPP_
#define LAX_SRC_CHARTS_LINE_GRAPH_HPP_

#include "disable_warnings.h"

LAX_DISABLE_WARNINGS()
#include <wx/pen.h>
LAX_REENABLE_WARNINGS()

#include "chart_setup.hpp"
#include "compiler/top_compiler.hpp"

namespace lax
{
    class LineGraphCtrl;
    class LineGraphSetup;

    class LineGraph:
        public ChartBase
    {
    public:
        using PenMap = NameMap<wxPen>;

        struct LineData
        {
            ChartData cd;
            PenMap penMap;
            int currentPen = 0;
            int ptRadius = 5;
            bool connect = true;
        };

        explicit LineGraph(ValueSystem& sys, LineData& d):
            ChartBase(sys, d.cd), m_dispData(&d) {}

        LineGraph(const LineGraph& lg):
            LineGraph(lg.getSystem(), *lg.m_dispData)
        {
            setPosition(lg.getPosition());
        }

        wxWindow* makeChart(wxWindow* parent) override;
        ChartSetup* createSetup(wxWindow* parent) override;
        bool updateIndexAndSeries() override;
        void serialize(nlohmann::json& root) const override;
        void deserialize(const nlohmann::json& root) override;

        wxPen& getPenFor(const ICaseString& s);

        std::string_view getId() const noexcept override
        {
            return "line";
        }
    private:

        void OnPropertyChange(wxPropertyGridEvent& evt);

        void doUpdate(int row) override;
        void onColRename(const SheetDataTable::Ptr& tbl, int col) override;
        void onScalarRename(const SheetDataTable::Ptr& tbl, int col) override;
        void setPropTitle(int index, const wxString& title);

        wxWeakRef<LineGraphCtrl> m_ctrl;
        LineData* const m_dispData;
        std::vector<wxPen*> m_pens;
        wxPGProperty* m_customize = nullptr;
        int m_firstPen = std::numeric_limits<int>::min();

        friend class LineGraphCtrl;
        friend class LineGraphSetup;
    };

    class LineGraphBuilder:
        public ChartBuilder, public CodeObject
    {
    public:
        Ptr clone(const std::shared_ptr<Table>& tbl, ICaseStringView name,
                PDataManager&, ValueSystem& sys) override;
        WeakRef<ChartBase> build() override;
        void setIndex(const Table& tbl, int idx) override;
        void addSeriesArray(const Table& tbl, int arrIndex) override;
        void addSeriesFunc(const Table& tbl, int scalarIndex) override;
    private:
        std::string_view getType() const noexcept override
        {
            return "line_graph";
        }

        ICaseStringView getName() const noexcept override
        {
            return m_name;
        }

        std::any makePData() const override;

        WeakRef<LineGraph> m_chart;
        ICaseStringView m_name;
        bool m_hasArraySeries = false;
    };
}

#endif // LAX_SRC_CHARTS_LINE_GRAPH_HPP_
