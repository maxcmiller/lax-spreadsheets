#ifndef LAX_SRC_CHARTS_CHART_PROPS_HPP_
#define LAX_SRC_CHARTS_CHART_PROPS_HPP_

#include "disable_warnings.h"

#include <memory>

LAX_DISABLE_WARNINGS()
#include <wx/bitmap.h>
#include <wx/brush.h>
#include <wx/pen.h>
#include <wx/window.h>
#include <wx/propgrid/editors.h>
#include <wx/propgrid/property.h>
LAX_REENABLE_WARNINGS()

WX_PG_DECLARE_VARIANT_DATA(wxBrush)
WX_PG_DECLARE_VARIANT_DATA(wxPen)

namespace lax
{
    namespace math
    {
        class Regression;
    }

    class BrushProperty:
        public wxPGProperty
    {
    public:
        BrushProperty(const wxString& label, const wxString& name,
                const wxBrush& b);

        void OnCustomPaint(wxDC& dc, const wxRect& rect,
                    wxPGPaintData& paintData) override;
        wxVariant ChildChanged(wxVariant& thisValue, int childIndex,
                    wxVariant& childValue) const override;
        void OnSetValue() override;

        wxVariant DoGetValue() const override
        {
            wxVariant v;
            return v << m_brush;
        }

        wxSize OnMeasureImage(int = -1) const override
        {
            return wxDefaultSize;
        }
    private:
        static wxPGChoices& getStyles();
        constexpr int findStyle(int style);

        constexpr static int m_styles[] = {
            wxBRUSHSTYLE_SOLID,
            wxBRUSHSTYLE_BDIAGONAL_HATCH,
            wxBRUSHSTYLE_CROSSDIAG_HATCH,
            wxBRUSHSTYLE_FDIAGONAL_HATCH,
            wxBRUSHSTYLE_CROSS_HATCH,
            wxBRUSHSTYLE_HORIZONTAL_HATCH,
            wxBRUSHSTYLE_VERTICAL_HATCH,
        };

        wxBrush m_brush;
    };

    class PenProperty:
        public wxPGProperty
    {
    public:
        PenProperty(const wxString& title, const wxString& name,
                    const wxPen& p);

        void OnCustomPaint(wxDC& dc, const wxRect& rect, wxPGPaintData&) override;
        wxVariant ChildChanged(wxVariant& thisValue, int childIndex,
                wxVariant& childValue) const override;
        void OnSetValue() override;

        wxVariant DoGetValue() const override
        {
            wxVariant v;
            return v << m_pen;
        }

        wxSize OnMeasureImage(int = -1) const override
        {
            return wxDefaultSize;
        }
    private:
        static wxPGChoices& getStyles();

        wxPen m_pen;
    };
}

#endif // LAX_SRC_CHARTS_CHART_PROPS_HPP_
