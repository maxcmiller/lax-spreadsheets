#ifndef LAX_SRC_CHARTS_BARCHART_HPP_
#define LAX_SRC_CHARTS_BARCHART_HPP_

#include "../disable_warnings.h"

LAX_DISABLE_WARNINGS()
#include <wx/brush.h>
#include <wx/weakref.h>
LAX_REENABLE_WARNINGS()

#include "chart_setup.hpp"
#include "compiler/top_compiler.hpp"

namespace lax
{
    class BarChartCtrl;
    class BarChartSetup;

    class BarChart:
        public ChartBase
    {
    public:
        using BrushMap = NameMap<wxBrush>;

        struct BarData
        {
            ChartData cd;
            BrushMap brushMap;
            int currentColor = 0;
        };

        BarChart(ValueSystem& sys, BarData& bd):
            ChartBase(sys, bd.cd), m_data(&bd) {}

        BarChart(const BarChart& bc):
            BarChart(getSystem(), *bc.m_data)
        {
            setPosition(bc.getPosition());
        }

        wxWindow* makeChart(wxWindow* parent) override;
        ChartSetup* createSetup(wxWindow* parent) override;
        bool updateIndexAndSeries() override;
        void serialize(nlohmann::json& j) const override;
        void deserialize(const nlohmann::json& j) override;

        wxBrush* getBrushFor(const ICaseString& name);

        std::string_view getId() const noexcept override
        {
            return "bar";
        }

        static int stringToStyle(std::string_view str);
    private:
        void doUpdate(int row) override;
        void onColRename(const SheetDataTable::Ptr& tbl, int col);

        // scalars don't concern us; only columns are allowed in bar charts
        void onScalarRename(const SheetDataTable::Ptr&, int) {}

        void OnChangeProperty(wxPropertyGridEvent& evt);

        wxWeakRef<BarChartCtrl> m_ctrl;
        BarData* const m_data;
        std::vector<wxBrush*> m_brushes;
        wxPGProperty* m_customize;

        friend BarChartCtrl;
        friend BarChartSetup;
    };

    class BarChartBuilder:
        public ChartBuilder, public CodeObject
    {
    public:
        Ptr clone(const std::shared_ptr<Table>& tbl, ICaseStringView name,
                PDataManager& mgr, ValueSystem& sys) override;
        WeakRef<ChartBase> build() override;
        void setIndex(const Table& tbl, int idx) override;
        void addSeriesArray(const Table& tbl, int arrIndex) override;
        void addSeriesFunc(const Table& tbl, int scalarIndex) override;
    private:
        std::string_view getType() const noexcept override
        {
            return "bar_chart";
        }

        ICaseStringView getName() const noexcept override
        {
            return m_name;
        }

        std::any makePData() const override;

        WeakRef<BarChart> m_chart;
        ICaseStringView m_name;
    };
}

#endif // LAX_SRC_CHARTS_BARCHART_HPP_
