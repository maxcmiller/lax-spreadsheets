#include "line_graph.hpp"

LAX_DISABLE_WARNINGS()
#include <wx/graphics.h>
#include <wx/propgrid/propgrid.h>
#include <wx/propgrid/advprops.h>
LAX_REENABLE_WARNINGS()

#include <nlohmann/json.hpp>

#include "axis_chart.hpp"
#include "chart_props.hpp"
#include "graphics_defaults.hpp"
#include "main.hpp"

namespace lax
{
    class LineGraphCtrl:
        public AxisChart
    {
    public:
        LineGraphCtrl(wxWindow* parent, LineGraph* lg):
            AxisChart(staticWeakCast<ChartBase>(lg->getWeakRef()),
                    parent), m_lg(lg)
        {
            updateSeries();
        }

        void updateSeries();
    protected:
        wxSize getGridSize() const override
        {
            return AxisChart::getGridSize();
        }
    private:
        int getVerLblWidth() const override
        {
            return m_vertAxis.getMaxSize().x;
        }

        int getLblHeight() const override
        {
            return m_horAxis.getMaxSize().y;
        }

        void drawFunction(wxDC& dc, const wxRect& r, const Object::Ptr& obj) const;

        void drawChartBody(wxDC& dc, const wxRect& r) const override;
        void drawLegendSquare(wxDC& dc, const wxRect& r, int seriesIndex) const override;
        void drawLabels(wxDC& dc, const wxRect& horiz, const wxRect& vert) const override;
        void drawGrid(wxDC& dc, const wxRect& r) const override;

        LineGraph* m_lg;
        ScaledAxis m_horAxis;
        ScaledAxis m_vertAxis;
    };

    void LineGraphCtrl::updateSeries()
    {
        int index = getChartData()->getIndex();
        m_horAxis.updateRange(*m_lg->getTable(), index, GetFont(), getGridSize().x);
        m_vertAxis.updateRange(*getChartData(), GetFont());

        InvalidateBestSize();
        SetSize(GetBestSize());
        GetParent()->Fit();
    }

    void LineGraphCtrl::drawLabels(wxDC& dc, const wxRect& horiz, const wxRect& vert) const
    {
        m_horAxis.drawLabels(dc, horiz, wxHORIZONTAL);
        m_vertAxis.drawLabels(dc, vert, wxVERTICAL);
    }

    void LineGraphCtrl::drawGrid(wxDC& dc, const wxRect& r) const
    {
        m_horAxis.drawGrid(dc, r, wxHORIZONTAL);
        m_vertAxis.drawGrid(dc, r, wxVERTICAL);
    }

    void LineGraphCtrl::drawLegendSquare(wxDC& dc, const wxRect& r, int idx) const
    {
        // TODO (Max#1#): Add different lines to line graph.
        dc.SetPen(*m_lg->m_pens[idx]);
        const int y = (r.GetTop() + r.GetBottom()) / 2;
        dc.DrawLine(r.GetLeft(), y, r.GetRight(), y);
    }

    void LineGraphCtrl::drawChartBody(wxDC& dc, const wxRect& r) const
    {
        const Table& tbl = *getChartData()->getTable();
        const int index = getChartData()->getIndex();
        const int beginX = r.GetLeft();
        const int endX = r.GetRight();
        const int beginY = r.GetBottom();
        const int endY = r.GetTop();
        const int radius = m_lg->m_dispData->ptRadius;
        const bool connect = m_lg->m_dispData->connect;

        const int nSeries = getChartData()->getNumSeries();
        auto idx = std::dynamic_pointer_cast<ArrayObject>(tbl.getArray(index));
        wxASSERT(idx);

        for (int ctr = 0; ctr < nSeries; ++ctr)
        {
            wxPoint last = wxDefaultPosition;

            wxPen& p = *m_lg->m_pens[ctr];
            wxBrush b = *wxTheBrushList->FindOrCreateBrush(p.GetColour());

            dc.SetBrush(b);
            dc.SetPen(p);

            auto obj = getChartData()->getObject(ctr);
            auto ser = std::dynamic_pointer_cast<ArrayObject>(obj);

            if (!ser)
            {
                drawFunction(dc, r, obj);
                continue;
            }

            wxASSERT(idx && ser);

            for (std::size_t row = 0; row < tbl.getNumRows(); ++row)
            {
                const auto x = std::dynamic_pointer_cast<NumberObject>((*idx)[row]);
                const auto y = std::dynamic_pointer_cast<NumberObject>((*ser)[row]);

                if (!x || !y)
                {
                    continue;
                }

                const int xCoord = m_horAxis.getPos(x->getValue(), beginX, endX);
                const int yCoord = m_vertAxis.getPos(y->getValue(), beginY, endY);
                dc.DrawCircle(xCoord, yCoord, radius);

                if (connect && last.IsFullySpecified())
                {
                    dc.DrawLine(last, wxPoint(xCoord, yCoord));
                }

                last = wxPoint(xCoord, yCoord);
            }
        }
    }

    void LineGraphCtrl::drawFunction(wxDC& dc, const wxRect& r,
            const Object::Ptr& obj) const
    {
        constexpr static int resolution = 1;
        const auto func = std::dynamic_pointer_cast<FunctionObject>(obj);

        if (!func)
        {
            return;
        }

        wxDCClipper clip(dc, r);

        const auto num = std::make_shared<NumberObject>(0.0);
        std::vector<Object::Ptr> args(1, num);

        const int beginX = r.GetLeft();
        const int endX = r.GetRight();
        const int beginY = r.GetBottom();
        const int endY = r.GetTop();

        const double valStart = m_horAxis.getMin();
        const double frac = static_cast<double>(resolution) / r.width;
        const double valStep = frac * (m_horAxis.getMax() - valStart);

        wxPoint start = wxDefaultPosition;

        for (int x = beginX; x <= endX; x += resolution)
        {
            const double xval = std::fma(x - beginX, valStep, valStart);
            num->setValue(xval);
            const auto got =
            std::dynamic_pointer_cast<NumberObject>(func->execute(args));

            if (!got)
            {
                start = wxDefaultPosition;
            }

            const double yval = got->getValue();
            const int yCoord = m_vertAxis.getPos(yval, beginY, endY);
            const wxPoint end(x, yCoord);

            if (start.IsFullySpecified())
            {
                dc.DrawLine(start, end);
            }

            start = end;
        }
    }

    class LineGraphSetup:
        public ChartSetup
    {
    public:
        LineGraphSetup(wxWindow* parent, LineGraph* lg);
    private:
        void OnPropertyChange(wxPropertyGridEvent& evt);

        wxDECLARE_EVENT_TABLE();
    };

LAX_DISABLE_WARNINGS()
    wxBEGIN_EVENT_TABLE(LineGraphSetup, ChartSetup)
        EVT_PG_CHANGING(wxID_ANY, LineGraphSetup::OnPropertyChange)
        EVT_PG_CHANGED(wxID_ANY, LineGraphSetup::OnPropertyChange)
    wxEND_EVENT_TABLE()
LAX_REENABLE_WARNINGS()

    LineGraphSetup::LineGraphSetup(wxWindow* parent, LineGraph* lg):
        ChartSetup(parent, staticWeakCast<ChartBase>(lg->getWeakRef()))
    {
        wxPropertyGridInterface* pgi = getPropGrid();

        wxPGProperty* radius =
        pgi->Append(new wxIntProperty(_("Point radius"), wxS("radius"), 5));
        radius->SetAttribute(wxS("Min"), 0);
        radius->SetAttribute(wxS("Max"), 20);
        radius->SetAttribute(wxS("Wrap"), false);
        radius->SetEditor(wxPGEditor_SpinCtrl);
        lg->m_customize = radius->GetParent();

        pgi->Append(new wxBoolProperty(_("Connect points"), wxS("connect"), true));

        lg->m_firstPen = lg->m_customize->GetChildCount();

        const int numSeries = getChart()->getNumSeries();
        wxString name;

        for (int i = 0; i < numSeries; ++i)
        {
            name.Printf("%d", i);

            const ICaseString id(getChart()->getSeriesName(i));
            wxString lbl = getChart()->getSeriesLabel(i);
            auto tbl = getChart()->getTable();
            auto prop = pgi->Append(new PenProperty(lbl, name, lg->getPenFor(id)));
            pgi->Expand(prop);
        }
    }

    void LineGraphSetup::OnPropertyChange(wxPropertyGridEvent& evt)
    {
        const auto lg = staticWeakCast<LineGraph>(getChart());
        const auto prop = ChartSetup::findProperty(evt.GetPropertyName());
        bool redraw = false, resize = false;

        switch (prop)
        {
        case ChartSetup::Property::kHorTitleName:
        case ChartSetup::Property::kVertTitleName:
            if (!lg->m_ctrl.get())
            {
                break;
            }

            // just ensure that titles can be shown...
            redraw = resize = true;
            break;
        default:
            if (evt.GetPropertyName() == wxS("radius"))
            {
                long r = static_cast<long>(evt.GetPropertyValue());
                lg->m_dispData->ptRadius = r;
                redraw = true;
            }
            else if (evt.GetPropertyName() == wxS("connect"))
            {
                lg->m_dispData->connect = evt.GetValue();
                redraw = true;
            }
            else
            {
                long i;

                if (evt.GetPropertyName().ToLong(&i))
                {
                    wxPen p;
                    p << evt.GetValue();
                    *lg->m_pens[i] = p;
                    redraw = true;
                }
            }
        }

        if (auto ctrl = lg->m_ctrl.get())
        {
            if (resize)
            {
                ctrl->InvalidateBestSize();
                ctrl->SetSize(ctrl->GetBestSize());
                ctrl->GetParent()->Fit();
            }

            if (redraw)
            {
                ctrl->repaint();
            }
        }

        evt.Skip();
    }

    wxWindow* LineGraph::makeChart(wxWindow* parent)
    {
        m_ctrl = new LineGraphCtrl(parent, this);
        return m_ctrl.get();
    }

    ChartSetup* LineGraph::createSetup(wxWindow* parent)
    {
        return new LineGraphSetup(parent, this);
    }

    bool LineGraph::updateIndexAndSeries()
    {
        if (auto ctrl = m_ctrl.get())
        {
            ctrl->updateSeries();
            ctrl->repaint();
        }

        const auto tbl = getTable();

        m_pens.reserve(getNumSeries());

        for (int i = 0; i < getNumSeries(); ++i)
        {
            ICaseString name(getSeriesName(i));
            m_pens.push_back(&getPenFor(name));
        }

        return true;
    }

    wxPen& LineGraph::getPenFor(const ICaseString& name)
    {
        auto& penMap = m_dispData->penMap;

        if (auto itr = penMap.find(name); itr != penMap.end())
        {
            return itr->second;
        }
        else
        {
            int penNo = m_dispData->currentPen++;
            wxPen p = GraphicsDefaults::get().getDefaultPen(penNo);
            itr = penMap.emplace(name, p).first;
            return itr->second;
        }
    }

    void LineGraph::serialize(nlohmann::json& j) const
    {
        nlohmann::json pens;

        for (const wxPen* p: m_pens)
        {
            nlohmann::json pen {
                {"color", p->GetColour().GetRGBA()},
                {"style", penStyleToString(p->GetStyle())},
                {"width", p->GetWidth()},
            };

            pens.push_back(pen);
        }

        j = nlohmann::json {
            {"radius", m_dispData->ptRadius},
            {"connect", m_dispData->connect},
            {"pens", pens},
        };
    }

    void LineGraph::deserialize(const nlohmann::json& j)
    {
        j.at("radius").get_to(m_dispData->ptRadius);
        j.at("connect").get_to(m_dispData->connect);

        const auto& pens = j.at("pens");

        if (pens.size() > static_cast<std::size_t>(getNumSeries()))
        {
            throw std::runtime_error("too many pens for line graph");
        }

        std::size_t i = 0;

        for (const auto& p: pens)
        {
            unsigned long color, width;
            p.at("color").get_to(color);
            p.at("width").get_to(width);

            int style = stringToPenStyle(p.at("style").get<std::string>());
            wxPen pen = *wxThePenList->FindOrCreatePen(wxColour(color), width, style);

            ICaseString name(getSeriesName(i));
            wxPen& rpen = m_dispData->penMap[name];
            rpen = pen;
            m_pens[i] = &rpen;
            ++i;
        }
    }

    void LineGraph::doUpdate(int)
    {
        if (auto ctrl = m_ctrl.get())
        {
            ctrl->updateSeries();
            ctrl->repaint();
        }
    }

    void LineGraph::onColRename(const SheetDataTable::Ptr& tbl, int col)
    {
        setPropTitle(getColumnIndex(col), tbl->GetColLabelValue(col));
    }

    void LineGraph::onScalarRename(const SheetDataTable::Ptr& tbl, int s)
    {
        setPropTitle(getScalarIndex(s), tbl->getScalarLabel(s));
    }

    void LineGraph::setPropTitle(int index, const wxString& title)
    {
        if (index < 0)
        {
            // column not in chart, so just ignore it
            return;
        }

        doUpdate(-1);

        if (m_customize)
        {
            m_customize->Item(index + m_firstPen)->SetLabel(title);
        }
    }

namespace
{
    struct LineGraphPData
    {
        LineGraphPData(ICaseStringView name)
        {
            data.cd.title = wxString(name->begin(), name->end());
        }

        wxPoint pos;
        LineGraph::LineData data;
    };
}

    std::any LineGraphBuilder::makePData() const
    {
        return std::make_any<LineGraphPData>(m_name);
    }

    LineGraphBuilder::Ptr LineGraphBuilder::clone(const std::shared_ptr<Table>& tbl,
            ICaseStringView name, PDataManager& mgr, ValueSystem& sys)
    {

        auto ret = std::make_unique<LineGraphBuilder>();
        ret->m_name = name;
        LineGraphPData* bcpd = std::any_cast<LineGraphPData>(&ret->getPData(mgr));
        ret->m_chart = makeValue<LineGraph>(sys, bcpd->data);
        ret->m_chart->setTable(tbl);
        ret->m_chart->setPosition(bcpd->pos);
        return ret;
    }

    void LineGraphBuilder::setIndex(const Table& tbl, int idx)
    {
        auto expect = ArrayObject::theType(NumberObject::theType());

        if (tbl.getNode(idx)->getType() != expect)
        {
            throw Failure("index to line graph must be Number");
        }

        m_chart->setIndex(idx);
    }

    void LineGraphBuilder::addSeriesArray(const Table& tbl, int arrIndex)
    {
        auto expect = ArrayObject::theType(NumberObject::theType());

        if (tbl.getNode(arrIndex)->getType() != expect)
        {
            throw Failure("series to line graph must be array of Number");
        }

        m_chart->addArraySeries(arrIndex);
        m_hasArraySeries = true;
    }

    void LineGraphBuilder::addSeriesFunc(const Table& tbl, int scalarIndex)
    {
        auto type = tbl.getScalar(scalarIndex).node->getType();
        auto impl = type.getImpl();

        if (auto func = std::dynamic_pointer_cast<FunctionTypeInfo>(impl))
        {
            auto numType = NumberObject::theType();

            if (func->getReturn({numType}) == numType)
            {
                m_chart->addScalarSeries(scalarIndex);
                return;
            }
        }

        throw Failure("series to line graph must be Number(Number)");
    }

    ChartBase::Ptr LineGraphBuilder::build()
    {
        if (!m_hasArraySeries)
        {
            throw Failure("line graph must have at least one array series element");
        }

        m_chart->updateIndexAndSeries();
        m_chart->addDependencies();
        return m_chart;
    }
}
