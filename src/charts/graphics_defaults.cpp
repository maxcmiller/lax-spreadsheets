#include "graphics_defaults.hpp"

#include <memory>
#include <unordered_map>

// NB: We reenable warnings *after* util/splitter.hpp because otherwise we
// get a warning inside an instantiation of Splitter due to a problem with
// wxWidgets's string implementation.
LAX_DISABLE_WARNINGS()
#include <wx/button.h>
#include <wx/choicdlg.h>
#include <wx/colordlg.h>
#include <wx/config.h>
#include <wx/listctrl.h>
#include <wx/log.h>
#include <wx/notebook.h>
#include <wx/sizer.h>

#include "util/splitter.hpp"
LAX_REENABLE_WARNINGS()

#include "main.hpp"
#include "widgets/reorder_list.hpp"

namespace lax
{
    namespace
    {
        inline int convToDigit(wxChar c)
        {
            if (c >= '0' && c <= '9')
            {
                return c - '0';
            }
            else
            {
                c = wxTolower(c);

                if (c >= 'a' && c <= 'f')
                {
                    return c - 'a' + 0xA;
                }
            }

            wxFAIL;
            return 0;
        }
    }

    void GraphicsDefaults::readConfig()
    {
        m_colors.clear();
        m_brushStyles.clear();
        m_penStyles.clear();
        m_penWidth = -1;

        wxConfigBase* cfg = wxConfigBase::Get();
        wxString buf;

        if (cfg->Read(wxS("/DefaultColors"), &buf))
        {
            unsigned long color = 0;

            for (wxChar c: buf)
            {
                if (wxIsxdigit(c))
                {
                    color <<= 4;
                    color |= convToDigit(c);
                }
                else if (c == ',')
                {
                    m_colors.push_back(wxColour(color));
                    color = 0;
                }
                else
                {
                    wxLogError(wxS("could not parse colors from %s"), buf);
                    m_colors.clear();
                    break;
                }
            }
        }

        if (cfg->Read(wxS("/DefaultBrushStyles"), &buf))
        {
            auto [first, last] = splitterFor(buf, ',');

            for (; first != last; ++first)
            {
                if (first->empty())
                {
                    continue;
                }

                int s = stringToBrushStyle(first->ToStdString());

                if (s == wxBRUSHSTYLE_INVALID)
                {
                    wxLogError(wxS("could not parse brush styles from %s"), buf);
                    m_brushStyles.clear();
                    break;
                }

                m_brushStyles.push_back(s);
            }
        }

        if (cfg->Read(wxS("/DefaultPenStyles"), &buf))
        {
            auto [first, last] = splitterFor(buf, ',');

            for (; first != last; ++first)
            {
                if (first->empty())
                {
                    continue;
                }

                int s = stringToPenStyle(first->ToStdString());

                if (s == wxPENSTYLE_INVALID)
                {
                    wxLogError(wxS("could not parse brush styles from %s"), buf);
                    m_penStyles.clear();
                    break;
                }

                m_penStyles.push_back(s);
            }
        }

        cfg->Read(wxS("/DefaultPenWidth"), &m_penWidth, -1);

        bool write = false;

        if (m_colors.empty())
        {
            wxColour defaults[] {
                *wxBLUE, *wxRED, *wxGREEN, *wxYELLOW, *wxBLACK,
            };

            m_colors.insert(m_colors.end(), std::begin(defaults), std::end(defaults));
            write = true;
        }

        if (m_brushStyles.empty())
        {
            wxBrushStyle defaults[] {
                wxBRUSHSTYLE_SOLID,
                wxBRUSHSTYLE_BDIAGONAL_HATCH,
                wxBRUSHSTYLE_CROSSDIAG_HATCH,
                wxBRUSHSTYLE_FDIAGONAL_HATCH,
                wxBRUSHSTYLE_CROSS_HATCH,
                wxBRUSHSTYLE_HORIZONTAL_HATCH,
                wxBRUSHSTYLE_VERTICAL_HATCH,
            };

            m_brushStyles.insert(m_brushStyles.end(), std::begin(defaults), std::end(defaults));
            write = true;
        }

        if (m_penStyles.empty())
        {
            wxPenStyle defaults[] {
                wxPENSTYLE_SOLID,
                wxPENSTYLE_LONG_DASH,
                wxPENSTYLE_SHORT_DASH,
                wxPENSTYLE_DOT,
                wxPENSTYLE_DOT_DASH,
            };

            m_penStyles.insert(m_penStyles.end(), std::begin(defaults), std::end(defaults));
            write = true;
        }

        if (m_penWidth <= 0)
        {
            m_penWidth = 1;
            write = true;
        }

        if (write)
        {
            writeConfig();
        }
    }

    void GraphicsDefaults::writeConfig()
    {
        wxConfigBase* cfg = wxConfigBase::Get();
        wxString buf, temp;

        for (const wxColour& c: m_colors)
        {
            temp.Printf("%x,", c.GetRGBA());
            buf += temp;
        }

        cfg->Write(wxS("/DefaultColors"), buf);
        buf.clear();

        for (int i: m_brushStyles)
        {
            buf += brushStyleToString(i);
            buf += ',';
        }

        cfg->Write(wxS("/DefaultBrushStyles"), buf);
        buf.clear();

        for (int i: m_penStyles)
        {
            buf += penStyleToString(i);
            buf += ',';
        }

        cfg->Write(wxS("/DefaultPenStyles"), buf);
    }

    wxBrush GraphicsDefaults::getDefaultBrush(int n) const
    {
        n %= m_colors.size() * m_brushStyles.size();

        const wxColour c = m_colors[n % m_colors.size()];
        const int style = m_brushStyles[n / m_colors.size()];

        return *wxTheBrushList->FindOrCreateBrush(c, style);
    }

    wxPen GraphicsDefaults::getDefaultPen(int n) const
    {
        n %= m_colors.size() * m_penStyles.size();

        const wxColour c = m_colors[n % m_colors.size()];
        const int style = m_penStyles[n / m_colors.size()];

        return *wxThePenList->FindOrCreatePen(c, m_penWidth, style);
    }

    const std::unordered_map<int, wxString>&
    GraphicsDefaults::brushNames()
    {
        static std::unordered_map<int, wxString> map
        {
            {wxBRUSHSTYLE_SOLID, _("Solid")},
            {wxBRUSHSTYLE_BDIAGONAL_HATCH, _("Backwards diagonal hatching")},
            {wxBRUSHSTYLE_CROSSDIAG_HATCH, _("Cross diagonal hatching")},
            {wxBRUSHSTYLE_CROSS_HATCH, _("Cross-hatching")},
            {wxBRUSHSTYLE_FDIAGONAL_HATCH, _("Forwards diagonal hatching")},
            {wxBRUSHSTYLE_HORIZONTAL_HATCH, _("Horizontal hatching")},
            {wxBRUSHSTYLE_VERTICAL_HATCH, _("Vertical hatching")},
        };

        return map;
    }

    const std::unordered_map<int, wxString>&
    GraphicsDefaults::penNames()
    {
        static std::unordered_map<int, wxString> map
        {
            {wxPENSTYLE_SOLID, _("Solid")},
            {wxPENSTYLE_DOT, _("Dotted")},
            {wxPENSTYLE_DOT_DASH, _("Dot-dash")},
            {wxPENSTYLE_LONG_DASH, _("Long dash")},
            {wxPENSTYLE_SHORT_DASH, _("Short dash")},
        };

        return map;
    }

    const char* brushStyleToString(int style)
    {
        switch (style)
        {
#define LAX_STYLE(name) \
        case name:        \
            return #name;\

            LAX_STYLE(wxBRUSHSTYLE_SOLID);
            LAX_STYLE(wxBRUSHSTYLE_BDIAGONAL_HATCH);
            LAX_STYLE(wxBRUSHSTYLE_CROSSDIAG_HATCH);
            LAX_STYLE(wxBRUSHSTYLE_CROSS_HATCH);
            LAX_STYLE(wxBRUSHSTYLE_FDIAGONAL_HATCH);
            LAX_STYLE(wxBRUSHSTYLE_VERTICAL_HATCH);
            LAX_STYLE(wxBRUSHSTYLE_HORIZONTAL_HATCH);
        default:
            wxFAIL_MSG(wxS("unhandled brush style"));
            return "wxBRUSHSTYLE_SOLID";
        }
    }

    const char* penStyleToString(int style)
    {
        switch (style)
        {
            LAX_STYLE(wxPENSTYLE_DOT);
            LAX_STYLE(wxPENSTYLE_SOLID);
            LAX_STYLE(wxPENSTYLE_DOT_DASH);
            LAX_STYLE(wxPENSTYLE_LONG_DASH);
            LAX_STYLE(wxPENSTYLE_SHORT_DASH);
#undef LAX_STYLE
        default:
            wxFAIL_MSG("missing case in LineGraph::getStyleString");
            return "wxPENSTYLE_SOLID";
        }
    }

    int stringToBrushStyle(std::string_view str)
    {
        static const std::unordered_map<std::string_view, int> styles {
#define LAX_STYLE(name) \
            std::pair(#name, name)
            LAX_STYLE(wxBRUSHSTYLE_SOLID),
            LAX_STYLE(wxBRUSHSTYLE_BDIAGONAL_HATCH),
            LAX_STYLE(wxBRUSHSTYLE_CROSSDIAG_HATCH),
            LAX_STYLE(wxBRUSHSTYLE_CROSS_HATCH),
            LAX_STYLE(wxBRUSHSTYLE_FDIAGONAL_HATCH),
            LAX_STYLE(wxBRUSHSTYLE_VERTICAL_HATCH),
            LAX_STYLE(wxBRUSHSTYLE_HORIZONTAL_HATCH),
        };

        if (auto itr = styles.find(str); itr != styles.end())
        {
            return itr->second;
        }
        else
        {
            wxLogError(wxS("unhandled brush style %s"), &str[0]);
            return wxBRUSHSTYLE_INVALID;
        }
    }

    int stringToPenStyle(std::string_view style)
    {
        static std::unordered_map<std::string_view, int> styleMap {
            LAX_STYLE(wxPENSTYLE_DOT),
            LAX_STYLE(wxPENSTYLE_SOLID),
            LAX_STYLE(wxPENSTYLE_DOT_DASH),
            LAX_STYLE(wxPENSTYLE_LONG_DASH),
            LAX_STYLE(wxPENSTYLE_SHORT_DASH),
#undef LAX_STYLE
        };

        if (auto itr = styleMap.find(style); itr != styleMap.end())
        {
            return itr->second;
        }
        else
        {
            wxLogError(wxS("unhandled pen style %s"), &style[0]);
            return wxPENSTYLE_INVALID;
        }
    }

    template <>
    struct ReorderTraits<wxColour>
    {
        void getLabel(const wxColour& c, wxString& out) const
        {
            out = wxToString(c);
        }

        void draw(const wxColour& c, wxDC& out) const
        {
            out.SetBrush(*wxTheBrushList->FindOrCreateBrush(c));
            out.DrawRectangle(wxPoint(0, 0), out.GetSize());
        }

        bool showDialog(wxColour& out, const wxColour* in) const
        {
            wxColour c = in? *in:wxNullColour;
            c = wxGetColourFromUser(nullptr, c);

            if (c.IsOk())
            {
                out = c;
            }

            return c.IsOk();
        }
    };

    struct ReorderStyle
    {
        const wxString& doGetLabel(int s,
                const std::unordered_map<int, wxString>& map,
                const wxString& fallback) const
        {
            if (auto itr = map.find(s); itr != map.end())
            {
                return itr->second;
            }
            else
            {
                return fallback;
            }
        }

        bool doShowDialog(int in, int& out,
                const std::unordered_map<int, wxString>& map,
                const wxString& prompt, const wxString& caption) const
        {
            wxArrayString arr;
            std::vector<int> data;
            int sel = 0;
            int i = 0;

            const int size = map.size();
            arr.reserve(size);
            data.reserve(size);

            for (const auto& p: map)
            {
                arr.push_back(p.second);
                data.push_back(p.first);

                if (p.first == in)
                {
                    sel = i;
                }

                ++i;
            }

            if (sel > size)
            {
                i = 0;
            }

            int index = wxGetSingleChoiceIndex(prompt, caption, arr, sel);

            if (index < 0)
            {
                return false;
            }

            out = data[index];
            return true;
        }
    };

    struct ReorderBrushStyle:
        ReorderStyle
    {
        void getLabel(int s, wxString& lbl) const
        {
            lbl = doGetLabel(s, GraphicsDefaults::brushNames(),
                    _("unknown brush style"));
        }

        void draw(int style, wxDC& dc) const
        {
            dc.SetBackground(*wxWHITE_BRUSH);
            dc.Clear();
            dc.SetBrush(*wxTheBrushList->FindOrCreateBrush(*wxBLACK, style));
            dc.DrawRectangle(wxPoint(0, 0), dc.GetSize());
        }

        bool showDialog(int& out, const int* in) const
        {
            int s = in? *in:wxBRUSHSTYLE_SOLID;

            if (doShowDialog(s, s, GraphicsDefaults::brushNames(),
                _("Choose the brush style:"), _("Change brush")))
            {
                out = s;
                return true;
            }
            else
            {
                return false;
            }
        }
    };

    struct ReorderPenStyle:
        ReorderStyle
    {
        void getLabel(int s, wxString& out) const
        {
            out = doGetLabel(s, GraphicsDefaults::penNames(),
                    _("unknown pen style"));
        }

        void draw(int style, wxDC& dc) const
        {
            dc.SetBackground(*wxWHITE_BRUSH);
            dc.Clear();
            dc.SetPen(*wxThePenList->FindOrCreatePen(*wxBLACK, 2, style));
            int halfH = dc.GetSize().y / 2;
            int w = dc.GetSize().x;
            dc.DrawLine(0, halfH, w, halfH);
        }

        bool showDialog(int& out, const int* in) const
        {
            int s = in? *in:wxPENSTYLE_SOLID;

            if (doShowDialog(s, s, GraphicsDefaults::penNames(),
                _("Choose the pen style:"), _("Change pen")))
            {
                out = s;
                return true;
            }
            else
            {
                return false;
            }
        }
    };

    class GraphicsSettingsDlg:
        public wxDialog
    {
    public:
        enum WindowIDs
        {
            kColor = 7000,
            kPenStyle,
            kBrushStyle,
        };

        GraphicsSettingsDlg();
    private:
        template <typename T, typename Traits = ReorderTraits<T> >
        using RList = ReorderList<std::vector<T>, Traits>;

        using ColorList = RList<wxColour>;
        using BrushList = RList<int, ReorderBrushStyle>;
        using PenList = RList<int, ReorderPenStyle>;

        void OnApply(wxCommandEvent& evt);
        void OnCancel(wxCommandEvent& evt);

        ColorList* m_colors;
        BrushList* m_brushes;
        PenList* m_pens;

        wxDECLARE_EVENT_TABLE();
    };

LAX_DISABLE_WARNINGS()
    wxBEGIN_EVENT_TABLE(GraphicsSettingsDlg, wxDialog)
        EVT_BUTTON(wxID_APPLY, GraphicsSettingsDlg::OnApply)
        EVT_BUTTON(wxID_CANCEL, GraphicsSettingsDlg::OnCancel)
    wxEND_EVENT_TABLE()
LAX_REENABLE_WARNINGS()

    GraphicsSettingsDlg::GraphicsSettingsDlg():
        wxDialog(&wxGetApp().getWindow(), wxID_ANY, _("Chart graphics settings"))
    {
        wxNotebook* nb;
        wxStdDialogButtonSizer* buttons;

        // initialize top level sizer
        std::unique_ptr<wxSizer> sizer(new wxBoxSizer(wxVERTICAL));
        sizer->Add(nb = new wxNotebook(this, wxID_ANY),
                wxSizerFlags(1).Expand());
        sizer->Add(buttons = new wxStdDialogButtonSizer,
                wxSizerFlags(0).Center());

        buttons->SetAffirmativeButton(new wxButton(this, wxID_APPLY));
        buttons->SetCancelButton(new wxButton(this, wxID_CANCEL));
        buttons->Realize();

        GraphicsDefaults& gd = GraphicsDefaults::get();

        m_colors = new ColorList(nb, wxID_ANY, gd.m_colors);
        nb->AddPage(m_colors, _("Default colors"), true);

        m_pens = new PenList(nb, wxID_ANY, gd.m_penStyles);
        nb->AddPage(m_pens, _("Default pen styles"));

        m_brushes = new BrushList(nb, wxID_ANY, gd.m_brushStyles);
        nb->AddPage(m_brushes, _("Default brush styles"));

        SetSizer(sizer.release());
    }

    void GraphicsSettingsDlg::OnApply(wxCommandEvent&)
    {
        m_colors->commit();
        m_pens->commit();
        m_brushes->commit();
        EndModal(true);
    }

    void GraphicsSettingsDlg::OnCancel(wxCommandEvent&)
    {
        EndModal(false);
    }

    void GraphicsDefaults::showDialog()
    {
        GraphicsSettingsDlg dlg;

        if (dlg.ShowModal())
        {
            writeConfig();
        }
    }
}
