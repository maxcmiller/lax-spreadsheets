#ifndef LAX_SRC_CHART_SETUP_HPP_
#define LAX_SRC_CHART_SETUP_HPP_

#include "disable_warnings.h"

#include <memory>
#include <vector>

LAX_DISABLE_WARNINGS()
#include <wx/bitmap.h>
#include <wx/panel.h>
#include <wx/propgrid/property.h>
#include <wx/propgrid/propgriddefs.h>
#include <wx/weakref.h>
LAX_REENABLE_WARNINGS()

#include <nlohmann/json_fwd.hpp>

#include "table.hpp"

class wxChoicebook;
class wxPGChoices;
class wxPropertyGridEvent;
class wxPropertyGridInterface;

namespace lax
{
    class ChartBase;
    class ChartSetup;

    class ChartFactory
    {
    public:
        using Ptr = std::unique_ptr<ChartFactory>;

        virtual ~ChartFactory() = default;

        virtual std::unique_ptr<ChartBase> makeChart() = 0;
        virtual std::string_view getId() const noexcept = 0;
    };

    class ChartBase:
        public TableBase, public TableObserver
    {
    public:
        using Ptr = WeakRef<ChartBase>;

        struct ChartData
        {
            wxString title;
            wxString horTitle;
            wxString verTitle;
        };

        ChartBase(ValueSystem& sys, ChartData& data):
            TableBase(10, 10), TableObserver(sys), m_data(&data) {}

        ChartBase(const ChartBase& other) = delete;
        ChartBase(ChartBase&& other) = delete;

        virtual ~ChartBase() = default;
        virtual wxWindow* makeChart(wxWindow* parent) = 0;
        virtual ChartSetup* createSetup(wxWindow* parent) = 0;
        virtual bool updateIndexAndSeries() = 0;
        virtual void serialize(nlohmann::json& root) const = 0;
        virtual void deserialize(const nlohmann::json& root) = 0;
        virtual std::string_view getId() const noexcept = 0;

        void setIndex(int idx);
        void addArraySeries(int s);
        void addScalarSeries(int s);
        int getColumnIndex(int tblCol) const;

        wxString getSeriesLabel(int ser) const;
        ICaseStringView getSeriesName(int ser) const;
        ValueNode::Ptr getNode(int ser) const;

        void addDependencies()
        {
            setName(m_data->title.ToStdString());
        }

        void setSetup(ChartSetup* cs) noexcept
        {
            m_setup = cs;
        }

        Object::Ptr getObject(int ser) const
        {
            return getNode(ser)->getObject();
        }

        ChartData& getChartData() noexcept
        {
            return *m_data;
        }

        const ChartData& getChartData() const noexcept
        {
            return *m_data;
        }

        auto getTable() const noexcept
        {
            return m_tbl;
        }

        void setTable(const std::shared_ptr<Table>& tbl) noexcept
        {
            m_tbl = tbl;
        }

        int getNumSeries() const noexcept
        {
            return m_series.size();
        }

        int getIndex() const noexcept
        {
            return m_index;
        }

        ChartSetup* getSetup() const noexcept
        {
            return m_setup.get();
        }

        void clear()
        {
            clearDependencies();
            m_series.clear();
        }

        int getScalarIndex(int s)
        {
            return getColumnIndex(-s - 1);
        }
    private:
        wxWeakRef<ChartSetup> m_setup;
        ChartData* m_data;
        std::shared_ptr<Table> m_tbl;
        std::vector<int> m_series;
        int m_index;
    };

    void to_json(nlohmann::json& j, const ChartBase& cd);
    void from_json(const nlohmann::json& j, ChartBase& cd);

    class ChartSetup:
        public wxPanel
    {
    public:
        enum class Property
        {
            kNotFound = -1,
            kTitleName,
            kHorTitleName,
            kVertTitleName,
            kNumProps
        };

        wxPropertyGridInterface* getPropGrid() const noexcept
        {
            return m_propGrid;
        }

        static Property findProperty(const wxString& name);
    protected:
        explicit ChartSetup(wxWindow* parent, const ChartBase::Ptr& data);

        ChartBase::Ptr getChart() const noexcept
        {
            return m_chart;
        }

        wxPropertyCategory* getCustomize() const noexcept
        {
            return m_customize;
        }
    private:
        void OnChange(wxPropertyGridEvent& evt);

        ChartBase::Ptr m_chart;
        wxPropertyGridInterface* m_propGrid;
        wxPropertyCategory* m_customize;

        wxDECLARE_EVENT_TABLE();
    };
}

#endif // LAX_SRC_CHART_SETUP_HPP_
