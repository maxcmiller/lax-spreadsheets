#ifndef LAX_SRC_CHARTS_AXIS_CHART_HPP_
#define LAX_SRC_CHARTS_AXIS_CHART_HPP_

#include "disable_warnings.h"

#include <memory>

LAX_DISABLE_WARNINGS()
#include <wx/dcmemory.h>
#include <wx/dcscreen.h>
#include <wx/window.h>
LAX_REENABLE_WARNINGS()

#include "chart_setup.hpp"

class wxDC;

namespace lax
{
    class AxisChart:
        public wxWindow
    {
    public:
        AxisChart(const ChartBase::Ptr& cd, wxWindow* parent,
                wxWindowID id = wxID_ANY,
                const wxPoint& pos = wxDefaultPosition,
                const wxSize& size = wxDefaultSize);

        virtual ~AxisChart() = default;

        void repaint()
        {
            m_redraw = true;
            Refresh();
        }
    protected:
        ChartBase::Ptr getChartData() const noexcept
        {
            return m_data;
        }

        wxString getLabel(int series) const
        {
            return m_data->getSeriesLabel(series);
        }

        wxSize DoGetBestClientSize() const override;
        void updateRects(wxDC& dc);
        virtual wxSize getGridSize() const = 0;
    private:
        virtual int getVerLblWidth() const = 0;
        virtual int getLblHeight() const = 0;
        virtual void drawChartBody(wxDC& dc, const wxRect& r) const = 0;
        virtual void drawLegendSquare(wxDC& dc, const wxRect& r, int seriesIndex) const = 0;
        virtual void drawLabels(wxDC& dc, const wxRect& horiz, const wxRect& vert) const = 0;
        virtual void drawGrid(wxDC& dc, const wxRect& r) const = 0;

        void OnPaint(wxPaintEvent& evt);
        void OnResize(wxSizeEvent& evt);

        int getChartLeft(wxDC& dc) const;
        void updateLegendRect(wxDC& dc, int width) const;
        void redraw(wxDC& dc);
        void drawTitles(wxDC& dc);
        void drawLegend(wxDC& dc);

        ChartBase::Ptr m_data;
        std::unique_ptr<wxBitmap> m_buffer;
        wxMemoryDC m_dc;
        wxRect m_horAxisRect;
        wxRect m_verAxisRect;
        wxRect m_chartRect;
        mutable wxRegion m_legendRect; // mutable because we modify it in GetBestClientSize
        bool m_redraw = false;

        wxDECLARE_EVENT_TABLE();
    };

    class LabelDrawer
    {
    public:
        LabelDrawer(int maxWidth, int slantLength):
            m_maxWidth(maxWidth), m_slantLength(slantLength) {}

        LabelDrawer(const LabelDrawer&) = delete;

        template <typename Iter>
        void updateSize(Iter firstLbl, Iter lastLbl);

        wxSize getMaxSize() const noexcept
        {
            return m_size;
        }

        void setMaxSize(const wxSize& size) noexcept
        {
            m_size = wxDefaultSize;
            m_diagonal = doUpdate(size);
        }

        void setMaxWidth(int w) noexcept
        {
            m_maxWidth = w;
        }

        void setSlantLength(int l) noexcept
        {
            m_slantLength = l;
        }

        template <typename Iter>
        void drawLabels(wxDC& dc, Iter first, Iter last, const wxRect& r,
                        int align = wxALIGN_CENTER) const;
    private:
        bool doUpdate(const wxSize& size);
        void doDraw(wxDC& dc, const wxString& lbl, const wxRect& r, int align) const;

        wxSize m_size;
        int m_maxWidth;
        int m_slantLength;
        bool m_diagonal;
    };

    class ScaledAxis
    {
    public:
        ScaledAxis():
            m_drawer(0, 60) {}

        ScaledAxis(const ScaledAxis&) = delete;

        void updateRange(const ChartBase& series,
                         const wxFont& font, int maxWidth = -1);

        void updateRange(const Table& tbl, int series,
                         const wxFont& font, int maxWidth = -1);

        void drawGrid(wxDC& dc, const wxRect& r, wxOrientation orient) const;
        void drawLabels(wxDC& dc, const wxRect& r, wxOrientation orient) const;
        int getPos(double val, int begin, int end) const noexcept;

        wxSize getMaxSize() const noexcept
        {
            return m_maxSize;
        }

        double getMin() const noexcept
        {
            return m_min;
        }

        double getMax() const noexcept
        {
            return m_max;
        }
    private:
        void doUpdateRange(const std::shared_ptr<ArrayObject>& series);
        void finishUpdate(const wxFont& font, int maxWidth);
        void formatFor(wxString& str, int i) const;

        void resetRange()
        {
            m_min = m_max = 0.0;
        }

        double m_min;
        double m_max;
        wxSize m_maxSize;
        mutable LabelDrawer m_drawer;
    };

    template <typename Iter>
    void LabelDrawer::updateSize(Iter first, Iter last)
    {
        wxScreenDC dc;
        m_size = wxDefaultSize;

        for (; first != last; ++first)
        {
            if ((m_diagonal = doUpdate(dc.GetTextExtent(*first))))
            {
                break;
            }
        }
    }

    template <typename Iter>
    void LabelDrawer::drawLabels(wxDC& dc, Iter first, Iter last,
                                 const wxRect& r, int align) const
    {
        const int n = std::distance(first, last);
        wxRect lblRect(r.x, r.y, r.width / n, r.height);

        for (; first != last; ++first)
        {
            doDraw(dc, *first, lblRect, align);
            lblRect.x += lblRect.width;
        }
    }
}

#endif // LAX_SRC_CHARTS_AXIS_CHART_HPP_
