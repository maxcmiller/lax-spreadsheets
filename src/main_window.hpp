#ifndef LAX_SRC_MAIN_WINDOW_HPP_
#define LAX_SRC_MAIN_WINDOW_HPP_

#include "disable_warnings.h"

LAX_DISABLE_WARNINGS()
#include <wx/aui/aui.h>
#include <wx/aui/auibar.h>
#include <wx/aui/auibook.h>
#include <wx/choicebk.h>
#include <wx/docview.h>
#include <wx/filehistory.h>
#include <wx/listctrl.h>
#include <wx/menu.h>
LAX_REENABLE_WARNINGS()

#include "table_code.hpp"

namespace lax
{
    class AutocompleteCtrl;
    class CompilationFailure;
    class SheetDoc;

    class MainWindow:
        public wxDocParentFrame
    {
    public:
        enum WindowIds
        {
            kSavePerspective = 6000,
            kTblCodeMenu,
            kCompLogMenu,
            kAutocompMenu,
            kNewPage,
            kDeletePage,
            kRenamePage,
            kRenameTable,
            kAppendRow,
            kAppendNRows,
            kRecalc,
            kRecompileTbl,
            kRecompileAll,
            kFormulaSettings,
            kGraphicSettings,
            kRecompImpl,
            kSheetNotebook,
            kShowDeps,
            kSaveTable,
        };

        MainWindow(wxDocManager* mgr, const wxString& label);
        ~MainWindow();

        wxPanel* addPage(const wxString& name, PageData& pg, int before = -1,
                    TableCodeCtrl::CodeSet::Ptr* set = nullptr);

        wxPanel* addPage(const wxString& name, PageData& pg,
                    TableCodeCtrl::CodeSet::Ptr* set, int before = -1)
        {
            return addPage(name, pg, before, set);
        }

        void removePage(const wxWindow* p);
        void removePage(int pgNum);
        wxWindow* findPageWithData(const void* data);
        void resetToStart();
        void logFailure(const wxString& name, const CompilationFailure& cf);
        void logFailure(const wxString& msg);

        void clearLog()
        {
            m_compLog->DeleteAllItems();
        }

        void switchTo(int pgNum)
        {
            m_sheets->SetSelection(pgNum);
            m_tblText->switchTo(pgNum);
        }

        void renamePage(int n, const wxString& title)
        {
            m_sheets->SetPageText(n, title);
        }

        SheetDoc* getDocument() const;
    private:
        void initMenu(wxFileHistory* fh);
        void initToolbar();
        wxWindow* removePlus();

        void OnSavePerspective(wxCommandEvent&);
        void OnClosePane(wxAuiManagerEvent&);
        void OnPerspectiveMenu(wxCommandEvent&);
        void OnButtonToMenu(wxCommandEvent&);
        void OnFormulaSettings(wxCommandEvent&);
        void OnGraphicSettings(wxCommandEvent&);
        void OnRecompile(wxCommandEvent&);
        void OnShowDeps(wxCommandEvent&);
        void OnFind(wxCommandEvent&);

        wxAuiManager m_mgr;
        wxMenuBar* m_menu;
        wxAuiToolBar* m_toolbar;
        wxAuiNotebook* m_sheets;
        TableCodeCtrl* m_tblText;
        wxListCtrl* m_compLog;
        wxSimplebook* m_autocomp;

        wxDECLARE_EVENT_TABLE();
    };
}

#endif // LAX_SRC_MAIN_WINDOW_HPP_
