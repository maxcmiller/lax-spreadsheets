#include "code_history.hpp"

#include <stdexcept>

namespace lax
{
    void TableCodeHistory::addCommand(const wxString& str, int pos, bool isInsert)
    {
        if (m_pos != m_history.size())
        {
            // clear out the more recent history
            m_history.erase(m_history.begin() + m_pos, m_history.end());
        }

        // decide whether to coalesce
        if (m_history.size() > 0 &&                 // safety check
            m_history.back().isInsert == isInsert &&// same action?
            str.size() == 1 &&                      // inserted/deleted 1 char?
            !std::isspace(str[0]) &&                // part of same word?
            !atLastCompilePoint())                  // not at compile point?
        {
            CmdData& back = m_history.back();

            if (isInsert)
            {
                if (back.pos + back.text.size() == static_cast<std::size_t>(pos))
                {
                    // adding to the end of word
                    back.text.Append(str);
                    return; // all done
                }
            }
            else
            {
                if (back.pos - 1 == pos)
                {
                    // deleting from end of word
                    --back.pos;
                    back.text.Prepend(str);
                    return; // all done
                }
            }
        }

        // cannot coalesce; just add to back
        m_history.push_back({str, pos, isInsert});
        ++m_pos;
    }

    const TableCodeHistory::CmdData& TableCodeHistory::undo()
    {
        if (m_pos == 0)
        {
            throw std::logic_error("undo history underrun");
        }

        return m_history[--m_pos];
    }

    const TableCodeHistory::CmdData& TableCodeHistory::redo()
    {
        if (m_pos == m_history.size())
        {
            throw std::logic_error("undo history overrun");
        }

        return m_history[m_pos++];
    }

    void TableCodeHistory::setCompilePoint()
    {
        if (m_compPos > 0 && m_pos == m_compPoints[m_compPos - 1])
        {
            // this position is already in the list of positions;
            // do nothing
            return;
        }

        // need to erase next part of history
        m_compPoints.erase(m_compPoints.begin() + m_compPos, m_compPoints.end());

        m_compPoints.push_back(m_pos);
        ++m_compPos;
    }
}
