#include "autocomplete.hpp"

#include <sstream>

LAX_DISABLE_WARNINGS()
#include <wx/sizer.h>
LAX_REENABLE_WARNINGS()

#include "compiler/formula_driver.hpp"
#include "main.hpp"
#include "util/scope_exit.hpp"

namespace lax
{
namespace
{
    struct AutocompData:
        wxClientData
    {
        AutocompData(const location& l, const wxString& d):
            loc(l), disp(d) {}

        location loc;
        wxString disp;
    };

    class AutocompFormulaVisitor:
        public AssignVisitor
    {
    public:
        using ItemMap = std::unordered_map<std::string, wxDataViewItem>;

        AutocompFormulaVisitor(const Importer::Ptr& imp, std::string_view dir,
                wxDataViewTreeCtrl* ctrl, ItemMap& item):
            m_importer(imp), m_oldItems(item), m_dir(dir), m_ctrl(ctrl), m_item() {}

        void setItem(const wxDataViewItem& item, const std::string& name) noexcept
        {
            m_item = item;
            m_parent = name;
        }

        void visit(const ReduceAssign&) override {}
        void visit(const ImportAssign& i) override;
        void visit(const ExprAssign& e) override;
        void visit(const ScalarTypeAssign& t) override;
        void visit(const TypeAssign& t) override;

        void readImport(std::string_view name, std::string_view text,
                const location& loc);
        wxDataViewItem addToTree(const std::string& name, bool isContainer,
                const location& loc, wxString&& disp);

        ItemMap&& getItemMap() noexcept
        {
            return std::move(m_newItems);
        }
    private:
        std::string getKey(const std::string& parent, const std::string& name) const;

        Importer::Ptr m_importer;
        ItemMap m_newItems;
        ItemMap& m_oldItems;
        std::string_view m_dir;
        std::string m_parent;
        wxDataViewTreeCtrl* m_ctrl;
        wxDataViewItem m_item;
    };

    void AutocompFormulaVisitor::visit(const ImportAssign& i)
    {
        readImport(i.getName(), i.getValue(), i.getLoc());
    }

    void AutocompFormulaVisitor::visit(const ExprAssign& e)
    {
        addToTree(static_cast<std::string>(e.getName()), false, e.getLoc(),
                e.stringize());
    }

    void AutocompFormulaVisitor::visit(const ScalarTypeAssign& t)
    {
        addToTree(static_cast<std::string>(t.getName()), false, t.getLoc(),
                t.stringize());
    }

    void AutocompFormulaVisitor::visit(const TypeAssign& t)
    {
        addToTree(static_cast<std::string>(t.getName()), false, t.getLoc(),
                t.stringize());
    }

    void AutocompFormulaVisitor::readImport(std::string_view name,
            std::string_view text, const location& loc)
    {
        auto env = m_importer->find(text, m_dir);

        if (!env)
        {
            return;
        }

        std::string nameStr(name);
        wxDataViewItem parent;

        auto other = m_item;

        if (!nameStr.empty())
        {
            wxString disp;
            disp.Printf("import \"%s\" as %s", &text[0], &name[0]);
            m_item = parent = addToTree(nameStr, true, loc, std::move(disp));
        }

        std::string otherStr = std::move(m_parent);
        m_parent = getKey(otherStr, nameStr);

        ScopeExit guard([this, &other, &otherStr]
        {
            m_item = other;
            m_parent = std::move(otherStr);
        });

        for (const auto& [name, obj]: *env)
        {
            addToTree(*name, false, loc, *name);
        }

        m_ctrl->Expand(parent);
    }

    wxDataViewItem AutocompFormulaVisitor::addToTree(const std::string& name,
            bool isContainer, const location& loc, wxString&& disp)
    {
        auto pos = disp.find('\n');

        if (pos != wxString::npos)
        {
            disp.erase(pos);
        }

        std::string key = getKey(m_parent, name);
        std::unique_ptr<AutocompData> data(new AutocompData {loc, disp});

        if (auto itr = m_oldItems.find(key); itr != m_oldItems.end())
        {
            auto node = m_oldItems.extract(itr);
            wxDataViewItem item = m_newItems.insert(std::move(node)).position->second;
            m_ctrl->SetItemData(item, data.release());
            return item;
        }
        else
        {
            wxDataViewItem item;

            if (isContainer)
            {
                item = m_ctrl->AppendContainer(m_item, name);
                m_ctrl->SetItemData(item, data.release());
                m_ctrl->Expand(item);
            }
            else
            {
                item = m_ctrl->AppendItem(m_item, name);
                m_ctrl->SetItemData(item, data.release());
            }

            m_newItems.emplace(std::move(key), item);
            return item;
        }
    }

    std::string AutocompFormulaVisitor::getKey(const std::string& parent,
            const std::string& name) const
    {

        if (parent.empty())
        {
            return name;
        }
        else
        {
            return parent + '.' + name;
        }
    }

    class AutocompTopVisitor:
        public TopVisitor
    {
    public:
        using ItemMap = AutocompFormulaVisitor::ItemMap;

        AutocompTopVisitor(const Importer::Ptr& imp, std::string_view dir,
                wxDataViewTreeCtrl* ctrl, ItemMap& items):
            m_child(imp, dir, ctrl, items), m_ctrl(ctrl) {}

        void visit(const ChartAssign&) override {}
        void visit(const TableAssign& tbl) override;
        void visit(const TopImportAssign& imp) override;

        ItemMap&& getItemMap() noexcept
        {
            return m_child.getItemMap();
        }
    private:
        AutocompFormulaVisitor m_child;
        wxDataViewTreeCtrl* m_ctrl;
    };

    void AutocompTopVisitor::visit(const TableAssign& tbl)
    {
        m_child.setItem(wxDataViewItem(), "");
        wxDataViewItem parent = m_child.addToTree(tbl.getName(), true,
                tbl.getLoc(), tbl.stringize());
        m_child.setItem(parent, static_cast<std::string>(tbl.getName()));

        for (const auto& f: tbl)
        {
            f->accept(m_child);
        }
    }

    void AutocompTopVisitor::visit(const TopImportAssign& tia)
    {
        m_child.setItem(wxDataViewItem(), "");
        m_child.readImport(tia.getName(), tia.getValue(), tia.getLoc());
    }
}

LAX_DISABLE_WARNINGS()
    wxBEGIN_EVENT_TABLE(AutocompleteCtrl, wxWindow)
        EVT_DATAVIEW_ITEM_ACTIVATED(wxID_ANY, AutocompleteCtrl::OnDblClick)
        EVT_DATAVIEW_ITEM_CONTEXT_MENU(wxID_ANY, AutocompleteCtrl::OnMenu)
        EVT_DATAVIEW_ITEM_START_EDITING(wxID_ANY, AutocompleteCtrl::OnEditLabel)
    wxEND_EVENT_TABLE()
LAX_REENABLE_WARNINGS()

    AutocompleteCtrl::AutocompleteCtrl(wxStyledTextCtrl* code, wxWindow* parent,
            wxWindowID id):
        wxWindow(parent, id),
        m_tree(new wxDataViewTreeCtrl(this, wxID_ANY)), m_code(code)
    {
        SetSizer(new wxBoxSizer(wxVERTICAL));
        GetSizer()->Add(m_tree, wxSizerFlags(1).Expand());
        m_code->Bind(wxEVT_STC_MODIFIED, &AutocompleteCtrl::OnTextChanged, this);
    }

    void AutocompleteCtrl::setSymbolList(const std::vector<TopAssign::Ptr>& vec)
    {
        auto doc = wxGetApp().getWindow().getDocument();
        auto imp = doc->getImporter();
        auto dir = doc->getDir();
        AutocompTopVisitor v(imp, dir, m_tree, m_map);

        for (const auto& node: vec)
        {
            node->accept(v);
        }

        for (const auto& [name, item]: m_map)
        {
            // the only elements left are no longer in the tree
            auto pos = name.rfind('.');

            if (pos != std::string::npos && m_map.count(name.substr(0, pos)))
            {
                // we're already deleting a parent of this node, so don't bother
                continue;
            }

            m_tree->DeleteItem(item);
        }

        m_map = v.getItemMap();
    }

    void AutocompleteCtrl::OnDblClick(wxDataViewEvent& evt)
    {
        wxDataViewItem item = evt.GetItem();
        int pos = m_code->GetCurrentPos();
        int column = m_code->GetColumn(pos);
        int line = m_code->GetCurrentLine();

        // +1 to convert from zero-based wxSTC to one-based bison
        m_code->InsertText(pos, getInsertText(item, line + 1, column + 1));
    }

namespace
{
    bool isInside(const location& loc, int line, int col)
    {
        const position& begin = loc.begin;
        const position& end = loc.end;

        if (begin.line == end.line)
        {
            return begin.line == line && begin.column <= col && end.column >= col;
        }
        else if (begin.line == line)
        {
            return begin.column <= col;
        }
        else if (end.line == line)
        {
            return end.column >= col;
        }
        else
        {
            return begin.line < line && end.line > line;
        }
    }
}

    wxString AutocompleteCtrl::getInsertText(const wxDataViewItem& item, int line, int col)
    {
        wxDataViewItem parent = m_tree->GetStore()->GetParent(item);
        wxString res = m_tree->GetItemText(item);

        if (parent.IsOk())
        {
            AutocompData* data = static_cast<AutocompData*>(m_tree->GetItemData(parent));

            if (!isInside(data->loc, line, col))
            {
                return getInsertText(parent, line, col) + '.' + res;
            }
        }

        return res;
    }

    void AutocompleteCtrl::OnTextChanged(wxStyledTextEvent& evt)
    {
        std::istringstream istr(m_code->GetText().ToStdString());
        FormulaDriver drv(istr, "<input>");
        std::vector<TopAssign::Ptr> vec;

        if (drv.parseAssigns(vec))
        {
            setSymbolList(vec);
        }

        evt.Skip();
    }

    void AutocompleteCtrl::OnEditLabel(wxDataViewEvent& evt)
    {
        evt.Veto();
    }

    void AutocompleteCtrl::OnMenu(wxDataViewEvent& evt)
    {
        wxDataViewItem item = evt.GetItem();

        if (!item.IsOk())
        {
            return;
        }

        AutocompData* data = static_cast<AutocompData*>(m_tree->GetItemData(item));

        auto gotoDef = [this, data](wxCommandEvent&)
        {
            const position& begin = data->loc.begin;
            const position& end = data->loc.end;
            m_code->GotoLine(begin.line);

            // -1 since wxSTC is zero-based
            const int first = m_code->FindColumn(begin.line - 1, begin.column - 1);
            const int last = m_code->FindColumn(end.line - 1, end.column - 1);
            m_code->SetSelection(first, last);
        };

        wxMenu menu(data->disp);
        const int gotoId = menu.Append(wxID_ANY, _("Go to definition..."))->GetId();
        menu.Bind(wxEVT_MENU, gotoDef, gotoId);
        m_tree->PopupMenu(&menu);
    }
}
