#ifndef LAX_SRC_SHEET_DOC_HPP_
#define LAX_SRC_SHEET_DOC_HPP_

#include <functional>
#include <vector>

#include "disable_warnings.h"

LAX_DISABLE_WARNINGS()
#include <wx/cmdproc.h>
#include <wx/choicebk.h>
#include <wx/debug.h>
#include <wx/docview.h>
#include <wx/grid.h>
#include <wx/timer.h>
LAX_REENABLE_WARNINGS()

#include <nlohmann/json_fwd.hpp>

#include "code_history.hpp"
#include "compiler/environment.hpp"
#include "compiler/object.hpp"
#include "compiler/top_compiler.hpp"
#include "table.hpp"

class wxAuiNotebookEvent;

namespace lax
{
    class ChartBase;
    struct CompileInfo;

    struct PageData
    {
        using Ptr = std::shared_ptr<PageData>;

        PageData(const wxString& n, const wxString& p, std::string_view file,
                std::string_view dir, const CompileInfo::ConstPtr& imp):
            name(n), path(p), comp(imp, file, dir, mgr), tables() {}

        bool recompile(const nlohmann::json* j = nullptr);

        wxString name;
        wxString path;
        std::string code;
        PDataManager mgr;
        TopCompiler comp;
        NameMap<SheetDataTable::Ptr> tblCache;
        std::vector<SheetDataTable::Ptr> tables;
        TableCodeHistory history;
    };

    void to_json(nlohmann::json& j, const PageData& pg);
    void from_json(const nlohmann::json& j, PageData& pg);

    class SheetDoc:
        public wxDocument
    {
    public:
        SheetDoc();

        auto begin() noexcept
        {
            return m_pages.begin();
        }

        auto end() noexcept
        {
            return m_pages.end();
        }

        auto begin() const noexcept
        {
            return m_pages.begin();
        }

        auto end() const noexcept
        {
            return m_pages.end();
        }

        PageData::Ptr getPage(int pg) const
        {
            return m_pages.at(pg);
        }

        std::string_view getDir() const noexcept
        {
            return m_dir;
        }

        std::string_view getFile() const noexcept
        {
            return m_filename;
        }

        Importer::Ptr getImporter() const noexcept
        {
            return Importer::Ptr(m_importer, &m_importer->imp);
        }

        bool DeleteContents() override;
        bool OnNewDocument() override;
    protected:
        bool DoOpenDocument(const wxString& filename) override;
        bool DoSaveDocument(const wxString& filename) override;
    private:
        bool writeSheet(wxOutputStream& out, const PageData& page);
        bool readSheet(wxInputStream& in, const wxString& file);
        bool changeFilename(const wxString& newFile);

        void OnUndo(wxCommandEvent& evt);
        void OnRedo(wxCommandEvent& evt);
        void OnNewPage(wxCommandEvent& evt);
        void OnChangePage(wxAuiNotebookEvent& evt);
        void OnEditCell(wxGridEvent& evt);
        void OnDeletePage(wxCommandEvent& evt);
        void OnRenamePage(wxCommandEvent& evt);
        void OnRecalc(wxCommandEvent& evt);
        void OnSaveTable(wxCommandEvent& evt);
        void OnBackup(wxTimerEvent& evt);

        std::function<bool()> getAddPage(int pg, PageData::Ptr page);
        std::function<bool()> getRemovePage(int pg, PageData::Ptr page);

        std::vector<PageData::Ptr> m_pages;
        int m_active;
        long m_pageNum;
        std::string m_dir;
        std::string m_filename;
        CompileInfo::Ptr m_importer;
        wxTimer m_autoSave;

        wxDECLARE_DYNAMIC_CLASS(SheetDoc);
        wxDECLARE_EVENT_TABLE();
    };

    class UpdateInfo:
        public wxObject
    {
    public:
        enum Type
        {
            kReloadAll,
            kMoved,
            kAddPage,
            kRemovePage,
            kRenamePage,
            kUpdateTable,
        };

        explicit UpdateInfo(Type t, SheetDataTable* tbl = nullptr,
                PageData* pg = nullptr, int pgNo = -1):
            m_type(t), m_table(tbl), m_page(pg), m_pageNum(pgNo) {}

        explicit UpdateInfo(Type t, PageData* pg, int pgNo,
                SheetDataTable* tbl = nullptr):
            m_type(t), m_table(tbl), m_page(pg), m_pageNum(pgNo) {}

        Type getType() const noexcept
        {
            return m_type;
        }

        SheetDataTable* getTable() const noexcept
        {
            return m_table;
        }

        PageData* getPage() const noexcept
        {
            return m_page;
        }

        int getPageNumber() const noexcept
        {
            return m_pageNum;
        }
    private:
        Type m_type;
        SheetDataTable* m_table;
        PageData* m_page;
        int m_pageNum;
    };

    class SheetCommand:
        public wxCommand
    {
    public:
        using FuncType = std::function<bool()>;

        explicit SheetCommand(FuncType doFunc,
                const wxString& name = wxEmptyString):
            wxCommand(false, name), m_doFunc(doFunc), m_undoFunc() {}

        SheetCommand(FuncType doFunc, FuncType undoFunc,
                const wxString& name = wxEmptyString):
            wxCommand(static_cast<bool>(undoFunc), name), m_doFunc(doFunc),
            m_undoFunc(undoFunc) {}

        bool Do() override;
        bool Undo() override;
    private:
        FuncType m_doFunc;
        FuncType m_undoFunc;
    };

    wxDECLARE_EVENT(LAX_TABLE_MOVE, wxCommandEvent);
}

#endif // LAX_SRC_SHEET_DOC_HPP_
