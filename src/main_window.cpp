#include "main_window.hpp"

#include <memory>

LAX_DISABLE_WARNINGS()
#include <wx/artprov.h>
#include <wx/bmpbuttn.h>
#include <wx/dcclient.h>
#include <wx/config.h>
#include <wx/log.h>
#include <wx/numdlg.h>
#include <wx/process.h>
#include <wx/scrolwin.h>
#include <wx/stdstream.h>
LAX_REENABLE_WARNINGS()

#include "autocomplete.hpp"
#include "charts/chart_setup.hpp"
#include "charts/graphics_defaults.hpp"
#include "compiler/bytecode.hpp"
#include "compiler/table_compiler.hpp"

namespace lax
{
LAX_DISABLE_WARNINGS()
    wxBEGIN_EVENT_TABLE(MainWindow, wxDocParentFrame)
        EVT_MENU(MainWindow::kSavePerspective, MainWindow::OnSavePerspective)
        EVT_MENU_RANGE(MainWindow::kTblCodeMenu, MainWindow::kCompLogMenu,
                MainWindow::OnPerspectiveMenu)
        EVT_MENU(MainWindow::kFormulaSettings, MainWindow::OnFormulaSettings)
        EVT_MENU(MainWindow::kGraphicSettings, MainWindow::OnGraphicSettings)
        EVT_AUI_PANE_CLOSE(MainWindow::OnClosePane)
        EVT_BUTTON(MainWindow::kRecompileTbl, MainWindow::OnButtonToMenu)
        EVT_MENU(MainWindow::kRecompImpl, MainWindow::OnRecompile)
        EVT_MENU(MainWindow::kShowDeps, MainWindow::OnShowDeps)
        EVT_MENU(wxID_FIND, MainWindow::OnFind)
        EVT_MENU(wxID_REPLACE, MainWindow::OnFind)
    wxEND_EVENT_TABLE()
LAX_REENABLE_WARNINGS()

    MainWindow::MainWindow(wxDocManager* mgr, const wxString& label):
        wxDocParentFrame(mgr, nullptr, wxID_ANY, label, wxDefaultPosition,
                wxDefaultSize, wxDEFAULT_FRAME_STYLE | wxMAXIMIZE),
        m_mgr(this), m_menu(new wxMenuBar(0)),
        m_toolbar(new wxAuiToolBar(this, wxID_ANY, wxDefaultPosition,
                wxDefaultSize, wxAUI_TB_HORZ_LAYOUT)),
        m_sheets(new wxAuiNotebook(this, kSheetNotebook, wxDefaultPosition,
                wxDefaultSize, wxAUI_NB_TOP)),
        m_tblText(new TableCodeCtrl(this, wxID_ANY)),
        m_compLog(new wxListCtrl(this, wxID_ANY, wxDefaultPosition, wxDefaultSize,
                wxLC_REPORT | wxLC_HRULES | wxLC_SINGLE_SEL)),
        m_autocomp(new wxSimplebook(this, wxID_ANY))
    {
        m_mgr.SetFlags(wxAUI_MGR_DEFAULT);

        initMenu(GetDocumentManager()->GetFileHistory());
        SetMenuBar(m_menu);

        initToolbar();
        m_toolbar->Realize();

        m_mgr.AddPane(m_toolbar, wxAuiPaneInfo().ToolbarPane().Top().Name("toolbar"));
        m_mgr.AddPane(m_sheets, wxAuiPaneInfo().CentrePane().DockFixed().Name("sheetbook"));
        m_mgr.AddPane(m_compLog, wxAuiPaneInfo().Bottom().Caption("Compilation log").
                BestSize(500, 250).Name(wxS("comp_log")));
        m_mgr.AddPane(m_tblText, wxAuiPaneInfo().Right().Caption(_("Table/Chart setup")).
                CloseButton().Name(wxS("table_code")).BestSize(300, 700));
        m_mgr.AddPane(m_autocomp, wxAuiPaneInfo().Right().Bottom().
                Caption(_("Autocomplete")).Name(wxS("autocomplete")));

        wxString perspective = wxConfigBase::Get()->Read(wxS("perspective"));

        if (!perspective.empty())
        {
            m_mgr.LoadPerspective(perspective);
        }

        m_mgr.Update();
        Centre(wxBOTH);

        m_sheets->AddPage(new wxWindow(m_sheets, wxID_ANY), wxS("+"));

        long nMax = -1;
        wxConfigBase::Get()->Read("Formula/StackMax", nMax);

        if (nMax >= 0)
        {
            BytecodeFunction::setMaxStack(nMax);
        }

        m_compLog->InsertColumn(0, _("Filename"));
        m_compLog->InsertColumn(1, _("Line"));
        m_compLog->InsertColumn(2, _("Message"));
    }

    MainWindow::~MainWindow()
    {
        m_mgr.UnInit();
    }

    void MainWindow::initMenu(wxFileHistory* fh)
    {
        // File
        {
            std::unique_ptr<wxMenu> file(new wxMenu);
            file->Append(wxID_NEW);
            file->Append(wxID_OPEN);

            if (fh)
            {
                wxMenu* history = new wxMenu;
                file->AppendSubMenu(history, _("&Recent files"));
                fh->UseMenu(history);
                fh->AddFilesToMenu();
            }

            file->Append(wxID_SAVE);
            file->Append(wxID_SAVEAS);
            file->AppendSeparator();
            file->Append(wxID_PRINT);
            file->AppendSeparator();
            file->Append(wxID_EXIT);

            m_menu->Append(file.release(), _("&File"));
        }

        // Edit
        {
            std::unique_ptr<wxMenu> edit(new wxMenu);
            edit->Append(wxID_UNDO);
            edit->Append(wxID_REDO);
            edit->AppendSeparator();
            edit->Append(kNewPage, _("New &sheet"));
            edit->Append(kDeletePage, _("&Delete sheet"));
            edit->Append(kRenamePage, _("Re&name sheet"));
            edit->Append(kRecalc, _("&Recalculate sheet\tCTRL+R"));
            edit->Append(kRecompImpl, _("Re&compile current table\tCTRL+SHIFT+R"));
            edit->Append(wxID_FIND);
            edit->Append(wxID_REPLACE);
            m_menu->Append(edit.release(), _("&Edit"));
        }

        // View
        {
            std::unique_ptr<wxMenu> view(new wxMenu);
            view->Append(kTblCodeMenu, _("Table/Chart setup"), "", wxITEM_CHECK)->Check();
            view->Append(kCompLogMenu, _("Compilation log"), "", wxITEM_CHECK)->Check();
            view->Append(kAutocompMenu, _("Autocomplete browser"), "", wxITEM_CHECK)->Check();
            view->Append(kSavePerspective, _("&Save perspective"));
            view->Append(kShowDeps, _("Show dependencies"));
            m_menu->Append(view.release(), _("&View"));
        }

        // Settings
        {
            std::unique_ptr<wxMenu> settings(new wxMenu);
            settings->Append(kFormulaSettings, _("&Formula settings"));
            settings->Append(kGraphicSettings, _("&Graphics settings"));
            m_menu->Append(settings.release(), _("&Settings"));
        }

        // Help
        {
            std::unique_ptr<wxMenu> help(new wxMenu);
            help->Append(wxID_ABOUT);
            help->Append(wxID_HELP);
            m_menu->Append(help.release(), _("&Help"));
        }

        wxAcceleratorEntry entries[1];
        entries[0].Set(wxACCEL_CTRL, 'Y', wxID_REDO);
        wxAcceleratorTable table(sizeof(entries)/sizeof(entries[0]), entries);
        SetAcceleratorTable(table);
    }

    void MainWindow::initToolbar()
    {
        m_toolbar->AddTool(wxID_NEW, _("New"), wxArtProvider::GetBitmap(wxART_NEW));
        m_toolbar->AddTool(wxID_OPEN, _("Open"), wxArtProvider::GetBitmap(wxART_FILE_OPEN));
        m_toolbar->AddTool(wxID_SAVE, _("Save"), wxArtProvider::GetBitmap(wxART_FILE_SAVE));
        m_toolbar->AddTool(wxID_SAVEAS, _("Save as"), wxArtProvider::GetBitmap(wxART_FILE_SAVE_AS));
        m_toolbar->AddSeparator();
        m_toolbar->AddTool(wxID_UNDO, _("Undo"), wxArtProvider::GetBitmap(wxART_UNDO));
        m_toolbar->AddTool(wxID_REDO, _("Redo"), wxArtProvider::GetBitmap(wxART_REDO));
        m_toolbar->AddTool(wxID_FIND, _("Find"), wxArtProvider::GetBitmap(wxART_FIND));
        m_toolbar->AddTool(wxID_REPLACE, _("Find and replace"),
                wxArtProvider::GetBitmap(wxART_FIND_AND_REPLACE));
    }

    void MainWindow::OnSavePerspective(wxCommandEvent&)
    {
        wxConfigBase::Get()->Write("perspective", m_mgr.SavePerspective());
    }

    void MainWindow::OnClosePane(wxAuiManagerEvent& evt)
    {
        int id = m_menu->FindMenuItem(_("View"), evt.GetPane()->caption);
        wxCHECK_RET(id != wxNOT_FOUND, wxS("could not find menu for that pane"));
        wxMenuItem* item = m_menu->FindItem(id);
        wxCHECK_RET(item, wxS("could not find menu for that pane"));
        item->Check(false);
    }

    void MainWindow::OnPerspectiveMenu(wxCommandEvent& evt)
    {
        const char* title = nullptr;

        switch (evt.GetId())
        {
        case kTblCodeMenu:
            title = "table_code";
            break;
        case kCompLogMenu:
            title = "comp_log";
            break;
        case kAutocompMenu:
            title = "autocomplete";
            break;
        default:
            wxFAIL_MSG(wxS("bad menu selection"));
            return;
        }

        wxAuiPaneInfo& pane = m_mgr.GetPane(title);
        pane.Show(evt.IsChecked());
        m_mgr.Update();
    }

    void MainWindow::OnButtonToMenu(wxCommandEvent& evt)
    {
        evt.SetEventType(wxEVT_MENU);
        ProcessEvent(evt);
    }

    wxPanel* MainWindow::addPage(const wxString& name, PageData& pg, int before,
            TableCodeCtrl::CodeSet::Ptr* set)
    {
        wxScrolledWindow* p = new wxScrolledWindow(m_sheets);
        p->SetScrollRate(10, 10);

        if (before < 0)
        {
            wxWindow* plus = removePlus();
            m_sheets->AddPage(p, name, true);
            m_sheets->AddPage(plus, wxS("+"));
        }
        else
        {
            m_sheets->InsertPage(before, p, name, true);
        }

        auto s = m_tblText->addCodeSet(pg);

        if (set)
        {
            *set = s;
        }

        auto autocomp = new AutocompleteCtrl(s->getTextCtrl(), m_autocomp);
        m_autocomp->AddPage(autocomp, "", true);

        return p;
    }

    void MainWindow::removePage(int pgNum)
    {
        if (m_sheets->GetSelection() == static_cast<int>(pgNum))
        {
            // make sure that we never switch to + tab
            m_sheets->SetSelection(0);
        }

        m_sheets->DeletePage(pgNum);
        m_tblText->removeCodeSet(pgNum);
    }

    void MainWindow::removePage(const wxWindow* p)
    {
        for (std::size_t ctr = 0; ctr < m_sheets->GetPageCount(); ++ctr)
        {
            if (m_sheets->GetPage(ctr) == p)
            {
                removePage(ctr);
            }
        }
    }

    wxWindow* MainWindow::findPageWithData(const void* data)
    {
        for (std::size_t ctr = 0; ctr < m_sheets->GetPageCount(); ++ctr)
        {
            if (m_sheets->GetPage(ctr)->GetClientData() == data)
            {
                return m_sheets->GetPage(ctr);
            }
        }

        wxFAIL_MSG(wxS("could not find page with data"));
        return nullptr;
    }

    void MainWindow::resetToStart()
    {
        // preserve the + page from destruction
        wxWindow* plus = removePlus();
        m_sheets->DeleteAllPages();
        m_tblText->clearAll();
        m_sheets->AddPage(plus, wxS("+"));
    }

    wxWindow* MainWindow::removePlus()
    {
        int pageAdd = m_sheets->GetPageCount() - 1;
        wxWindow* plus = m_sheets->GetPage(pageAdd);
        m_sheets->RemovePage(pageAdd);
        return plus;
    }

    void MainWindow::OnFormulaSettings(wxCommandEvent&)
    {
        // TODO (Max#1#): Add more formula settings
        long got =
        wxGetNumberFromUser(_("Maximum number of stack frames (0 for no maximum):"),
                "", "", BytecodeFunction::getMaxStack(), 0, 1'000'000);

        if (got >= 0)
        {
            BytecodeFunction::setMaxStack(got);
            wxConfigBase::Get()->Write("Formula/StackMax", got);
        }
    }

    void MainWindow::OnGraphicSettings(wxCommandEvent&)
    {
        GraphicsDefaults::get().showDialog();
    }

    void MainWindow::logFailure(const wxString& name, const CompilationFailure& cf)
    {
        m_compLog->Hide();

        long index = m_compLog->GetItemCount();
        m_compLog->InsertItem(index, "");
        m_compLog->SetItem(index, 2, "Compiling " + name + " failed:");
        ++index;

        wxString buf;

        for (const auto& error: cf)
        {
            m_compLog->InsertItem(index, "");
            m_compLog->SetItem(index, 0, *error.loc.begin.filename);
            buf.Printf("%d", error.loc.begin.line);
            m_compLog->SetItem(index, 1, buf);
            m_compLog->SetItem(index, 2, error.msg);
            ++index;
        }

        for (int i = 0; i < m_compLog->GetColumnCount(); ++i)
        {
            m_compLog->SetColumnWidth(i, wxLIST_AUTOSIZE);
        }

        m_compLog->Show();
    }

    void MainWindow::logFailure(const wxString& msg)
    {
        long index = m_compLog->GetItemCount();
        m_compLog->InsertItem(index, "");
        m_compLog->SetItem(index, 2, msg);
    }

    void MainWindow::OnRecompile(wxCommandEvent& evt)
    {
        evt.SetEventType(wxEVT_BUTTON);
        evt.SetId(kRecompileTbl);
        wxPostEvent(m_tblText, evt);
    }

    void MainWindow::OnShowDeps(wxCommandEvent&)
    {
        class ImageDialog:
            public wxDialog
        {
        public:
            ImageDialog(wxWindow* parent, const wxBitmap& bmp):
                wxDialog(parent, wxID_ANY, _("Dependency tree")), m_bmp(bmp)
            {
                Bind(wxEVT_PAINT, &ImageDialog::OnPaint, this, GetId());
                SetClientSize(m_bmp.GetSize());
            }
        private:
            void OnPaint(wxPaintEvent&)
            {
                wxPaintDC dc(this);
                dc.DrawBitmap(m_bmp, 0, 0);
            }

            const wxBitmap& m_bmp;
        };

        wxProcess* proc = wxProcess::Open(wxS("dot -Tpng"));

        if (!proc)
        {
            wxLogError(_("Failed to run dot."));
            return;
        }

        {
            std::string graph = ValueObserver::getDepGraph();
            proc->GetOutputStream()->Write(graph.c_str(), graph.size());
            proc->CloseOutput();
        }

        auto onDotExit = [this, proc](wxProcessEvent& evt)
        {
            evt.Skip(); // tell proc to delete itself after we return

            if (evt.GetExitCode() == 0)
            {
                // success
                wxImage img;

                if (!img.LoadFile(*proc->GetInputStream(), wxS("image/png")))
                {
                    return;
                }

                wxBitmap bmp(img);
                ImageDialog dlg(this, bmp);
                dlg.ShowModal();
            }
            else
            {
                wxLogError(_("dot exited with non-zero exit status."));
                std::cerr << "log from dot:\n";
                std::cerr << wxStdInputStream(*proc->GetErrorStream()).rdbuf() << '\n';
            }
        };

        proc->Bind(wxEVT_END_PROCESS, onDotExit, wxID_ANY);
    }

    void MainWindow::OnFind(wxCommandEvent& evt)
    {
        m_tblText->wxEvtHandler::ProcessEvent(evt);
    }

    SheetDoc* MainWindow::getDocument() const
    {
        return static_cast<SheetDoc*>(m_docManager->GetDocuments().front());
    }
}
