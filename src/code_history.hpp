#ifndef LAX_SRC_CODE_HISTORY_HPP_
#define LAX_SRC_CODE_HISTORY_HPP_

#include "disable_warnings.h"

#include <vector>

LAX_DISABLE_WARNINGS()
#include <wx/debug.h>
#include <wx/string.h>
LAX_REENABLE_WARNINGS()

namespace lax
{
    class TableCodeHistory
    {
    public:
        struct CmdData
        {
            wxString text;
            int pos;
            bool isInsert;
        };

        TableCodeHistory() = default;
        TableCodeHistory(const TableCodeHistory&) = delete;

        const CmdData& undo();
        const CmdData& redo();
        void addCommand(const wxString& text, int pos, bool isInsert);
        void setCompilePoint();

        template <typename Func>
        void prevCompilePoint(Func f)
        {
            wxCHECK_RET(m_compPos > 1, "cannot go to previous compilation point");
            --m_compPos;
            const auto point = m_compPoints[m_compPos - 1];
            moveTo(point, f);
        }

        template <typename Func>
        void nextCompilePoint(Func f)
        {
            wxCHECK_RET(m_compPos != m_compPoints.size(),
                    "cannot go to next compilation point");
            const auto point = m_compPoints[m_compPos];
            moveTo(point, f);
            ++m_compPos;
        }

        std::size_t getPos() const noexcept
        {
            return m_pos;
        }

        bool atLastCompilePoint() const noexcept
        {
            return !m_compPoints.empty() && m_compPoints.back() == m_pos;
        }
    private:
        template <typename Func>
        void moveTo(std::size_t point, Func f)
        {
            while (m_pos > point)
            {
                f(undo());
            }

            while (m_pos < point)
            {
                f(redo());
            }
        }

        std::vector<CmdData> m_history;
        std::vector<std::size_t> m_compPoints;
        std::size_t m_pos = 0;
        std::size_t m_compPos = 0;
    };
}

#endif // LAX_SRC_CODE_HISTORY_HPP_
