#ifndef LAX_SRC_WIDGETS_REORDER_LIST_HPP_
#define LAX_SRC_WIDGETS_REORDER_LIST_HPP_

#include "../disable_warnings.h"

#include <type_traits>

LAX_DISABLE_WARNINGS()
#include <wx/dc.h>
#include <wx/panel.h>
LAX_REENABLE_WARNINGS()

class wxButton;
class wxDataViewEvent;
class wxDataViewListCtrl;

namespace lax
{
    template <typename T>
    struct ReorderTraits
    {
        static_assert(!std::is_same_v<T, T>,
                "no specialization provided for ReorderTraits");

        ReorderTraits(const ReorderTraits& tr);

        void getLabel(const T& t, wxString& out) const;
        void draw(const T& t, wxDC& out) const;
        bool showDialog(T& out, const T* in) const;
    };

    class ReorderListBase:
        public wxPanel
    {
    protected:
        ReorderListBase() = default;

        bool Create(wxWindow* parent, wxWindowID id, int nElems,
                const wxPoint& pos = wxDefaultPosition,
                const wxSize& size = wxDefaultSize);

        virtual ~ReorderListBase() = default;
    private:
        virtual void addEntry(int i, wxDC& dc, wxString& lbl) = 0;
        virtual bool showDialog(int i) = 0;
        virtual void swapEntries(int i, int j) = 0;
        virtual void removeEntry(int i) = 0;
        wxVariant getRow(int row);

        void OnDblClick(wxDataViewEvent& evt);
        void OnChangeSel(wxDataViewEvent& evt);
        void OnMoveItem(wxCommandEvent& evt);
        void OnAddItem(wxCommandEvent& evt);
        void OnRemoveItem(wxCommandEvent& evt);

        wxDataViewListCtrl* m_list;
        wxButton* m_remove;
        wxButton* m_up;
        wxButton* m_down;

        wxDECLARE_EVENT_TABLE();
    };

    template <
        typename Container,
        typename Traits = ReorderTraits<typename Container::value_type>
    >
    class ReorderList:
        public ReorderListBase
    {
    public:
        ReorderList(wxWindow* parent, wxWindowID id, Container& c,
                Traits traits = Traits(),
                const wxPoint& pos = wxDefaultPosition,
                const wxSize& size = wxDefaultSize):
            ReorderListBase(), m_ref(c), m_copy(c), m_traits(std::move(traits))
        {
            Create(parent, id, c.size(), pos, size);
        }

        void commit()
        {
            m_ref = std::move(m_copy);
        }
    private:
        void addEntry(int idx, wxDC& dc, wxString& lbl) override
        {
            const auto& t = m_copy.at(idx);
            m_traits.draw(t, dc);
            m_traits.getLabel(t, lbl);
        }

        bool showDialog(int i) override
        {
            using ValueType = typename Container::value_type;
            const ValueType* in = nullptr;
            ValueType* out;

            if (i < 0)
            {
                out = &m_copy.emplace_back();
            }
            else
            {
                out = &m_copy[i];
                in = out;
            }

            const bool success = m_traits.showDialog(*out, in);

            if (!success && i < 0)
            {
                m_copy.pop_back();
            }

            return success;
        }

        void swapEntries(int i, int j) override
        {
            using std::swap;
            swap(m_copy[i], m_copy[j]);
        }

        void removeEntry(int i) override
        {
            m_copy.erase(m_copy.begin() + i);
        }

        Container& m_ref;
        Container m_copy;
        Traits m_traits;
    };
}

#endif // LAX_SRC_WIDGETS_REORDER_LIST_HPP_
