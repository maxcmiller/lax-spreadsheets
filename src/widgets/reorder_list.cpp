#include "reorder_list.hpp"

LAX_DISABLE_WARNINGS()
#include <wx/button.h>
#include <wx/dataview.h>
#include <wx/dcmemory.h>
#include <wx/sizer.h>
LAX_REENABLE_WARNINGS()

namespace lax
{
LAX_DISABLE_WARNINGS()
    wxBEGIN_EVENT_TABLE(ReorderListBase, wxPanel)
        EVT_DATAVIEW_ITEM_ACTIVATED(wxID_ANY, ReorderListBase::OnDblClick)
        EVT_DATAVIEW_SELECTION_CHANGED(wxID_ANY, ReorderListBase::OnChangeSel)
        EVT_BUTTON(wxID_UP, ReorderListBase::OnMoveItem)
        EVT_BUTTON(wxID_DOWN, ReorderListBase::OnMoveItem)
        EVT_BUTTON(wxID_ADD, ReorderListBase::OnAddItem)
        EVT_BUTTON(wxID_REMOVE, ReorderListBase::OnRemoveItem)
    wxEND_EVENT_TABLE()
LAX_REENABLE_WARNINGS()

    bool ReorderListBase::Create(wxWindow* parent, wxWindowID id, int nElems,
            const wxPoint& pos, const wxSize& size)
    {
        if (!wxPanel::Create(parent, id, pos, size))
        {
            return false;
        }

        m_list = new wxDataViewListCtrl(this, wxID_ANY);
        m_up = new wxButton(this, wxID_UP);
        m_up->Disable();
        m_down = new wxButton(this, wxID_DOWN);
        m_down->Disable();
        auto add = new wxButton(this, wxID_ADD);
        m_remove = new wxButton(this, wxID_REMOVE);
        m_remove->Disable();

        wxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);
        SetSizer(sizer);

        sizer->Add(m_list, wxSizerFlags(1).Expand());

        wxSizer* buttons = new wxBoxSizer(wxVERTICAL);
        sizer->Add(buttons, wxSizerFlags(0).Center());

        wxSizerFlags buttonFlags;
        buttonFlags.Center();
        buttons->Add(m_up, buttonFlags);
        buttons->Add(m_down, buttonFlags);
        buttons->Add(add, buttonFlags);
        buttons->Add(m_remove, buttonFlags);

        m_list->AppendIconTextColumn(_("Value"));

        for (int i = 0; i < nElems; ++i)
        {
            wxVector<wxVariant> vec(1, getRow(i));
            m_list->AppendItem(vec, i);
        }

        return true;
    }

    wxVariant ReorderListBase::getRow(int row)
    {
        wxBitmap bmp(32, 32);
        wxMemoryDC dc(bmp);
        wxString lbl;
        addEntry(row, dc, lbl);

        wxIcon icon;
        icon.CopyFromBitmap(bmp);

        wxVariant v;
        v << wxDataViewIconText(lbl, icon);
        return v;
    }

    void ReorderListBase::OnDblClick(wxDataViewEvent&)
    {
        const int index = m_list->GetSelectedRow();

        if (showDialog(index))
        {
            m_list->SetValue(getRow(index), index, 0);
        }
    }

    void ReorderListBase::OnChangeSel(wxDataViewEvent&)
    {
        const int sel = m_list->GetSelectedRow();

        // disable if none selected
        m_remove->Enable(sel >= 0);
        // enable if row other than first selected
        m_up->Enable(sel > 0);
        // enable if row other than last selected
        m_down->Enable(sel >= 0 && sel < m_list->GetItemCount() - 1);
    }

    void ReorderListBase::OnAddItem(wxCommandEvent&)
    {
        if (showDialog(-1))
        {
            const int row = m_list->GetItemCount();
            m_list->AppendItem(wxVector<wxVariant>(1, getRow(row)));
        }
    }

    void ReorderListBase::OnRemoveItem(wxCommandEvent&)
    {
        const int sel = m_list->GetSelectedRow();
        m_list->DeleteItem(sel);
        removeEntry(sel);
    }

    void ReorderListBase::OnMoveItem(wxCommandEvent& evt)
    {
        wxDataViewListStore* store = m_list->GetStore();
        const int sel = m_list->GetSelectedRow();
        const int other = evt.GetId() == wxID_DOWN? sel + 1:sel - 1;
        const auto selItem = store->GetItem(sel);
        const auto otherItem = store->GetItem(other);
        wxVariant selVal, otherVal;

        store->GetValue(selVal, selItem, 0);
        store->GetValue(otherVal, otherItem, 0);
        store->SetValue(otherVal, selItem, 0);
        store->SetValue(selVal, otherItem, 0);

        m_list->SelectRow(other);

        wxDataViewEvent dve;
        OnChangeSel(dve);

        swapEntries(sel, other);
    }
}
