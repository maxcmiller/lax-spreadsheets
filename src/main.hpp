#ifndef LAX_SRC_MAIN_HPP_
#define LAX_SRC_MAIN_HPP_

#include "disable_warnings.h"

LAX_DISABLE_WARNINGS()
#include <wx/app.h>
#include <wx/docview.h>
LAX_REENABLE_WARNINGS()

#include "main_window.hpp"

namespace lax
{
    class SheetsGuiApp:
        public wxApp
    {
    public:
        bool OnInit() override;
        int OnExit() override;
        bool OnExceptionInMainLoop() override;
        void OnUnhandledException() override;
        void OnInitCmdLine(wxCmdLineParser& p) override;
        bool OnCmdLineParsed(wxCmdLineParser& p) override;

        MainWindow& getWindow()
        {
            return *m_win;
        }
    private:
        void OnAbout(wxCommandEvent&);
        void OnHelp(wxCommandEvent&);

        void fatalError(const wxString& msg = wxEmptyString);
        bool createConfig();
        void openDocument();

        static wxString unknownError;

        wxDocManager* m_mgr;
        MainWindow* m_win = nullptr;
        wxString m_file;

        wxDECLARE_EVENT_TABLE();
    };
}

wxDECLARE_APP(lax::SheetsGuiApp);

#endif // LAX_SRC_MAIN_HPP_
