#include "table.hpp"

#include <fstream>
#include <sstream>

LAX_DISABLE_WARNINGS()
#include <wx/choicdlg.h>
#include <wx/log.h>
#include <wx/sstream.h>
#include <wx/stdstream.h>
LAX_REENABLE_WARNINGS()

#include <nlohmann/json.hpp>

#include "compiler/bytecode.hpp"
#include "compiler/formula_driver.hpp"
#include "compiler/importer/stdlib_finder.hpp"
#include "main.hpp"
#include "util/scope_exit.hpp"

void to_json(nlohmann::json& j, const wxString& str);
void from_json(const nlohmann::json& j, wxString& str);

namespace lax
{
    void SheetDataTable::GuiObserver::doUpdate(int)
    {
        if (wxGrid* grid = m_tbl->GetView())
        {
            wxGridTableMessage msg(m_tbl, wxGRIDTABLE_REQUEST_VIEW_GET_VALUES);
            grid->ProcessTableMessage(msg);
        }
    }

    SheetDataTable::~SheetDataTable()
    {
        if (GetView())
        {
            // If we don't disable the control, then the program will crash when
            // GetView()'s dtor tries to do it itself because the latter uses
            // the table (this object) without checking for null first.
            GetView()->DisableCellEditControl();
            GetView()->SetTable(nullptr);
        }

        m_scalarAttr->DecRef();
    }

    bool SheetDataTable::hasType(const TypeInfo& type)
    {
        static std::unordered_set<TypeInfo> types {
            NumberObject::theType(),
            StringObject::theType(),
            BoolObject::theType(),
            ErrorObject::theType(),
        };

        return types.count(type);
    }

    int SheetDataTable::getColumnSize() const noexcept
    {
        return m_cols.empty()? 0:m_numRows;
    }

    int SheetDataTable::GetNumberRows()
    {
        return getColumnSize() + m_impl->getNumReduces() + m_scalars.size();
    }

    int SheetDataTable::GetNumberCols()
    {
        if (m_cols.empty())
        {
            // there may still be scalars in the table
            return 1;
        }
        else
        {
            return m_cols.size();
        }
    }

    wxString SheetDataTable::GetTypeName(int row, int col)
    {
        TypeInfo type;
        wxASSERT(!GetView() ||
                (GetView()->GetNumberRows() == GetNumberRows() &&
                 GetView()->GetNumberCols() == GetNumberCols()));

        if (row < getColumnSize())
        {
            if (auto ptr = m_impl->getObject(col, row))
            {
                type = ptr->getType();
            }
            else
            {
                type = m_impl->getType(col);
            }
        }
        else
        {
            std::size_t index = row - getColumnSize();

            if (index < m_impl->getNumReduces())
            {
                if (auto vn = m_impl->getReduce(col, index))
                {
                    if (auto ptr = vn->getObject())
                    {
                        type = ptr->getType();
                    }
                    else
                    {
                        type = vn->getType();
                    }
                }
            }
            else
            {
                index -= m_impl->getNumReduces();
                auto vn = m_scalars.at(index)->node;

                if (auto ptr = vn->getObject())
                {
                    type = ptr->getType();
                }
                else
                {
                    type = vn->getType();
                }
            }
        }

        if (type == TypeInfo::nullType() || !hasType(type))
        {
            type = StringObject::theType();
        }

        return type.getName();
    }

    wxString SheetDataTable::GetValue(int row, int col)
    {
        if (auto ptr = getObject(row, col))
        {
            return ptr->stringize();
        }
        else
        {
            return wxEmptyString;
        }
    }

    void SheetDataTable::SetValue(int row, int col, const wxString& val)
    {
        TypeInfo type = m_impl->getType(col);

        try
        {
            auto got = type.construct(val.ToStdString());

            if (!got)
            {
                wxLogError(_("Could not convert \"%s\" to type %s."), val,
                        type.getName());
            }
            else
            {
                setObject(row, col, got);
            }
        }
        catch (std::exception& ex)
        {
            wxLogError(_("Could not convert \"%s\" to type %s:\n%s"), val,
                    type.getName(), ex.what());
        }
    }

    bool SheetDataTable::CanGetValueAs(int row, int col, const wxString& type)
    {
        return type == StringObject::theType().getName() ||
               GetTypeName(row, col) == type;
    }

    bool SheetDataTable::CanSetValueAs(int row, int col, const wxString& type)
    {
        return GetTypeName(row, col) == type;
    }

    double SheetDataTable::GetValueAsDouble(int row, int col)
    {
        auto num = std::dynamic_pointer_cast<NumberObject>(getObject(row, col));

        if (!num)
        {
            return 0;
        }

        return num->getValue();
    }

    bool SheetDataTable::GetValueAsBool(int row, int col)
    {
        return getObject(row, col) == BoolObject::getTrue();
    }

    void* SheetDataTable::GetValueAsCustom(int row, int col, const wxString&)
    {
        return getObject(row, col).get();
    }

    void SheetDataTable::SetValueAsDouble(int row, int col, double d)
    {
        if (CanSetValueAs(row, col, NumberObject::theType().getName()))
        {
            setObject(row, col, std::make_shared<NumberObject>(d));
        }
    }

    void SheetDataTable::SetValueAsBool(int row, int col, bool b)
    {
        if (CanSetValueAs(row, col, BoolObject::theType().getName()))
        {
            setObject(row, col, BoolObject::get(b));
        }
    }

    bool SheetDataTable::InsertRows(std::size_t pos, std::size_t numRows)
    {
        pos = std::min(pos, static_cast<std::size_t>(m_numRows));

        m_numRows += numRows;
        m_impl->insert(pos, numRows);

        if (GetView() && !m_cols.empty())
        {
            // only notify of addition if we will in fact display these rows
            wxGridTableMessage msg(this, wxGRIDTABLE_NOTIFY_ROWS_INSERTED, pos,
                    numRows);
            GetView()->ProcessTableMessage(msg);
            GetView()->GetParent()->Fit();
        }

        return true;
    }

    bool SheetDataTable::DeleteRows(std::size_t pos, std::size_t numRows)
    {
        m_numRows -= numRows;
        m_impl->erase(pos, numRows);

        if (GetView())
        {
            wxGridTableMessage msg(this, wxGRIDTABLE_NOTIFY_ROWS_DELETED, pos,
                    numRows);
            GetView()->ProcessTableMessage(msg);
            GetView()->GetParent()->Fit();
        }

        return true;
    }

    void SheetDataTable::update(const std::shared_ptr<Table>& tbl, ValueSystem& sys)
    {
        int oldCols = GetNumberCols();
        int oldRows = GetNumberRows();

        m_cols.clear();
        m_scalars.clear();
        m_reduces.clear();
        m_observers.clear();
        m_impl = tbl;

        if (!m_guiObs.isValid())
        {
            m_guiObs = makeValue<GuiObserver>(sys, this);
        }

        if (m_title.empty())
        {
            m_title = &(*m_impl->getTableName())[0];
        }

        wxGrid* grid = GetView();
        const int width = grid? grid->GetDefaultColSize():WXGRID_DEFAULT_COL_WIDTH;
        const int height = grid? grid->GetDefaultRowSize():WXGRID_DEFAULT_ROW_HEIGHT;

        // update normal (array member) rows
        for (std::size_t ctr = 0; ctr < m_impl->getNumCols(); ++ctr)
        {
            // first look for the variable in saved variables
            TypeInfo type = m_impl->getType(ctr);
            std::any& data = m_impl->getColData(ctr);

            ColumnInfo* col = std::any_cast<ColumnInfo>(&data);

            if (!col)
            {
                col = &data.emplace<ColumnInfo>();
                col->data = m_impl->getArray(ctr);
                col->label = wxString(&(*m_impl->getName(ctr))[0]);
                col->width = width;
            }

            col->index = ctr;

            if (type.shouldDisplay())
            {
                m_cols.push_back(col);
                m_impl->getNode(ctr)->addDepOnMe(m_guiObs, true);
            }
        }

        // update merged attribute for scalars
        if (!m_cols.empty())
        {
            m_scalarAttr->SetSize(1, m_cols.size());
        }
        else
        {
            m_scalarAttr->SetSize(1, 1);
        }


        // update reductions
        for (std::size_t ctr = 0; ctr < m_impl->getNumReduces(); ++ctr)
        {
            std::any& data = m_impl->getReduceData(ctr);
            ReduceInfo* info = std::any_cast<ReduceInfo>(&data);

            if (!info)
            {
                const Table::ReduceData& sd = m_impl->getReduce(ctr);
                info = &data.emplace<ReduceInfo>();
                info->label = *sd.name;
                info->height = height;
            }

            for (const ColumnInfo* col: m_cols)
            {
                if (auto reduce = m_impl->getReduce(col->index, ctr))
                {
                    reduce->addDepOnMe(m_guiObs, false);
                }
            }

            m_reduces.push_back(info);
        }

        // update scalars in the table
        for (std::size_t ctr = 0; ctr < m_impl->getNumScalars(); ++ctr)
        {
            const Table::ScalarData& sd = m_impl->getScalar(ctr);
            const TypeInfo type = sd.node->getType();
            std::any& data = m_impl->getScalarData(ctr);
            ScalarInfo* info = std::any_cast<ScalarInfo>(&data);

            if (!info)
            {
                info = &data.emplace<ScalarInfo>();
                info->label = *sd.name;
                info->node = sd.node;
                info->height = height;
            }

            info->index = ctr;

            // remember to render it in the table
            if (type.shouldDisplay())
            {
                m_scalars.emplace_back(info);
                sd.node->addDepOnMe(m_guiObs, false);
            }
        }

        m_impl->setNumRows(m_numRows);

        // inform the view *before* we recalculate to ensure that wxGrid
        // will not request recently deleted columns
        if (GetView())
        {
            auto informChange = [this](int change, bool isCol)
            {
                if (change == 0)
                {
                    return;
                }

                wxGridTableMessage msg(this, -1);

                if (change < 0)
                {
                    msg.SetId(isCol? wxGRIDTABLE_NOTIFY_COLS_DELETED:
                                     wxGRIDTABLE_NOTIFY_ROWS_DELETED);
                    msg.SetCommandInt(0);
                    msg.SetCommandInt2(-change);
                }
                else if (change > 0)
                {
                    msg.SetId(isCol? wxGRIDTABLE_NOTIFY_COLS_APPENDED:
                                     wxGRIDTABLE_NOTIFY_ROWS_APPENDED);
                    msg.SetCommandInt(change);
                }

                GetView()->ProcessTableMessage(msg);
            };

            informChange(GetNumberCols() - oldCols, true);
            informChange(GetNumberRows() - oldRows, false);
        }

        recalc();
    }

    void SheetDataTable::recalc()
    {
        m_impl->forceRecompute();
        // note that we do not need to update the view; our GuiObserver does
        // this for us
    }

    Object::Ptr SheetDataTable::getObject(int row, int col) const
    {
        if (row < getColumnSize())
        {
            return m_impl->getObject(m_cols.at(col)->index, row);
        }
        else
        {
            std::size_t index = row - getColumnSize();

            if (index < m_impl->getNumReduces())
            {
                int cindex = m_cols.at(col)->index;
                auto reduce = m_impl->getReduce(cindex, row - m_impl->getNumRows());

                if (reduce)
                {
                    return reduce->getObject();
                }
                else
                {
                    return nullptr;
                }
            }
            else
            {
                index -= m_impl->getNumReduces();
                return m_scalars.at(index)->node->getObject();
            }
        }
    }

    void SheetDataTable::setObject(int row, int col, const Object::Ptr& obj)
    {
        if (row < getColumnSize())
        {
            m_impl->setObject(m_cols[col]->index, row, obj);
        }
        else
        {
            std::size_t index = row - getColumnSize();

            if (index < m_impl->getNumReduces())
            {
                auto reduce = m_impl->getReduce(col, row - m_impl->getNumRows());

                if (reduce)
                {
                    return reduce->setObject(obj);
                }
            }
            else
            {
                index -= m_impl->getNumReduces();
                return m_impl->setScalar(m_scalars.at(index)->index, obj);
            }
        }
    }

    bool SheetDataTable::isReadOnly(int row, int col) const
    {
        if (row < getColumnSize())
        {
            return m_impl->isReadOnly(row, m_cols[col]->index);
        }
        else
        {
            int index = row - getColumnSize() - m_reduces.size();

            if (index < 0)
            {
                // is a reduction; not modifiable
                return true;
            }
            else
            {
                // read only if there is a function
                return m_impl->getScalar(m_scalars.at(index)->index).readOnly;
            }
        }
    }

    wxString SheetDataTable::GetColLabelValue(int col)
    {
        if (m_cols.empty())
        {
            return _("Value");
        }
        else
        {
            return m_cols.at(col)->label;
        }
    }

    wxString SheetDataTable::GetRowLabelValue(int row)
    {
        if (row < getColumnSize())
        {
            wxString str;
            str.Printf("%d", row + 1);
            return str;
        }
        else
        {
            return getLabel(row);
        }
    }

    void SheetDataTable::SetColLabelValue(int col, const wxString& str)
    {
        if (m_cols.empty())
        {
            return;
        }

        m_cols.at(col)->label = str;

        for (const auto& obs: m_observers)
        {
            obs->columnRename(shared_from_this(), col);
        }
    }

    void SheetDataTable::SetRowLabelValue(int row, const wxString& lbl)
    {
        getLabel(row) = lbl;
        const int r = row - getColumnSize() - m_impl->getNumReduces();

        if (r >= 0)
        {
            // only notify observers if this is a scalar
            for (const auto& obs: m_observers)
            {
                obs->scalarRename(shared_from_this(), r);
            }
        }
    }

    wxString& SheetDataTable::getLabel(int row)
    {
        if (row >= getColumnSize())
        {
            std::size_t rindex = row - getColumnSize();

            if (rindex < m_impl->getNumReduces())
            {
                return m_reduces.at(rindex)->label;
            }
            else
            {
                rindex -= m_impl->getNumReduces();
                return m_scalars.at(rindex)->label;
            }
        }

        throw std::out_of_range("label index out of range");
    }

    wxGridCellAttr* SheetDataTable::GetAttr(int row, int col,
            wxGridCellAttr::wxAttrKind kind)
    {
        if (static_cast<std::size_t>(row) >= getColumnSize() + m_impl->getNumReduces())
        {
            // apparently we have to increment the refcount since it is decremented
            // afterwards (why???)
            if (col > 0)
            {
                wxGridCellAttr* attr = new wxGridCellAttr;
                attr->SetSize(0, -col);
                return attr;
            }
            else
            {
                m_scalarAttr->IncRef();
                return m_scalarAttr;
            }
        }
        else
        {
            return wxGridTableBase::GetAttr(row, col, kind);
        }
    }

    int SheetDataTable::setSize(wxEventType evt, int idx, int size)
    {
        if (m_cols.empty())
        {
            return WXGRID_DEFAULT_COL_WIDTH;
        }

        int ret = -1;

        if (evt == wxEVT_GRID_COL_SIZE)
        {
            ret = m_cols.at(idx)->width;
            m_cols.at(idx)->width = size;
        }
        else if (evt == wxEVT_GRID_ROW_SIZE)
        {
            int i = idx - getColumnSize();

            if (i >= 0 &&
                static_cast<std::size_t>(i) < m_impl->getNumReduces())
            {
                ret = m_reduces[i]->height;
                m_reduces[i]->height = size;
            }
            else if (i >= 0)
            {
                i -= m_impl->getNumReduces();
                ret = m_scalars[i]->height;
                m_scalars[i]->height = size;
            }
        }
        else
        {
            wxFAIL_MSG(wxS("SheetDataTable::setSize with unknown event"));
        }

        return ret;
    }

    void SheetDataTable::SetView(wxGrid* grid)
    {
        wxGridTableBase::SetView(grid);

        if (!grid)
        {
            return;
        }

        for (std::size_t i = 0; i < m_cols.size(); ++i)
        {
            grid->SetColSize(i, m_cols[i]->width);
        }

        int i = getColumnSize();

        for (const ReduceInfo* info: m_reduces)
        {
            grid->SetRowSize(i++, info->height);
        }

        for (const ScalarInfo* info: m_scalars)
        {
            grid->SetRowSize(i++, info->height);
        }
    }

    void SheetDataTable::saveTable(const wxString& file, int index)
    {
        if (static_cast<FileTypes>(index) != FileTypes::CSVFile)
        {
            throw std::logic_error("unknown file type");
        }

        std::ofstream ostr;
        ostr.exceptions(std::ofstream::badbit | std::ofstream::failbit);

        try
        {
            ostr.open(file, std::ios_base::out | std::ios_base::binary);

            auto pushValue = [&ostr](const std::string& val)
            {
                ostr << '"';

                for (char c: val)
                {
                    if (c == '"')
                    {
                        ostr << '"';
                    }

                    ostr << c;
                }

                ostr << '"';
            };

            for (int i = 0; i < GetNumberCols(); ++i)
            {
                ostr << ',';
                pushValue(static_cast<std::string>(GetColLabelValue(i)));
            }

            ostr << "\r\n";

            for (int row = 0; row < GetNumberRows(); ++row)
            {
                pushValue(static_cast<std::string>(GetRowLabelValue(row)));

                for (int col = 0; col < GetNumberCols(); ++col)
                {
                    ostr << ',';
                    pushValue(static_cast<std::string>(GetValue(row, col)));
                }

                ostr << "\r\n";
            }
        }
        catch (std::ios_base::failure& ex)
        {
            wxLogError(_("Could not write to %s:\n%s"), file, ex.what());
            ostr.close();
            wxRemove(file);
        }
    }

    using nlohmann::json;

    void to_json(json& j, const TypeInfo& type)
    {
        j = type.getName();
    }

    void from_json(const json& j, TypeInfo& type)
    {
        const std::string str = j.get<std::string>();
        type = TypeRegistry::parseType(str);

        if (type == TypeInfo::nullType())
        {
            throw std::runtime_error("could not parse type " + str);
        }
    }

    namespace
    {
        json serializeObject(const Object::Ptr& obj)
        {
            if (obj && typeid(*obj) != typeid(ErrorObject))
            {
                return json(obj->serialize());
            }
            else
            {
                return json(nullptr);
            }
        }

        json serializeColumns(const SheetDataTable& tbl)
        {
            if (tbl.getColumnSize() == 0)
            {
                // there really no columns to serialize; GetColsCount returns 1,
                // but there are only scalars in the table
                return json {};
            }

            const Table& t = tbl.getImpl();
            json cols;

            for (int colCtr = 0; colCtr < tbl.GetColsCount(); ++colCtr)
            {
                json vals;

                for (std::size_t rowCtr = 0; rowCtr < t.getNumRows(); ++rowCtr)
                {
                    Object::Ptr obj = tbl.getObject(rowCtr, colCtr);
                    vals.push_back(serializeObject(obj));
                }

                json c {
                    {"dispName", tbl.GetColLabelValue(colCtr)},
                    {"idName", *t.getName(colCtr)},
                    {"type", t.getType(colCtr)},
                    {"values", vals},
                    {"width", tbl.getColWidth(colCtr)},
                };

                cols.push_back(c);
            }

            return cols;
        }

        void deserializeColumns(const json& j, SheetDataTable& tbl)
        {
            if (j.size() != static_cast<std::size_t>(tbl.GetNumberCols()) &&
                tbl.getColumnSize() != 0)
            {
                throw std::runtime_error("wrong number of columns in file");
            }

            int i = 0;

            // here we *assume* that the columns are presented in the same order
            // in the data as in the table code
            for (const auto& col: j)
            {
                wxString dispName;
                int width;
                col.at("dispName").get_to(dispName);
                col.at("width").get_to(width);

                tbl.SetColLabelValue(i, dispName);
                tbl.setColWidth(i, width);

                int rowNum = 0;

                for (const auto& row: col.at("values"))
                {
                    if (row.is_null())
                    {
                        ++rowNum;
                        continue;
                    }

                    tbl.SetValue(rowNum++, i, row.get<wxString>());
                }

                ++i;
            }
        }

        json serializeReduces(const SheetDataTable& tbl)
        {
            const Table& t = tbl.getImpl();
            const auto maxRows = t.getNumRows();
            json reduces;

            for (std::size_t i = 0; i < t.getNumReduces(); ++i)
            {
                const auto rowIndex = maxRows + i;

                json r {
                    {"dispName", tbl.GetRowLabelValue(rowIndex)},
                    {"idName", *t.getReduceName(i)},
                    {"height", tbl.getReduceHeight(i)}
                };

                reduces.push_back(r);
            }

            return reduces;
        }

        void deserializeReduces(const json& j, SheetDataTable& tbl)
        {
            int i = tbl.getImpl().getNumRows();

            for (std::size_t index = 0; index < j.size(); ++index)
            {
                wxString dispName;
                int height;
                const json& r = j.at(index);
                r.at("dispName").get_to(dispName);
                r.at("height").get_to(height);
                tbl.SetRowLabelValue(i++, dispName);
                tbl.setReduceHeight(index, height);
            }
        }

        json serializeScalars(const SheetDataTable& tbl)
        {
            const Table& t = tbl.getImpl();
            const int scalarOffset = tbl.getColumnSize() + t.getNumReduces();
            json scalars;

            for (int i = 0; i < tbl.getNumScalars(); ++i)
            {
                const auto rowIndex = scalarOffset + i;
                Object::Ptr obj = tbl.getObject(rowIndex, 0);

                json s {
                    {"dispName", tbl.GetRowLabelValue(rowIndex)},
                    {"typeName", tbl.GetTypeName(rowIndex, 0)},
                    {"value", serializeObject(obj)},
                    {"height", tbl.getScalarHeight(i)},
                };

                scalars.push_back(s);
            }

            return scalars;
        }

        void deserializeScalars(const json& j, SheetDataTable& tbl)
        {
            const Table& t = tbl.getImpl();
            const auto maxRows = tbl.getColumnSize();
            int scalarNum = maxRows + t.getNumReduces();
            int index = 0;

            for (const auto& s: j)
            {
                wxString dispName;
                TypeInfo type;
                int height;

                s.at("dispName").get_to(dispName);
                s.at("typeName").get_to(type);
                s.at("height").get_to(height);

                json val = s.at("value");

                if (!val.is_null())
                {
                    std::string str;
                    val.get_to(str);

                    try
                    {
                        Object::Ptr obj = type.construct(str);
                        tbl.setObject(scalarNum, 0, obj);
                    }
                    catch (std::exception& ex)
                    {
                        wxLogError(_("Failed to parse %s as type %s:\n%s"),
                                str, type.getName(), ex.what());
                    }
                }

                tbl.setScalarHeight(index++, height);
                tbl.SetRowLabelValue(scalarNum, dispName);
                ++scalarNum;
            }
        }
    }

    void to_json(json& j, const SheetDataTable& tbl)
    {
        const Table& t = tbl.getImpl();

        j = json {
            {"rowCount", t.getNumRows()},
            {"xPos", tbl.getPosition().x},
            {"yPos", tbl.getPosition().y},
            {"columns", serializeColumns(tbl)},
            {"reduces", serializeReduces(tbl)},
            {"scalars", serializeScalars(tbl)},
        };
    }

    void from_json(const json& j, SheetDataTable& tbl)
    {
        int rowCount;
        wxPoint pos;
        wxString str;

        j.at("rowCount").get_to(rowCount);
        j.at("xPos").get_to(pos.x);
        j.at("yPos").get_to(pos.y);

        tbl.setPosition(pos);
        tbl.AppendRows(rowCount);

        deserializeColumns(j.at("columns"), tbl);
        deserializeReduces(j.at("reduces"), tbl);
        deserializeScalars(j.at("scalars"), tbl);
    }
}

void to_json(nlohmann::json& j, const wxString& str)
{
    using nlohmann::to_json;
    j = str.ToStdString();
}

void from_json(const nlohmann::json& j, wxString& str)
{
    str = j.get<std::string>();
}
