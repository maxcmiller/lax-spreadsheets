#include "table_code.hpp"

LAX_DISABLE_WARNINGS()
#include <wx/artprov.h>
#include <wx/bmpbuttn.h>
#include <wx/log.h>
#include <wx/sizer.h>
#include <wx/stc/stc.h>
#include <wx/settings.h>
LAX_REENABLE_WARNINGS()

#include "charts/chart_setup.hpp"
#include "main_window.hpp"
#include "table.hpp"
#include "util/scope_exit.hpp"

namespace lax
{
    namespace
    {
        class CompileValidator:
            public wxValidator
        {
        public:
            explicit CompileValidator(std::string& tbl):
                m_tbl(tbl) {}

            CompileValidator(const CompileValidator&) = default;

            bool TransferFromWindow() override;
            bool TransferToWindow() override;

            CompileValidator* Clone() const override
            {
                return new CompileValidator(*this);
            }
        private:
            std::string& m_tbl;
        };

        bool CompileValidator::TransferFromWindow()
        {
            const wxStyledTextCtrl* txt = dynamic_cast<wxStyledTextCtrl*>(GetWindow());
            wxCHECK_MSG(txt, false, wxS("StdStringVal needs a wxTextEntry window"));
            m_tbl = txt->GetValue().ToStdString();
            return true;
        }

        bool CompileValidator::TransferToWindow()
        {
            wxStyledTextCtrl* txt = dynamic_cast<wxStyledTextCtrl*>(GetWindow());
            wxCHECK_MSG(txt, false, wxS("StdStringVal needs a wxTextEntry window"));
            txt->SetValue(m_tbl);
            return true;
        }
    }

    class TableCodeCtrl::CodeSet::CodeTextCtrl:
        public wxStyledTextCtrl
    {
        auto getChangeFunc(bool isRedo)
        {
            return [this, isRedo](const TableCodeHistory::CmdData& cmd)
            {
                bool wasAtSave, toSave;

                if (isRedo)
                {
                    wasAtSave = (m_history->getPos() - 1 == m_savePt);
                }
                else
                {
                    wasAtSave = (m_history->getPos() + 1 == m_savePt);
                }

                toSave = (m_history->getPos() == m_savePt);
                execCommand(cmd, cmd.isInsert == isRedo, wasAtSave, toSave);
            };
        }
    public:
        explicit CodeTextCtrl(wxWindow* parent, TableCodeHistory* tch):
            wxStyledTextCtrl(parent), m_history(tch)
        {
            SetUndoCollection(false);
            SetUseTabs(false);
        }

        void DoUndo();
        void DoRedo();

        void setText(const wxString& text)
        {
            m_inUndo = true;
            SetText(text);
            m_inUndo = false;
        }

        void prevCompPoint()
        {
            m_history->prevCompilePoint(getChangeFunc(false));
        }

        void nextCompPoint()
        {
            m_history->nextCompilePoint(getChangeFunc(true));
        }

        void setCompPoint()
        {
            m_history->setCompilePoint();
        }

        void setSavePoint() noexcept
        {
            m_savePt = m_history->getPos();
            savePtEvent(wxEVT_STC_SAVEPOINTREACHED);
        }

        void OnFindEvent(wxFindDialogEvent& evt);
    private:
        void OnModified(wxStyledTextEvent& evt);
        void OnKeyPress(wxKeyEvent&);
        void execCommand(const TableCodeHistory::CmdData& cmd, bool isInsert,
                bool wasAtSave, bool toSave);
        void savePtEvent(wxEventType type);

        TableCodeHistory* m_history;
        std::size_t m_savePt = 0;
        bool m_inUndo = false;

        wxDECLARE_EVENT_TABLE();
    };

LAX_DISABLE_WARNINGS()
    wxBEGIN_EVENT_TABLE(TableCodeCtrl::CodeSet::CodeTextCtrl, wxStyledTextCtrl)
        EVT_STC_MODIFIED(wxID_ANY, TableCodeCtrl::CodeSet::CodeTextCtrl::OnModified)
        EVT_KEY_DOWN(TableCodeCtrl::CodeSet::CodeTextCtrl::OnKeyPress)
        EVT_FIND(wxID_ANY, TableCodeCtrl::CodeSet::CodeTextCtrl::OnFindEvent)
        EVT_FIND_CLOSE(wxID_ANY, TableCodeCtrl::CodeSet::CodeTextCtrl::OnFindEvent)
        EVT_FIND_NEXT(wxID_ANY, TableCodeCtrl::CodeSet::CodeTextCtrl::OnFindEvent)
        EVT_FIND_REPLACE(wxID_ANY, TableCodeCtrl::CodeSet::CodeTextCtrl::OnFindEvent)
        EVT_FIND_REPLACE_ALL(wxID_ANY, TableCodeCtrl::CodeSet::CodeTextCtrl::OnFindEvent)
    wxEND_EVENT_TABLE()
LAX_REENABLE_WARNINGS()

    void TableCodeCtrl::CodeSet::CodeTextCtrl::savePtEvent(wxEventType type)
    {
        wxStyledTextEvent evt;
        evt.SetEventType(type);
        evt.SetEventObject(this);
        wxPostEvent(this, evt);
    }

    void TableCodeCtrl::CodeSet::CodeTextCtrl::OnModified(wxStyledTextEvent& evt)
    {
        const int modType = evt.GetModificationType();

        if (!m_inUndo && (modType & wxSTC_PERFORMED_USER) &&
            (modType & (wxSTC_MOD_INSERTTEXT | wxSTC_MOD_BEFOREDELETE)))
        {
            const bool wasAtSave = m_history->getPos() == m_savePt;

            const int pos = evt.GetPosition();
            const int end = pos + evt.GetLength();
            const bool isInsert =
            (modType & wxSTC_MOD_INSERTTEXT) == wxSTC_MOD_INSERTTEXT;
            const wxString str = GetTextRange(pos, end);

            m_history->addCommand(str, pos, isInsert);

            if (wasAtSave)
            {
                savePtEvent(wxEVT_STC_SAVEPOINTLEFT);
            }
        }

        evt.Skip();
    }

    void TableCodeCtrl::CodeSet::CodeTextCtrl::OnKeyPress(wxKeyEvent& evt)
    {
        const int mod = evt.GetModifiers();
        const auto key = evt.GetUnicodeKey();

        try
        {
            switch (key)
            {
            case 'Y':
                if (mod == wxMOD_CONTROL)
                {
                    DoRedo();
                    return;
                }

                break;
            case 'Z':
                if (mod == wxMOD_CONTROL)
                {
                    DoUndo();
                    return;
                }
                else if (mod == (wxMOD_CONTROL | wxMOD_SHIFT))
                {
                    DoRedo();
                    return;
                }

                break;
            default:
                break;
            }
        }
        catch (std::logic_error&) {}

        evt.Skip();
    }

    void TableCodeCtrl::CodeSet::CodeTextCtrl::execCommand(
            const TableCodeHistory::CmdData& cmd, bool isInsert, bool wasAtSave,
            bool toSave)
    {
        m_inUndo = true;

        if (!isInsert)
        {
            DeleteRange(cmd.pos, cmd.text.size());
            SetCurrentPos(cmd.pos);
        }
        else
        {
            InsertText(cmd.pos, cmd.text);
            SetCurrentPos(cmd.pos + cmd.text.size());
        }

        ClearSelections();

        if (wasAtSave)
        {
            savePtEvent(wxEVT_STC_SAVEPOINTLEFT);
        }
        else if (toSave)
        {
            savePtEvent(wxEVT_STC_SAVEPOINTREACHED);
        }

        m_inUndo = false;
    }

    void TableCodeCtrl::CodeSet::CodeTextCtrl::DoUndo()
    {
        const bool wasAtSave = m_history->getPos() == m_savePt;
        const auto& cmd = m_history->undo();
        execCommand(cmd, !cmd.isInsert, wasAtSave,
                m_history->getPos() == m_savePt);
    }

    void TableCodeCtrl::CodeSet::CodeTextCtrl::DoRedo()
    {
        const bool toSave = (m_history->getPos() + 1 == m_savePt);
        const auto& cmd = m_history->redo();
        execCommand(cmd, cmd.isInsert, m_history->getPos() == m_savePt,
                toSave);
    }

    void TableCodeCtrl::CodeSet::CodeTextCtrl::OnFindEvent(wxFindDialogEvent& evt)
    {
        const int type = evt.GetEventType();

        if (type == wxEVT_FIND_CLOSE)
        {
            evt.GetDialog()->Destroy();
            return;
        }

        int flags = 0;

        if (evt.GetFlags() & wxFR_MATCHCASE)
        {
            flags |= wxSTC_FIND_MATCHCASE;
        }

        if (evt.GetFlags() & wxFR_WHOLEWORD)
        {
            flags |= wxSTC_FIND_WHOLEWORD;
        }

        const bool down = (evt.GetFlags() & wxFR_DOWN);
        long from, to;
        GetSelection(&from, &to);

        if (from != to)
        {
            SetCurrentPos(down? to:from);
        }

        SearchAnchor();
        auto searchFunc = (evt.GetFlags() & wxFR_DOWN)?
                          &wxStyledTextCtrl::SearchNext:
                          &wxStyledTextCtrl::SearchPrev;

        if (type == wxEVT_FIND || type == wxEVT_FIND_NEXT)
        {
            (this->*searchFunc)(flags, evt.GetFindString());
        }
        else if (type == wxEVT_FIND_REPLACE)
        {
            if (from != to)
            {
                Replace(from, to, evt.GetReplaceString());
            }
        }
        else if (type == wxEVT_FIND_REPLACE_ALL)
        {
            SetCurrentPos(down? 0:GetLastPosition());
            SearchAnchor();

            const wxString find = evt.GetFindString();
            const wxString replace = evt.GetReplaceString();

            while (true)
            {
                (this->*searchFunc)(flags, find);
                GetSelection(&from, &to);

                if (from != to)
                {
                    Replace(from, to, replace);
                }
                else
                {
                    break;
                }
            }
        }
    }

    TableCodeCtrl::CodeSet::CodeSet(TableCodeCtrl* parent, PageData& pg,
            std::size_t idx):
        m_parent(parent), m_index(idx),
        m_nb(new wxAuiNotebook(parent->m_tbls, wxID_ANY, wxDefaultPosition,
                wxDefaultSize, wxAUI_NB_TOP))
    {
        m_parent->m_tbls->ShowNewPage(m_nb);
        wxWindow* win;

        m_nb->AddPage(win = new wxWindow(m_nb, wxID_ANY),
                _("Table code"), true);
        m_nb->AddPage(m_charts = new wxChoicebook(m_nb, wxID_ANY),
                _("Chart setup"), false);

        wxBitmap recomp =
        wxArtProvider::GetBitmap(wxART_EXECUTABLE_FILE, wxART_BUTTON);

        m_inAddTbl = true;
        ScopeExit guard([this] {m_inAddTbl = false;});

        wxSizer* bookSizer = new wxBoxSizer(wxVERTICAL);
        bookSizer->Add(new wxBitmapButton(win, MainWindow::kRecompileTbl, recomp),
                wxSizerFlags(0).Align(wxALIGN_RIGHT));
        m_codeCtrl = new CodeTextCtrl(win, &pg.history);
        bookSizer->Add(m_codeCtrl, wxSizerFlags(1).Expand());

        win->SetSizer(bookSizer);
        win->Layout();

        wxFont font = *wxTheFontList->FindOrCreateFont(10, wxFONTFAMILY_TELETYPE,
                wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false);

        // TODO (Max#1#): Do syntax-highlighting for table code
        int numMargin = m_codeCtrl->TextWidth(wxSTC_STYLE_LINENUMBER, "9999");
        m_codeCtrl->SetMarginWidth(0, numMargin);
        m_codeCtrl->SetValidator(CompileValidator(pg.code));
        m_codeCtrl->StyleSetFont(wxSTC_STYLE_DEFAULT, font);
        m_codeCtrl->SetFont(font);
        m_codeCtrl->StyleClearAll();
        m_codeCtrl->SetTabWidth(4);
        m_codeCtrl->setText(pg.code);
        m_codeCtrl->setCompPoint();
        m_codeCtrl->SetClientData(&pg);
    }

    bool TableCodeCtrl::CodeSet::addChart(const wxString& name,
            const WeakRef<ChartBase>& data)
    {
        bool success = m_chartIndices.emplace(name, m_charts->GetPageCount()).second;

        if (!success)
        {
            return false;
        }

        m_charts->AddPage(data->createSetup(m_charts), name, true);
        return true;
    }

    void TableCodeCtrl::CodeSet::clearCharts()
    {
        m_chartIndices.clear();
        m_charts->DeleteAllPages();
    }

    bool TableCodeCtrl::CodeSet::removeChart(const wxString& name)
    {
        auto itr = m_chartIndices.find(name);

        if (itr == m_chartIndices.end())
        {
            return false;
        }

        const int index = itr->second;
        m_charts->DeletePage(itr->second);
        m_chartIndices.erase(itr);

        for (std::size_t ctr = index; ctr < m_charts->GetPageCount(); ++ctr)
        {
            // go thru items after the one removed and ensure that their index
            // is correct (by decrementing)
            --m_chartIndices[m_charts->GetPageText(ctr)];
        }

        return true;
    }

    bool TableCodeCtrl::CodeSet::renameChart(const wxString& old,
            const wxString& newName)
    {
        auto itr = m_chartIndices.find(old);

        if (itr == m_chartIndices.end() ||
            !m_chartIndices.emplace(newName, itr->second).second)
        {
            return false;
        }

        m_charts->SetPageText(itr->second, newName);
        m_chartIndices.erase(itr);
        return true;
    }

    void TableCodeCtrl::CodeSet::changeCompile(bool forward)
    {
        if (forward)
        {
            m_codeCtrl->nextCompPoint();
        }
        else
        {
            m_codeCtrl->prevCompPoint();
        }

        m_codeCtrl->GetValidator()->TransferFromWindow();
    }

    void TableCodeCtrl::CodeSet::switchToMe()
    {
        m_parent->m_tbls->ChangeSelection(m_index);
    }

    bool TableCodeCtrl::CodeSet::showChart(const wxString& name)
    {
        m_nb->ChangeSelection(1); // move to chart page
        auto itr = m_chartIndices.find(name);

        if (itr == m_chartIndices.end())
        {
            return false;
        }

        m_charts->ChangeSelection(itr->second);
        return true;
    }

    void TableCodeCtrl::CodeSet::setModified(ModifyType mod)
    {
        if (m_inAddTbl)
        {
            return;
        }

        static wxString modStr = _(" (modified)");

        switch (mod)
        {
        case ModifyType::kCompiled:
            m_codeCtrl->setSavePoint();
            m_codeCtrl->setCompPoint();
            break;
        case ModifyType::kAtSavePt:
        case ModifyType::kModified:
            break;
        }
    }

    void* TableCodeCtrl::CodeSet::TransferDataFromWindow()
    {
        m_codeCtrl->GetValidator()->TransferFromWindow();
        return m_codeCtrl->GetClientData();
    }

    wxStyledTextCtrl* TableCodeCtrl::CodeSet::getTextCtrl() const noexcept
    {
        return m_codeCtrl;
    }

LAX_DISABLE_WARNINGS()
    wxBEGIN_EVENT_TABLE(TableCodeCtrl, wxPanel)
        EVT_BUTTON(MainWindow::kRecompileTbl, TableCodeCtrl::OnRecompile)
        EVT_STC_SAVEPOINTLEFT(wxID_ANY, TableCodeCtrl::OnTextEvent)
        EVT_STC_SAVEPOINTREACHED(wxID_ANY, TableCodeCtrl::OnTextEvent)
        EVT_MENU(wxID_FIND, TableCodeCtrl::OnFind)
        EVT_MENU(wxID_REPLACE, TableCodeCtrl::OnFind)
    wxEND_EVENT_TABLE()
LAX_REENABLE_WARNINGS()

    TableCodeCtrl::TableCodeCtrl(wxWindow* parent, wxWindowID id,
            const wxPoint& pos, const wxSize& size):
        wxPanel(parent, id, pos, size), m_tbls(nullptr), m_sets()
    {
        std::unique_ptr<wxBoxSizer> sizer(new wxBoxSizer(wxVERTICAL));
        sizer->Add(m_tbls = new wxSimplebook(this), wxSizerFlags(1).Expand());
        SetSizer(sizer.release());
    }

    void TableCodeCtrl::OnRecompile(wxCommandEvent& evt)
    {
        int sel = m_tbls->GetSelection();
        CodeSet::Ptr set = m_sets.at(sel);
        evt.SetClientData(set->TransferDataFromWindow());
        evt.Skip();
    }

    void TableCodeCtrl::OnTextEvent(wxStyledTextEvent& evt)
    {
        int sel = m_tbls->GetSelection();
        CodeSet::Ptr set = m_sets.at(sel);

        auto mod = evt.GetEventType() == wxEVT_STC_SAVEPOINTLEFT?
        CodeSet::ModifyType::kModified:CodeSet::ModifyType::kAtSavePt;

        set->setModified(mod);
        evt.Skip();
    }

    void TableCodeCtrl::OnFind(wxCommandEvent& evt)
    {
        int sel = m_tbls->GetSelection();
        CodeSet::Ptr set = m_sets.at(sel);

        int flags = 0;

        if (evt.GetId() == wxID_REPLACE)
        {
            flags |= wxFR_REPLACEDIALOG;
        }

        wxFindReplaceDialog* dlg = new wxFindReplaceDialog(set->m_codeCtrl,
                &m_frData, _("Find and replace"), flags);

        auto findEvt = &TableCodeCtrl::CodeSet::CodeTextCtrl::OnFindEvent;
        dlg->Bind(wxEVT_FIND, findEvt, set->m_codeCtrl);
        dlg->Bind(wxEVT_FIND_CLOSE, findEvt, set->m_codeCtrl);
        dlg->Bind(wxEVT_FIND_NEXT, findEvt, set->m_codeCtrl);
        dlg->Bind(wxEVT_FIND_REPLACE, findEvt, set->m_codeCtrl);
        dlg->Bind(wxEVT_FIND_REPLACE_ALL, findEvt, set->m_codeCtrl);
        dlg->Show();
    }
}
