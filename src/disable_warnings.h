#ifndef LAX_DISABLE_WARNINGS_H_
#define LAX_DISABLE_WARNINGS_H_

#ifdef __GNUG__
# define LAX_DISABLE_WARNINGS() \
_Pragma("GCC diagnostic push") \
_Pragma("GCC diagnostic ignored \"-Wdeprecated-copy\"") \
_Pragma("GCC diagnostic ignored \"-Wignored-qualifiers\"") \
_Pragma("GCC diagnostic ignored \"-Wcast-function-type\"")

# define LAX_REENABLE_WARNINGS() _Pragma("GCC diagnostic pop")
#else
# define LAX_DISABLE_WARNINGS()
# define LAX_REENABLE_WARNINGS()
#endif // __GNUG__

#endif // LAX_DISABLE_WARNINGS_H_
