#ifndef LAX_SRC_TABLE_CODE_HPP_
#define LAX_SRC_TABLE_CODE_HPP_

#include "disable_warnings.h"

#include <memory>
#include <unordered_map>
#include <vector>

LAX_DISABLE_WARNINGS()
#include <wx/aui/auibook.h>
#include <wx/choicebk.h>
#include <wx/fdrepdlg.h>
#include <wx/log.h>
#include <wx/panel.h>
#include <wx/simplebook.h>
LAX_REENABLE_WARNINGS()

#include "sheet_doc.hpp"

class wxStyledTextCtrl;
class wxStyledTextEvent;

namespace lax
{
    class ChartBase;
    class SheetDataTable;

    class TableCodeCtrl:
        public wxPanel
    {
    public:
        class CodeSet
        {
        public:
            class CodeTextCtrl;
            using Ptr = std::shared_ptr<CodeSet>;

            enum class ModifyType
            {
                kCompiled,
                kAtSavePt,
                kModified,
            };

            CodeSet(const CodeSet&) = delete;
            CodeSet& operator=(const CodeSet&) = delete;

            void* TransferDataFromWindow();

            bool removeChart(const wxString& name);
            bool renameChart(const wxString& old, const wxString& newName);
            void switchToMe();
            bool showChart(const wxString& name);
            void setModified(ModifyType mod);
            wxStyledTextCtrl* getTextCtrl() const noexcept;

            void nextCompile()
            {
                changeCompile(true);
            }

            void prevCompile()
            {
                changeCompile(false);
            }

            bool addChart(const wxString& name, const WeakRef<ChartBase>& data);
            void clearCharts();
        private:
            CodeSet(TableCodeCtrl* parent, PageData& pg, std::size_t idx);
            void changeCompile(bool forward);

            TableCodeCtrl* m_parent;
            std::size_t m_index;
            wxAuiNotebook* m_nb;
            CodeTextCtrl* m_codeCtrl;
            wxChoicebook* m_charts;
            std::unordered_map<wxString, std::size_t, wxStringHash> m_chartIndices;
            bool m_inAddTbl = false; // kludge to avoid an assert failure

            friend class TableCodeCtrl;
        };

        TableCodeCtrl(wxWindow* parent, wxWindowID id,
                const wxPoint& pos = wxDefaultPosition,
                const wxSize& size = wxDefaultSize);

        CodeSet::Ptr addCodeSet(PageData& pg)
        {
            m_sets.push_back(CodeSet::Ptr(new CodeSet(this, pg, m_sets.size())));
            return m_sets.back(); // ctor adds to back
        }

        void clearAll()
        {
            m_tbls->DeleteAllPages();
            m_sets.clear();
        }

        void switchTo(int pgNum)
        {
            m_tbls->ChangeSelection(pgNum);
        }

        void removeCodeSet(int n)
        {
            m_tbls->RemovePage(n);
            m_sets.erase(m_sets.begin() + n);
        }
    private:
        void OnRecompile(wxCommandEvent&);
        void OnTextEvent(wxStyledTextEvent&);
        void OnFind(wxCommandEvent&);

        wxSimplebook* m_tbls;
        std::vector<CodeSet::Ptr> m_sets;
        wxFindReplaceData m_frData;

        wxDECLARE_EVENT_TABLE();
    };
}

#endif // LAX_SRC_TABLE_CODE_HPP_
