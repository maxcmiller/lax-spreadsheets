#include "exponent_regression.hpp"

#include <cmath>
#include <iostream>

namespace lax
{
namespace math
{
    double ExponentRegression::doCalculate(double in) const
    {
        return m_a * std::exp(m_b * in);
    }

    bool ExponentRegression::doReset(const std::vector<PairedData>& vec)
    {
        constexpr double c = 0.5;
        constexpr double tau = 0.5;
        constexpr double minAlpha = std::numeric_limits<double>::epsilon();

        if (vec.size() < 2)
        {
            // we can't really get a good answer, and makeGuess assumes that
            // there is at least one element, so just fail now
            return false;
        }

        makeGuess(vec);
        std::pair grad = calcGradient(vec);

        for (int i = 0; i < maxIter; ++i)
        {
            if (std::isnan(grad.first) ||
                std::isnan(grad.second))
            {
                return false;
            }

            if ((std::abs(grad.first) < m_prec &&
                 std::abs(grad.second) < m_prec))
            {
                return true;
            }

            // do a backtracking line search
            // see https://en.wikipedia.org/wiki/Backtracking_line_search

            const double m = grad.first * grad.first + grad.second * grad.second;
            const double t = c * m;
            const double error = calcError(m_a, m_b, vec);
            double alpha = 0.001;

            while (true)
            {
                const double new_a = m_a - alpha * grad.first;
                const double new_b = m_b - alpha * grad.second;

                if (std::isnan(new_a) || std::isnan(new_b))
                {
                    return false;
                }

                if (error - calcError(new_a, new_b, vec) >= alpha * t)
                {
                    // Armijo condition satisfied; done
                    m_a = new_a;
                    m_b = new_b;
                    grad = calcGradient(vec);
                    break;
                }
                else if (alpha < minAlpha)
                {
                    // at this point, alpha is really small, so we are probably
                    // very close to the minimum; just return so we don't loop
                    // indefinitely
                    return true;
                }
                else
                {
                    alpha *= tau;
                }
            }
        }

        return !std::isnan(grad.first) && !std::isnan(grad.second);
    }

    void ExponentRegression::makeGuess(const std::vector<PairedData>& vec)
    {
        m_b = 0.0;
        PairedData prev = vec.front();
        int count = 0;

        // Since we are calculating the logarithm of a geometric mean, we
        // can simply take the arithmetic mean of the logarithms.
        for (auto itr = vec.begin() + 1; itr != vec.end(); ++itr)
        {
            double ratio = itr->second / prev.second;
            double diff = itr->first - prev.first;

            if (diff == 0)
            {
                continue;
            }

            // add ln(pow(ratio, 1/diff)) but without the pow
            m_b += std::log(ratio) / diff;
            prev = *itr;
            ++count;
        }

        m_b /= count;

        // find a-value
        m_a = 1.0;
        int asign = 0; // for tracking the sign

        for (const auto& val: vec)
        {
            double got = std::exp(m_b * val.first);
            double ratio = val.second / got;
            int sign = ratio < 0.0? -1:1;

            // we have to ensure that pow receives a non-negative value, so just
            // track the sign separately
            if (asign == 0)
            {
                asign = sign;
            }
            else if (asign != sign)
            {
                // non-matching signs; cannot shift up and down, so just return
                m_a = std::numeric_limits<double>::quiet_NaN();
                return;
            }

            m_a *= std::pow(std::abs(ratio), 1.0 / vec.size());
        }

        m_a *= asign;
    }

    std::pair<double, double>
    ExponentRegression::calcGradient(const std::vector<PairedData>& vec)
    {
        PairedData grad(0.0, 0.0);

        for (auto [x, y]: vec)
        {
            double diff = calculate(x) - y;
            double ebxi = std::exp(m_b * x);
            grad.first += ebxi * diff;
            grad.second += m_a * x * ebxi * diff;
        }

        return grad;
    }

    double ExponentRegression::calcError(double a, double b,
            const std::vector<PairedData>& vec)
    {
        double total = 0.0;

        for (auto [x, y]: vec)
        {
            const double add = a * std::exp(b * x) - y;
            total += add * add;
        }

        return total;
    }

    void ExponentRegression::print(std::ostream& ostr) const
    {
        ostr << m_a << "*e^" << m_b << 'x';
    }
}
}
