#include "matrix.hpp"

#include <algorithm>
#include <iostream>

namespace lax
{
namespace math
{
    Matrix::Matrix(int nRows, int nCols):
        m_matrix(new double[nRows * nCols]), m_rows(new double*[nRows]),
        m_nRows(nRows), m_nCols(nCols)
    {
        for (int i = 0; i < m_nRows; ++i)
        {
            m_rows[i] = m_matrix + i * m_nCols;
        }
    }

    Matrix::Matrix(const Matrix& rhs):
        Matrix(rhs.getRows(), rhs.getCols())
    {
        std::copy(rhs.m_matrix, rhs.m_matrix + m_nCols * m_nRows, m_matrix);
        std::transform(rhs.m_rows, rhs.m_rows + m_nRows, m_rows,
                [this, &rhs](double* row)
        {
            return m_matrix + (row - rhs.m_matrix);
        });
    }

    Matrix::Matrix(Matrix&& rhs) noexcept:
        m_matrix(rhs.m_matrix), m_rows(rhs.m_rows), m_nRows(rhs.m_nRows),
        m_nCols(rhs.m_nCols)
    {
        rhs.m_matrix = nullptr;
        rhs.m_rows = nullptr;
        rhs.m_nRows = 0;
        rhs.m_nCols = 0;
    }

    double& Matrix::get(int row, int col)
    {
        checkIndex(row, col);
        return m_rows[row][col];
    }

    void Matrix::checkIndex(int row, int col) const
    {
        checkRowIndex(row);

        if (col >= m_nCols || col < 0)
        {
            throw std::out_of_range("column out of range in matrix");
        }
    }

    void Matrix::checkRowIndex(int row) const
    {
        if (row >= m_nRows || row < 0)
        {
            throw std::out_of_range("row out of range in matrix");
        }
    }

    void Matrix::swapRows(int lhs, int rhs)
    {
        using std::swap;
        checkRowIndex(lhs);
        checkRowIndex(rhs);
        swap(m_rows[lhs], m_rows[rhs]);
    }

    void Matrix::multRow(int row, double val)
    {
        checkRowIndex(row);
        double* const r = m_rows[row];

        for (int i = 0; i < m_nCols; ++i)
        {
            r[i] *= val;
        }
    }

    void Matrix::addMultRow(int to, int from, double mult)
    {
        checkRowIndex(to);
        checkRowIndex(from);

        double* const toRow = m_rows[to];
        const double* const fromRow = m_rows[from];

        for (int col = 0; col < m_nCols; ++col)
        {
            toRow[col] += fromRow[col] * mult;
        }
    }

    bool operator==(const Matrix& lhs, const Matrix& rhs)
    {
        if (lhs.getCols() != rhs.getCols() ||
            lhs.getRows() != rhs.getRows())
        {
            return false;
        }

        for (int i = 0; i < lhs.getRows(); ++i)
        {
            for (int j = 0; j < lhs.getCols(); ++j)
            {
                if (lhs.get(i, j) != rhs.get(i, j))
                {
                    return false;
                }
            }
        }

        return true;
    }

    std::ostream& operator<<(std::ostream& ostr, const Matrix& rhs)
    {
        ostr << "[ ";

        for (int i = 0; i < rhs.getRows(); ++i)
        {
            ostr << "[ ";

            for (int j = 0; j < rhs.getCols(); ++j)
            {
                ostr << rhs.get(i, j) << ' ';
            }

            ostr << "] ";
        }

        ostr << ']';

        return ostr;
    }

    namespace
    {
        std::pair<int, double> argMax(const Matrix& m, int col, int firstRow)
        {
            int bestIndex = firstRow;
            double bestValue = m.get(firstRow, col);
            ++firstRow;

            for (; firstRow < m.getRows(); ++firstRow)
            {
                const double val = m.get(firstRow, col);

                if (val > bestValue)
                {
                    bestIndex = firstRow;
                    bestValue = val;
                }
            }

            return std::pair(bestIndex, bestValue);
        }
    }

    GaussResult gaussElimination(Matrix& m, bool stopOnDependent)
    {
        GaussResult res = GaussResult::Success;
        int row = 0, col = 0;

        while (row < m.getRows() && col < m.getCols())
        {
            const auto [maxIndex, maxVal] = argMax(m, col, row);

            if (maxVal == 0.0)
            {
                res = GaussResult::Dependent;
                ++col;

                if (stopOnDependent)
                {
                    return res;
                }
                else
                {
                    continue;
                }
            }

            m.swapRows(maxIndex, row);
            m.multRow(row, 1.0 / m.get(row, col));

            for (int i = 0; i < m.getRows(); ++i)
            {
                if (i == row)
                {
                    continue;
                }

                m.addMultRow(i, row, -m.get(i, col));
            }

            ++row, ++col;
        }

        return res;
    }
}
}
