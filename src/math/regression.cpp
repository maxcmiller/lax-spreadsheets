#include "regression.hpp"

#include <cmath>
#include <numeric>

namespace lax
{
namespace math
{
    bool Regression::reset(const std::vector<PairedData>& vec)
    {
        if (doReset(vec))
        {
            m_detCoeff = calcDetCoeff(vec);
            return true;
        }

        return false;
    }

    double Regression::calcDetCoeff(const std::vector<PairedData>& vec) const
    {
        // calculate mean of y-values
        const double sumY = std::accumulate(vec.begin(), vec.end(), 0.0,
                [](double d, const PairedData& p)
        {
            return d + p.second;
        });

        const double meanY = sumY / vec.size();

        // calculate total sum of squares
        const double sstot = std::accumulate(vec.begin(), vec.end(), 0.0,
                [meanY](double d, const PairedData& p)
        {
            const double add = p.second - meanY;
            return d + add * add;
        });

        // calculate residual sum of squares
        const double ssres = std::accumulate(vec.begin(), vec.end(), 0.0,
                [this](double d, const PairedData& p)
        {
            const double add = p.second - calculate(p.first);
            return d + add * add;
        });

        return 1.0 - ssres / sstot;
    }
}
}
