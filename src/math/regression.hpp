#ifndef LAX_SRC_MATH_REGRESSION_HPP_
#define LAX_SRC_MATH_REGRESSION_HPP_

#include <iosfwd>
#include <limits>
#include <vector>

namespace lax
{
namespace math
{
    class Regression
    {
    public:
        using PairedData = std::pair<double, double>;

        Regression():
            m_detCoeff(std::numeric_limits<double>::quiet_NaN()) {}

        Regression(const Regression&) = delete;
        virtual ~Regression() = default;

        bool reset(const std::vector<PairedData>& vec);

        double getCoeffOfDetermination() const noexcept
        {
            return m_detCoeff;
        }

        double calculate(double in) const
        {
            return doCalculate(in);
        }
    private:
        virtual bool doReset(const std::vector<PairedData>& vec) = 0;
        virtual double doCalculate(double in) const = 0;
        virtual void print(std::ostream& ostr) const = 0;

        double calcDetCoeff(const std::vector<PairedData>& vec) const;

        double m_detCoeff;

        friend std::ostream& operator<<(std::ostream& ostr, const Regression& rhs)
        {
            rhs.print(ostr);
            return ostr;
        }
    };
}
}

#endif // LAX_SRC_MATH_REGRESSION_HPP_
