#ifndef LAX_SRC_MATH_POLYNOMIAL_REGRESSION_HPP_
#define LAX_SRC_MATH_POLYNOMIAL_REGRESSION_HPP_

#include <iosfwd>
#include <limits>
#include <vector>

#include "regression.hpp"

namespace lax
{
namespace math
{
    class PolynomialRegression:
        public Regression
    {
    public:
        using PairedData = std::pair<double, double>;

        explicit PolynomialRegression(int degree):
            m_coeff(degree + 1, std::numeric_limits<double>::quiet_NaN()) {}

        PolynomialRegression(const PolynomialRegression&) = delete;

        auto begin() const noexcept
        {
            return m_coeff.begin();
        }

        auto end() const noexcept
        {
            return m_coeff.end();
        }

        std::size_t size() const noexcept
        {
            return m_coeff.size();
        }

        int getDegree() const noexcept
        {
            return m_coeff.size() - 1;
        }
    private:
        bool doReset(const std::vector<PairedData>& vec) override;
        double doCalculate(double in) const override;
        void print(std::ostream& ostr) const override;

        std::vector<double> m_coeff;
    };
}
}

#endif // LAX_SRC_MATH_POLYNOMIAL_REGRESSION_HPP_
