#ifndef LAX_SRC_MATH_EXPONENT_REGRESSION_HPP_
#define LAX_SRC_MATH_EXPONENT_REGRESSION_HPP_

#include <cmath>

#include "regression.hpp"

namespace lax
{
namespace math
{
    class ExponentRegression:
        public Regression
    {
    public:
        constexpr static double prec = 1e-3;
        constexpr static int maxIter = 1000;

        ExponentRegression():
            ExponentRegression(prec) {}

        explicit ExponentRegression(double p):
            m_prec(p), m_a(std::numeric_limits<double>::quiet_NaN()), m_b(m_a) {}

        ExponentRegression(const ExponentRegression&) = delete;

        double getA() const noexcept
        {
            return m_a;
        }

        double getB() const noexcept
        {
            return m_b;
        }
    private:
        bool doReset(const std::vector<PairedData>& vec) override;
        double doCalculate(double in) const override;
        void print(std::ostream& ostr) const override;

        void makeGuess(const std::vector<PairedData>& vec);
        PairedData calcGradient(const std::vector<PairedData>& vec);
        static double calcError(double a, double b, const std::vector<PairedData>& vec);

        bool isClose(double lhs, double rhs) const noexcept
        {
            const double diff = std::abs(lhs - rhs);
            return diff <= std::max(std::abs(lhs), std::abs(rhs)) * m_prec;
        }

        double m_prec;
        double m_a;
        double m_b;
    };
}
}

#endif // LAX_SRC_MATH_EXPONENT_REGRESSION_HPP_
