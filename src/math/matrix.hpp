#ifndef LAX_SRC_MATH_MATRIX_HPP_
#define LAX_SRC_MATH_MATRIX_HPP_

#include <array>
#include <iosfwd>
#include <type_traits>

namespace lax
{
namespace math
{
    namespace detail
    {
        template <typename...Rest>
        struct GetSize {};

        template <typename First>
        struct GetSize<First>:
            std::integral_constant<int, First::size> {};

        template <typename First, typename...Rest>
        struct GetSize<First, Rest...>:
            GetSize<Rest...>
        {
            static_assert(First::size == GetSize<Rest...>::value,
                    "size must be consistent among rows in matrix");
        };

        template <typename...Rows>
        constexpr int GetSize_v = GetSize<Rows...>::value;

        template <std::size_t Size>
        struct Row
        {
            template <typename...Ts>
            Row(Ts...ts):
                m_data{std::forward<Ts>(ts)...}
            {
                static_assert(sizeof...(Ts) == Size);
            }

            constexpr static int size = Size;
            std::array<double, Size> m_data;
        };

        template <typename...Rows>
        struct AreRows {};

        template <std::size_t I, typename...Rows>
        struct AreRows<Row<I>, Rows...>:
            AreRows<Rows...> {};

        template <std::size_t I>
        struct AreRows<Row<I> >
        {
            using type = void;
        };

        template <typename...Rows>
        using AreRows_t = typename AreRows<Rows...>::type;
    }

    class Matrix
    {
    public:
        Matrix(int nRows, int nCols);
        Matrix(const Matrix& rhs);
        Matrix(Matrix&& rhs) noexcept;

        template <typename...Rows, typename = detail::AreRows_t<Rows...> >
        Matrix(Rows&&...rows):
            Matrix(std::make_index_sequence<sizeof...(Rows)>(),
                   std::forward<Rows>(rows)...) {}

        Matrix& operator=(Matrix rhs) noexcept
        {
            swap(*this, rhs);
            return *this;
        }

        ~Matrix()
        {
            delete [] m_matrix;
            delete [] m_rows;
        }

        int getRows() const noexcept
        {
            return m_nRows;
        }

        int getCols() const noexcept
        {
            return m_nCols;
        }

        bool isEmpty() const noexcept
        {
            return m_nRows <= 0 || m_nCols <= 0;
        }

        double& get(int row, int col);

        double get(int row, int col) const
        {
            return const_cast<Matrix&>(*this).get(row, col);
        }

        void swapRows(int lhs, int rhs);
        void multRow(int row, double val);
        void addMultRow(int to, int from, double mult);
    private:
        template <typename...Rows, std::size_t...Is>
        Matrix(std::index_sequence<Is...>, Rows&&...rows):
            Matrix(sizeof...(Is), detail::GetSize_v<Rows...>)
        {
            for (int i = 0; i < m_nCols; ++i)
            {
                ((m_rows[Is][i] = rows.m_data[i]),...);
            }
        }

        void checkIndex(int row, int col) const;
        void checkRowIndex(int row) const;

        double* m_matrix;
        double** m_rows;
        int m_nRows;
        int m_nCols;

        friend void swap(Matrix& lhs, Matrix& rhs) noexcept
        {
            using std::swap;
            swap(lhs.m_matrix, rhs.m_matrix);
            swap(lhs.m_rows, rhs.m_rows);
            swap(lhs.m_nRows, rhs.m_nRows);
            swap(lhs.m_nCols, rhs.m_nCols);
        }
    };

    template <typename...Ts>
    detail::Row<sizeof...(Ts)> makeRow(Ts...ts)
    {
        return detail::Row<sizeof...(Ts)>(ts...);
    }

    bool operator==(const Matrix& lhs, const Matrix& rhs);

    inline bool operator!=(const Matrix& lhs, const Matrix& rhs)
    {
        return !(lhs == rhs);
    }

    std::ostream& operator<<(std::ostream& ostr, const Matrix& rhs);

    enum class GaussResult
    {
        Success,
        Dependent,
    };

    GaussResult gaussElimination(Matrix& m, bool stopOnDependent);
}
}

#endif // LAX_SRC_MATH_MATRIX_HPP_
