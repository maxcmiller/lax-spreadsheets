#ifndef LAX_SRC_MATH_LOGARITHM_REGRESSION_HPP_
#define LAX_SRC_MATH_LOGARITHM_REGRESSION_HPP_

#include "regression.hpp"

namespace lax
{
namespace math
{
    class LogarithmRegression:
        public Regression
    {
    public:
        LogarithmRegression() = default;
        LogarithmRegression(const LogarithmRegression&) = delete;

        double getA() const noexcept
        {
            return m_a;
        }

        double getB() const noexcept
        {
            return m_b;
        }
    private:
        bool doReset(const std::vector<PairedData>& vec) override;
        double doCalculate(double in) const override;
        void print(std::ostream& ostr) const override;

        double m_a;
        double m_b;
    };
}
}

#endif // LAX_SRC_MATH_LOGARITHM_REGRESSION_HPP_
