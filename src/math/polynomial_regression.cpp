#include "polynomial_regression.hpp"

#include <algorithm>
#include <iostream>
#include <numeric>

#include "matrix.hpp"

namespace lax
{
namespace math
{
    namespace
    {
        inline void multByX(std::vector<double>& tempSum,
                const std::vector<std::pair<double, double> >& vec)
        {
            std::transform(tempSum.begin(), tempSum.end(), vec.begin(),
                    tempSum.begin(), [](double d, const auto& p)
            {
                return d * p.first;
            });
        }
    }

    bool PolynomialRegression::doReset(const std::vector<PairedData>& vec)
    {
        Matrix m(m_coeff.size(), m_coeff.size() + 1);
        std::vector<double> tempSum(vec.size());

        // first we'll calculate the right-hand side

        // copy the y-values to the tempSum
        std::transform(vec.begin(), vec.end(), tempSum.begin(),
                [](const PairedData& p)
        {
            return p.second;
        });

        const int lastCol = m_coeff.size(); // matrix is one wider than m_coeff

        for (std::size_t i = 0; i < m_coeff.size(); ++i)
        {
            if (i != 0)
            {
                // multiply each element by the corresponding x
                // (only if i != 0 since we start with just the y's)
                multByX(tempSum, vec);
            }

            // we want the sum of tempSum
            m.get(i, lastCol) =
            std::accumulate(tempSum.begin(), tempSum.end(), 0.0);
        }

        // now we'll calculate the coefficient matrix

        // the first element is just the sum of x^0, which is
        // vec.size()
        m.get(0, 0) = vec.size();

        // initialize tempSum to all ones
        std::fill(tempSum.begin(), tempSum.end(), 1.0);

        const int total = 2 * m_coeff.size();
        const int maxIndex = m_coeff.size() - 1;
        int row = 0;
        int col = 1;

        for (int i = 0; i < total; ++i)
        {
            // multiply each element in tempSum by x
            multByX(tempSum, vec);
            const double sum = std::accumulate(tempSum.begin(), tempSum.end(), 0.0);

            // elements along a diagonal going up left-to-right are the same
            for (int r = row, c = col;
                 static_cast<std::size_t>(r) < m_coeff.size() && c >= 0;
                 ++r, --c)
            {
                m.get(r, c) = sum;
            }

            // we go right until we hit the end, then go down
            if (col == maxIndex)
            {
                ++row;
            }
            else
            {
                ++col;
            }
        }

        // now that the matrix has been set up, do elimination
        if (gaussElimination(m, true) == GaussResult::Success)
        {
            // we have enough info, so get the coefficients
            for (std::size_t i = 0; i < m_coeff.size(); ++i)
            {
                m_coeff[i] = m.get(i, lastCol);
            }

            return true;
        }
        else
        {
            // not enough info
            return false;
        }
    }

    double PolynomialRegression::doCalculate(double in) const
    {
        double exp = 1.0;
        double res = 0.0;

        for (double coeff: m_coeff)
        {
            res += exp * coeff;
            exp *= in;
        }

        return res;
    }

    void PolynomialRegression::print(std::ostream& ostr) const
    {
        const auto flags = ostr.flags();

        for (int i = m_coeff.size() - 1; i >= 0; --i)
        {
            ostr << m_coeff[i];

            if (i > 0)
            {
                ostr << "x^" << std::noshowpos << i << std::showpos;
            }
        }

        ostr.flags(flags);
    }
}
}
