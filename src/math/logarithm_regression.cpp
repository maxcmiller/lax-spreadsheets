#include "logarithm_regression.hpp"

#include <algorithm>
#include <iostream>

#include "exponent_regression.hpp"

namespace lax
{
namespace math
{
    double LogarithmRegression::doCalculate(double in) const
    {
        return m_a * std::log(in) + m_b;
    }

    bool LogarithmRegression::doReset(const std::vector<PairedData>& vec)
    {
        ExponentRegression reg;

        std::vector<PairedData> reversed(vec.size());
        std::transform(vec.begin(), vec.end(), reversed.begin(), [](const auto& p)
        {
            return std::pair(p.second, p.first);
        });

        if (!reg.reset(reversed))
        {
            return false;
        }

        m_a = 1 / reg.getB();
        m_b = -std::log(reg.getA()) / reg.getB();
        return true;
    }

    void LogarithmRegression::print(std::ostream& ostr) const
    {
        ostr << m_a << "*ln(x) + " << m_b;
    }
}
}
