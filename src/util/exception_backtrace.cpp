// Note: This file is written to be compiled by the GNU C++
// compiler on a system with execinfo.h.

#ifdef LAX_EXCEPTION_BACKTRACE
#include "exception_backtrace.h"

#ifndef _GNU_SOURCE
# define _GNU_SOURCE
#endif // _GNU_SOURCE

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cxxabi.h>
#include <dlfcn.h>
#include <execinfo.h>
#include <typeinfo>

#ifndef LAX_EXC_BACKTRACE_SIZE
# define LAX_EXC_BACKTRACE_SIZE 200
#endif // LAX_EXC_BACKTRACE_SIZE

namespace
{
    void* backtraceBuf[LAX_EXC_BACKTRACE_SIZE];
    int nBacktraces;
}

extern "C" void __cxa_throw(void* thrown_exception,
        std::type_info* tinfo, void(*dest)(void*))
{
    using ThrowType = void(*)(void*, std::type_info*, void(*)(void*));
    static ThrowType realThrow = nullptr;

    if (!realThrow)
    {
        realThrow = reinterpret_cast<ThrowType>(dlsym(RTLD_NEXT, "__cxa_throw"));
    }

    nBacktraces = backtrace(backtraceBuf, LAX_EXC_BACKTRACE_SIZE);
    realThrow(thrown_exception, tinfo, dest);
}

void lax_print_exbt()
{
    char** const names = backtrace_symbols(backtraceBuf, nBacktraces);

    if (!names)
    {
        perror("backtrace_symbols");

        for (int i = 0; i < nBacktraces; ++i)
        {
            fprintf(stderr, "%p\n", backtraceBuf[i]);
        }

        return;
    }

    for (int i = 0; i < nBacktraces; ++i)
    {
        char* func = names[i];

        for (; *func; ++func)
        {
            if (*func == '(')
            {
                ++func;

                if (*func == '+' || *func == ')')
                {
                    break;
                }

                for (char* c = func; *c; ++c)
                {
                    if (*c == ')' || *c == '+')
                    {
                        *c = '\0';
                        break;
                    }
                }

                break;
            }
        }

        if (*func && *func != '+' && *func != ')')
        {
            if (strncmp(func, "_Z", 2) == 0)
            {
                int status = 0;
                char* disp = abi::__cxa_demangle(func, NULL, NULL, &status);

                if (status == 0)
                {
                    fprintf(stderr, "%s\n", disp);
                    free(disp);
                    continue;
                }
            }
            else
            {
                fprintf(stderr, "%s\n", func);
                continue;
            }
        }

        fprintf(stderr, "%s\n", names[i]);
    }

    free(names);
}
#endif // LAX_EXCEPTION_BACKTRACE
