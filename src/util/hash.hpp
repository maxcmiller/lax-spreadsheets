// Copyright Max Miller 2021.

// This file is part of Lax.

// Lax is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Note that hashCombine is instead covered under the Boost Software License,
// Version 1.0 (see http://www.boost.org/LICENSE_1_0.txt).

/**
    @file hash.hpp
    @brief Contains some hashing utilities.
*/

#ifndef LAX_SRC_UTIL_HASH_HPP_
#define LAX_SRC_UTIL_HASH_HPP_

#include <functional>

namespace lax
{
    /**
        @brief Basically equivalent to std::hash, but with support for tuples
        and pairs.
    */
    template <typename T>
    struct hash:
        std::hash<T> {};

    /**
        @brief Combines two hash values into a single hash value.

        @note Taken largely from the ubiquitous boost::hash_combine.
        @see https://www.boost.org/doc/libs/1_78_0/doc/html/hash/reference.html#boost.hash_combine.
    */
    inline constexpr std::size_t hashCombine(std::size_t lhs, std::size_t rhs) noexcept
    {
        // basically the reciprocal of the golden ratio
        // values taken from
        // https://en.wikipedia.org/wiki/Hash_function#Fibonacci_hashing
        constexpr std::size_t add =
            sizeof(std::size_t) == 2? 0x9e37:
            sizeof(std::size_t) == 4? 0x9e3779b9:
            sizeof(std::size_t) == 8? 0x9e3779b97f4a7c15:
            std::numeric_limits<std::size_t>::max();
        static_assert(add != std::numeric_limits<std::size_t>::max(),
                "unexpected size for std::size_t");

        return lhs ^ (rhs + add + (lhs << 6) + (lhs >> 2));
    }

    ///< @brief No-op; for completeness.
    inline constexpr std::size_t hashCombine(std::size_t lhs) noexcept
    {
        return lhs;
    }

    /**
        @brief Combines three or more hash values by repeated calls to
        hashCombine.
    */
    template <typename...Rest>
    inline constexpr std::size_t hashCombine(std::size_t first, Rest...rest) noexcept
    {
        return hashCombine(first, hashCombine(rest...));
    }

    /**
        @brief Hashes tuples.

        Note that any type could be hashed using @c std::tie to create a tuple
        of references which may then be hashed.
    */
    template <typename...Ts>
    struct hash<std::tuple<Ts...> >:
        private hash<Ts>...
    {
    private:
        using Tuple = std::tuple<Ts...>;
    public:
        std::size_t operator()(const Tuple& rhs) const
        {
            constexpr std::size_t count = std::tuple_size_v<Tuple>;
            return (*this)(rhs, std::make_index_sequence<count>());
        }
    private:
        template <std::size_t...Is>
        std::size_t operator()(const Tuple& rhs, std::index_sequence<Is...>)
        {
            return hashCombine(static_cast<const hash<Ts>&>(*this)(std::get<Is>(rhs))...);
        }
    };

    ///< @brief Hash pairs.
    template <typename T, typename U>
    struct hash<std::pair<T, U> >:
        private hash<T>, private hash<U>
    {
        std::size_t operator()(const std::pair<T, U>& rhs) const
        {
            std::size_t t = static_cast<const hash<T>&>(*this)(rhs.first);
            std::size_t u = static_cast<const hash<U>&>(*this)(rhs.second);
            return hashCombine(t, u);
        }
    };

    ///< @brief Hash a sequence.
    template <typename Iter>
    std::size_t hash_value(Iter first, Iter last)
    {
        hash<typename Iter::value_type> h;
        std::size_t val = 0;

        for (; first != last; ++first)
        {
            val = hashCombine(val, h(*first));
        }

        return val;
    }
}

#endif // LAX_SRC_UTIL_HASH_HPP_
