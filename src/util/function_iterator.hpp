#ifndef LAX_SRC_UTIL_FUNCTION_ITERATOR_HPP_
#define LAX_SRC_UTIL_FUNCTION_ITERATOR_HPP_

#include <iterator>
#include <optional>

namespace lax
{
    namespace detail
    {
        template <typename T, typename = void>
        struct DifferenceType
        {
            using type = decltype(std::declval<T>() - std::declval<T>());
        };

        template <typename T>
        struct DifferenceType<
            T,
            decltype(std::distance(std::declval<T>(), std::declval<T>()))
        >
        {
            using type =
            decltype(std::distance(std::declval<T>(), std::declval<T>()));
        };

        template <typename T>
        using DifferenceType_t = typename DifferenceType<T>::type;
    }

    template <typename BaseItr, typename Func>
    class FunctionIterator
    {
    public:
        using reference = std::invoke_result_t<Func, BaseItr>;

        using value_type        = std::decay_t<reference>;
        using pointer           = std::remove_reference_t<reference>*;
        using difference_type   = typename detail::DifferenceType_t<BaseItr>;
        using iterator_category = std::input_iterator_tag;

        template <typename BItr, typename F>
        FunctionIterator(BItr&& i, F&& f):
            m_itr(std::forward<BItr>(i)), m_func(std::forward<F>(f)) {}

        FunctionIterator(BaseItr&& i):
            m_itr(std::forward<BaseItr>(i)), m_func() {}

        FunctionIterator(const FunctionIterator& rhs) = default;
        FunctionIterator(FunctionIterator&& rhs) = default;

        FunctionIterator& operator=(const FunctionIterator& rhs) = default;
        FunctionIterator& operator=(FunctionIterator&& rhs) = default;

        decltype(auto) operator*() const
        {
            return (*m_func)(m_itr);
        }

        FunctionIterator& operator++() noexcept(noexcept(++this->m_itr))
        {
            ++m_itr;
            return *this;
        }
    private:
        BaseItr m_itr;
        std::optional<Func> m_func;

        friend bool operator==(const FunctionIterator& lhs, const FunctionIterator& rhs)
        {
            return lhs.m_itr == rhs.m_itr;
        }

        friend bool operator!=(const FunctionIterator& lhs, const FunctionIterator& rhs)
        {
            return lhs.m_itr != rhs.m_itr;
        }
    };

    template <typename BaseItr, typename Func>
    FunctionIterator(BaseItr&&, Func&&)->
    FunctionIterator<std::remove_reference_t<BaseItr>, std::remove_reference_t<Func> >;
}

#endif // LAX_SRC_UTIL_FUNCTION_ITERATOR_HPP_
