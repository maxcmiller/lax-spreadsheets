// Copyright Max Miller 2021.

// This file is part of Lax.

// Lax is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
    @file weak_ref.hpp
    @brief Contains @ref WeakRef and @ref WeakRefControl for weak references.
*/

#ifndef LAX_SRC_UTIL_WEAK_REF_HPP_
#define LAX_SRC_UTIL_WEAK_REF_HPP_

#include <type_traits>

#include "hash.hpp"

namespace lax
{
    template <typename T>
    class WeakRef;

namespace detail
{
    struct ControlBlock
    {
        std::size_t count = 0;
        bool alive = true;
    };

    struct WeakCtrlImpl {};
    struct WeakFromThisBase {};

    template <typename T, typename = void>
    struct CorrectWeakFromThis;
}

    class WeakRefControl;

    /**
        @brief Weak reference to a T.

        This class monitors when the owner of a T deletes it and causes all
        subsequent accesses to fail with an exception.
    */
    template <typename T>
    class WeakRef
    {
    public:
        ///< @brief Constructs empty WeakRef.
        WeakRef() noexcept:
            m_ctrl(nullptr), m_ptr(nullptr) {}

        ///< @brief Creating the first WeakRef, using the control and pointer.
        WeakRef(const WeakRefControl& ctrl, T* ptr) noexcept;

        WeakRef(const WeakRef& ctrl) noexcept:
            m_ctrl(ctrl.m_ctrl), m_ptr(ctrl.m_ptr)
        {
            if (m_ctrl)
            {
                ++m_ctrl->count;
            }
        }

        ///< @brief Creating a WeakRef from a WeakRef to a convertible pointer.
        template <typename U>
        WeakRef(const WeakRef<U>& rhs) noexcept:
            m_ctrl(rhs.m_ctrl), m_ptr(rhs.get())
        {
            static_assert(std::is_convertible_v<U*, T*> || std::is_same_v<T*, U*>);

            if (m_ctrl)
            {
                ++m_ctrl->count;
            }
        }

        template <typename U>
        WeakRef(const WeakRef<U>& rhs, T* ptr) noexcept:
            WeakRef(rhs.m_ctrl, ptr) {}

        WeakRef& operator=(WeakRef other) noexcept
        {
            swap(*this, other);
            return *this;
        }

        ///< @brief If last reference to control block, deletes it.
        ~WeakRef()
        {
            if (m_ctrl && --m_ctrl->count == 0)
            {
                delete m_ctrl;
            }
        }

        ///< @brief Throws @c std::runtime_error if the pointer has been deleted.
        T* operator->() const
        {
            return get();
        }

        ///< @brief Throws @c std::runtime_error if the pointer has been deleted.
        T& operator*() const
        {
            return *get();
        }

        T* get() const
        {
            if (m_ctrl && m_ctrl->alive)
            {
                return m_ptr;
            }
            else
            {
                throw std::runtime_error("pointer has expired");
            }
        }

        bool isValid() const noexcept
        {
            return m_ctrl->alive;
        }

        operator bool() const noexcept
        {
            return isValid() && m_ptr;
        }
    private:
        WeakRef(detail::ControlBlock* b, T* ptr) noexcept;

        detail::ControlBlock* m_ctrl;
        T* m_ptr;

        friend void swap(WeakRef& lhs, WeakRef& rhs) noexcept
        {
            using std::swap;
            swap(lhs.m_ctrl, rhs.m_ctrl);
            swap(lhs.m_ptr, rhs.m_ptr);
        }

        template <typename U>
        friend class WeakRef;

        friend class WeakRefControl;
    };

#define LAX_WEAK_REF_COMP(op) \
    template <typename T, typename U> \
    inline bool operator op(const WeakRef<T>& lhs, const WeakRef<U>& rhs) \
    { \
        return lhs.get() op rhs.get(); \
    }

    LAX_WEAK_REF_COMP(==)
    LAX_WEAK_REF_COMP(!=)
    LAX_WEAK_REF_COMP(<)
    LAX_WEAK_REF_COMP(>)
    LAX_WEAK_REF_COMP(<=)
    LAX_WEAK_REF_COMP(>=)
#undef LAX_WEAK_REF_COMP

    /**
        @brief Control object for @ref WeakRef pointers.

        This class contains the logic for managing the weakly referenced
        objects. Typically, you will ensure that this object is deleted
        before its associated objects are.
    */
    class WeakRefControl
    {
    public:
        WeakRefControl():
            m_ref(new detail::ControlBlock, nullptr) {}

        WeakRefControl(const WeakRefControl&) = delete;
        WeakRefControl(WeakRefControl&&) noexcept = default;

        WeakRefControl& operator=(const WeakRefControl&) = delete;
        WeakRefControl& operator=(WeakRefControl&&) noexcept = default;

        ///< @brief Changes control block to indicate deleted objects.
        ~WeakRefControl()
        {
            m_ref.m_ctrl->alive = false;
        }
    private:
        // use of WeakRef here simplifies reference-counting
        // note that this uses an extraneous pointer, but that is only a
        // minor cost
        WeakRef<detail::WeakCtrlImpl> m_ref;

        template <typename T>
        friend class WeakRef;
    };

    template <typename T>
    inline WeakRef<T>::WeakRef(const WeakRefControl& ctrl, T* ptr) noexcept:
        WeakRef(ctrl.m_ref.m_ctrl, ptr) {}

    template <typename T>
    struct hash<WeakRef<T> >:
        private hash<T*>
    {
        std::size_t operator()(const WeakRef<T>& rhs) const noexcept
        {
            return static_cast<hash<T*> >(*this)(rhs.get());
        }
    };

    template <typename To, typename From>
    WeakRef<To> dynamicWeakCast(const WeakRef<From>& rhs)
    {
        return WeakRef<To>(rhs, dynamic_cast<To*>(rhs.get()));
    }

    template <typename To, typename From>
    WeakRef<To> staticWeakCast(const WeakRef<From>& rhs)
    {
        return WeakRef<To>(rhs, static_cast<To*>(rhs.get()));
    }

    template <typename To, typename From>
    WeakRef<To> constWeakCast(const WeakRef<From>& rhs)
    {
        return WeakRef<To>(rhs, const_cast<To*>(rhs.get()));
    }

    template <typename T>
    class WeakRefFromThis:
        private detail::WeakFromThisBase
    {
    public:
        WeakRef<T> getWeakRef() noexcept
        {
            return constWeakCast<T>(m_ref);
        }

        WeakRef<const T> getWeakRef() const noexcept
        {
            return m_ref;
        }
    protected:
        explicit WeakRefFromThis() = default;

        WeakRefFromThis(const WeakRefFromThis&) noexcept:
            m_ref() {}

        WeakRefFromThis(WeakRefFromThis&&) noexcept:
            m_ref() {}

        WeakRefFromThis& operator=(const WeakRefFromThis&) noexcept
        {
            // no-op
            return *this;
        }

        WeakRefFromThis& operator=(WeakRefFromThis&&) noexcept
        {
            // no-op
            return *this;
        }

        ~WeakRefFromThis() = default;
    private:
        mutable WeakRef<const T> m_ref;

        template <typename, typename>
        friend struct detail::CorrectWeakFromThis;
    };

namespace detail
{
    template <typename T, typename>
    struct CorrectWeakFromThis
    {
        static void correct(T* ptr, const WeakRef<T>& ref) noexcept
        {
            ptr->m_ref = ref;
        }
    };

    template <typename T>
    struct CorrectWeakFromThis<
        T,
        std::enable_if_t<
            !std::is_base_of_v<WeakFromThisBase, T>
        >
    >
    {
        static void correct(T*, const WeakRef<T>&) noexcept
        {
            // no-op
        }
    };
}

    template <typename T>
    WeakRef<T>::WeakRef(detail::ControlBlock* b, T* ptr) noexcept:
        m_ctrl(b), m_ptr(ptr)
    {
        ++m_ctrl->count;
        detail::CorrectWeakFromThis<T>::correct(ptr, *this);
    }
}

#endif // LAX_SRC_UTIL_WEAK_REF_HPP_
