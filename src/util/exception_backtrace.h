#ifndef ESS_SRC_UTIL_EXCEPTION_BACKTRACE_H_
#define ESS_SRC_UTIL_EXCEPTION_BACKTRACE_H_

#include <stdio.h>

#ifdef LAX_EXCEPTION_BACKTRACE
void lax_print_exbt();
#else
# ifdef __cplusplus
extern "C"
#endif // __cplusplus
inline void lax_print_exbt()
{
    fputs("exception occurred\n", stderr);
}
#endif // ESS_EXCEPTION_BACKTRACE

#endif // ESS_SRC_UTIL_EXCEPTION_BACKTRACE_H_
