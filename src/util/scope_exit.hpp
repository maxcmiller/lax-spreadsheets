#ifndef LAX_SRC_UTIL_SCOPE_EXIT_HPP_
#define LAX_SRC_UTIL_SCOPE_EXIT_HPP_

namespace lax
{
    namespace detail
    {
        template <
            typename Func,
            bool = std::is_final_v<Func> || !std::is_class_v<Func>
        >
        class ScopeExitBase:
            protected Func
        {
        protected:
            template <typename...Args>
            ScopeExitBase(Args&&...args):
                Func(std::forward<Args>(args)...) {}

            ScopeExitBase(const ScopeExitBase&) = delete;
            ScopeExitBase(ScopeExitBase&&) = delete;

            ~ScopeExitBase() = default;
        };

        template <typename Func>
        class ScopeExitBase<Func, true>
        {
        protected:
            template <typename...Args>
            ScopeExitBase(Args&&...args):
                m_func(std::forward<Args>(args)...) {}

            ScopeExitBase(const ScopeExitBase&) = delete;
            ScopeExitBase(ScopeExitBase&&) = delete;

            ~ScopeExitBase() = default;

            void operator()() noexcept(noexcept(std::declval<Func>()()))
            {
                m_func();
            }
        private:
            Func m_func;
        };
    }

    template <typename Func>
    class ScopeExit:
        private detail::ScopeExitBase<Func>
    {
        using BaseType = detail::ScopeExitBase<Func>;
    public:
        template <typename...Args>
        ScopeExit(Args&&...args):
            BaseType(std::forward<Args>(args)...), m_run(true) {}

        ScopeExit(const ScopeExit&) = delete;
        ScopeExit(ScopeExit&&) = delete;

        ~ScopeExit() noexcept(noexcept((*this)()))
        {
            if (m_run)
            {
                (*this)();
            }
        }

        void release() noexcept
        {
            m_run = false;
        }

        void runNow() noexcept(noexcept((*this)()))
        {
            if (m_run)
            {
                (*this)();
            }

            m_run = false;
        }
    private:
        bool m_run;
    };

    template <typename Func, typename = std::invoke_result_t<Func> >
    ScopeExit(Func&&)->ScopeExit<std::remove_reference_t<Func> >;

    template <typename T>
    class SetAndUnset
    {
    public:
        template <typename...Args>
        SetAndUnset(T& lhs, Args&&...args):
            m_scopeExit(getClosure(lhs))
        {
            lhs = T(std::forward<Args>(args)...);
        }

        void release() noexcept
        {
            m_scopeExit.release();
        }

        void runNow() noexcept(noexcept(this->m_scopeExit.runNow()))
        {
            m_scopeExit.runNow();
        }
    private:
        static auto getClosure(T& lhs)
        {
            return [&lhs, rhs = lhs]() mutable
            {
                lhs = rhs;
            };
        }

        ScopeExit<decltype(getClosure(std::declval<T&>()))> m_scopeExit;
    };

    template <typename T, typename...Args>
    SetAndUnset(T& lhs, Args&&...args)->SetAndUnset<T>;
}

#endif // LAX_SRC_UTIL_SCOPE_EXIT_HPP_
