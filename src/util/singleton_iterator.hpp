#ifndef LAX_SRC_UTIL_SINGLETON_ITERATOR_HPP_
#define LAX_SRC_UTIL_SINGLETON_ITERATOR_HPP_

#include <iterator>
#include <memory>
#include <type_traits>

namespace lax
{
    namespace detail
    {
        struct SingletonIteratorEnd_t {};
    }

    constexpr detail::SingletonIteratorEnd_t endSingleton;

    template <typename T, typename DiffType = int>
    class SingletonIterator
    {
    public:
        using value_type        = std::remove_const_t<T>;
        using reference         = std::add_const_t<T>&;
        using pointer           = std::add_const_t<T>*;
        using difference_type   = DiffType;
        using iterator_category = std::random_access_iterator_tag;

        static_assert(std::is_signed_v<DiffType>);

        template <typename...Args>
        explicit SingletonIterator(Args&&...args):
            m_val(std::make_shared<T>(std::forward<Args>(args)...)), m_ctr(0) {}

        SingletonIterator(detail::SingletonIteratorEnd_t, difference_type n):
            m_val(), m_ctr(n) {}

        SingletonIterator(const SingletonIterator& rhs) = default;
        SingletonIterator(SingletonIterator&&) = default;
        SingletonIterator& operator=(const SingletonIterator& rhs) = default;
        SingletonIterator& operator=(SingletonIterator&&) = default;

        reference operator*() const noexcept
        {
            return *m_val;
        }

        reference operator[](std::size_t i) const noexcept
        {
            return *m_val;
        }

        pointer operator->() const noexcept
        {
            return m_val.get();
        }

        SingletonIterator& operator++() noexcept
        {
            ++m_ctr;
            return *this;
        }

        SingletonIterator operator++(int) noexcept
        {
            return SingletonIterator(m_val, m_ctr++);
        }

        SingletonIterator& operator--() noexcept
        {
            --m_ctr;
            return *this;
        }

        SingletonIterator operator--(int) noexcept
        {
            return SingletonIterator(m_val, m_ctr--);
        }

        SingletonIterator& operator+=(difference_type rhs) &
        {
            m_ctr += rhs;
            return *this;
        }

        SingletonIterator&& operator+=(difference_type rhs) &&
        {
            m_ctr += rhs;
            return std::move(*this);
        }

        SingletonIterator& operator-=(difference_type rhs) &
        {
            m_ctr -= rhs;
            return *this;
        }

        SingletonIterator&& operator-=(difference_type rhs) &&
        {
            m_ctr -= rhs;
            return std::move(*this);
        }
    private:
        SingletonIterator(const std::shared_ptr<T>& p, difference_type n):
            m_val(p), m_ctr(n) {}

        std::shared_ptr<T> m_val;
        difference_type m_ctr;

#define LAX_SINGLE_OP(op)                                     \
        friend auto operator op(const SingletonIterator& lhs, \
                                const SingletonIterator& rhs) \
        {                                                     \
            return lhs.m_ctr op rhs.m_ctr;                    \
        }

        LAX_SINGLE_OP(-)
        LAX_SINGLE_OP(<)
        LAX_SINGLE_OP(>)
        LAX_SINGLE_OP(<=)
        LAX_SINGLE_OP(>=)
        LAX_SINGLE_OP(==)
        LAX_SINGLE_OP(!=)
    };

    template <typename T, typename DiffType>
    auto operator+(const SingletonIterator<T, DiffType>& lhs, DiffType rhs)
    {
        auto temp = lhs;
        return std::move(lhs) += rhs;
    }

    template <typename T, typename DiffType>
    auto operator-(const SingletonIterator<T, DiffType>& lhs, DiffType rhs)
    {
        auto temp = lhs;
        return std::move(lhs) -= rhs;
    }
}

#endif // LAX_SRC_UTIL_SINGLETON_ITERATOR_HPP_
