#include "icase_string.hpp"

namespace lax
{
    int ICaseStringView::compare(const ICaseStringView& rhs) const noexcept
    {
        const std::size_t maxIndex = std::min(size(), rhs.size());

        for (std::size_t i = 0; i < maxIndex; ++i)
        {
            const char left = std::tolower(static_cast<unsigned char>((*this)[i]));
            const char right = std::tolower(static_cast<unsigned char>(rhs[i]));

            if (left < right)
            {
                return -1;
            }
            else if (left > right)
            {
                return 1;
            }
        }

        if (size() < rhs.size())
        {
            return -1;
        }
        else if (size() > rhs.size())
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
}

namespace std
{
    size_t hash<lax::ICaseStringView>::operator()(const lax::ICaseStringView& rhs)
            const noexcept
    {
        std::string lowered('\0', rhs->size());
        std::transform(rhs->begin(), rhs->end(), lowered.begin(),
                [](unsigned char c) {return std::tolower(c);});
        return static_cast<const hash<string>&>(*this)(lowered);
    }
}
