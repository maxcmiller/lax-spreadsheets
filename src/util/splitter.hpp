#ifndef LAX_SRC_UTIL_SPLITTER_HPP_
#define LAX_SRC_UTIL_SPLITTER_HPP_

#include <iterator>
#include <optional>

namespace lax
{
    template <typename Iter, typename Container>
    class Splitter
    {
        using IterTraits = std::iterator_traits<Iter>;
    public:
        using value_type        = Container;
        using reference         = const value_type&;
        using pointer           = const value_type*;
        using difference_type   = std::ptrdiff_t;
        using iterator_category = std::forward_iterator_tag;

        static_assert(std::is_base_of_v<
            std::forward_iterator_tag,
            typename IterTraits::iterator_category
        >, "Iter must be a ForwardIterator");

        using ElemType = typename IterTraits::value_type;

        Splitter() = default;

        template <typename E>
        Splitter(Iter start, Iter end, E&& e):
            m_impl(Impl {start, end, std::forward<E>(e), {}})
        {
            ++*this;
        }

        Splitter(const Splitter& rhs) = default;
        Splitter(Splitter&& rhs) = default;

        Splitter& operator=(const Splitter& rhs) = default;
        Splitter& operator=(Splitter&& rhs) = default;

        reference operator*() const noexcept
        {
            assertValid();
            return m_impl->m_result;
        }

        pointer operator->() const noexcept
        {
            assertValid();
            return &m_impl->m_result;
        }

        Splitter& operator++()
        {
            assertValid();

            if (m_impl->m_start == m_impl->m_end)
            {
                m_impl.reset();
                return *this;
            }

            const Iter first = m_impl->m_start;

            while (++m_impl->m_start != m_impl->m_end &&
                   *m_impl->m_start != m_impl->m_elem) {}

            m_impl->m_result.clear();
            m_impl->m_result.insert(m_impl->m_result.end(), first, m_impl->m_start);

            if (m_impl->m_start != m_impl->m_end)
            {
                ++m_impl->m_start;
            }

            return *this;
        }

        Splitter operator++(int)
        {
            Splitter temp(*this);
            ++*this;
            return temp;
        }
    private:
        struct Impl
        {
            Iter m_start;
            Iter m_end;
            ElemType m_elem;
            Container m_result;
        };

        bool isEnd() const noexcept
        {
            return !m_impl.has_value();
        }

        void assertValid() const
        {
            if (isEnd())
            {
                throw std::logic_error("invalid operation on Splitter end iterator");
            }
        }

        std::optional<Impl> m_impl;

        friend bool operator==(const Splitter& lhs, const Splitter& rhs) noexcept
        {
            if (lhs.isEnd() || rhs.isEnd())
            {
                return lhs.isEnd() && rhs.isEnd();
            }

            return lhs.m_impl->m_start == rhs.m_impl->m_start;
        }

        friend bool operator!=(const Splitter& lhs, const Splitter& rhs) noexcept
        {
            return !(lhs == rhs);
        }
    };

    template <typename Container>
    using SplitterFor = Splitter<
        typename Container::const_iterator,
        Container
    >;

    template <typename Container, typename E>
    std::pair<SplitterFor<Container>, SplitterFor<Container> >
    splitterFor(const Container& c, E&& e)
    {
        using Spl = SplitterFor<Container>;
        return std::pair(Spl(c.begin(), c.end(), std::forward<E>(e)), Spl());
    }
}

#endif // LAX_SRC_UTIL_SPLITTER_HPP_
