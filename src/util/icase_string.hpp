#ifndef LAX_SRC_UTIL_ICASE_STRING_HPP_
#define LAX_SRC_UTIL_ICASE_STRING_HPP_

#include <functional>
#include <string>

namespace lax
{
    class ICaseString;

    class ICaseStringView:
        private std::string_view
    {
    public:
        ICaseStringView() = default;

        explicit ICaseStringView(const char* rhs):
            basic_string_view(rhs) {}

        explicit ICaseStringView(const std::string& rhs):
            basic_string_view(rhs) {}

        explicit ICaseStringView(std::string_view rhs):
            basic_string_view(rhs) {}

        ICaseStringView(const ICaseString& rhs);

        std::string_view& operator*() & noexcept
        {
            return *this;
        }

        const std::string_view& operator*() const& noexcept
        {
            return *this;
        }

        std::string_view&& operator*() && noexcept
        {
            return std::move(*this);
        }

        std::string_view* operator->() noexcept
        {
            return this;
        }

        const std::string_view* operator->() const noexcept
        {
            return this;
        }

        int compare(const ICaseStringView& rhs) const noexcept;
    };

#define LAX_ICASE_COMPARE(op) \
    inline bool operator op(const ICaseStringView& lhs, const ICaseStringView& rhs) noexcept \
    { \
        return lhs.compare(rhs) op 0; \
    }

    LAX_ICASE_COMPARE(==)
    LAX_ICASE_COMPARE(!=)
    LAX_ICASE_COMPARE(<)
    LAX_ICASE_COMPARE(>)
    LAX_ICASE_COMPARE(<=)
    LAX_ICASE_COMPARE(>=)
#undef LAX_ICASE_COMPARE

    class ICaseString:
        private std::string
    {
    public:
        ICaseString() = default;

        explicit ICaseString(const char* rhs):
            basic_string(rhs) {}

        explicit ICaseString(const std::string& rhs):
            basic_string(rhs) {}

        explicit ICaseString(std::string&& rhs):
            basic_string(std::move(rhs)) {}

        explicit ICaseString(std::string_view rhs):
            basic_string(rhs) {}

        explicit ICaseString(ICaseStringView rhs):
            basic_string(*rhs) {}

        std::string& operator*() & noexcept
        {
            return *this;
        }

        const std::string& operator*() const& noexcept
        {
            return *this;
        }

        std::string&& operator*() && noexcept
        {
            return std::move(*this);
        }

        std::string* operator->() noexcept
        {
            return this;
        }

        const std::string* operator->() const noexcept
        {
            return this;
        }

        int compare(const ICaseString& rhs) const noexcept
        {
            return static_cast<ICaseStringView>(*this).compare(rhs);
        }
    };

    inline ICaseStringView::ICaseStringView(const ICaseString& rhs):
        basic_string_view(*rhs) {}

#define LAX_ICASE_COMPARE(op, left, right, lexpr, rexpr) \
    inline bool operator op(const ICaseString##left& lhs, const ICaseString##right& rhs) noexcept \
    { \
        return lexpr(lhs) op rexpr(rhs); \
    }

#define LAX_ALL_ICASE(lhs, rhs, lexpr, rexpr) \
    LAX_ICASE_COMPARE(==, lhs, rhs, lexpr, rexpr) \
    LAX_ICASE_COMPARE(!=, lhs, rhs, lexpr, rexpr) \
    LAX_ICASE_COMPARE(<, lhs, rhs, lexpr, rexpr) \
    LAX_ICASE_COMPARE(>, lhs, rhs, lexpr, rexpr) \
    LAX_ICASE_COMPARE(<=, lhs, rhs, lexpr, rexpr) \
    LAX_ICASE_COMPARE(>=, lhs, rhs, lexpr, rexpr)

#define LAX_CAST static_cast<ICaseStringView>

    LAX_ALL_ICASE(, , LAX_CAST, LAX_CAST)
    LAX_ALL_ICASE(, View, , LAX_CAST)
    LAX_ALL_ICASE(View, , LAX_CAST,)

#undef LAX_ICASE_COMPARE
#undef LAX_ALL_ICASE

    inline ICaseString operator""_is(const char* str, std::size_t n) noexcept
    {
        return static_cast<ICaseString>(std::string(str, n));
    }

    inline ICaseStringView operator""_isview(const char* str, std::size_t n) noexcept
    {
        return static_cast<ICaseStringView>(std::string_view(str, n));
    }
}

namespace std
{
    template <>
    struct hash<lax::ICaseStringView>:
        private hash<string>
    {
        size_t operator()(const lax::ICaseStringView& rhs) const noexcept;
    };

    template <>
    struct hash<lax::ICaseString>:
        private hash<lax::ICaseStringView>
    {
        size_t operator()(const lax::ICaseString& rhs) const noexcept
        {
            const lax::ICaseStringView sv(rhs);
            return static_cast<const hash<lax::ICaseStringView>&>(*this)(sv);
        }
    };
}

#endif // LAX_SRC_UTIL_ICASE_STRING_HPP_
