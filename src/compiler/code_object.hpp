// Copyright Max Miller 2021.

// This file is part of Lax.

// Lax is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Lax is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Lax.  If not, see <https://www.gnu.org/licenses/>.

/**
    @file code_object.hpp
    @brief Contains a @ref CodeObject, the base class for tables and charts,
    as well as other associated classes.
*/

#ifndef LAX_SRC_COMPILER_CODE_OBJECT_HPP_
#define LAX_SRC_COMPILER_CODE_OBJECT_HPP_

#include <any>

#include "util/icase_string.hpp"
#include "util/hash.hpp"

namespace lax
{
    /**
        @brief Manager for persistent data.

        Persistent data is data that is maintained across compilations. Examples
        include actual table data, chart styles, and chart titles.
    */
    class PDataManager
    {
    public:
        using Key = std::pair<ICaseString, std::string>;

        PDataManager() = default;
        PDataManager(const PDataManager&) = delete;
        PDataManager(PDataManager&&) = default;

        PDataManager& operator=(const PDataManager&) = delete;
        PDataManager& operator=(PDataManager&&) = default;

        /**
            @brief Returns associated data for @param name, @param type or
            @c nullptr if none is found.
        */
        std::any* getPData(ICaseStringView name, const std::string& type)
        {
            Key k(name, type);
            return getPData(k);
        }

        ///< @overload
        std::any* getPData(const Key& key);

        /**
            @brief Adds associated data for @param name, @param type,
            returning the inserted data.
        */
        std::any& addPData(ICaseStringView name, const std::string& type, std::any&& data)
        {
            Key k(name, type);
            return addPData(k, std::move(data));
        }

        ///< @overload
        std::any& addPData(const Key& key, std::any&& data)
        {
            return m_map[key] = std::move(data);
        }
    private:
        using DataMap = std::unordered_map<Key, std::any, hash<Key> >;

        DataMap m_map;
    };

    /**
        @brief The base class for tables and charts, which contains important
        methods for persistent data and serialization.
    */
    class CodeObject
    {
    public:
        virtual ~CodeObject() = default;
    protected:
        /**
            @brief Get associated persistent data.

            Persistent data is based on both name (retrieved from @ref getName)
            and type (retrieved from @ref getType) of the object. If no
            associated data is found, a persistent data object is created by
            @ref makePData.

            The returned reference is valid during the lifetime of @param mgr.
        */
        std::any& getPData(PDataManager& mgr) const;
    private:
        ///< Get the case-insensitive name of the object.
        virtual ICaseStringView getName() const noexcept = 0;

        ///< Get the case-sensitive name of the type.
        virtual std::string_view getType() const noexcept = 0;

        ///< Create a persistent data object.
        virtual std::any makePData() const = 0;
    };
}

#endif // LAX_SRC_COMPILER_CODE_OBJECT_HPP_
