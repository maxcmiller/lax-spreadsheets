#include "formula_ast.hpp"

#include <algorithm>
#include <cassert>

#include "type_info.hpp"

namespace lax
{
    FormulaExpr::~FormulaExpr() = default;

#define LAX_VISITOR_TYPE FormulaVisitor
#define LAX_AST_TYPE(type)                      \
    const char* type::getType() const noexcept  \
    {                                           \
        return #type;                           \
    }                                           \
                                                \
    void type::accept(LAX_VISITOR_TYPE& v) const\
    {                                           \
        v.visit(*this);                         \
    }

    BinaryExpr::BinaryExpr(const location& loc,
            parser::token_type op, Ptr lhs, Ptr rhs):
        FormulaExpr(loc), m_op(), m_lhs(lhs), m_rhs(rhs)
    {
        switch (op)
        {
#define LAX_OP_CASE(tok, op)            \
        case parser::token::kLAX_##tok: \
            m_op = op;                  \
            break;
        LAX_OP_CASE(PLUS, kSum)
        LAX_OP_CASE(MINUS, kDiff)
        LAX_OP_CASE(STAR, kProduct)
        LAX_OP_CASE(SLASH, kQuotient)
        LAX_OP_CASE(LESS, kLess)
        LAX_OP_CASE(LESS_EQUAL, kLessEqual)
        LAX_OP_CASE(GREATER, kGreater)
        LAX_OP_CASE(GREATER_EQUAL, kGreaterEqual)
        LAX_OP_CASE(EQUAL, kEqual)
        LAX_OP_CASE(NOT_EQUAL, kNotEqual)
        LAX_OP_CASE(AND, kAnd)
        LAX_OP_CASE(OR, kOr)
        LAX_OP_CASE(EXP, kPower)
#undef LAX_OP_CASE
        default:
            throw std::logic_error("unhandled case in switch on token type");
        }
    }

    LAX_AST_TYPE(BinaryExpr)

    std::string BinaryExpr::stringize() const
    {
        static const char* const ops[kNumOps] = {
            "+",
            "-",
            "*",
            "/",
            "<",
            "<=",
            ">",
            ">=",
            "=",
            "!=",
            "and",
            "or",
            "^",
        };

        assert(m_op != kNumOps);

        return '(' + m_lhs->stringize() + ' ' + ops[m_op] + ' ' +
                m_rhs->stringize() + ')';
    }

    UnaryExpr::UnaryExpr(const location& loc, parser::token_type op,
            Ptr arg):
        FormulaExpr(loc), m_op(), m_arg(arg)
    {
        switch (op)
        {
        case parser::token::kLAX_MINUS:
            m_op = kNegate;
            break;
        case parser::token::kLAX_NOT:
            m_op = kNot;
            break;
        case parser::token::kLAX_VERTBAR:
            m_op = kAbs;
            break;
        default:
            throw std::logic_error("unhandled case in switch on token type");
        }
    }

    LAX_AST_TYPE(UnaryExpr)

    std::string UnaryExpr::stringize() const
    {
        const char* const ops[kNumOps] = {
            "-",
            "not ",
            "|"
        };

        assert(m_op != kNumOps);

        if (m_op != kAbs)
        {
            return ops[m_op] + m_arg->stringize();
        }
        else
        {
            return "|" + m_arg->stringize() + "|";
        }
    }

    LAX_AST_TYPE(CallExpr)

    std::string CallExpr::stringize() const
    {
        std::string str(m_expr->stringize() + '(');

        for (const auto& arg: m_args)
        {
            str += arg->stringize();

            if (&arg != &m_args.back())
            {
                str += ", ";
            }
        }

        return str + ')';
    }

    LAX_AST_TYPE(ElemExpr)

    std::string ElemExpr::stringize() const
    {
        return m_lhs->stringize() + '[' + m_rhs->stringize() + ']';
    }

    namespace
    {
        template <typename T, typename Tag, typename Base>
        constexpr const char* simpleName()
        {
            static_assert(!std::is_same_v<T, T>, "no name defined");
            return "";
        }

#define LAX_BASE FormulaExpr
#define LAX_SIMPLE_TYPE(name, type, tag)                \
        template <>                                     \
        constexpr const char* simpleName<type, tag, LAX_BASE>()   \
        {                                               \
            return name;                                \
        }

        LAX_SIMPLE_TYPE("NumExpr", double, tags::SimpleType)
        LAX_SIMPLE_TYPE("StringExpr", std::string, tags::SimpleType)

        template <>
        constexpr const char* simpleName<
            std::pair<ICaseString, VarExprType>,
            tags::FlatVarType,
            FormulaExpr
        >()
        {
            return "VarExpr";
        }

#undef LAX_BASE
#define LAX_BASE FormulaAssign

        LAX_SIMPLE_TYPE("TypeAssign", TypeData, tags::SimpleType)
        LAX_SIMPLE_TYPE("ScalarTypeAssign", TypeData, tags::ScalarType)
        LAX_SIMPLE_TYPE("ImportAssign", std::string, tags::ImportType)

        template <>
        constexpr const char* simpleName<
            std::pair<FormulaExpr::Ptr, bool>,
            tags::SimpleType,
            FormulaAssign
        >()
        {
            return "ExprAssign";
        }

#undef LAX_BASE
#undef LAX_SIMPLE_TYPE

        template <typename T, typename Tag>
        std::string strSimple(const T&)
        {
            static_assert(!std::is_same_v<T, T>, "no stringization defined");
            return "";
        }

        template <>
        std::string strSimple<std::string, tags::SimpleType>(const std::string& s)
        {
            return '"' + s + '"';
        }

        template <>
        std::string strSimple<
            std::pair<ICaseString, VarExprType>,
            tags::FlatVarType
        >(const std::pair<ICaseString, VarExprType>& s)
        {
            switch (s.second)
            {
            case VarExprType::Exact:
                return "$" + *s.first;
            case VarExprType::ToScalar:
                return "@" + *s.first;
            case VarExprType::Inferred:
                return *s.first;
            }

            assert(!"unhandled VarExprType");
            return *s.first;
        }

        template <>
        std::string strSimple<double, tags::SimpleType>(const double& d)
        {
            return std::to_string(d);
        }
    }

    template <typename T, typename Tag>
    const char* SimpleExpr<T, Tag>::getType() const noexcept
    {
        return simpleName<T, Tag, FormulaExpr>();
    }

    template <typename T, typename Tag>
    std::string SimpleExpr<T, Tag>::stringize() const
    {
        return strSimple<T, Tag>(m_data);
    }

    template <typename T, typename Tag>
    void SimpleExpr<T, Tag>::accept(FormulaVisitor& v) const
    {
        v.visit(*this);
    }

    LAX_AST_TYPE(FuncLiteral)

    FormulaVisitor::~FormulaVisitor() = default;

    template class SimpleExpr<std::pair<ICaseString, VarExprType>, tags::FlatVarType>;
    template class SimpleExpr<double>;
    template class SimpleExpr<std::string>;

    const char* ReduceAssign::getType() const noexcept
    {
        return "ReduceAssign";
    }

    std::string ReduceAssign::stringize() const
    {
        std::string str(getName());
        str += " reduces (";

        for (const std::string& id: m_idents)
        {
            str += id + ", ";
        }

        str.pop_back();
        str.back() = ')';

        return str;
    }

    void ReduceAssign::accept(AssignVisitor& v) const
    {
        v.visit(*this);
    }

    ChartAssign::ChartAssign(const location& loc, std::string&& name,
            std::vector<std::string>&& ext, const JSONObject& obj):
        TopAssign(loc, std::move(name), std::move(ext))
    {
        using namespace std::literals::string_literals;

        const char* currentName = nullptr;
        const char* currentType = nullptr;

        try
        {
            currentName = "index";
            currentType = "string";
            m_data.index = std::get<std::string>(*obj.objs.at("index"_is));

            currentName = "type";
            m_data.type = std::get<std::string>(*obj.objs.at("type"_is));

            currentName = "series";
            currentType = "array of strings";

            const auto& series = std::get<JSONArray>(*obj.objs.at("series"_is));
            m_data.series.resize(series.elems.size());
            std::transform(series.elems.begin(), series.elems.end(),
                    m_data.series.begin(), [](const auto& s)
            {
                return std::get<std::string>(s);
            });
        }
        catch (std::out_of_range&)
        {
            throw parser::syntax_error(loc,
                    "missing "s + currentName + " in chart");
        }
        catch (std::bad_variant_access&)
        {
            throw parser::syntax_error(loc,
                    currentName + " should be of type "s + currentType);
        }
    }

    const char* ChartAssign::getType() const noexcept
    {
        return "ChartAssign";
    }

    std::string ChartAssign::stringize() const
    {
        std::string str("chart ");
        str += getName();
        str += " extends ";

        for (auto itr = beginExtends(); itr != endExtends(); ++itr)
        {
            str += *itr + ", ";
        }

        str.pop_back();
        str.pop_back();

        str += " {\ntype: \"";
        str += m_data.type;
        str += "\"\nindex: \"";
        str += m_data.index;
        str += "\"\nseries: [";

        for (const auto& s: m_data.series)
        {
            str += s + ", ";
        }

        if (!m_data.series.empty())
        {
            str.pop_back();
            str.pop_back();
        }

        str += "]\n}\n";

        return str;
    }

    void ChartAssign::accept(TopVisitor& v) const
    {
        v.visit(*this);
    }

    TableAssign::TableAssign(const location& loc, std::string&& name,
            std::vector<std::string>&& ext, std::vector<FormulaAssign::Ptr>&& vec,
            TableType tt):
        TopAssign(loc, std::move(name), std::move(ext)), m_vec(std::move(vec)),
        m_type(tt) {}

    const char* TableAssign::getType() const noexcept
    {
        return "TableAssign";
    }

    std::string TableAssign::stringize() const
    {
        std::string str = isConst()? "const ":"table ";
        str += getName();

        if (numExtends() > 0)
        {
            str += " extends ";

            for (auto itr = beginExtends(); itr != endExtends(); ++itr)
            {
                str += *itr + ", ";
            }

            str.pop_back();
            str.pop_back();
        }

        str += " {\n";

        for (const auto& a: *this)
        {
            str += a->stringize() + '\n';
        }

        str += '}';

        return str;
    }

    void TableAssign::accept(TopVisitor& v) const
    {
        v.visit(*this);
    }

    const char* TopImportAssign::getType() const noexcept
    {
        return "TopImportAssign";
    }

    std::string TopImportAssign::stringize() const
    {
        std::string res = "import \"" + m_val + '\"';

        if (!getName().empty())
        {
            res += " as " + getName();
        }

        return res;
    }

    void TopImportAssign::accept(TopVisitor& v) const
    {
        return v.visit(*this);
    }

    template <typename T, typename Tags>
    const char* SimpleAssign<T, Tags>::getType() const noexcept
    {
        return simpleName<T, Tags, FormulaAssign>();
    }

    template <typename T, typename Tags>
    void SimpleAssign<T, Tags>::accept(AssignVisitor& v) const
    {
        v.visit(*this);
    }

    template <>
    std::string SimpleAssign<TypeData, tags::SimpleType>::stringize() const
    {
        return std::string(getName()) + " as " + m_val.stringize();
    }

    template <>
    std::string SimpleAssign<TypeData, tags::ScalarType>::stringize() const
    {
        return "scalar" + std::string(getName()) + " as " + m_val.stringize();
    }

    template <>
    std::string SimpleAssign<
        std::pair<FormulaExpr::Ptr, bool>,
        tags::SimpleType
    >::stringize() const
    {
        return std::string(getName()) + ": " + m_val.first->stringize();
    }

    template <>
    std::string SimpleAssign<std::string, tags::ImportType>::stringize() const
    {
        std::string outer = "import \"" + m_val + '\"';
        std::string name(getName());

        if (!name.empty())
        {
            outer += " as " + name;
        }

        return outer;
    }

    template class SimpleAssign<TypeData, tags::SimpleType>;
    template class SimpleAssign<TypeData, tags::ScalarType>;
    template class SimpleAssign<std::pair<FormulaExpr::Ptr, bool>, tags::SimpleType>;
    template class SimpleAssign<std::string, tags::ImportType>;

    std::string FuncLiteral::stringize() const
    {
        std::string str = "fn(";

        for (const std::shared_ptr<TypeAssign>& arg: m_args)
        {
            str += arg->stringize() + ", ";
        }

        if (!m_args.empty())
        {
            str.pop_back();
            str.pop_back();
        }

        str += ") {" + m_exp->stringize() + '}';
        return str;
    }

    TypeInfo TypeData::getType() const
    {
        if (m_params.empty())
        {
            return TypeRegistry::getType(m_name);
        }
        else
        {
            std::vector<TypeInfo> types(m_params.size());
            std::transform(m_params.begin(), m_params.end(), types.begin(),
                    [](const TypeData& d) {return d.getType();});
            return TypeRegistry::getType(m_name, types);
        }
    }

    std::string TypeData::stringize() const
    {
        if (m_params.empty())
        {
            return m_name;
        }

        std::string got;
        char endDelim;
        auto first = m_params.begin();

        if (m_name == "Function")
        {
            got = m_params.front().stringize() + '(';
            endDelim = ')';
            ++first;
        }
        else
        {
            got = m_name + '<';
            endDelim = '>';
        }

        for (auto itr = first; itr != m_params.end(); ++itr)
        {
            got += itr->stringize() + ", ";
        }

        got.pop_back();
        got.back() = endDelim;

        return got;
    }
}
