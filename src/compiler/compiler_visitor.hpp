#ifndef LAX_SRC_COMPILER_COMPILER_VISITOR_HPP_
#define LAX_SRC_COMPILER_COMPILER_VISITOR_HPP_

#include "bytecode.hpp"
#include "formula_ast.hpp"

namespace lax
{
    enum class ConvToScalar
    {
        Yes,
        No,
        Never,
    };

    inline ConvToScalar toNo(ConvToScalar cts) noexcept
    {
        return cts == ConvToScalar::Never? cts:ConvToScalar::No;
    }

    class TypeNode
    {
    public:
        using Ptr = std::unique_ptr<TypeNode>;

        explicit TypeNode(const location& loc, const TypeInfo& type = TypeInfo::nullType()):
            m_type(type), m_loc(loc) {}

        virtual ~TypeNode() = default;
        virtual void addBytecode(BytecodeFunction& f, ValueNode& v, ConvToScalar cts) = 0;

        TypeInfo getType() const noexcept
        {
            return m_type;
        }

        location getLoc() const noexcept
        {
            return m_loc;
        }
    protected:
        void setType(const TypeInfo& type) noexcept
        {
            m_type = type;
        }
    private:
        TypeInfo m_type;
        location m_loc;
    };

    class BinaryTypeNode:
        public TypeNode
    {
    public:
        BinaryTypeNode(const location& loc, Ptr&& lhs, Ptr&& rhs, BinaryExpr::Operation op);

        void addBytecode(BytecodeFunction& f, ValueNode& v, ConvToScalar cts) override;
    private:
        Ptr m_lhs;
        Ptr m_rhs;
        OpCode m_instr;
    };

    class UnaryTypeNode:
        public TypeNode
    {
    public:
        UnaryTypeNode(const location& loc, Ptr&& rhs, UnaryExpr::Operations op);

        void addBytecode(BytecodeFunction& f, ValueNode& v, ConvToScalar cts) override;
    private:
        Ptr m_rhs;
        OpCode m_instr;
    };

    class CallTypeNode:
        public TypeNode
    {
    public:
        CallTypeNode(const location& loc, Ptr&& lhs, std::vector<Ptr>&& args);

        void addBytecode(BytecodeFunction& f, ValueNode& v, ConvToScalar cts) override;
    private:
        Ptr m_lhs;
        std::vector<Ptr> m_args;
    };

    class LiteralTypeNode:
        public TypeNode
    {
    public:
        explicit LiteralTypeNode(const location& loc, const Object::Ptr& obj, OpCode op):
            TypeNode(loc, obj->getType()), m_obj(obj), m_op(op) {}

        void addBytecode(BytecodeFunction& f, ValueNode& v, ConvToScalar cts) override;
    private:
        Object::Ptr m_obj;
        OpCode m_op;
    };

    class VarTypeNode:
        public TypeNode
    {
    public:
        explicit VarTypeNode(const location& loc, const ValueNode::Ptr& n, VarExprType type);
        void addBytecode(BytecodeFunction& f, ValueNode& v, ConvToScalar cts) override;
    private:
        ValueNode::Ptr m_node;
    };

    class ElemTypeNode:
        public TypeNode
    {
    public:
        ElemTypeNode(const location& loc, Ptr&& lhs, Ptr&& rhs);

        void addBytecode(BytecodeFunction& f, ValueNode& v, ConvToScalar cts) override;
    private:
        Ptr m_lhs;
        Ptr m_rhs;
    };

    class CounterTypeNode:
        public TypeNode
    {
    public:
        explicit CounterTypeNode(const location& loc):
            TypeNode(loc, NumberObject::theType()) {}

        void addBytecode(BytecodeFunction& f, ValueNode&, ConvToScalar) override
        {
            f.pushInstruct(Instruction(OpCode::kOpCounter));
        }
    };

    class SizeTypeNode:
        public TypeNode
    {
    public:
        explicit SizeTypeNode(const location& loc, Ptr&& arg);
        void addBytecode(BytecodeFunction& f, ValueNode& v, ConvToScalar cts) override;
    private:
        Ptr m_arg;
    };

    class IfTypeNode:
        public TypeNode
    {
    public:
        IfTypeNode(const location& loc, Ptr&& cond, Ptr&& ifTrue, Ptr&& ifFalse);
        void addBytecode(BytecodeFunction& f, ValueNode& v, ConvToScalar cts) override;
    private:
        Ptr m_cond;
        Ptr m_ifTrue;
        Ptr m_ifFalse;
    };

    class ColumnToScalarNode:
        public TypeNode
    {
    public:
        explicit ColumnToScalarNode(const location& loc, Ptr&& rhs);

        void addBytecode(BytecodeFunction& f, ValueNode& v, ConvToScalar cts) override;
    private:
        Ptr m_rhs;
    };

    class CompileVisitor:
        public FormulaVisitor
    {
    public:
        CompileVisitor(BytecodeFunction& f, ValueNode& n,
                const Environment::Ptr& env, ExprType type):
            m_func(f), m_node(n), m_env(env), m_typeNode(), m_iterNum(-1),
            m_expType(type) {}

        void visit(const BinaryExpr&) override;
        void visit(const CallExpr&) override;
        void visit(const NumExpr&) override;
        void visit(const StringExpr&) override;
        void visit(const UnaryExpr&) override;
        void visit(const VarExpr&) override;
        void visit(const FuncLiteral&) override;
        void visit(const ElemExpr&) override;

        void compile(const FormulaExpr& expr);

        TypeInfo getType() const noexcept
        {
            return m_typeNode->getType();
        }
    private:
        void compileIf(const CallExpr&);
        void compileSize(const CallExpr&);

        BytecodeFunction& m_func;
        ValueNode& m_node;
        Environment::Ptr m_env;
        TypeNode::Ptr m_typeNode;
        int m_iterNum;
        ExprType m_expType;
        bool m_inFunc = false;
    };
}

#endif // LAX_SRC_COMPILER_COMPILER_VISITOR_HPP_
