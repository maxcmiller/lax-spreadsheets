#ifndef LAX_SRC_OBJECT_HPP_
#define LAX_SRC_OBJECT_HPP_

#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#include "location.hpp"
#include "type_info.hpp"

namespace lax
{
    class Object
    {
    public:
        using Ptr = std::shared_ptr<Object>;

        virtual ~Object();

        virtual TypeInfo getType() const = 0;
        virtual std::string stringize() const = 0;

        virtual std::string serialize() const
        {
            return stringize();
        }
    };

    class NumberObject:
        public Object
    {
    public:
        explicit NumberObject(double d):
            m_val(d) {}

        TypeInfo getType() const override
        {
            return theType();
        }

        std::string stringize() const override;
        std::string serialize() const override;

        static TypeInfo theType();

        double getValue() const noexcept
        {
            return m_val;
        }

        void setValue(double d) noexcept
        {
            m_val = d;
        }
    private:
        double m_val;
    };

    class StringObject:
        public Object
    {
    public:
        explicit StringObject(std::string str):
            m_val(std::move(str)) {}

        TypeInfo getType() const override
        {
            return theType();
        }

        std::string stringize() const override;

        static TypeInfo theType();

        std::string getValue() const
        {
            return m_val;
        }

        void setValue(std::string str)
        {
            m_val = std::move(str);
        }
    private:
        std::string m_val;
    };

    class BoolObject:
        public Object
    {
    public:
        static std::shared_ptr<BoolObject> getTrue()
        {
            static std::shared_ptr<BoolObject> self(new BoolObject(true));
            return self;
        }

        static std::shared_ptr<BoolObject> getFalse()
        {
            static std::shared_ptr<BoolObject> self(new BoolObject(false));
            return self;
        }

        static std::shared_ptr<BoolObject> get(bool b)
        {
            return b? getTrue():getFalse();
        }

        TypeInfo getType() const override
        {
            return theType();
        }

        std::string stringize() const override;

        static TypeInfo theType();

        bool getValue() const noexcept
        {
            return m_val;
        }
    private:
        explicit BoolObject(bool b):
            m_val(b) {}

        bool m_val;
    };

    class ArrayObject:
        public Object
    {
        using Container = std::vector<Object::Ptr>;
    public:
        using iterator = typename Container::iterator;
        using const_iterator = typename Container::const_iterator;

        explicit ArrayObject(const TypeInfo& type):
            m_data(), m_type(type), m_myType(theType(type)) {}

        template <typename Iter>
        ArrayObject(const TypeInfo& type, Iter first, Iter last):
            ArrayObject(type)
        {
            insert(m_data.end(), first, last);
        }

        std::string stringize() const override;

        static TypeInfo theType(const TypeInfo& underlying)
        {
            return TypeInfo(ArrayTypeInfo::getArrayInfo(underlying));
        }

        TypeInfo getType() const override
        {
            return m_myType;
        }

        TypeInfo getUnderlying() const noexcept
        {
            return m_type;
        }

        auto begin() const noexcept
        {
            return m_data.begin();
        }

        auto end() const noexcept
        {
            return m_data.end();
        }

        auto begin() noexcept
        {
            return m_data.begin();
        }

        auto end() noexcept
        {
            return m_data.end();
        }

        std::size_t size() const noexcept
        {
            return m_data.size();
        }

        Object::Ptr& operator[](std::size_t index)
        {
            return m_data.at(index);
        }

        const Object::Ptr& operator[](std::size_t index) const
        {
            return m_data.at(index);
        }

        void erase(const_iterator itr) noexcept
        {
            m_data.erase(itr);
        }

        void erase(const_iterator first, const_iterator last) noexcept
        {
            m_data.erase(first, last);
        }

        // strong guarantee
        void push(Ptr obj)
        {
            insert(m_data.end(), obj);
        }

        // strong guarantee
        void insert(const_iterator before, Ptr obj)
        {
            checkType(obj);
            m_data.insert(before, obj);
        }

        // strong guarantee
        template <typename FwdIter>
        void insert(const_iterator before, FwdIter first, FwdIter last)
        {
            checkType(first, last);
            m_data.insert(before, first, last);
        }

        void pop() noexcept
        {
            m_data.pop_back();
        }

        void resize(int size)
        {
            m_data.resize(size);
        }

        void reset(TypeInfo type, int size = 0)
        {
            m_myType = theType(type);
            m_type = type;
            m_data.resize(size);
        }
    protected:
        ArrayObject(const TypeInfo& type, const TypeInfo& myType):
            m_data(), m_type(type), m_myType(myType) {}
    private:
        void checkType(const Ptr& p)
        {
            if (p && p->getType() != getUnderlying())
            {
                throw std::runtime_error("mismatched type in compound");
            }
        }

        template <typename Iter>
        void checkType(Iter first, Iter last)
        {
            for (; first != last; ++first)
            {
                checkType(*first);
            }
        }

        Container m_data;
        TypeInfo m_type;
        TypeInfo m_myType;
    };

    class ColumnObject:
        public ArrayObject
    {
    public:
        explicit ColumnObject(const TypeInfo& type):
            ArrayObject(type, theType(type)) {}

        template <typename Iter>
        ColumnObject(const TypeInfo& type, Iter first, Iter last):
            ColumnObject(type)
        {
            insert(end(), first, last);
        }

        static TypeInfo theType(const TypeInfo& underlying)
        {
            return TypeInfo(ColumnTypeInfo::getColumnInfo(underlying));
        }
    };

    class ErrorObject:
        public Object
    {
    public:
        enum Error
        {
            kBadArgs,
            kDomainError,
            kRangeError,
            kStackOverflow,
            kMiscError
        };

        ErrorObject(std::string_view msg, Error cat, std::string_view abbr):
            m_cat(cat), m_msg(msg), m_abbr(abbr) {}

        ErrorObject(std::string_view msg, Error cat):
            m_cat(cat), m_msg(msg), m_abbr(abbrString(cat)) {}

        TypeInfo getType() const override
        {
            return theType();
        }

        std::string stringize() const override;

        static TypeInfo theType();

        Error getCategory() const noexcept
        {
            return m_cat;
        }

        std::string getMessage() const
        {
            return m_msg;
        }

        std::string getAbbreviation() const
        {
            return m_abbr;
        }

        static const char* abbrString(Error e);
    private:
        Error m_cat;
        std::string m_msg;
        std::string m_abbr;
    };

    class FunctionObject:
        public Object
    {
    public:
        class FuncError:
            public std::exception
        {
        public:
            explicit FuncError(std::shared_ptr<ErrorObject> obj):
                m_obj(obj) {}

            const char* what() const noexcept override;

            auto getError() const noexcept
            {
                return m_obj;
            }
        private:
            std::shared_ptr<ErrorObject> m_obj;
        };

        TypeInfo getType() const override final
        {
            return TypeInfo(m_type);
        }

        virtual Ptr execute(const std::vector<Ptr>& args) const = 0;
//        virtual bool isPure() const noexcept = 0;
        TypeInfo getResult(const std::vector<TypeInfo>& args) const
        {
            return m_type->getReturn(args);
        }
    protected:
        FunctionObject(const std::shared_ptr<FunctionTypeInfo>& t):
            m_type(t) {}

        auto getFuncTypeInfo() const noexcept
        {
            return m_type;
        }

        void setFuncTypeInfo(const std::shared_ptr<FunctionTypeInfo>& t) noexcept
        {
            m_type = t;
        }
    private:
        std::shared_ptr<FunctionTypeInfo> m_type;
    };
}

#endif // LAX_SRC_OBJECT_HPP_
