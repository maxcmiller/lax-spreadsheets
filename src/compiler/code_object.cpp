// Copyright Max Miller 2021.

// This file is part of Lax.

// Lax is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Lax is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Lax.  If not, see <https://www.gnu.org/licenses/>.

#include "code_object.hpp"

namespace lax
{
    std::any* PDataManager::getPData(const Key& key)
    {
        if (auto itr = m_map.find(key); itr != m_map.end())
        {
            return &itr->second;
        }
        else
        {
            return nullptr;
        }
    }

    std::any& CodeObject::getPData(PDataManager& mgr) const
    {
        PDataManager::Key key(getName(), getType());
        auto got = mgr.getPData(key);

        if (!got)
        {
            got = &mgr.addPData(key, makePData());
        }

        return *got;
    }
}
