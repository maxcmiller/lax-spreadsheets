#include "table_compiler.hpp"

#include <deque>
#include <fstream>
#include <sstream>

#include "bytecode.hpp"
#include "formula_ast.hpp"
#include "formula_driver.hpp"
#include "importer/path_finder.hpp"
#include "importer/stdlib_finder.hpp"
#include "util/scope_exit.hpp"
#include "util/singleton_iterator.hpp"

namespace lax
{
    Environment::ConstPtr CompileLoader::load(const std::filesystem::path& p)
    {
        std::ifstream in(p);
        FormulaDriver drv(in, p.string());
        auto tbl = drv.parseTable();

        if (!tbl)
        {
            CompilationFailure cf;

            for (auto itr = drv.beginErrors(); itr != drv.endErrors(); ++itr)
            {
                cf.pushError(itr->loc, itr->msg);
            }

            throw cf;
        }

        return m_tbl.recompile(
                CompileFlags()
                    .setAssigns(tbl->begin(), tbl->end())
                    .setEnv(nullptr)
                    .setKind(Table::Kind::Import)
                    .setDir(p.parent_path().string()));
    }

    class Table::CompileAssign:
        public AssignVisitor
    {
    public:
        CompileAssign(Table* tbl, CompilationFailure& errReporter,
                const Environment::Ptr& env,
                std::size_t numReduces, const CompileFlags& flags):
            m_tbl(tbl), m_msg(errReporter), m_env(env), m_dir(flags.dir),
            m_reduceNo(0), m_numReduces(numReduces), m_kind(flags.kind),
            m_mgr(flags.mgr) {}

        void visit(const ExprAssign& a) override;
        void visit(const TypeAssign& t) override;
        void visit(const ScalarTypeAssign& st) override;
        void visit(const ImportAssign& i) override;
        void visit(const ReduceAssign& a) override;
    private:
        ValueNode* findVar(const ICaseString& str, const location& loc);
        void addArray(const std::string& name, bool isReadOnly,
                std::size_t nReduces, const ValueNode::Ptr& n, const TypeInfo& t);
        void addScalar(const std::string& name, const ValueNode::Ptr& n,
                bool readOnly);
        void compileTypeAssign(const location& loc, std::string str,
                const TypeData& t, bool isScalar);

        Table* m_tbl;
        CompilationFailure& m_msg;
        const Environment::Ptr m_env;
        const std::string_view m_dir;
        std::size_t m_reduceNo;
        const std::size_t m_numReduces;
        const Kind m_kind;
        PDataManager* m_mgr;
    };

    void Table::CompileAssign::visit(const ExprAssign& a)
    {
        try
        {
            ExprType isConst = ExprType::kArray;

            if (m_kind != Kind::Column || a.getValue().second)
            {
                isConst = ExprType::kScalar;
            }

            ExprPtr exp = a.getValue().first;
            ValueNode::Ptr n = compile(exp, m_env, isConst);
            n->setName(*m_tbl->getTableName() + '.' + std::string(a.getName()));
            TypeInfo type = n->getType();

            if (isConst == ExprType::kArray)
            {
                n->setType(ColumnObject::theType(type));
                addArray(std::string(a.getName()), true, m_numReduces, n, type);
            }
            else
            {
                n->setObject(n->getFunc()->execute(std::vector<Object::Ptr>()));
                addScalar(std::string(a.getName()), n, true);
            }

            if (!m_env->addObject(a.getName(), n))
            {
                m_msg.pushError(exp->getLoc(), std::string(a.getName()) +
                        " has already been defined");
            }
        }
        catch (CompilationError& ex)
        {
            m_msg.pushError(ex.getLoc(), ex.what());
        }
    }

    void Table::CompileAssign::compileTypeAssign(const location& loc,
            std::string name, const TypeData& t, bool isScalar)
    {
        if (m_kind == Kind::Import)
        {
            m_msg.pushError(loc, "declaration not allowed in import table\n");
            return;
        }

        TypeInfo type = t.getType();
        ValueNode::Ptr n;

        if (type == TypeInfo::nullType())
        {
            m_msg.pushError(loc, "could not find type " + t.stringize());
            return;
        }

        TypeInfo nodeType = isScalar? type:ColumnObject::theType(type);

        if (!m_env->addObject(name, nodeType, &n))
        {
            m_msg.pushError(loc, name + " has already been defined");
            return;
        }

        n->setName(*m_tbl->getTableName() + name);

        if (!isScalar)
        {
            n->setType(nodeType);
            addArray(name, false, m_numReduces, n, type);
        }
        else
        {
            addScalar(name, n, false);
        }
    }

    void Table::CompileAssign::visit(const TypeAssign& a)
    {
        compileTypeAssign(a.getLocation(), static_cast<std::string>(a.getName()),
                a.getValue(), m_kind != Kind::Column);
    }

    void Table::CompileAssign::visit(const ScalarTypeAssign& st)
    {
        compileTypeAssign(st.getLocation(), static_cast<std::string>(st.getName()),
                st.getValue(), true);
    }

    void Table::CompileAssign::visit(const ImportAssign& a)
    {
        std::string str = a.getValue();

        try
        {
            auto env = m_tbl->m_finder->find(str, m_dir);

            if (!env)
            {
                m_msg.pushError(a.getLocation(), "failed to load import " + str);
                return;
            }

            if (a.getName() == "")
            {
                m_env->copyVarsFrom(*env);
            }
            else if (!m_env->addEnvironment(a.getName(), env))
            {
                m_msg.pushError(a.getLocation(), "failed to import " + str +
                        std::string(a.getName()) + " already assigned");
                return;
            }
        }
        catch (CompilationFailure& ex)
        {
            m_msg.pushError(a.getLocation(), "In import " + str);
            m_msg.copyFrom(ex);
        }
        catch (std::bad_alloc&)
        {
            throw;
        }
        catch (std::exception& ex)
        {
            m_msg.pushError(a.getLocation(), ex.what());
        }
    }

    namespace
    {
        class ReduceFunc:
            public FunctionObject
        {
        public:
            ReduceFunc(const ValueNode::Ptr& f,
                    const ValueNode::Ptr& arg,
                    const TypeInfo& ret):
                FunctionObject(FixedFuncTypeInfo::create(ret, {})),
                m_func(f), m_arg(arg) {}

            Ptr execute(const std::vector<Ptr>&) const override
            {
                auto func = std::dynamic_pointer_cast<FunctionObject>(m_func->getObject());
                assert(func);
                return func->execute({m_arg->getObject()});
            }

            std::string stringize() const
            {
                return "<ReduceFunc>";
            }
        private:
            ValueNode::Ptr m_func;
            ValueNode::Ptr m_arg;
        };
    }

    void Table::CompileAssign::visit(const ReduceAssign& a)
    {
        std::shared_ptr<FunctionTypeInfo> f;

        ValueNode* func = findVar(ICaseString(a.getName()), a.getLocation());

        if (!func)
        {
            return;
        }

        auto sfunc = func->getWeakRef();
        f = func->getType().castTo<FunctionTypeInfo>();

        if (!f)
        {
            m_msg.pushError(a.getLocation(), std::string(a.getName()) +
                    " is not a function");
            return;
        }

        auto reducePos = m_tbl->m_data.reduces.size(); // number of current reduction

        for (const std::string& var: a)
        {
            const ICaseString ivar(var);
            ValueNode* n = findVar(ivar, a.getLocation());

            if (!n)
            {
                return;
            }

            auto sn = n->getWeakRef();
            TypeInfo ret = f->getReturn({n->getType()});

            if (ret == TypeInfo::nullType())
            {
                m_msg.pushError(a.getLocation(), std::string(a.getName()) +
                        " cannot be called on" + n->getType().getName());
                continue;
            }

            auto vn = ValueNode::make(ret, m_env->getSystem());
            vn->setFormula(std::make_shared<ReduceFunc>(sfunc, sn, ret));
            vn->addDependency(sfunc, false);
            vn->addDependency(sn, false);
            vn->setName(*m_tbl->getTableName() + '.' + std::string(a.getName()) +
                    '.' + var);

            auto got = std::find_if(m_tbl->m_data.cols.begin(), m_tbl->m_data.cols.end(),
                    [&ivar](const Table::Column& col)
            {
                return col.name == ivar;
            });

            if (got == m_tbl->m_data.cols.end())
            {
                m_msg.pushError(a.getLocation(), std::string(a.getName()) +
                        " is not a column");
                continue;
            }

            got->reduces.at(reducePos) = vn;
            assert(m_env->addObject(std::to_string(m_reduceNo++), vn));
        }

        m_tbl->m_data.reduces.emplace_back(a.getName(), m_mgr);
    }

    ValueNode* Table::CompileAssign::findVar(const ICaseString& str, const location& loc)
    {
        try
        {
            return &m_env->findObject(str);
        }
        catch (std::out_of_range&)
        {
            m_msg.pushError(loc, "could not find variable " + *str);
            return nullptr;
        }
    }

namespace
{
    struct ColumnPData
    {
        std::any data;
        std::shared_ptr<ColumnObject> obj;
    };
}

    std::any* Table::Column::getData(PDataManager* mgr)
    {
        if (!mgr)
        {
            return nullptr;
        }

        m_pdataName = ICaseString(*name + '@' + type.getName());
        ColumnPData* cpd = std::any_cast<ColumnPData>(&getPData(*mgr));
        obj = cpd->obj;
        node->setObject(obj);
        return &cpd->data;
    }

    std::any Table::Column::makePData() const
    {
        ColumnPData cpd;
        cpd.obj = std::make_shared<ColumnObject>(type);
        return std::any(std::move(cpd));
    }

    void Table::CompileAssign::addArray(const std::string& name, bool isReadOnly,
            std::size_t nReduces, const ValueNode::Ptr& n, const TypeInfo& t)
    {
        Column& c = m_tbl->m_data.cols.emplace_back(name, isReadOnly, nReduces,
                n, t, m_mgr);

        c.obj->resize(m_tbl->m_nonCode->numRows);
    }

namespace
{
    struct ScalarPData
    {
        std::any data;
        Object::Ptr obj;
    };
}

    std::any* Table::ScalarData::getData(PDataManager* mgr)
    {
        if (!mgr)
        {
            return nullptr;
        }

        m_pdataName = ICaseString(*name + '@' + node->getType().getName());
        ScalarPData* spd = std::any_cast<ScalarPData>(&getPData(*mgr));
        assert(spd);
        node->setObject(spd->obj);
        return &spd->data;
    }

    std::any Table::ScalarData::makePData() const
    {
        return std::any(ScalarPData{});
    }

    void Table::CompileAssign::addScalar(const std::string& name,
            const ValueNode::Ptr& n, bool readOnly)
    {
        m_tbl->m_data.scalars.emplace_back(n, name, readOnly, m_mgr);
    }

    Table::Table(const Importer::ConstPtr& f, const ICaseString& name):
        m_finder(f), m_data(), m_name(name),
        m_nonCode(nullptr)
    {
        if (!m_finder)
        {
            auto f = std::make_unique<Importer>();
            f->addImportType("std", StdlibFinder::get());
            m_finder = std::move(f);
        }
    }

    Table::Table(const Table& tbl, CopyCompileInfo):
        m_finder(tbl.m_finder), m_data(), m_name(tbl.m_name),
        m_nonCode(tbl.m_nonCode) {}

    Environment::Ptr Table::recompile(const CompileFlags& flags)
    {
        assert(flags.mgr || flags.kind == Kind::Import);

        // retrieve persistent data
        if (flags.kind != Kind::Import)
        {
            m_nonCode = std::any_cast<NonCodeData>(&getPData(*flags.mgr));
            assert(m_nonCode);
        }

        auto newEnv = std::make_shared<Environment>(flags.env);
        auto old = std::move(m_data);

        ScopeExit guard([this, &old]
        {
            swap(m_data, old);
        });

        CompilationFailure cf;

        const std::size_t nReduces =
        std::count_if(flags.first, flags.last, [](const auto& a)
        {
            return typeid(*a) == typeid(ReduceAssign);
        });

        m_data.reduces.reserve(nReduces);

        CompileAssign ca(this, cf, newEnv, nReduces, flags);

        for (auto itr = flags.first; itr != flags.last; ++itr)
        {
            (*itr)->accept(ca);
        }

        if (cf.size() != 0)
        {
            throw cf;
        }

        guard.release();
        return newEnv;
    }

    namespace
    {
        using SortedDepType =
        std::deque<ValueObserver::Ptr>;

        void topoSort(SortedDepType& out, const ValueObserver::Ptr& root,
                ValueNode::ModifyFlag flag)
        {
            if ((root->isModified() & flag) == flag)
            {
                return;
            }

            for (auto itr = root->beginDepsOnMe(); itr != root->endDepsOnMe(); ++itr)
            {
                const auto ptr = itr->first;

                if (!ptr.isValid())
                {
                    continue;
                }

                topoSort(out, ptr, itr->second? flag:ValueNode::kAllModified);
            }

            out.push_front(root);
            root->setModified(flag);
        }

        void recalc(const SortedDepType& order, int row)
        {
            for (const auto& p: order)
            {
                p->update(row);
            }
        }
    }

    void Table::doRecalc(const ValueNode::Ptr& n, int row)
    {
        SortedDepType order;
        topoSort(order, n, ValueNode::kOneModified);
        recalc(order, row);
    }

    void Table::setObject(std::size_t col, std::size_t row, const Object::Ptr& obj)
    {
        Column& c = m_data.cols.at(col);
        (*c.obj)[row] = obj;
        doRecalc(c.node, row);
    }

    void Table::setScalar(std::size_t n, const Object::Ptr& obj)
    {
        ScalarData& sd = m_data.scalars.at(n);
        sd.node->setObject(obj);
        doRecalc(sd.node, -1);
    }

    void Table::forceRecompute()
    {
        // TODO (ljcas#1#): Fix forceRecompute to be more efficient.
        SortedDepType order;

        for (const Column& c: m_data.cols)
        {
            topoSort(order, c.node, ValueNode::kAllModified);
        }

        for (const ScalarData& s: m_data.scalars)
        {
            topoSort(order, s.node, ValueNode::kAllModified);
        }

        recalc(order, -1);
    }

    void Table::setNumRows(int newSize)
    {
        m_nonCode->numRows = newSize;

        for (const Column& c: m_data.cols)
        {
            c.obj->resize(newSize);
        }
    }

    void Table::insert(int firstIdx, int n)
    {
        // TODO (Max#1#): Change Table::insert so it doesn't recalc everything
        SingletonIterator<Object::Ptr> first(nullptr), last(endSingleton, n);

        for (const Column& c: m_data.cols)
        {
            ArrayObject& a = *c.obj;
            a.insert(a.begin() + firstIdx, first, last);
        }

        m_nonCode->numRows += n;
        forceRecompute();
    }

    void Table::erase(int firstIdx, int n)
    {
        for (const Column& c: m_data.cols)
        {
            ArrayObject& a = *c.obj;
            auto first = a.begin() + firstIdx;
            a.erase(first, first + n);
        }

        m_nonCode->numRows -= n;
        forceRecompute();
    }

    void Table::setArray(std::size_t col, const std::shared_ptr<ColumnObject>& arr)
    {
        Column& c = m_data.cols.at(col);

        if (arr && c.obj && arr->size() < getNumRows())
        {
            arr->resize(c.obj->size());
        }

        c.obj = arr;
        c.node->setObject(c.obj);
    }

    bool Table::isReadOnly(std::size_t row, std::size_t col) const
    {
        if (row < m_nonCode->numRows)
        {
            // is normal cell; check column
            return m_data.cols.at(col).readOnly;
        }
        else if (row < m_nonCode->numRows + m_data.reduces.size())
        {
            // is reduce; always from formula (and read-only)
            return true;
        }
        else
        {
            // is scalar; check
            std::size_t index = row - m_nonCode->numRows - m_data.reduces.size();
            return m_data.scalars.at(index).readOnly;
        }
    }
}
