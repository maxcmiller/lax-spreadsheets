#include "type_info.hpp"

#include <algorithm>
#include <cassert>
#include <list>
#include <map>

namespace lax
{
    bool TypeRegistry::removeType(const TypeInfo& t)
    {
        if (auto itr = theMap().find(t.getName()); itr != theMap().end())
        {
            theMap().erase(itr);
            return true;
        }
        else
        {
            return false;
        }
    }

    bool TypeRegistry::removeTemplate(std::string_view t)
    {
        if (auto itr = theTemplMap().find(t); itr != theTemplMap().end())
        {
            theTemplMap().erase(itr);
            return true;
        }
        else
        {
            return false;
        }
    }

    TypeInfo TypeRegistry::getType(std::string_view name)
    {
        if (auto itr = theMap().find(name); itr != theMap().end())
        {
            return itr->second;
        }
        else
        {
            return TypeInfo::nullType();
        }
    }

    TypeInfo TypeRegistry::getType(std::string_view name,
            const std::vector<TypeInfo>& args)
    {
        if (auto itr = theTemplMap().find(name); itr != theTemplMap().end())
        {
            return itr->second->getType(args);
        }
        else
        {
            return TypeInfo::nullType();
        }
    }

    TypeInfo TypeRegistry::parseType(std::string_view type)
    {
        auto itr = type.cbegin();
        TypeInfo got = doParse(type, itr);

        if (itr != type.cend())
        {
            return TypeInfo::nullType();
        }

        return got;
    }

    namespace
    {
        void skipSpace(std::string_view str, std::string_view::const_iterator& itr)
        {
            itr = std::find_if(itr, str.cend(),
                    [](unsigned char c) {return !std::isspace(c);});
        }
    }

    TypeInfo TypeRegistry::doParse(std::string_view str,
            std::string_view::const_iterator& itr)
    {
        skipSpace(str, itr);

        // find end of identifier (i.e. not alphanumeric or _)
        auto tempItr = std::find_if(itr, str.cend(), [](unsigned char c)
        {
            return c != '_' && !std::isalnum(c);
        });

        // pull out identifier
        std::string_view name(&*itr, tempItr - itr);
        itr = tempItr; // skip ahead to end
        skipSpace(str, itr);

        if (itr == str.cend() ||
            (*itr != '<' && *itr != '('))
        {
            // either at the end of the string, on a comma, or syntax error
            return getType(name);
        }

        TypeInfo outer;

        if (*itr == '<')
        {
            // this is a template
            ++itr;
            auto args = parseArgs(str, itr, '>');

            if (args.size() > 0 && args.back() == TypeInfo::nullType())
            {
                // error parsing arguments
                return TypeInfo::nullType();
            }

            outer = getType(name, args);
        }
        else
        {
            // this is a function
            outer = getType(name);
        }

        skipSpace(str, itr);

        // now we can parse as a function (allows a function to have a function
        // or template return type)
        while (itr != str.cend() && *itr == '(')
        {
            // parse function
            ++itr;

            if (outer == TypeInfo::nullType())
            {
                // could not find return type
                return outer;
            }

            auto args = parseArgs(str, itr, ')');

            if (args.size() > 0 && args.back() == TypeInfo::nullType())
            {
                // error parsing arguments
                return TypeInfo::nullType();
            }

            outer = TypeInfo(FixedFuncTypeInfo::create(outer, std::move(args)));
            skipSpace(str, itr);
        }

        return outer;
    }

    std::vector<TypeInfo> TypeRegistry::parseArgs(std::string_view str,
            std::string_view::const_iterator& itr, char end)
    {
        std::vector<TypeInfo> args;

        for (; itr != str.cend() && *itr != end; )
        {
            args.push_back(doParse(str, itr));

            if (args.back() == TypeInfo::nullType() ||
                itr == str.cend() || (*itr != end && *itr != ','))
            {
                // error parsing type argument or end or , skipped
                return {TypeInfo::nullType()};
            }

            if (*itr == ',')
            {
                ++itr;
            }

            skipSpace(str, itr);
        }

        if (itr == str.cend())
        {
            // missing end char
            return {TypeInfo::nullType()};
        }

        ++itr; // move past end char
        skipSpace(str, itr);

        return args;
    }

    std::shared_ptr<ArrayTypeInfo>
    ArrayTypeInfo::getArrayInfo(const TypeInfo& under)
    {
        static std::unordered_map<TypeInfo, std::shared_ptr<ArrayTypeInfo> > map;

        if (auto itr = map.find(under); itr != map.end())
        {
            return itr->second;
        }
        else
        {
            auto got = std::shared_ptr<ArrayTypeInfo>(new ArrayTypeInfo(under));
            map.emplace(under, got);
            return got;
        }
    }

    std::shared_ptr<ColumnTypeInfo>
    ColumnTypeInfo::getColumnInfo(const TypeInfo& under)
    {
        static std::unordered_map<TypeInfo, std::shared_ptr<ColumnTypeInfo> > map;

        if (auto itr = map.find(under); itr != map.end())
        {
            return itr->second;
        }
        else
        {
            auto got = std::shared_ptr<ColumnTypeInfo>(new ColumnTypeInfo(under));
            map.emplace(under, got);
            return got;
        }
    }

    bool ColumnTypeInfo::isCompatible(const TypeInfoImpl& other) const
    {
        if (other.isArray())
        {
            const auto& arr = dynamic_cast<const ArrayTypeInfo&>(other);
            return arr.getUnderlying() == getUnderlying();
        }

        return false;
    }

    FixedFuncTypeInfo::Ptr
    FixedFuncTypeInfo::create(const TypeInfo& ret, std::vector<TypeInfo>&& args)
    {
        static std::list<Ptr> types;

        auto itr = std::lower_bound(types.begin(), types.end(), nullptr,
                [&ret, &args](const Ptr& lhs, const Ptr& rhs)
        {
            const auto& other = lhs? lhs:rhs;
            bool got;

            if (other->getReturn() == ret)
            {
                got = std::lexicographical_compare(other->begin(), other->end(),
                        args.begin(), args.end());
            }
            else
            {
                got = other->getReturn() < ret;
            }

            return lhs? got:!got;
        });

        if (itr != types.end() && (*itr)->getReturn() == ret &&
            std::equal(args.begin(), args.end(), (*itr)->begin(), (*itr)->end()))
        {
            return *itr;
        }
        else
        {
            auto got = std::make_shared<FixedFuncTypeInfo>(ret, std::move(args));
            types.insert(itr, got);
            return got;
        }
    }

    TypeInfo FixedFuncTypeInfo::getReturn(const std::vector<TypeInfo>& args) const
    {
        if (std::equal(begin(), end(), args.begin(), args.end()))
        {
            return m_type;
        }
        else
        {
            return TypeInfo::nullType();
        }
    }

    bool FixedFuncTypeInfo::isCompatible(const TypeInfoImpl& rhs) const
    {
        auto other = dynamic_cast<const FunctionTypeInfo*>(&rhs);
        return other && other->getReturn(m_args) == m_type;
    }

    ConvResult
    FixedFuncTypeInfo::needsConversion(const std::vector<TypeInfo>& args,
                                       TypeInfo& result,
                                       std::vector<TypeInfo>& to) const
    {
        if (args.size() != m_args.size())
        {
            return ConvResult::kImpossible;
        }

        ConvResult res = ConvResult::kNoneNeeded;

        for (std::size_t ctr = 0; ctr < m_args.size(); ++ctr)
        {
            TypeInfo myArg = m_args[ctr], other = args[ctr];

            if (myArg != other)
            {
                TypeInfo under;

                if (!other.isColumn() ||
                    (under = other.castTo<ColumnTypeInfo>()->getUnderlying()) != myArg)
                {
                    return ConvResult::kImpossible;
                }

                to[ctr] = under;
                res = ConvResult::kNeedsConv;
            }
        }

        result = m_type;
        return res;
    }

    std::string FixedFuncTypeInfo::makeName(const TypeInfo& ret,
            const std::vector<TypeInfo>& args)
    {
        std::string str = ret.getName() + '(';

        for (const TypeInfo& arg: args)
        {
            str += arg.getName() + ", ";
        }

        if (args.empty())
        {
            str += ')';
        }
        else
        {
            str.pop_back();
            str.back() = ')';
        }

        return str;
    }

    namespace
    {
        class ArrayTemplate:
            public TemplateTypeInfo
        {
        public:
            TypeInfo getType(const std::vector<TypeInfo>& params) override
            {
                if (params.size() == 1)
                {
                    return TypeInfo(ArrayTypeInfo::getArrayInfo(params.front()));
                }
                else
                {
                    return TypeInfo::nullType();
                }
            }
        };

        RegisterTemplate arrayTempl("Array", std::make_unique<ArrayTemplate>());

        class FunctionTemplate:
            public TemplateTypeInfo
        {
        public:
            TypeInfo getType(const std::vector<TypeInfo>& params) override
            {
                return TypeInfo(FixedFuncTypeInfo::create(params.front(),
                        params.begin() + 1, params.end()));
            }
        };

        RegisterTemplate funcTempl("Function", std::make_unique<FunctionTemplate>());
    }
}
