#ifndef LAX_SRC_ENVIRONMENT_HPP_
#define LAX_SRC_ENVIRONMENT_HPP_

#include <functional>
#include <initializer_list>
#include <unordered_set>

#include "util/icase_string.hpp"
#include "util/weak_ref.hpp"
#include "object.hpp"

namespace lax
{
    template <typename Mapped>
    using NameMap = std::unordered_map<ICaseString, Mapped>;

    class ValueObserver;

    class ValueSystem
    {
    public:
        ValueSystem() = default;
        ValueSystem(const ValueSystem&) = delete;
        ValueSystem(ValueSystem&&) = default;

        ValueSystem& operator=(const ValueSystem&) = delete;
        ValueSystem& operator=(ValueSystem&&) = default;

        static ValueSystem& staticSystem()
        {
            static ValueSystem sys;
            return sys;
        }
    private:
        // note that m_ctrl *must* go first to ensure that anything referencing
        // held values in m_vals' dtor knows about the deletion
        WeakRefControl m_ctrl;
        std::vector<std::unique_ptr<ValueObserver> > m_vals;

        template <typename T, typename...Args>
        friend WeakRef<T> makeValue(ValueSystem& sys, Args&&...args);
    };

    class ValueObserver:
        public WeakRefFromThis<ValueObserver>
    {
    public:
        using Ptr = WeakRef<ValueObserver>;

        enum ModifyFlag
        {
            kNoneModified= 0x0,
            kOneModified = 0x2,
            kAllModified = 0x3,
        };

        virtual ~ValueObserver()
        {
            ms_valList.erase(this);
        }

        auto beginDepsOnMe() const noexcept
        {
            return m_depOnMe.begin();
        }

        auto endDepsOnMe() const noexcept
        {
            return m_depOnMe.end();
        }

        std::size_t numDepsOnMe() const noexcept
        {
            return m_depOnMe.size();
        }

        auto beginDeps() const noexcept
        {
            return m_deps.begin();
        }

        auto endDeps() const noexcept
        {
            return m_deps.end();
        }

        std::size_t numDeps() const noexcept
        {
            return m_deps.size();
        }

        void clearDependencies();
        void addDependency(const Ptr& dep, bool isArray);

        void addDepOnMe(const Ptr& dep, bool isArray)
        {
            dep->addDependency(getWeakRef(), isArray);
        }

        void setModified(ModifyFlag flag) noexcept
        {
            m_isModified |= flag;
        }

        void unsetModified() noexcept
        {
            m_isModified = kNoneModified;
        }

        int isModified() const noexcept
        {
            return m_isModified;
        }

        void update(int row)
        {
            doUpdate(row);
            unsetModified();
        }

        void setName(std::string_view name)
        {
            m_name = std::string(name);
        }

        void setName(std::string&& name)
        {
            m_name = std::move(name);
        }

        static std::string getDepGraph();
    protected:
        ValueObserver(ValueSystem& sys):
            m_sys(sys)
        {
            ms_valList.insert(this);
        }

        ValueObserver(const ValueObserver&) = delete;
        ValueObserver(ValueObserver&&) = delete;

        ValueSystem& getSystem() const noexcept
        {
            return m_sys;
        }
    private:
        using DepType = std::pair<WeakRef<ValueObserver>, bool>;
        using DepSet = std::unordered_set<DepType, hash<DepType> >;

        virtual void doUpdate(int row) = 0;

        ValueSystem& m_sys;
        std::string m_name;
        DepSet m_depOnMe;
        DepSet m_deps;
        int m_isModified = false;

        static std::unordered_set<ValueObserver*> ms_valList;
    };

    template <typename T, typename...Args>
    WeakRef<T> makeValue(ValueSystem& sys, Args&&...args)
    {
        auto val = std::make_unique<T>(sys, std::forward<Args>(args)...);
        WeakRef<T> ret(sys.m_ctrl, val.get());
        sys.m_vals.push_back(std::move(val));
        return ret;
    }

    class ValueNode:
        public ValueObserver
    {
    public:
        using Ptr = WeakRef<ValueNode>;

        ValueNode(ValueSystem& sys, const TypeInfo& type):
            ValueObserver(sys), m_type(type), m_obj(nullptr), m_func() {}

        static Ptr make(const TypeInfo& type, ValueSystem& sys)
        {
            return makeValue<ValueNode>(sys, type);
        }

        Object::Ptr getObject() const noexcept
        {
            return m_obj;
        }

        TypeInfo getType() const noexcept
        {
            return m_type;
        }

        void setType(const TypeInfo& type) noexcept
        {
            m_type = type;
        }

        auto getFunc() const noexcept
        {
            return m_func;
        }

        void setObject(Object::Ptr obj);

        void setFormula(std::shared_ptr<FunctionObject> f)
        {
            m_func = f;
        }

        Ptr getWeakRef()
        {
            return staticWeakCast<ValueNode>(ValueObserver::getWeakRef());
        }
    private:
        void doUpdate(int row) override;

        TypeInfo m_type;
        Object::Ptr m_obj;
        std::shared_ptr<FunctionObject> m_func;
    };

    class Environment
    {
    public:
        using Ptr = std::shared_ptr<Environment>;
        using ConstPtr = std::shared_ptr<const Environment>;

        enum RemoveSuccess
        {
            kSuccess,
            kNotFound,
            kHasDeps,
        };

        explicit Environment(ValueSystem& sys):
            m_sys(&sys) {}

        explicit Environment(const ConstPtr& outer):
            m_sys(outer->m_sys), m_map(), m_outer(outer) {}

        template <typename Iter>
        Environment(ValueSystem& sys, Iter first, Iter last,
                const ConstPtr& outer = nullptr):
            m_sys(&sys), m_map(first, last), m_outer(outer) {}

        Environment(ValueSystem& sys,
                std::initializer_list<std::pair<std::string, Object::Ptr> >&& il);

        Environment(const Environment&) = delete;
        Environment(Environment&&) = default;

        Environment& operator=(const Environment&) = delete;
        Environment& operator=(Environment&&) = default;

        ValueSystem& getSystem() const noexcept
        {
            return *m_sys;
        }

        void setOuter(const Environment::ConstPtr& env) noexcept
        {
            m_outer = env;
        }

        bool addObject(std::string_view name, const TypeInfo& type,
                ValueNode::Ptr* res = nullptr, bool isParam = false);

        bool addObject(std::string_view name, ValueNode::Ptr obj, bool isParam = false)
        {
            return m_map.emplace(name, std::pair(obj, isParam)).second;
        }

        void moveVarsFrom(Environment&& other);
        void copyVarsFrom(const Environment& other);

        ValueNode& findObject(const ICaseString& name, bool* isParam = nullptr) const;

        void clear()
        {
            m_map.clear();
            m_envs.clear();
        }

        bool hasObject(const ICaseString& name) const
        {
            return m_map.count(name);
        }

        bool addEnvironment(std::string_view name, const ConstPtr& env)
        {
            return m_envs.emplace(name, env).second;
        }

        auto begin() noexcept
        {
            return m_map.begin();
        }

        auto end() noexcept
        {
            return m_map.end();
        }

        auto begin() const noexcept
        {
            return m_map.begin();
        }

        auto end() const noexcept
        {
            return m_map.end();
        }

        auto getOuter() const noexcept
        {
            return m_outer;
        }
    private:
        ValueSystem* m_sys;
        NameMap<std::pair<ValueNode::Ptr, bool> > m_map;
        NameMap<ConstPtr> m_envs;
        ConstPtr m_outer;
    };
}

#endif // LAX_SRC_ENVIRONMENT_HPP_
