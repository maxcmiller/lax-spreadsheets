#include "compiler_visitor.hpp"

namespace lax
{
    BinaryTypeNode::BinaryTypeNode(const location& loc, Ptr&& lhs, Ptr&& rhs,
                                   BinaryExpr::Operation op):
        TypeNode(loc), m_lhs(std::move(lhs)), m_rhs(std::move(rhs))
    {
        using Index = std::pair<TypeInfo, BinaryExpr::Operation>;
        using OpData = std::pair<OpCode, TypeInfo>;

        static const std::map<Index, OpData> ops
        {
#define LAX_BIN_ENTRY(type, op, res, ret) \
            std::make_pair(           \
                std::make_pair(type::theType(), BinaryExpr::op),\
                std::make_pair(OpCode::res, ret::theType()))

            LAX_BIN_ENTRY(BoolObject, kEqual, kOpEqualBool, BoolObject),
            LAX_BIN_ENTRY(BoolObject, kNotEqual, kOpNotEqualBool, BoolObject),
            LAX_BIN_ENTRY(NumberObject, kSum, kOpAdd, NumberObject),
            LAX_BIN_ENTRY(NumberObject, kDiff, kOpSubtract, NumberObject),
            LAX_BIN_ENTRY(NumberObject, kProduct, kOpMult, NumberObject),
            LAX_BIN_ENTRY(NumberObject, kQuotient, kOpDiv, NumberObject),
            LAX_BIN_ENTRY(NumberObject, kPower, kOpPower, NumberObject),
            LAX_BIN_ENTRY(StringObject, kSum, kOpConcat, StringObject),
            LAX_BIN_ENTRY(BoolObject, kAnd, kOpAndBool, BoolObject),
            LAX_BIN_ENTRY(BoolObject, kOr, kOpOrBool, BoolObject),

#define LAX_COMP_ENTRY(type, abbr)                                              \
            LAX_BIN_ENTRY(type, kEqual, kOpEqual##abbr, BoolObject),        \
            LAX_BIN_ENTRY(type, kNotEqual, kOpNotEqual##abbr, BoolObject),  \
            LAX_BIN_ENTRY(type, kLess, kOpLess##abbr, BoolObject),          \
            LAX_BIN_ENTRY(type, kGreater, kOpGreater##abbr, BoolObject),    \
            LAX_BIN_ENTRY(type, kLessEqual, kOpLessEqual##abbr, BoolObject),\
            LAX_BIN_ENTRY(type, kGreaterEqual, kOpGreaterEqual##abbr, BoolObject)

            LAX_COMP_ENTRY(NumberObject, Num),
            LAX_COMP_ENTRY(StringObject, String)
#undef LAX_COMP_ENTRY
#undef LAX_BIN_ENTRY
        };

        if (m_lhs->getType().isColumn())
        {
            m_lhs = std::make_unique<ColumnToScalarNode>(m_lhs->getLoc(), std::move(m_lhs));
        }

        if (m_rhs->getType().isColumn())
        {
            m_rhs = std::make_unique<ColumnToScalarNode>(m_rhs->getLoc(), std::move(m_rhs));
        }

        if (m_lhs->getType() != m_rhs->getType())
        {
            throw CompilationError(getLoc(), ErrorType::kMismatchedBinType);
        }

        if (auto itr = ops.find(std::make_pair(m_lhs->getType(), op)); itr != ops.end())
        {
            m_instr = itr->second.first;
            setType(itr->second.second);
        }
        else
        {
            throw CompilationError(getLoc(), ErrorType::kInvalidBinOp);
        }
    }

    void BinaryTypeNode::addBytecode(BytecodeFunction& f, ValueNode& v, ConvToScalar cts)
    {
        m_lhs->addBytecode(f, v, toNo(cts));
        m_rhs->addBytecode(f, v, toNo(cts));
        f.pushInstruct(Instruction(m_instr));
    }

    void CompileVisitor::visit(const BinaryExpr& bin)
    {
        bin.getLeft()->accept(*this);
        auto lhs = std::move(m_typeNode);
        bin.getRight()->accept(*this);

        m_typeNode.reset(new BinaryTypeNode(bin.getLoc(), std::move(lhs),
                            std::move(m_typeNode), bin.getOp()));
    }

    UnaryTypeNode::UnaryTypeNode(const location& loc, Ptr&& rhs, UnaryExpr::Operations op):
        TypeNode(loc), m_rhs(std::move(rhs)), m_instr()
    {
        if (m_rhs->getType().isColumn())
        {
            m_rhs = std::make_unique<ColumnToScalarNode>(loc, std::move(m_rhs));
        }

        if (op == UnaryExpr::kNot)
        {
            if (m_rhs->getType() != BoolObject::theType())
            {
                throw CompilationError(getLoc(), ErrorType::kInvalidUnaryOp);
            }

            m_instr = OpCode::kOpNotBool;
            setType(BoolObject::theType());
            return;
        }

        if (m_rhs->getType() != NumberObject::theType())
        {
            throw CompilationError(getLoc(), ErrorType::kInvalidUnaryOp);
        }

        setType(NumberObject::theType());

        switch (op)
        {
        case UnaryExpr::kNegate:
            m_instr = OpCode::kOpNegate;
            break;
        case UnaryExpr::kAbs:
            m_instr = OpCode::kOpAbs;
            break;
        default:
            throw std::logic_error("missing case in unary op switch");
        }
    }

    void UnaryTypeNode::addBytecode(BytecodeFunction& f, ValueNode& v, ConvToScalar cts)
    {
        m_rhs->addBytecode(f, v, toNo(cts));
        f.pushInstruct(Instruction(m_instr));
    }

    void CompileVisitor::visit(const UnaryExpr& exp)
    {
        exp.getArgument()->accept(*this);
        m_typeNode = std::make_unique<UnaryTypeNode>(exp.getLoc(),
                                                     std::move(m_typeNode),
                                                     exp.getOp());
    }

    CallTypeNode::CallTypeNode(const location& loc, Ptr&& lhs, std::vector<Ptr>&& rhs):
        TypeNode(loc), m_lhs(std::move(lhs)), m_args(std::move(rhs))
    {
        if (m_lhs->getType().isColumn())
        {
            m_lhs = std::make_unique<ColumnToScalarNode>(loc, std::move(m_lhs));
        }

        auto f = m_lhs->getType().castTo<FunctionTypeInfo>();

        std::vector<TypeInfo> args(m_args.size()), preferred(m_args.size());
        TypeInfo result;

        std::transform(m_args.begin(), m_args.end(), args.begin(), [](const auto& n)
        {
            return n->getType();
        });

        switch (f->needsConversion(args, result, preferred))
        {
        case ConvResult::kImpossible:
        {
            std::string msg(1, '(');

            for (const TypeInfo& ti: args)
            {
                msg += ti.getName() + ", ";
            }

            msg.pop_back();
            msg.back() = ')';

            throw CompilationError(getLoc(), ErrorType::kBadFuncArgs, msg);
        }
        case ConvResult::kNoneNeeded:
            break;
        case ConvResult::kNeedsConv:
            for (std::size_t i = 0; i < m_args.size(); ++i)
            {
                if (preferred[i] != TypeInfo::nullType())
                {
                    m_args[i] = std::make_unique<ColumnToScalarNode>(
                                        m_args[i]->getLoc(), std::move(m_args[i]));
                }
            }
        }

        setType(result);
    }

    void CallTypeNode::addBytecode(BytecodeFunction& f, ValueNode& v, ConvToScalar cts)
    {
        m_lhs->addBytecode(f, v, toNo(cts));

        for (const auto& arg: m_args)
        {
            arg->addBytecode(f, v, toNo(cts));
        }

        f.pushInstruct(Instruction(m_args.size()));
    }

    void CompileVisitor::visit(const CallExpr& call)
    {
        if (auto var = std::dynamic_pointer_cast<VarExpr>(call.getExpr()); var)
        {
            ICaseStringView name(var->getValue().first);

            if (name == "IF"_isview)
            {
                compileIf(call);
                return;
            }
            else if (name == "SIZE"_isview)
            {
                compileSize(call);
                return;
            }
            else if (name == "COUNTER"_isview)
            {
                m_typeNode = std::make_unique<CounterTypeNode>(call.getLoc());
                return;
            }
        }

        try
        {
            call.getExpr()->accept(*this);
        }
        catch (CompilationError& e)
        {
            if (e.getType() == ErrorType::kUnknownVar &&
                e.getExtra() == "self")
            {
                throw CompilationError(e.getLoc(),
                        ErrorType::kFuncExplicitReturn);
            }

            throw;
        }

        auto f = m_typeNode->getType().castTo<FunctionTypeInfo>();

        if (!f)
        {
            if (auto var = std::dynamic_pointer_cast<VarExpr>(call.getExpr());
                var && var->getValue().first == "self"_isview)
            {
                throw CompilationError(call.getLoc(),
                        ErrorType::kFuncExplicitReturn);
            }

            throw CompilationError(call.getLoc(), ErrorType::kNeedFunc);
        }

        auto func = std::move(m_typeNode);
        std::vector<TypeNode::Ptr> args(call.size());

        for (std::size_t i = 0; i < call.size(); ++i)
        {
            call.begin()[i]->accept(*this);
            args[i] = std::move(m_typeNode);
        }

        m_typeNode = std::make_unique<CallTypeNode>(call.getLoc(),
                            std::move(func), std::move(args));
    }

    void LiteralTypeNode::addBytecode(BytecodeFunction& f, ValueNode&, ConvToScalar)
    {
        f.pushInstruct(Instruction(m_op, m_obj));
    }

    void CompileVisitor::visit(const NumExpr& num)
    {
        auto obj = std::make_shared<NumberObject>(num.getValue());
        m_typeNode = std::make_unique<LiteralTypeNode>(num.getLoc(), obj, OpCode::kOpConst);
    }

    void CompileVisitor::visit(const StringExpr& str)
    {
        auto obj = std::make_shared<StringObject>(str.getValue());
        m_typeNode = std::make_unique<LiteralTypeNode>(str.getLoc(), obj, OpCode::kOpConst);
    }

    VarTypeNode::VarTypeNode(const location& loc, const ValueNode::Ptr& n, VarExprType vet):
        TypeNode(loc, n->getType()), m_node(n)
    {
        if (vet == VarExprType::Exact)
        {
            TypeInfo type = getType();

            if (type.isColumn())
            {
                auto col = type.castTo<ColumnTypeInfo>();
                assert(col);
                setType(ArrayObject::theType(col->getUnderlying()));
            }
        }
    }

    void VarTypeNode::addBytecode(BytecodeFunction& f, ValueNode& v, ConvToScalar cts)
    {
        f.pushInstruct(Instruction(OpCode::kOpLoad, m_node));
        v.addDependency(m_node, cts == ConvToScalar::Yes);
    }

    void CompileVisitor::visit(const VarExpr& var)
    {
        auto [name, type] = var.getValue();

        try
        {
            bool isOuter;
            auto& obj = m_env->findObject(static_cast<ICaseString>(name), &isOuter);

            if (m_inFunc && isOuter)
            {
                m_func.addOuter(obj);
            }

            if ((m_expType == ExprType::kScalar ||
                 name->find('.') != std::string::npos) &&
                type != VarExprType::ToScalar)
            {
                // If the expression is a scalar or the variable comes from
                // another table, then we definitely want to use the array
                // directly, if applicable. However, we also want to throw an
                // error if @-notation is used, so don't do this if it is.
                type = VarExprType::Exact;
            }

            auto pobj = obj.getWeakRef();
            m_typeNode = std::make_unique<VarTypeNode>(var.getLoc(), pobj, type);

            if (type == VarExprType::ToScalar)
            {
                if (!m_typeNode->getType().isColumn())
                {
                    throw CompilationError(var.getLoc(), ErrorType::kNonArrayVar, *name);
                }

                m_typeNode = std::make_unique<ColumnToScalarNode>(var.getLoc(),
                                    std::move(m_typeNode));
            }
        }
        catch (std::out_of_range&)
        {
            throw CompilationError(var.getLoc(), ErrorType::kUnknownVar, *name);
        }
    }

    IfTypeNode::IfTypeNode(const location& loc, Ptr&& cond, Ptr&& ifTrue, Ptr&& ifFalse):
        TypeNode(loc), m_cond(std::move(cond)), m_ifTrue(std::move(ifTrue)),
        m_ifFalse(std::move(ifFalse))
    {
        if (m_cond->getType() != BoolObject::theType())
        {
            throw CompilationError(getLoc(), ErrorType::kBadIfCond);
        }

        if (m_ifTrue->getType() != m_ifFalse->getType())
        {
            if (m_ifTrue->getType().isColumn())
            {
                m_ifTrue = std::make_unique<ColumnToScalarNode>(
                                    m_ifTrue->getLoc(), std::move(m_ifTrue));
            }
            else if (m_ifFalse->getType().isColumn())
            {
                m_ifFalse = std::make_unique<ColumnToScalarNode>(
                                m_ifFalse->getLoc(), std::move(m_ifFalse));
            }
        }

        if (m_ifTrue->getType() != m_ifFalse->getType())
        {
            throw CompilationError(getLoc(), ErrorType::kBadIfResult);
        }

        setType(m_ifTrue->getType());
    }

    void IfTypeNode::addBytecode(BytecodeFunction& f, ValueNode& v, ConvToScalar cts)
    {
        m_cond->addBytecode(f, v, toNo(cts));

        int toElse;
        int pastElse;

        int ifInstr = f.pushInstruct(Instruction(OpCode::kOpIf));
        toElse = f.size();

        m_ifTrue->addBytecode(f, v, cts);

        int gotoInstr = f.pushInstruct(Instruction(OpCode::kOpGoto));
        toElse = f.size() - toElse;
        f.setJumpOn(ifInstr, toElse);
        pastElse = f.size();

        m_ifFalse->addBytecode(f, v, cts);

        pastElse = f.size() - pastElse;
        f.setJumpOn(gotoInstr, pastElse);
    }

    void CompileVisitor::compileIf(const CallExpr& expr)
    {
        if (expr.size() != 3)
        {
            throw CompilationError(expr.getLoc(), ErrorType::kBadIfNum);
        }

        (*expr.begin())->accept(*this);
        auto cond = std::move(m_typeNode);
        expr.begin()[1]->accept(*this);
        auto ifTrue = std::move(m_typeNode);
        expr.begin()[2]->accept(*this);
        m_typeNode = std::make_unique<IfTypeNode>(expr.getLoc(), std::move(cond),
                std::move(ifTrue), std::move(m_typeNode));
    }

    SizeTypeNode::SizeTypeNode(const location& loc, Ptr&& arg):
        TypeNode(loc, NumberObject::theType()), m_arg(std::move(arg))
    {
        if (!m_arg->getType().isArray())
        {
            throw CompilationError(getLoc(), ErrorType::kBadFuncArgs, "SIZE");
        }
    }

    void SizeTypeNode::addBytecode(BytecodeFunction& f, ValueNode& v, ConvToScalar cts)
    {
        m_arg->addBytecode(f, v, toNo(cts));
        f.pushInstruct(Instruction(OpCode::kOpArraySize));
    }

    void CompileVisitor::compileSize(const CallExpr& expr)
    {
        if (expr.size() != 1)
        {
            throw CompilationError(expr.getLoc(), ErrorType::kBadFuncArgs, "SIZE");
        }

        (*expr.begin())->accept(*this);
        m_typeNode = std::make_unique<SizeTypeNode>(expr.getLoc(), std::move(m_typeNode));
    }

    void CompileVisitor::visit(const FuncLiteral& func)
    {
        auto env = std::make_shared<Environment>(m_env);
        auto f = std::make_shared<BytecodeArgFunction>();

        for (const auto& a: func)
        {
            ValueNode::Ptr vn;
            TypeInfo t = a->getValue().getType();
            env->addObject(a->getName(), t, &vn, true);
            vn->setName(a->getName());
            f->addArg(vn);
        }

        TypeInfo ret;
        const TypeData& retStr = func.getReturn();

        if (!retStr.getName().empty())
        {
            if ((ret = retStr.getType()) == TypeInfo::nullType())
            {
                throw CompilationError(func.getLoc(), ErrorType::kUnknownType,
                        retStr.stringize());
            }

            using namespace std::literals::string_view_literals;

            ValueNode::Ptr vn;
            f->setReturn(ret);
            env->addObject("self", f->getType(), &vn);
            vn->setObject(f);
            vn->setName("self"sv);
        }

        ValueNode::Ptr vn = ValueNode::make(TypeInfo::nullType(), m_env->getSystem());
        CompileVisitor other(*f, *vn, env, m_expType);
        other.m_inFunc = true;
        other.compile(*func.getExpr());

        if (ret == TypeInfo::nullType())
        {
            f->setReturn(other.getType());
        }
        else if (ret != other.getType())
        {
            throw CompilationError(func.getLoc(), ErrorType::kMismatchedFuncRet);
        }

        // make sure to add dependencies from vn to our dependency list
        for (auto itr = vn->beginDeps(); itr != vn->endDeps(); ++itr)
        {
            m_node.addDependency(itr->first, itr->second);
        }

        m_typeNode = std::make_unique<LiteralTypeNode>(func.getLoc(), f, OpCode::kOpLoadClosure);
    }

    ElemTypeNode::ElemTypeNode(const location& loc, Ptr&& lhs, Ptr&& rhs):
        TypeNode(loc), m_lhs(std::move(lhs)), m_rhs(std::move(rhs))
    {
        auto arrType = m_lhs->getType().castTo<ArrayTypeInfo>();

        if (!arrType)
        {
            throw CompilationError(getLoc(), ErrorType::kNonArrayVar);
        }

        if (m_rhs->getType() != NumberObject::theType())
        {
            throw CompilationError(getLoc(), ErrorType::kBadFuncArgs);
        }

        setType(arrType->getUnderlying());
    }

    void ElemTypeNode::addBytecode(BytecodeFunction& f, ValueNode& v, ConvToScalar cts)
    {
        m_lhs->addBytecode(f, v, toNo(cts));
        m_rhs->addBytecode(f, v, toNo(cts));
        f.pushInstruct(Instruction(OpCode::kOpArrayElem));
    }

    void CompileVisitor::visit(const ElemExpr& exp)
    {
        exp.getLeft()->accept(*this);
        auto lhs = std::move(m_typeNode);
        exp.getRight()->accept(*this);
        m_typeNode = std::make_unique<ElemTypeNode>(exp.getLoc(),
                            std::move(lhs), std::move(m_typeNode));
    }

    ColumnToScalarNode::ColumnToScalarNode(const location& loc, Ptr&& rhs):
        TypeNode(loc), m_rhs(std::move(rhs))
    {
        if (auto type = m_rhs->getType().castTo<ColumnTypeInfo>())
        {
            setType(type->getUnderlying());
        }
        else
        {
            throw std::logic_error("attempted to convert non-column object to scalar");
        }
    }

    void ColumnToScalarNode::addBytecode(BytecodeFunction& f, ValueNode& v, ConvToScalar cts)
    {
        if (cts == ConvToScalar::Never)
        {
            throw CompilationError(getLoc(), ErrorType::kNotInArray);
        }

        m_rhs->addBytecode(f, v, ConvToScalar::Yes);
        f.pushInstruct(Instruction(OpCode::kOpArray));
    }

    void CompileVisitor::compile(const FormulaExpr& expr)
    {
        expr.accept(*this);

        if (m_expType == ExprType::kArray &&
            m_typeNode->getType().isColumn())
        {
            m_typeNode = std::make_unique<ColumnToScalarNode>(
                                m_typeNode->getLoc(), std::move(m_typeNode));
        }

        ConvToScalar cts;

        switch (m_expType)
        {
        case ExprType::kArray:
            cts = ConvToScalar::Yes;
            break;
        case ExprType::kScalar:
            cts = ConvToScalar::Never;
            break;
        }

        m_typeNode->addBytecode(m_func, m_node, cts);
    }
}
