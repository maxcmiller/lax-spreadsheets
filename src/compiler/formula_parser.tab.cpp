// A Bison parser, made by GNU Bison 3.7.

// Skeleton implementation for Bison LALR(1) parsers in C++

// Copyright (C) 2002-2015, 2018-2020 Free Software Foundation, Inc.

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// As a special exception, you may create a larger work that contains
// part or all of the Bison parser skeleton and distribute that work
// under terms of your choice, so long as that work isn't itself a
// parser generator using the skeleton or a modified version thereof
// as a parser skeleton.  Alternatively, if you modify or redistribute
// the parser skeleton itself, you may (at your option) remove this
// special exception, which will cause the skeleton and the resulting
// Bison output files to be licensed under the GNU General Public
// License without this special exception.

// This special exception was added by the Free Software Foundation in
// version 2.2 of Bison.

// DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
// especially those whose name start with YY_ or yy_.  They are
// private implementation details that can be changed or removed.



// First part of user prologue.
#line 1 "formula_parser.ypp"

#include <string>
#include <vector>

#include "formula_ast.hpp"
#include "formula_driver.hpp"
#include "type_info.hpp"

#define LAX_BINARY_EXP(loc, tok, left, right) \
BinaryExpr::make(loc, token::kLAX_##tok, left, right)

#define LAX_UNARY_EXP(loc, tok, arg) \
UnaryExpr::make(loc, token::kLAX_##tok, arg)

namespace
{
auto makeTopImport(const std::shared_ptr<lax::FormulaAssign>& i)
{
    using namespace lax;
    auto imp = std::static_pointer_cast<ImportAssign>(i);
    return std::make_shared<TopImportAssign>(imp);
}
}

#line 66 "formula_parser.tab.cpp"


#include "formula_parser.tab.hpp"




#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> // FIXME: INFRINGES ON USER NAME SPACE.
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif


// Whether we are compiled with exception support.
#ifndef YY_EXCEPTIONS
# if defined __GNUC__ && !defined __EXCEPTIONS
#  define YY_EXCEPTIONS 0
# else
#  define YY_EXCEPTIONS 1
# endif
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K].location)
/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

# ifndef YYLLOC_DEFAULT
#  define YYLLOC_DEFAULT(Current, Rhs, N)                               \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).begin  = YYRHSLOC (Rhs, 1).begin;                   \
          (Current).end    = YYRHSLOC (Rhs, N).end;                     \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).begin = (Current).end = YYRHSLOC (Rhs, 0).end;      \
        }                                                               \
    while (false)
# endif


// Enable debugging if requested.
#if YYDEBUG

// A pseudo ostream that takes yydebug_ into account.
# define YYCDEBUG if (yydebug_) (*yycdebug_)

# define YY_SYMBOL_PRINT(Title, Symbol)         \
  do {                                          \
    if (yydebug_)                               \
    {                                           \
      *yycdebug_ << Title << ' ';               \
      yy_print_ (*yycdebug_, Symbol);           \
      *yycdebug_ << '\n';                       \
    }                                           \
  } while (false)

# define YY_REDUCE_PRINT(Rule)          \
  do {                                  \
    if (yydebug_)                       \
      yy_reduce_print_ (Rule);          \
  } while (false)

# define YY_STACK_PRINT()               \
  do {                                  \
    if (yydebug_)                       \
      yy_stack_print_ ();                \
  } while (false)

#else // !YYDEBUG

# define YYCDEBUG if (false) std::cerr
# define YY_SYMBOL_PRINT(Title, Symbol)  YYUSE (Symbol)
# define YY_REDUCE_PRINT(Rule)           static_cast<void> (0)
# define YY_STACK_PRINT()                static_cast<void> (0)

#endif // !YYDEBUG

#define yyerrok         (yyerrstatus_ = 0)
#define yyclearin       (yyla.clear ())

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYRECOVERING()  (!!yyerrstatus_)

#line 157 "formula_parser.ypp"
namespace lax {
#line 164 "formula_parser.tab.cpp"

  /// Build a parser object.
  parser::parser (FormulaDriver& drv_yyarg)
#if YYDEBUG
    : yydebug_ (false),
      yycdebug_ (&std::cerr),
#else
    :
#endif
      yy_lac_established_ (false),
      drv (drv_yyarg)
  {}

  parser::~parser ()
  {}

  parser::syntax_error::~syntax_error () YY_NOEXCEPT YY_NOTHROW
  {}

  /*---------------.
  | symbol kinds.  |
  `---------------*/



  // by_state.
  parser::by_state::by_state () YY_NOEXCEPT
    : state (empty_state)
  {}

  parser::by_state::by_state (const by_state& that) YY_NOEXCEPT
    : state (that.state)
  {}

  void
  parser::by_state::clear () YY_NOEXCEPT
  {
    state = empty_state;
  }

  void
  parser::by_state::move (by_state& that)
  {
    state = that.state;
    that.clear ();
  }

  parser::by_state::by_state (state_type s) YY_NOEXCEPT
    : state (s)
  {}

  parser::symbol_kind_type
  parser::by_state::kind () const YY_NOEXCEPT
  {
    if (state == empty_state)
      return symbol_kind::S_YYEMPTY;
    else
      return YY_CAST (symbol_kind_type, yystos_[+state]);
  }

  parser::stack_symbol_type::stack_symbol_type ()
  {}

  parser::stack_symbol_type::stack_symbol_type (YY_RVREF (stack_symbol_type) that)
    : super_type (YY_MOVE (that.state), YY_MOVE (that.location))
  {
    switch (that.kind ())
    {
      case symbol_kind::S_assign: // assign
      case symbol_kind::S_import: // import
      case symbol_kind::S_reduce: // reduce
        value.YY_MOVE_OR_COPY< AssignPtr > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_expr: // expr
      case symbol_kind::S_mono_expr: // mono_expr
        value.YY_MOVE_OR_COPY< ExprPtr > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_json_array: // json_array
      case symbol_kind::S_json_elems: // json_elems
        value.YY_MOVE_OR_COPY< JSONArray > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_json_field: // json_field
        value.YY_MOVE_OR_COPY< JSONField > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_json_object: // json_object
      case symbol_kind::S_json_fields: // json_fields
        value.YY_MOVE_OR_COPY< JSONObject > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_json_value: // json_value
        value.YY_MOVE_OR_COPY< JSONValue > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_decl: // decl
      case symbol_kind::S_arg_decl: // arg_decl
        value.YY_MOVE_OR_COPY< TypeAssignPtr > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_type_name: // type_name
      case symbol_kind::S_ret_spec: // ret_spec
        value.YY_MOVE_OR_COPY< TypeData > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_NUMBER: // "number"
      case symbol_kind::S_number: // number
        value.YY_MOVE_OR_COPY< double > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_chart: // chart
        value.YY_MOVE_OR_COPY< std::shared_ptr<ChartAssign>  > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_table: // table
        value.YY_MOVE_OR_COPY< std::shared_ptr<TableAssign>  > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_IDENT: // "identifier"
      case symbol_kind::S_STRING: // "string"
      case symbol_kind::S_nest_name: // nest_name
        value.YY_MOVE_OR_COPY< std::string > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_table_assigns: // table_assigns
        value.YY_MOVE_OR_COPY< std::vector<AssignPtr>  > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_arg_list: // arg_list
      case symbol_kind::S_args: // args
        value.YY_MOVE_OR_COPY< std::vector<ExprPtr>  > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_arg_decls: // arg_decls
      case symbol_kind::S_arg_decl_list: // arg_decl_list
        value.YY_MOVE_OR_COPY< std::vector<TypeAssignPtr>  > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_type_params: // type_params
        value.YY_MOVE_OR_COPY< std::vector<TypeData>  > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_assigns: // assigns
        value.YY_MOVE_OR_COPY< std::vector<std::shared_ptr<TopAssign> >  > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_reduce_args: // reduce_args
      case symbol_kind::S_extension: // extension
      case symbol_kind::S_ext_list: // ext_list
        value.YY_MOVE_OR_COPY< std::vector<std::string>  > (YY_MOVE (that.value));
        break;

      default:
        break;
    }

#if 201103L <= YY_CPLUSPLUS
    // that is emptied.
    that.state = empty_state;
#endif
  }

  parser::stack_symbol_type::stack_symbol_type (state_type s, YY_MOVE_REF (symbol_type) that)
    : super_type (s, YY_MOVE (that.location))
  {
    switch (that.kind ())
    {
      case symbol_kind::S_assign: // assign
      case symbol_kind::S_import: // import
      case symbol_kind::S_reduce: // reduce
        value.move< AssignPtr > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_expr: // expr
      case symbol_kind::S_mono_expr: // mono_expr
        value.move< ExprPtr > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_json_array: // json_array
      case symbol_kind::S_json_elems: // json_elems
        value.move< JSONArray > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_json_field: // json_field
        value.move< JSONField > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_json_object: // json_object
      case symbol_kind::S_json_fields: // json_fields
        value.move< JSONObject > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_json_value: // json_value
        value.move< JSONValue > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_decl: // decl
      case symbol_kind::S_arg_decl: // arg_decl
        value.move< TypeAssignPtr > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_type_name: // type_name
      case symbol_kind::S_ret_spec: // ret_spec
        value.move< TypeData > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_NUMBER: // "number"
      case symbol_kind::S_number: // number
        value.move< double > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_chart: // chart
        value.move< std::shared_ptr<ChartAssign>  > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_table: // table
        value.move< std::shared_ptr<TableAssign>  > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_IDENT: // "identifier"
      case symbol_kind::S_STRING: // "string"
      case symbol_kind::S_nest_name: // nest_name
        value.move< std::string > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_table_assigns: // table_assigns
        value.move< std::vector<AssignPtr>  > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_arg_list: // arg_list
      case symbol_kind::S_args: // args
        value.move< std::vector<ExprPtr>  > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_arg_decls: // arg_decls
      case symbol_kind::S_arg_decl_list: // arg_decl_list
        value.move< std::vector<TypeAssignPtr>  > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_type_params: // type_params
        value.move< std::vector<TypeData>  > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_assigns: // assigns
        value.move< std::vector<std::shared_ptr<TopAssign> >  > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_reduce_args: // reduce_args
      case symbol_kind::S_extension: // extension
      case symbol_kind::S_ext_list: // ext_list
        value.move< std::vector<std::string>  > (YY_MOVE (that.value));
        break;

      default:
        break;
    }

    // that is emptied.
    that.kind_ = symbol_kind::S_YYEMPTY;
  }

#if YY_CPLUSPLUS < 201103L
  parser::stack_symbol_type&
  parser::stack_symbol_type::operator= (const stack_symbol_type& that)
  {
    state = that.state;
    switch (that.kind ())
    {
      case symbol_kind::S_assign: // assign
      case symbol_kind::S_import: // import
      case symbol_kind::S_reduce: // reduce
        value.copy< AssignPtr > (that.value);
        break;

      case symbol_kind::S_expr: // expr
      case symbol_kind::S_mono_expr: // mono_expr
        value.copy< ExprPtr > (that.value);
        break;

      case symbol_kind::S_json_array: // json_array
      case symbol_kind::S_json_elems: // json_elems
        value.copy< JSONArray > (that.value);
        break;

      case symbol_kind::S_json_field: // json_field
        value.copy< JSONField > (that.value);
        break;

      case symbol_kind::S_json_object: // json_object
      case symbol_kind::S_json_fields: // json_fields
        value.copy< JSONObject > (that.value);
        break;

      case symbol_kind::S_json_value: // json_value
        value.copy< JSONValue > (that.value);
        break;

      case symbol_kind::S_decl: // decl
      case symbol_kind::S_arg_decl: // arg_decl
        value.copy< TypeAssignPtr > (that.value);
        break;

      case symbol_kind::S_type_name: // type_name
      case symbol_kind::S_ret_spec: // ret_spec
        value.copy< TypeData > (that.value);
        break;

      case symbol_kind::S_NUMBER: // "number"
      case symbol_kind::S_number: // number
        value.copy< double > (that.value);
        break;

      case symbol_kind::S_chart: // chart
        value.copy< std::shared_ptr<ChartAssign>  > (that.value);
        break;

      case symbol_kind::S_table: // table
        value.copy< std::shared_ptr<TableAssign>  > (that.value);
        break;

      case symbol_kind::S_IDENT: // "identifier"
      case symbol_kind::S_STRING: // "string"
      case symbol_kind::S_nest_name: // nest_name
        value.copy< std::string > (that.value);
        break;

      case symbol_kind::S_table_assigns: // table_assigns
        value.copy< std::vector<AssignPtr>  > (that.value);
        break;

      case symbol_kind::S_arg_list: // arg_list
      case symbol_kind::S_args: // args
        value.copy< std::vector<ExprPtr>  > (that.value);
        break;

      case symbol_kind::S_arg_decls: // arg_decls
      case symbol_kind::S_arg_decl_list: // arg_decl_list
        value.copy< std::vector<TypeAssignPtr>  > (that.value);
        break;

      case symbol_kind::S_type_params: // type_params
        value.copy< std::vector<TypeData>  > (that.value);
        break;

      case symbol_kind::S_assigns: // assigns
        value.copy< std::vector<std::shared_ptr<TopAssign> >  > (that.value);
        break;

      case symbol_kind::S_reduce_args: // reduce_args
      case symbol_kind::S_extension: // extension
      case symbol_kind::S_ext_list: // ext_list
        value.copy< std::vector<std::string>  > (that.value);
        break;

      default:
        break;
    }

    location = that.location;
    return *this;
  }

  parser::stack_symbol_type&
  parser::stack_symbol_type::operator= (stack_symbol_type& that)
  {
    state = that.state;
    switch (that.kind ())
    {
      case symbol_kind::S_assign: // assign
      case symbol_kind::S_import: // import
      case symbol_kind::S_reduce: // reduce
        value.move< AssignPtr > (that.value);
        break;

      case symbol_kind::S_expr: // expr
      case symbol_kind::S_mono_expr: // mono_expr
        value.move< ExprPtr > (that.value);
        break;

      case symbol_kind::S_json_array: // json_array
      case symbol_kind::S_json_elems: // json_elems
        value.move< JSONArray > (that.value);
        break;

      case symbol_kind::S_json_field: // json_field
        value.move< JSONField > (that.value);
        break;

      case symbol_kind::S_json_object: // json_object
      case symbol_kind::S_json_fields: // json_fields
        value.move< JSONObject > (that.value);
        break;

      case symbol_kind::S_json_value: // json_value
        value.move< JSONValue > (that.value);
        break;

      case symbol_kind::S_decl: // decl
      case symbol_kind::S_arg_decl: // arg_decl
        value.move< TypeAssignPtr > (that.value);
        break;

      case symbol_kind::S_type_name: // type_name
      case symbol_kind::S_ret_spec: // ret_spec
        value.move< TypeData > (that.value);
        break;

      case symbol_kind::S_NUMBER: // "number"
      case symbol_kind::S_number: // number
        value.move< double > (that.value);
        break;

      case symbol_kind::S_chart: // chart
        value.move< std::shared_ptr<ChartAssign>  > (that.value);
        break;

      case symbol_kind::S_table: // table
        value.move< std::shared_ptr<TableAssign>  > (that.value);
        break;

      case symbol_kind::S_IDENT: // "identifier"
      case symbol_kind::S_STRING: // "string"
      case symbol_kind::S_nest_name: // nest_name
        value.move< std::string > (that.value);
        break;

      case symbol_kind::S_table_assigns: // table_assigns
        value.move< std::vector<AssignPtr>  > (that.value);
        break;

      case symbol_kind::S_arg_list: // arg_list
      case symbol_kind::S_args: // args
        value.move< std::vector<ExprPtr>  > (that.value);
        break;

      case symbol_kind::S_arg_decls: // arg_decls
      case symbol_kind::S_arg_decl_list: // arg_decl_list
        value.move< std::vector<TypeAssignPtr>  > (that.value);
        break;

      case symbol_kind::S_type_params: // type_params
        value.move< std::vector<TypeData>  > (that.value);
        break;

      case symbol_kind::S_assigns: // assigns
        value.move< std::vector<std::shared_ptr<TopAssign> >  > (that.value);
        break;

      case symbol_kind::S_reduce_args: // reduce_args
      case symbol_kind::S_extension: // extension
      case symbol_kind::S_ext_list: // ext_list
        value.move< std::vector<std::string>  > (that.value);
        break;

      default:
        break;
    }

    location = that.location;
    // that is emptied.
    that.state = empty_state;
    return *this;
  }
#endif

  template <typename Base>
  void
  parser::yy_destroy_ (const char* yymsg, basic_symbol<Base>& yysym) const
  {
    if (yymsg)
      YY_SYMBOL_PRINT (yymsg, yysym);
  }

#if YYDEBUG
  template <typename Base>
  void
  parser::yy_print_ (std::ostream& yyo, const basic_symbol<Base>& yysym) const
  {
    std::ostream& yyoutput = yyo;
    YYUSE (yyoutput);
    if (yysym.empty ())
      yyo << "empty symbol";
    else
      {
        symbol_kind_type yykind = yysym.kind ();
        yyo << (yykind < YYNTOKENS ? "token" : "nterm")
            << ' ' << yysym.name () << " ("
            << yysym.location << ": ";
        YYUSE (yykind);
        yyo << ')';
      }
  }
#endif

  void
  parser::yypush_ (const char* m, YY_MOVE_REF (stack_symbol_type) sym)
  {
    if (m)
      YY_SYMBOL_PRINT (m, sym);
    yystack_.push (YY_MOVE (sym));
  }

  void
  parser::yypush_ (const char* m, state_type s, YY_MOVE_REF (symbol_type) sym)
  {
#if 201103L <= YY_CPLUSPLUS
    yypush_ (m, stack_symbol_type (s, std::move (sym)));
#else
    stack_symbol_type ss (s, sym);
    yypush_ (m, ss);
#endif
  }

  void
  parser::yypop_ (int n)
  {
    yystack_.pop (n);
  }

#if YYDEBUG
  std::ostream&
  parser::debug_stream () const
  {
    return *yycdebug_;
  }

  void
  parser::set_debug_stream (std::ostream& o)
  {
    yycdebug_ = &o;
  }


  parser::debug_level_type
  parser::debug_level () const
  {
    return yydebug_;
  }

  void
  parser::set_debug_level (debug_level_type l)
  {
    yydebug_ = l;
  }
#endif // YYDEBUG

  parser::state_type
  parser::yy_lr_goto_state_ (state_type yystate, int yysym)
  {
    int yyr = yypgoto_[yysym - YYNTOKENS] + yystate;
    if (0 <= yyr && yyr <= yylast_ && yycheck_[yyr] == yystate)
      return yytable_[yyr];
    else
      return yydefgoto_[yysym - YYNTOKENS];
  }

  bool
  parser::yy_pact_value_is_default_ (int yyvalue)
  {
    return yyvalue == yypact_ninf_;
  }

  bool
  parser::yy_table_value_is_error_ (int yyvalue)
  {
    return yyvalue == yytable_ninf_;
  }

  int
  parser::operator() ()
  {
    return parse ();
  }

  int
  parser::parse ()
  {
    int yyn;
    /// Length of the RHS of the rule being reduced.
    int yylen = 0;

    // Error handling.
    int yynerrs_ = 0;
    int yyerrstatus_ = 0;

    /// The lookahead symbol.
    symbol_type yyla;

    /// The locations where the error started and ended.
    stack_symbol_type yyerror_range[3];

    /// The return value of parse ().
    int yyresult;

    /// Discard the LAC context in case there still is one left from a
    /// previous invocation.
    yy_lac_discard_ ("init");

#if YY_EXCEPTIONS
    try
#endif // YY_EXCEPTIONS
      {
    YYCDEBUG << "Starting parse\n";


    /* Initialize the stack.  The initial state will be set in
       yynewstate, since the latter expects the semantical and the
       location values to have been already stored, initialize these
       stacks with a primary value.  */
    yystack_.clear ();
    yypush_ (YY_NULLPTR, 0, YY_MOVE (yyla));

  /*-----------------------------------------------.
  | yynewstate -- push a new symbol on the stack.  |
  `-----------------------------------------------*/
  yynewstate:
    YYCDEBUG << "Entering state " << int (yystack_[0].state) << '\n';
    YY_STACK_PRINT ();

    // Accept?
    if (yystack_[0].state == yyfinal_)
      YYACCEPT;

    goto yybackup;


  /*-----------.
  | yybackup.  |
  `-----------*/
  yybackup:
    // Try to take a decision without lookahead.
    yyn = yypact_[+yystack_[0].state];
    if (yy_pact_value_is_default_ (yyn))
      goto yydefault;

    // Read a lookahead token.
    if (yyla.empty ())
      {
        YYCDEBUG << "Reading a token\n";
#if YY_EXCEPTIONS
        try
#endif // YY_EXCEPTIONS
          {
            symbol_type yylookahead (yylex (drv));
            yyla.move (yylookahead);
          }
#if YY_EXCEPTIONS
        catch (const syntax_error& yyexc)
          {
            YYCDEBUG << "Caught exception: " << yyexc.what() << '\n';
            error (yyexc);
            goto yyerrlab1;
          }
#endif // YY_EXCEPTIONS
      }
    YY_SYMBOL_PRINT ("Next token is", yyla);

    if (yyla.kind () == symbol_kind::S_YYerror)
    {
      // The scanner already issued an error message, process directly
      // to error recovery.  But do not keep the error token as
      // lookahead, it is too special and may lead us to an endless
      // loop in error recovery. */
      yyla.kind_ = symbol_kind::S_YYUNDEF;
      goto yyerrlab1;
    }

    /* If the proper action on seeing token YYLA.TYPE is to reduce or
       to detect an error, take that action.  */
    yyn += yyla.kind ();
    if (yyn < 0 || yylast_ < yyn || yycheck_[yyn] != yyla.kind ())
      {
        if (!yy_lac_establish_ (yyla.kind ()))
           goto yyerrlab;
        goto yydefault;
      }

    // Reduce or error.
    yyn = yytable_[yyn];
    if (yyn <= 0)
      {
        if (yy_table_value_is_error_ (yyn))
          goto yyerrlab;
        if (!yy_lac_establish_ (yyla.kind ()))
           goto yyerrlab;

        yyn = -yyn;
        goto yyreduce;
      }

    // Count tokens shifted since error; after three, turn off error status.
    if (yyerrstatus_)
      --yyerrstatus_;

    // Shift the lookahead token.
    yypush_ ("Shifting", state_type (yyn), YY_MOVE (yyla));
    yy_lac_discard_ ("shift");
    goto yynewstate;


  /*-----------------------------------------------------------.
  | yydefault -- do the default action for the current state.  |
  `-----------------------------------------------------------*/
  yydefault:
    yyn = yydefact_[+yystack_[0].state];
    if (yyn == 0)
      goto yyerrlab;
    goto yyreduce;


  /*-----------------------------.
  | yyreduce -- do a reduction.  |
  `-----------------------------*/
  yyreduce:
    yylen = yyr2_[yyn];
    {
      stack_symbol_type yylhs;
      yylhs.state = yy_lr_goto_state_ (yystack_[yylen].state, yyr1_[yyn]);
      /* Variants are always initialized to an empty instance of the
         correct type. The default '$$ = $1' action is NOT applied
         when using variants.  */
      switch (yyr1_[yyn])
    {
      case symbol_kind::S_assign: // assign
      case symbol_kind::S_import: // import
      case symbol_kind::S_reduce: // reduce
        yylhs.value.emplace< AssignPtr > ();
        break;

      case symbol_kind::S_expr: // expr
      case symbol_kind::S_mono_expr: // mono_expr
        yylhs.value.emplace< ExprPtr > ();
        break;

      case symbol_kind::S_json_array: // json_array
      case symbol_kind::S_json_elems: // json_elems
        yylhs.value.emplace< JSONArray > ();
        break;

      case symbol_kind::S_json_field: // json_field
        yylhs.value.emplace< JSONField > ();
        break;

      case symbol_kind::S_json_object: // json_object
      case symbol_kind::S_json_fields: // json_fields
        yylhs.value.emplace< JSONObject > ();
        break;

      case symbol_kind::S_json_value: // json_value
        yylhs.value.emplace< JSONValue > ();
        break;

      case symbol_kind::S_decl: // decl
      case symbol_kind::S_arg_decl: // arg_decl
        yylhs.value.emplace< TypeAssignPtr > ();
        break;

      case symbol_kind::S_type_name: // type_name
      case symbol_kind::S_ret_spec: // ret_spec
        yylhs.value.emplace< TypeData > ();
        break;

      case symbol_kind::S_NUMBER: // "number"
      case symbol_kind::S_number: // number
        yylhs.value.emplace< double > ();
        break;

      case symbol_kind::S_chart: // chart
        yylhs.value.emplace< std::shared_ptr<ChartAssign>  > ();
        break;

      case symbol_kind::S_table: // table
        yylhs.value.emplace< std::shared_ptr<TableAssign>  > ();
        break;

      case symbol_kind::S_IDENT: // "identifier"
      case symbol_kind::S_STRING: // "string"
      case symbol_kind::S_nest_name: // nest_name
        yylhs.value.emplace< std::string > ();
        break;

      case symbol_kind::S_table_assigns: // table_assigns
        yylhs.value.emplace< std::vector<AssignPtr>  > ();
        break;

      case symbol_kind::S_arg_list: // arg_list
      case symbol_kind::S_args: // args
        yylhs.value.emplace< std::vector<ExprPtr>  > ();
        break;

      case symbol_kind::S_arg_decls: // arg_decls
      case symbol_kind::S_arg_decl_list: // arg_decl_list
        yylhs.value.emplace< std::vector<TypeAssignPtr>  > ();
        break;

      case symbol_kind::S_type_params: // type_params
        yylhs.value.emplace< std::vector<TypeData>  > ();
        break;

      case symbol_kind::S_assigns: // assigns
        yylhs.value.emplace< std::vector<std::shared_ptr<TopAssign> >  > ();
        break;

      case symbol_kind::S_reduce_args: // reduce_args
      case symbol_kind::S_extension: // extension
      case symbol_kind::S_ext_list: // ext_list
        yylhs.value.emplace< std::vector<std::string>  > ();
        break;

      default:
        break;
    }


      // Default location.
      {
        stack_type::slice range (yystack_, yylen);
        YYLLOC_DEFAULT (yylhs.location, range, yylen);
        yyerror_range[1].location = yylhs.location;
      }

      // Perform the reduction.
      YY_REDUCE_PRINT (yyn);
#if YY_EXCEPTIONS
      try
#endif // YY_EXCEPTIONS
        {
          switch (yyn)
            {
  case 2: // first: START_EXPR expr
#line 261 "formula_parser.ypp"
                            {drv.setFormula(YY_MOVE (yystack_[0].value.as < ExprPtr > ()));}
#line 999 "formula_parser.tab.cpp"
    break;

  case 3: // first: START_ASSIGN assigns
#line 262 "formula_parser.ypp"
                            {drv.setAssign(YY_MOVE (yystack_[0].value.as < std::vector<std::shared_ptr<TopAssign> >  > ()));}
#line 1005 "formula_parser.tab.cpp"
    break;

  case 4: // first: START_TABLE table_assigns
#line 263 "formula_parser.ypp"
                            {drv.setTable(TableAssign::make(yystack_[0].location, "", {}, YY_MOVE (yystack_[0].value.as < std::vector<AssignPtr>  > ())));}
#line 1011 "formula_parser.tab.cpp"
    break;

  case 5: // expr: expr "+" expr
#line 267 "formula_parser.ypp"
                            {yylhs.value.as < ExprPtr > () = LAX_BINARY_EXP(yylhs.location, PLUS, YY_MOVE (yystack_[2].value.as < ExprPtr > ()), YY_MOVE (yystack_[0].value.as < ExprPtr > ()));}
#line 1017 "formula_parser.tab.cpp"
    break;

  case 6: // expr: expr "-" expr
#line 268 "formula_parser.ypp"
                            {yylhs.value.as < ExprPtr > () = LAX_BINARY_EXP(yylhs.location, MINUS, YY_MOVE (yystack_[2].value.as < ExprPtr > ()), YY_MOVE (yystack_[0].value.as < ExprPtr > ()));}
#line 1023 "formula_parser.tab.cpp"
    break;

  case 7: // expr: expr "*" expr
#line 269 "formula_parser.ypp"
                            {yylhs.value.as < ExprPtr > () = LAX_BINARY_EXP(yylhs.location, STAR, YY_MOVE (yystack_[2].value.as < ExprPtr > ()), YY_MOVE (yystack_[0].value.as < ExprPtr > ()));}
#line 1029 "formula_parser.tab.cpp"
    break;

  case 8: // expr: expr "/" expr
#line 270 "formula_parser.ypp"
                            {yylhs.value.as < ExprPtr > () = LAX_BINARY_EXP(yylhs.location, SLASH, YY_MOVE (yystack_[2].value.as < ExprPtr > ()), YY_MOVE (yystack_[0].value.as < ExprPtr > ()));}
#line 1035 "formula_parser.tab.cpp"
    break;

  case 9: // expr: expr "^" expr
#line 271 "formula_parser.ypp"
                            {yylhs.value.as < ExprPtr > () = LAX_BINARY_EXP(yylhs.location, EXP, YY_MOVE (yystack_[2].value.as < ExprPtr > ()), YY_MOVE (yystack_[0].value.as < ExprPtr > ()));}
#line 1041 "formula_parser.tab.cpp"
    break;

  case 10: // expr: expr "<" expr
#line 272 "formula_parser.ypp"
                            {yylhs.value.as < ExprPtr > () = LAX_BINARY_EXP(yylhs.location, LESS, YY_MOVE (yystack_[2].value.as < ExprPtr > ()), YY_MOVE (yystack_[0].value.as < ExprPtr > ()));}
#line 1047 "formula_parser.tab.cpp"
    break;

  case 11: // expr: expr ">" expr
#line 273 "formula_parser.ypp"
                            {yylhs.value.as < ExprPtr > () = LAX_BINARY_EXP(yylhs.location, GREATER, YY_MOVE (yystack_[2].value.as < ExprPtr > ()), YY_MOVE (yystack_[0].value.as < ExprPtr > ()));}
#line 1053 "formula_parser.tab.cpp"
    break;

  case 12: // expr: expr "=" expr
#line 274 "formula_parser.ypp"
                            {yylhs.value.as < ExprPtr > () = LAX_BINARY_EXP(yylhs.location, EQUAL, YY_MOVE (yystack_[2].value.as < ExprPtr > ()), YY_MOVE (yystack_[0].value.as < ExprPtr > ()));}
#line 1059 "formula_parser.tab.cpp"
    break;

  case 13: // expr: expr "<=" expr
#line 275 "formula_parser.ypp"
                            {yylhs.value.as < ExprPtr > () = LAX_BINARY_EXP(yylhs.location, LESS_EQUAL, YY_MOVE (yystack_[2].value.as < ExprPtr > ()), YY_MOVE (yystack_[0].value.as < ExprPtr > ()));}
#line 1065 "formula_parser.tab.cpp"
    break;

  case 14: // expr: expr ">=" expr
#line 276 "formula_parser.ypp"
                            {yylhs.value.as < ExprPtr > () = LAX_BINARY_EXP(yylhs.location, GREATER_EQUAL, YY_MOVE (yystack_[2].value.as < ExprPtr > ()), YY_MOVE (yystack_[0].value.as < ExprPtr > ()));}
#line 1071 "formula_parser.tab.cpp"
    break;

  case 15: // expr: expr "!=" expr
#line 277 "formula_parser.ypp"
                            {yylhs.value.as < ExprPtr > () = LAX_BINARY_EXP(yylhs.location, NOT_EQUAL, YY_MOVE (yystack_[2].value.as < ExprPtr > ()), YY_MOVE (yystack_[0].value.as < ExprPtr > ()));}
#line 1077 "formula_parser.tab.cpp"
    break;

  case 16: // expr: expr "and" expr
#line 278 "formula_parser.ypp"
                            {yylhs.value.as < ExprPtr > () = LAX_BINARY_EXP(yylhs.location, AND, YY_MOVE (yystack_[2].value.as < ExprPtr > ()), YY_MOVE (yystack_[0].value.as < ExprPtr > ()));}
#line 1083 "formula_parser.tab.cpp"
    break;

  case 17: // expr: expr "or" expr
#line 279 "formula_parser.ypp"
                            {yylhs.value.as < ExprPtr > () = LAX_BINARY_EXP(yylhs.location, OR, YY_MOVE (yystack_[2].value.as < ExprPtr > ()), YY_MOVE (yystack_[0].value.as < ExprPtr > ()));}
#line 1089 "formula_parser.tab.cpp"
    break;

  case 18: // expr: "-" expr
#line 280 "formula_parser.ypp"
                            {yylhs.value.as < ExprPtr > () = LAX_UNARY_EXP(yylhs.location, MINUS, YY_MOVE (yystack_[0].value.as < ExprPtr > ()));}
#line 1095 "formula_parser.tab.cpp"
    break;

  case 19: // expr: "not" expr
#line 281 "formula_parser.ypp"
                            {yylhs.value.as < ExprPtr > () = LAX_UNARY_EXP(yylhs.location, NOT, YY_MOVE (yystack_[0].value.as < ExprPtr > ()));}
#line 1101 "formula_parser.tab.cpp"
    break;

  case 20: // expr: "string"
#line 282 "formula_parser.ypp"
                            {yylhs.value.as < ExprPtr > () = StringExpr::make(yylhs.location, YY_MOVE (yystack_[0].value.as < std::string > ()));}
#line 1107 "formula_parser.tab.cpp"
    break;

  case 21: // expr: number
#line 283 "formula_parser.ypp"
                            {yylhs.value.as < ExprPtr > () = NumExpr::make(yylhs.location, YY_MOVE (yystack_[0].value.as < double > ()));}
#line 1113 "formula_parser.tab.cpp"
    break;

  case 22: // expr: mono_expr
#line 285 "formula_parser.ypp"
                            {yylhs.value.as < ExprPtr > () = YY_MOVE (yystack_[0].value.as < ExprPtr > ());}
#line 1119 "formula_parser.tab.cpp"
    break;

  case 23: // mono_expr: "$" nest_name
#line 289 "formula_parser.ypp"
                            {yylhs.value.as < ExprPtr > () = VarExpr::make(yylhs.location, std::pair(YY_MOVE (yystack_[0].value.as < std::string > ()), VarExprType::Exact));}
#line 1125 "formula_parser.tab.cpp"
    break;

  case 24: // mono_expr: "@" nest_name
#line 290 "formula_parser.ypp"
                            {yylhs.value.as < ExprPtr > () = VarExpr::make(yylhs.location, std::pair(YY_MOVE (yystack_[0].value.as < std::string > ()), VarExprType::ToScalar));}
#line 1131 "formula_parser.tab.cpp"
    break;

  case 25: // mono_expr: nest_name
#line 291 "formula_parser.ypp"
                            {yylhs.value.as < ExprPtr > () = VarExpr::make(yylhs.location, std::pair(YY_MOVE (yystack_[0].value.as < std::string > ()), VarExprType::Inferred));}
#line 1137 "formula_parser.tab.cpp"
    break;

  case 26: // mono_expr: mono_expr "(" arg_list ")"
#line 292 "formula_parser.ypp"
                            {yylhs.value.as < ExprPtr > () = CallExpr::make(yylhs.location, YY_MOVE (yystack_[3].value.as < ExprPtr > ()), YY_MOVE (yystack_[1].value.as < std::vector<ExprPtr>  > ()));}
#line 1143 "formula_parser.tab.cpp"
    break;

  case 27: // mono_expr: "(" expr ")"
#line 293 "formula_parser.ypp"
                            {yylhs.value.as < ExprPtr > () = YY_MOVE (yystack_[1].value.as < ExprPtr > ());}
#line 1149 "formula_parser.tab.cpp"
    break;

  case 28: // mono_expr: "(" error ")"
#line 294 "formula_parser.ypp"
                            {yylhs.value.as < ExprPtr > () = nullptr; yyerrok;}
#line 1155 "formula_parser.tab.cpp"
    break;

  case 29: // mono_expr: "|" expr "|"
#line 295 "formula_parser.ypp"
                            {yylhs.value.as < ExprPtr > () = LAX_UNARY_EXP(yylhs.location, VERTBAR, YY_MOVE (yystack_[1].value.as < ExprPtr > ()));;}
#line 1161 "formula_parser.tab.cpp"
    break;

  case 30: // mono_expr: "|" error "|"
#line 296 "formula_parser.ypp"
                            {yylhs.value.as < ExprPtr > () = nullptr; yyerrok;}
#line 1167 "formula_parser.tab.cpp"
    break;

  case 31: // mono_expr: mono_expr "[" expr "]"
#line 297 "formula_parser.ypp"
                            {yylhs.value.as < ExprPtr > () = ElemExpr::make(yylhs.location, YY_MOVE (yystack_[3].value.as < ExprPtr > ()), YY_MOVE (yystack_[1].value.as < ExprPtr > ()));}
#line 1173 "formula_parser.tab.cpp"
    break;

  case 32: // mono_expr: mono_expr "[" error "]"
#line 298 "formula_parser.ypp"
                            {yylhs.value.as < ExprPtr > () = nullptr; yyerrok;}
#line 1179 "formula_parser.tab.cpp"
    break;

  case 33: // mono_expr: "fn" "(" arg_decls ")" ret_spec "{" expr "}"
#line 300 "formula_parser.ypp"
                            {yylhs.value.as < ExprPtr > () = FuncLiteral::make(yylhs.location, YY_MOVE (yystack_[3].value.as < TypeData > ()), YY_MOVE (yystack_[5].value.as < std::vector<TypeAssignPtr>  > ()), YY_MOVE (yystack_[1].value.as < ExprPtr > ()));}
#line 1185 "formula_parser.tab.cpp"
    break;

  case 34: // mono_expr: "fn" "(" arg_decls ")" ret_spec "{" error "}"
#line 302 "formula_parser.ypp"
                            {yylhs.value.as < ExprPtr > () = nullptr; yyerrok;}
#line 1191 "formula_parser.tab.cpp"
    break;

  case 35: // arg_list: %empty
#line 306 "formula_parser.ypp"
                            {yylhs.value.as < std::vector<ExprPtr>  > () = std::vector<FormulaExpr::Ptr>();}
#line 1197 "formula_parser.tab.cpp"
    break;

  case 36: // arg_list: args
#line 307 "formula_parser.ypp"
                            {yylhs.value.as < std::vector<ExprPtr>  > () = YY_MOVE (yystack_[0].value.as < std::vector<ExprPtr>  > ());}
#line 1203 "formula_parser.tab.cpp"
    break;

  case 37: // args: expr
#line 311 "formula_parser.ypp"
                            {yylhs.value.as < std::vector<ExprPtr>  > ().push_back(YY_MOVE (yystack_[0].value.as < ExprPtr > ()));}
#line 1209 "formula_parser.tab.cpp"
    break;

  case 38: // args: error
#line 312 "formula_parser.ypp"
                            {yylhs.value.as < std::vector<ExprPtr>  > () = std::vector<ExprPtr>();}
#line 1215 "formula_parser.tab.cpp"
    break;

  case 39: // args: args "," expr
#line 313 "formula_parser.ypp"
                            {yylhs.value.as < std::vector<ExprPtr>  > () = YY_MOVE (yystack_[2].value.as < std::vector<ExprPtr>  > ()); yylhs.value.as < std::vector<ExprPtr>  > ().push_back(YY_MOVE (yystack_[0].value.as < ExprPtr > ()));}
#line 1221 "formula_parser.tab.cpp"
    break;

  case 40: // args: args "," error
#line 314 "formula_parser.ypp"
                            {yylhs.value.as < std::vector<ExprPtr>  > () = YY_MOVE (yystack_[2].value.as < std::vector<ExprPtr>  > ());}
#line 1227 "formula_parser.tab.cpp"
    break;

  case 41: // number: "number"
#line 317 "formula_parser.ypp"
                            {yylhs.value.as < double > () = YY_MOVE (yystack_[0].value.as < double > ());}
#line 1233 "formula_parser.tab.cpp"
    break;

  case 42: // arg_decls: %empty
#line 320 "formula_parser.ypp"
                            {yylhs.value.as < std::vector<TypeAssignPtr>  > () = std::vector<TypeAssignPtr>();}
#line 1239 "formula_parser.tab.cpp"
    break;

  case 43: // arg_decls: arg_decl_list
#line 321 "formula_parser.ypp"
                            {yylhs.value.as < std::vector<TypeAssignPtr>  > () = YY_MOVE (yystack_[0].value.as < std::vector<TypeAssignPtr>  > ());}
#line 1245 "formula_parser.tab.cpp"
    break;

  case 44: // arg_decl_list: arg_decl
#line 325 "formula_parser.ypp"
                            {yylhs.value.as < std::vector<TypeAssignPtr>  > ().push_back(YY_MOVE (yystack_[0].value.as < TypeAssignPtr > ()));}
#line 1251 "formula_parser.tab.cpp"
    break;

  case 45: // arg_decl_list: error
#line 326 "formula_parser.ypp"
                            {yylhs.value.as < std::vector<TypeAssignPtr>  > () = std::vector<TypeAssignPtr>();}
#line 1257 "formula_parser.tab.cpp"
    break;

  case 46: // arg_decl_list: arg_decl_list "," arg_decl
#line 327 "formula_parser.ypp"
                            {yylhs.value.as < std::vector<TypeAssignPtr>  > () = YY_MOVE (yystack_[2].value.as < std::vector<TypeAssignPtr>  > ()); yylhs.value.as < std::vector<TypeAssignPtr>  > ().push_back(YY_MOVE (yystack_[0].value.as < TypeAssignPtr > ()));}
#line 1263 "formula_parser.tab.cpp"
    break;

  case 47: // arg_decl_list: arg_decl_list "," error
#line 328 "formula_parser.ypp"
                            {yylhs.value.as < std::vector<TypeAssignPtr>  > () = YY_MOVE (yystack_[2].value.as < std::vector<TypeAssignPtr>  > ());}
#line 1269 "formula_parser.tab.cpp"
    break;

  case 48: // arg_decl: decl
#line 332 "formula_parser.ypp"
                            {yylhs.value.as < TypeAssignPtr > () = YY_MOVE (yystack_[0].value.as < TypeAssignPtr > ());}
#line 1275 "formula_parser.tab.cpp"
    break;

  case 49: // arg_decl: "identifier"
#line 333 "formula_parser.ypp"
                            {yylhs.value.as < TypeAssignPtr > () = TypeAssign::make(yylhs.location, YY_MOVE (yystack_[0].value.as < std::string > ()), "Number");}
#line 1281 "formula_parser.tab.cpp"
    break;

  case 50: // ret_spec: %empty
#line 337 "formula_parser.ypp"
                            {yylhs.value.as < TypeData > () = TypeData("");}
#line 1287 "formula_parser.tab.cpp"
    break;

  case 51: // ret_spec: "->" type_name
#line 338 "formula_parser.ypp"
                            {yylhs.value.as < TypeData > () = YY_MOVE (yystack_[0].value.as < TypeData > ());}
#line 1293 "formula_parser.tab.cpp"
    break;

  case 52: // nest_name: "identifier"
#line 354 "formula_parser.ypp"
                            {yylhs.value.as < std::string > () = YY_MOVE (yystack_[0].value.as < std::string > ());}
#line 1299 "formula_parser.tab.cpp"
    break;

  case 53: // nest_name: nest_name "." "identifier"
#line 355 "formula_parser.ypp"
                            {yylhs.value.as < std::string > () = YY_MOVE (yystack_[2].value.as < std::string > ()) + "." + YY_MOVE (yystack_[0].value.as < std::string > ());}
#line 1305 "formula_parser.tab.cpp"
    break;

  case 54: // type_name: nest_name
#line 359 "formula_parser.ypp"
                            {yylhs.value.as < TypeData > () = TypeData(YY_MOVE (yystack_[0].value.as < std::string > ()));}
#line 1311 "formula_parser.tab.cpp"
    break;

  case 55: // type_name: nest_name "<" type_params ">"
#line 360 "formula_parser.ypp"
                                {yylhs.value.as < TypeData > () = TypeData(YY_MOVE (yystack_[3].value.as < std::string > ()), YY_MOVE (yystack_[1].value.as < std::vector<TypeData>  > ()));}
#line 1317 "formula_parser.tab.cpp"
    break;

  case 56: // type_name: type_name "(" type_params ")"
#line 361 "formula_parser.ypp"
                                {YY_MOVE (yystack_[1].value.as < std::vector<TypeData>  > ()).insert(YY_MOVE (yystack_[1].value.as < std::vector<TypeData>  > ()).begin(), YY_MOVE (yystack_[3].value.as < TypeData > ())); yylhs.value.as < TypeData > () = TypeData("Function", YY_MOVE (yystack_[1].value.as < std::vector<TypeData>  > ()));}
#line 1323 "formula_parser.tab.cpp"
    break;

  case 57: // type_params: type_name
#line 365 "formula_parser.ypp"
                            {yylhs.value.as < std::vector<TypeData>  > ().push_back(YY_MOVE (yystack_[0].value.as < TypeData > ()));}
#line 1329 "formula_parser.tab.cpp"
    break;

  case 58: // type_params: error
#line 366 "formula_parser.ypp"
                            {yylhs.value.as < std::vector<TypeData>  > () = std::vector<TypeData>();}
#line 1335 "formula_parser.tab.cpp"
    break;

  case 59: // type_params: type_params "," type_name
#line 367 "formula_parser.ypp"
                            {yylhs.value.as < std::vector<TypeData>  > () = YY_MOVE (yystack_[2].value.as < std::vector<TypeData>  > ()); yylhs.value.as < std::vector<TypeData>  > ().push_back(YY_MOVE (yystack_[0].value.as < TypeData > ()));}
#line 1341 "formula_parser.tab.cpp"
    break;

  case 60: // type_params: type_params "," error
#line 368 "formula_parser.ypp"
                            {yylhs.value.as < std::vector<TypeData>  > () = YY_MOVE (yystack_[2].value.as < std::vector<TypeData>  > ());}
#line 1347 "formula_parser.tab.cpp"
    break;

  case 61: // decl: "identifier" "as" type_name
#line 371 "formula_parser.ypp"
                            {yylhs.value.as < TypeAssignPtr > () = TypeAssign::make(yylhs.location, YY_MOVE (yystack_[2].value.as < std::string > ()), YY_MOVE (yystack_[0].value.as < TypeData > ()));}
#line 1353 "formula_parser.tab.cpp"
    break;

  case 62: // assign: "identifier" ":" expr
#line 374 "formula_parser.ypp"
                            {yylhs.value.as < AssignPtr > () = ExprAssign::make(yylhs.location, YY_MOVE (yystack_[2].value.as < std::string > ()), std::pair(YY_MOVE (yystack_[0].value.as < ExprPtr > ()), false));}
#line 1359 "formula_parser.tab.cpp"
    break;

  case 63: // assign: "scalar" "identifier" ":" expr
#line 375 "formula_parser.ypp"
                            {yylhs.value.as < AssignPtr > () = ExprAssign::make(yylhs.location, YY_MOVE (yystack_[2].value.as < std::string > ()), std::pair(YY_MOVE (yystack_[0].value.as < ExprPtr > ()), true));}
#line 1365 "formula_parser.tab.cpp"
    break;

  case 64: // assign: decl
#line 376 "formula_parser.ypp"
                            {yylhs.value.as < AssignPtr > () = YY_MOVE (yystack_[0].value.as < TypeAssignPtr > ());}
#line 1371 "formula_parser.tab.cpp"
    break;

  case 65: // assign: "scalar" decl
#line 377 "formula_parser.ypp"
                            {yylhs.value.as < AssignPtr > () = ScalarTypeAssign::make(yylhs.location,
                                static_cast<std::string>(YY_MOVE (yystack_[0].value.as < TypeAssignPtr > ())->getName()),
                                YY_MOVE (yystack_[0].value.as < TypeAssignPtr > ())->getValue());}
#line 1379 "formula_parser.tab.cpp"
    break;

  case 66: // import: "import" "string"
#line 383 "formula_parser.ypp"
                            {yylhs.value.as < AssignPtr > () = ImportAssign::make(yylhs.location, "", YY_MOVE (yystack_[0].value.as < std::string > ()));}
#line 1385 "formula_parser.tab.cpp"
    break;

  case 67: // import: "import" "string" "as" "identifier"
#line 384 "formula_parser.ypp"
                            {yylhs.value.as < AssignPtr > () = ImportAssign::make(yylhs.location, YY_MOVE (yystack_[0].value.as < std::string > ()), YY_MOVE (yystack_[2].value.as < std::string > ()));}
#line 1391 "formula_parser.tab.cpp"
    break;

  case 68: // reduce: nest_name "reduces" "(" reduce_args ")"
#line 389 "formula_parser.ypp"
                            {yylhs.value.as < AssignPtr > () = ReduceAssign::make(yylhs.location, YY_MOVE (yystack_[4].value.as < std::string > ()), YY_MOVE (yystack_[1].value.as < std::vector<std::string>  > ()));}
#line 1397 "formula_parser.tab.cpp"
    break;

  case 69: // reduce_args: "identifier"
#line 393 "formula_parser.ypp"
                            {yylhs.value.as < std::vector<std::string>  > ().push_back(YY_MOVE (yystack_[0].value.as < std::string > ()));}
#line 1403 "formula_parser.tab.cpp"
    break;

  case 70: // reduce_args: error
#line 394 "formula_parser.ypp"
                            {yylhs.value.as < std::vector<std::string>  > () = std::vector<std::string>();}
#line 1409 "formula_parser.tab.cpp"
    break;

  case 71: // reduce_args: reduce_args "," "identifier"
#line 395 "formula_parser.ypp"
                            {yylhs.value.as < std::vector<std::string>  > () = YY_MOVE (yystack_[2].value.as < std::vector<std::string>  > ()); yylhs.value.as < std::vector<std::string>  > ().push_back(YY_MOVE (yystack_[0].value.as < std::string > ()));}
#line 1415 "formula_parser.tab.cpp"
    break;

  case 72: // reduce_args: reduce_args "," error
#line 396 "formula_parser.ypp"
                            {yylhs.value.as < std::vector<std::string>  > () = YY_MOVE (yystack_[2].value.as < std::vector<std::string>  > ());}
#line 1421 "formula_parser.tab.cpp"
    break;

  case 73: // assigns: %empty
#line 400 "formula_parser.ypp"
                            {yylhs.value.as < std::vector<std::shared_ptr<TopAssign> >  > () = std::vector<TopAssign::Ptr>();}
#line 1427 "formula_parser.tab.cpp"
    break;

  case 74: // assigns: assigns chart
#line 401 "formula_parser.ypp"
                            {yylhs.value.as < std::vector<std::shared_ptr<TopAssign> >  > () = YY_MOVE (yystack_[1].value.as < std::vector<std::shared_ptr<TopAssign> >  > ()); yylhs.value.as < std::vector<std::shared_ptr<TopAssign> >  > ().push_back(YY_MOVE (yystack_[0].value.as < std::shared_ptr<ChartAssign>  > ()));}
#line 1433 "formula_parser.tab.cpp"
    break;

  case 75: // assigns: assigns table
#line 402 "formula_parser.ypp"
                            {yylhs.value.as < std::vector<std::shared_ptr<TopAssign> >  > () = YY_MOVE (yystack_[1].value.as < std::vector<std::shared_ptr<TopAssign> >  > ()); yylhs.value.as < std::vector<std::shared_ptr<TopAssign> >  > ().push_back(YY_MOVE (yystack_[0].value.as < std::shared_ptr<TableAssign>  > ()));}
#line 1439 "formula_parser.tab.cpp"
    break;

  case 76: // assigns: assigns import
#line 403 "formula_parser.ypp"
                            {yylhs.value.as < std::vector<std::shared_ptr<TopAssign> >  > () = YY_MOVE (yystack_[1].value.as < std::vector<std::shared_ptr<TopAssign> >  > ()); yylhs.value.as < std::vector<std::shared_ptr<TopAssign> >  > ().push_back(makeTopImport(YY_MOVE (yystack_[0].value.as < AssignPtr > ())));}
#line 1445 "formula_parser.tab.cpp"
    break;

  case 77: // chart: "chart" "identifier" extension json_object
#line 408 "formula_parser.ypp"
                            {yylhs.value.as < std::shared_ptr<ChartAssign>  > () = ChartAssign::make(yylhs.location, YY_MOVE (yystack_[2].value.as < std::string > ()), YY_MOVE (yystack_[1].value.as < std::vector<std::string>  > ()), YY_MOVE (yystack_[0].value.as < JSONObject > ()));}
#line 1451 "formula_parser.tab.cpp"
    break;

  case 78: // table: "table" "identifier" extension "{" table_assigns "}"
#line 413 "formula_parser.ypp"
                            {yylhs.value.as < std::shared_ptr<TableAssign>  > () = TableAssign::make(yylhs.location, YY_MOVE (yystack_[4].value.as < std::string > ()), YY_MOVE (yystack_[3].value.as < std::vector<std::string>  > ()), YY_MOVE (yystack_[1].value.as < std::vector<AssignPtr>  > ()));}
#line 1457 "formula_parser.tab.cpp"
    break;

  case 79: // table: "const" "identifier" extension "{" table_assigns "}"
#line 415 "formula_parser.ypp"
                            {yylhs.value.as < std::shared_ptr<TableAssign>  > () = TableAssign::makeConst(yylhs.location, YY_MOVE (yystack_[4].value.as < std::string > ()), YY_MOVE (yystack_[3].value.as < std::vector<std::string>  > ()), YY_MOVE (yystack_[1].value.as < std::vector<AssignPtr>  > ()));}
#line 1463 "formula_parser.tab.cpp"
    break;

  case 80: // extension: %empty
#line 419 "formula_parser.ypp"
                            {yylhs.value.as < std::vector<std::string>  > () = std::vector<std::string>();}
#line 1469 "formula_parser.tab.cpp"
    break;

  case 81: // extension: "extends" ext_list
#line 420 "formula_parser.ypp"
                            {yylhs.value.as < std::vector<std::string>  > () = YY_MOVE (yystack_[0].value.as < std::vector<std::string>  > ());}
#line 1475 "formula_parser.tab.cpp"
    break;

  case 82: // ext_list: ext_list "," "identifier"
#line 424 "formula_parser.ypp"
                            {yylhs.value.as < std::vector<std::string>  > () = YY_MOVE (yystack_[2].value.as < std::vector<std::string>  > ()); yylhs.value.as < std::vector<std::string>  > ().push_back(YY_MOVE (yystack_[0].value.as < std::string > ()));}
#line 1481 "formula_parser.tab.cpp"
    break;

  case 83: // ext_list: "identifier"
#line 425 "formula_parser.ypp"
                            {yylhs.value.as < std::vector<std::string>  > ().push_back(YY_MOVE (yystack_[0].value.as < std::string > ()));}
#line 1487 "formula_parser.tab.cpp"
    break;

  case 84: // ext_list: ext_list "," error
#line 426 "formula_parser.ypp"
                            {yylhs.value.as < std::vector<std::string>  > () = YY_MOVE (yystack_[2].value.as < std::vector<std::string>  > ()); yyerrok;}
#line 1493 "formula_parser.tab.cpp"
    break;

  case 85: // ext_list: ext_list ","
#line 427 "formula_parser.ypp"
                            {yylhs.value.as < std::vector<std::string>  > () = YY_MOVE (yystack_[1].value.as < std::vector<std::string>  > ());}
#line 1499 "formula_parser.tab.cpp"
    break;

  case 86: // json_object: "{" json_fields "}"
#line 431 "formula_parser.ypp"
                            {yylhs.value.as < JSONObject > () = YY_MOVE (yystack_[1].value.as < JSONObject > ());}
#line 1505 "formula_parser.tab.cpp"
    break;

  case 87: // json_fields: json_field
#line 435 "formula_parser.ypp"
                            {yylhs.value.as < JSONObject > () = JSONObject(YY_MOVE (yystack_[0].value.as < JSONField > ()));}
#line 1511 "formula_parser.tab.cpp"
    break;

  case 88: // json_fields: json_fields json_field
#line 436 "formula_parser.ypp"
                            {yylhs.value.as < JSONObject > () = YY_MOVE (yystack_[1].value.as < JSONObject > ()); yylhs.value.as < JSONObject > ().append(YY_MOVE (yystack_[0].value.as < JSONField > ()), yylhs.location);}
#line 1517 "formula_parser.tab.cpp"
    break;

  case 89: // json_fields: json_fields error
#line 437 "formula_parser.ypp"
                            {yylhs.value.as < JSONObject > () = YY_MOVE (yystack_[1].value.as < JSONObject > ());}
#line 1523 "formula_parser.tab.cpp"
    break;

  case 90: // json_fields: error
#line 438 "formula_parser.ypp"
                            {yylhs.value.as < JSONObject > () = JSONObject();}
#line 1529 "formula_parser.tab.cpp"
    break;

  case 91: // json_field: "string" ":" json_value
#line 442 "formula_parser.ypp"
                            {yylhs.value.as < JSONField > () = JSONField(ICaseString(YY_MOVE (yystack_[2].value.as < std::string > ())), YY_MOVE (yystack_[0].value.as < JSONValue > ()));}
#line 1535 "formula_parser.tab.cpp"
    break;

  case 92: // json_field: "identifier" ":" json_value
#line 443 "formula_parser.ypp"
                            {yylhs.value.as < JSONField > () = JSONField(ICaseString(YY_MOVE (yystack_[2].value.as < std::string > ())), YY_MOVE (yystack_[0].value.as < JSONValue > ()));}
#line 1541 "formula_parser.tab.cpp"
    break;

  case 93: // json_field: "%" "identifier" ":" json_value
#line 444 "formula_parser.ypp"
                            {yylhs.value.as < JSONField > () = JSONField(ICaseString("%" + YY_MOVE (yystack_[2].value.as < std::string > ())), YY_MOVE (yystack_[0].value.as < JSONValue > ()));}
#line 1547 "formula_parser.tab.cpp"
    break;

  case 94: // json_array: "[" json_elems "]"
#line 448 "formula_parser.ypp"
                            {yylhs.value.as < JSONArray > () = YY_MOVE (yystack_[1].value.as < JSONArray > ());}
#line 1553 "formula_parser.tab.cpp"
    break;

  case 95: // json_array: "[" "]"
#line 449 "formula_parser.ypp"
                            {yylhs.value.as < JSONArray > () = JSONArray();}
#line 1559 "formula_parser.tab.cpp"
    break;

  case 96: // json_elems: json_value
#line 453 "formula_parser.ypp"
                            {yylhs.value.as < JSONArray > () = JSONArray(YY_MOVE (yystack_[0].value.as < JSONValue > ()));}
#line 1565 "formula_parser.tab.cpp"
    break;

  case 97: // json_elems: json_elems "," json_value
#line 454 "formula_parser.ypp"
                            {yylhs.value.as < JSONArray > () = YY_MOVE (yystack_[2].value.as < JSONArray > ()); yylhs.value.as < JSONArray > ().append(YY_MOVE (yystack_[0].value.as < JSONValue > ()));}
#line 1571 "formula_parser.tab.cpp"
    break;

  case 98: // json_elems: json_elems "," error
#line 455 "formula_parser.ypp"
                            {yylhs.value.as < JSONArray > () = YY_MOVE (yystack_[2].value.as < JSONArray > ());}
#line 1577 "formula_parser.tab.cpp"
    break;

  case 99: // json_elems: error
#line 456 "formula_parser.ypp"
                            {yylhs.value.as < JSONArray > () = JSONArray();}
#line 1583 "formula_parser.tab.cpp"
    break;

  case 100: // json_value: "string"
#line 460 "formula_parser.ypp"
                            {yylhs.value.as < JSONValue > () = JSONValue(YY_MOVE (yystack_[0].value.as < std::string > ()));}
#line 1589 "formula_parser.tab.cpp"
    break;

  case 101: // json_value: "identifier"
#line 461 "formula_parser.ypp"
                            {yylhs.value.as < JSONValue > () = JSONValue(YY_MOVE (yystack_[0].value.as < std::string > ()));}
#line 1595 "formula_parser.tab.cpp"
    break;

  case 102: // json_value: json_array
#line 462 "formula_parser.ypp"
                            {yylhs.value.as < JSONValue > () = JSONValue(YY_MOVE (yystack_[0].value.as < JSONArray > ()));}
#line 1601 "formula_parser.tab.cpp"
    break;

  case 103: // json_value: json_object
#line 463 "formula_parser.ypp"
                            {yylhs.value.as < JSONValue > () = JSONValue(YY_MOVE (yystack_[0].value.as < JSONObject > ()));}
#line 1607 "formula_parser.tab.cpp"
    break;

  case 104: // table_assigns: %empty
#line 467 "formula_parser.ypp"
                            {yylhs.value.as < std::vector<AssignPtr>  > () = std::vector<FormulaAssign::Ptr>();}
#line 1613 "formula_parser.tab.cpp"
    break;

  case 105: // table_assigns: table_assigns assign
#line 468 "formula_parser.ypp"
                            {yylhs.value.as < std::vector<AssignPtr>  > () = YY_MOVE (yystack_[1].value.as < std::vector<AssignPtr>  > ()); yylhs.value.as < std::vector<AssignPtr>  > ().push_back(YY_MOVE (yystack_[0].value.as < AssignPtr > ()));}
#line 1619 "formula_parser.tab.cpp"
    break;

  case 106: // table_assigns: table_assigns import
#line 469 "formula_parser.ypp"
                            {yylhs.value.as < std::vector<AssignPtr>  > () = YY_MOVE (yystack_[1].value.as < std::vector<AssignPtr>  > ()); yylhs.value.as < std::vector<AssignPtr>  > ().push_back(YY_MOVE (yystack_[0].value.as < AssignPtr > ()));}
#line 1625 "formula_parser.tab.cpp"
    break;

  case 107: // table_assigns: table_assigns reduce
#line 470 "formula_parser.ypp"
                            {yylhs.value.as < std::vector<AssignPtr>  > () = YY_MOVE (yystack_[1].value.as < std::vector<AssignPtr>  > ()); yylhs.value.as < std::vector<AssignPtr>  > ().push_back(YY_MOVE (yystack_[0].value.as < AssignPtr > ()));}
#line 1631 "formula_parser.tab.cpp"
    break;

  case 108: // table_assigns: table_assigns error
#line 471 "formula_parser.ypp"
                            {yylhs.value.as < std::vector<AssignPtr>  > () = YY_MOVE (yystack_[1].value.as < std::vector<AssignPtr>  > ());}
#line 1637 "formula_parser.tab.cpp"
    break;


#line 1641 "formula_parser.tab.cpp"

            default:
              break;
            }
        }
#if YY_EXCEPTIONS
      catch (const syntax_error& yyexc)
        {
          YYCDEBUG << "Caught exception: " << yyexc.what() << '\n';
          error (yyexc);
          YYERROR;
        }
#endif // YY_EXCEPTIONS
      YY_SYMBOL_PRINT ("-> $$ =", yylhs);
      yypop_ (yylen);
      yylen = 0;

      // Shift the result of the reduction.
      yypush_ (YY_NULLPTR, YY_MOVE (yylhs));
    }
    goto yynewstate;


  /*--------------------------------------.
  | yyerrlab -- here on detecting error.  |
  `--------------------------------------*/
  yyerrlab:
    // If not already recovering from an error, report this error.
    if (!yyerrstatus_)
      {
        ++yynerrs_;
        context yyctx (*this, yyla);
        std::string msg = yysyntax_error_ (yyctx);
        error (yyla.location, YY_MOVE (msg));
      }


    yyerror_range[1].location = yyla.location;
    if (yyerrstatus_ == 3)
      {
        /* If just tried and failed to reuse lookahead token after an
           error, discard it.  */

        // Return failure if at end of input.
        if (yyla.kind () == symbol_kind::S_YYEOF)
          YYABORT;
        else if (!yyla.empty ())
          {
            yy_destroy_ ("Error: discarding", yyla);
            yyla.clear ();
          }
      }

    // Else will try to reuse lookahead token after shifting the error token.
    goto yyerrlab1;


  /*---------------------------------------------------.
  | yyerrorlab -- error raised explicitly by YYERROR.  |
  `---------------------------------------------------*/
  yyerrorlab:
    /* Pacify compilers when the user code never invokes YYERROR and
       the label yyerrorlab therefore never appears in user code.  */
    if (false)
      YYERROR;

    /* Do not reclaim the symbols of the rule whose action triggered
       this YYERROR.  */
    yypop_ (yylen);
    yylen = 0;
    YY_STACK_PRINT ();
    goto yyerrlab1;


  /*-------------------------------------------------------------.
  | yyerrlab1 -- common code for both syntax error and YYERROR.  |
  `-------------------------------------------------------------*/
  yyerrlab1:
    yyerrstatus_ = 3;   // Each real token shifted decrements this.
    // Pop stack until we find a state that shifts the error token.
    for (;;)
      {
        yyn = yypact_[+yystack_[0].state];
        if (!yy_pact_value_is_default_ (yyn))
          {
            yyn += symbol_kind::S_YYerror;
            if (0 <= yyn && yyn <= yylast_
                && yycheck_[yyn] == symbol_kind::S_YYerror)
              {
                yyn = yytable_[yyn];
                if (0 < yyn)
                  break;
              }
          }

        // Pop the current state because it cannot handle the error token.
        if (yystack_.size () == 1)
          YYABORT;

        yyerror_range[1].location = yystack_[0].location;
        yy_destroy_ ("Error: popping", yystack_[0]);
        yypop_ ();
        YY_STACK_PRINT ();
      }
    {
      stack_symbol_type error_token;

      yyerror_range[2].location = yyla.location;
      YYLLOC_DEFAULT (error_token.location, yyerror_range, 2);

      // Shift the error token.
      yy_lac_discard_ ("error recovery");
      error_token.state = state_type (yyn);
      yypush_ ("Shifting", YY_MOVE (error_token));
    }
    goto yynewstate;


  /*-------------------------------------.
  | yyacceptlab -- YYACCEPT comes here.  |
  `-------------------------------------*/
  yyacceptlab:
    yyresult = 0;
    goto yyreturn;


  /*-----------------------------------.
  | yyabortlab -- YYABORT comes here.  |
  `-----------------------------------*/
  yyabortlab:
    yyresult = 1;
    goto yyreturn;


  /*-----------------------------------------------------.
  | yyreturn -- parsing is finished, return the result.  |
  `-----------------------------------------------------*/
  yyreturn:
    if (!yyla.empty ())
      yy_destroy_ ("Cleanup: discarding lookahead", yyla);

    /* Do not reclaim the symbols of the rule whose action triggered
       this YYABORT or YYACCEPT.  */
    yypop_ (yylen);
    YY_STACK_PRINT ();
    while (1 < yystack_.size ())
      {
        yy_destroy_ ("Cleanup: popping", yystack_[0]);
        yypop_ ();
      }

    return yyresult;
  }
#if YY_EXCEPTIONS
    catch (...)
      {
        YYCDEBUG << "Exception caught: cleaning lookahead and stack\n";
        // Do not try to display the values of the reclaimed symbols,
        // as their printers might throw an exception.
        if (!yyla.empty ())
          yy_destroy_ (YY_NULLPTR, yyla);

        while (1 < yystack_.size ())
          {
            yy_destroy_ (YY_NULLPTR, yystack_[0]);
            yypop_ ();
          }
        throw;
      }
#endif // YY_EXCEPTIONS
  }

  void
  parser::error (const syntax_error& yyexc)
  {
    error (yyexc.location, yyexc.what ());
  }

  const char *
  parser::symbol_name (symbol_kind_type yysymbol)
  {
    static const char *const yy_sname[] =
    {
    "end of input", "error", "invalid token", "identifier", "string",
  "number", "+", "-", "*", "/", "^", "<", ">", "<=", ">=", "=", "!=",
  "and", "or", "not", "|", "(", ")", "{", "}", "[", "]", "$", "@", "%",
  ",", ".", ":", "->", "as", "fn", "import", "reduces", "scalar", "chart",
  "table", "const", "extends", "START_EXPR", "START_ASSIGN", "START_TABLE",
  "UMINUS", "$accept", "expr", "mono_expr", "arg_list", "args", "number",
  "nest_name", "type_name", "type_params", "decl", "assign", "import",
  "arg_decl", "reduce", "reduce_args", "extension", "ext_list",
  "table_assigns", "assigns", "arg_decls", "arg_decl_list", "ret_spec",
  "chart", "table", "json_object", "json_fields", "json_field",
  "json_array", "json_elems", "json_value", "first", YY_NULLPTR
    };
    return yy_sname[yysymbol];
  }



  // parser::context.
  parser::context::context (const parser& yyparser, const symbol_type& yyla)
    : yyparser_ (yyparser)
    , yyla_ (yyla)
  {}

  int
  parser::context::expected_tokens (symbol_kind_type yyarg[], int yyargn) const
  {
    // Actual number of expected tokens
    int yycount = 0;

#if YYDEBUG
    // Execute LAC once. We don't care if it is successful, we
    // only do it for the sake of debugging output.
    if (!yyparser_.yy_lac_established_)
      yyparser_.yy_lac_check_ (yyla_.kind ());
#endif

    for (int yyx = 0; yyx < YYNTOKENS; ++yyx)
      {
        symbol_kind_type yysym = YY_CAST (symbol_kind_type, yyx);
        if (yysym != symbol_kind::S_YYerror
            && yysym != symbol_kind::S_YYUNDEF
            && yyparser_.yy_lac_check_ (yysym))
          {
            if (!yyarg)
              ++yycount;
            else if (yycount == yyargn)
              return 0;
            else
              yyarg[yycount++] = yysym;
          }
      }
    if (yyarg && yycount == 0 && 0 < yyargn)
      yyarg[0] = symbol_kind::S_YYEMPTY;
    return yycount;
  }


  bool
  parser::yy_lac_check_ (symbol_kind_type yytoken) const
  {
    // Logically, the yylac_stack's lifetime is confined to this function.
    // Clear it, to get rid of potential left-overs from previous call.
    yylac_stack_.clear ();
    // Reduce until we encounter a shift and thereby accept the token.
#if YYDEBUG
    YYCDEBUG << "LAC: checking lookahead " << symbol_name (yytoken) << ':';
#endif
    std::ptrdiff_t lac_top = 0;
    while (true)
      {
        state_type top_state = (yylac_stack_.empty ()
                                ? yystack_[lac_top].state
                                : yylac_stack_.back ());
        int yyrule = yypact_[+top_state];
        if (yy_pact_value_is_default_ (yyrule)
            || (yyrule += yytoken) < 0 || yylast_ < yyrule
            || yycheck_[yyrule] != yytoken)
          {
            // Use the default action.
            yyrule = yydefact_[+top_state];
            if (yyrule == 0)
              {
                YYCDEBUG << " Err\n";
                return false;
              }
          }
        else
          {
            // Use the action from yytable.
            yyrule = yytable_[yyrule];
            if (yy_table_value_is_error_ (yyrule))
              {
                YYCDEBUG << " Err\n";
                return false;
              }
            if (0 < yyrule)
              {
                YYCDEBUG << " S" << yyrule << '\n';
                return true;
              }
            yyrule = -yyrule;
          }
        // By now we know we have to simulate a reduce.
        YYCDEBUG << " R" << yyrule - 1;
        // Pop the corresponding number of values from the stack.
        {
          std::ptrdiff_t yylen = yyr2_[yyrule];
          // First pop from the LAC stack as many tokens as possible.
          std::ptrdiff_t lac_size = std::ptrdiff_t (yylac_stack_.size ());
          if (yylen < lac_size)
            {
              yylac_stack_.resize (std::size_t (lac_size - yylen));
              yylen = 0;
            }
          else if (lac_size)
            {
              yylac_stack_.clear ();
              yylen -= lac_size;
            }
          // Only afterwards look at the main stack.
          // We simulate popping elements by incrementing lac_top.
          lac_top += yylen;
        }
        // Keep top_state in sync with the updated stack.
        top_state = (yylac_stack_.empty ()
                     ? yystack_[lac_top].state
                     : yylac_stack_.back ());
        // Push the resulting state of the reduction.
        state_type state = yy_lr_goto_state_ (top_state, yyr1_[yyrule]);
        YYCDEBUG << " G" << int (state);
        yylac_stack_.push_back (state);
      }
  }

  // Establish the initial context if no initial context currently exists.
  bool
  parser::yy_lac_establish_ (symbol_kind_type yytoken)
  {
    /* Establish the initial context for the current lookahead if no initial
       context is currently established.

       We define a context as a snapshot of the parser stacks.  We define
       the initial context for a lookahead as the context in which the
       parser initially examines that lookahead in order to select a
       syntactic action.  Thus, if the lookahead eventually proves
       syntactically unacceptable (possibly in a later context reached via a
       series of reductions), the initial context can be used to determine
       the exact set of tokens that would be syntactically acceptable in the
       lookahead's place.  Moreover, it is the context after which any
       further semantic actions would be erroneous because they would be
       determined by a syntactically unacceptable token.

       yy_lac_establish_ should be invoked when a reduction is about to be
       performed in an inconsistent state (which, for the purposes of LAC,
       includes consistent states that don't know they're consistent because
       their default reductions have been disabled).

       For parse.lac=full, the implementation of yy_lac_establish_ is as
       follows.  If no initial context is currently established for the
       current lookahead, then check if that lookahead can eventually be
       shifted if syntactic actions continue from the current context.  */
    if (!yy_lac_established_)
      {
#if YYDEBUG
        YYCDEBUG << "LAC: initial context established for "
                 << symbol_name (yytoken) << '\n';
#endif
        yy_lac_established_ = true;
        return yy_lac_check_ (yytoken);
      }
    return true;
  }

  // Discard any previous initial lookahead context.
  void
  parser::yy_lac_discard_ (const char* evt)
  {
   /* Discard any previous initial lookahead context because of Event,
      which may be a lookahead change or an invalidation of the currently
      established initial context for the current lookahead.

      The most common example of a lookahead change is a shift.  An example
      of both cases is syntax error recovery.  That is, a syntax error
      occurs when the lookahead is syntactically erroneous for the
      currently established initial context, so error recovery manipulates
      the parser stacks to try to find a new initial context in which the
      current lookahead is syntactically acceptable.  If it fails to find
      such a context, it discards the lookahead.  */
    if (yy_lac_established_)
      {
        YYCDEBUG << "LAC: initial context discarded due to "
                 << evt << '\n';
        yy_lac_established_ = false;
      }
  }

  int
  parser::yy_syntax_error_arguments_ (const context& yyctx,
                                                 symbol_kind_type yyarg[], int yyargn) const
  {
    /* There are many possibilities here to consider:
       - If this state is a consistent state with a default action, then
         the only way this function was invoked is if the default action
         is an error action.  In that case, don't check for expected
         tokens because there are none.
       - The only way there can be no lookahead present (in yyla) is
         if this state is a consistent state with a default action.
         Thus, detecting the absence of a lookahead is sufficient to
         determine that there is no unexpected or expected token to
         report.  In that case, just report a simple "syntax error".
       - Don't assume there isn't a lookahead just because this state is
         a consistent state with a default action.  There might have
         been a previous inconsistent state, consistent state with a
         non-default action, or user semantic action that manipulated
         yyla.  (However, yyla is currently not documented for users.)
         In the first two cases, it might appear that the current syntax
         error should have been detected in the previous state when
         yy_lac_check was invoked.  However, at that time, there might
         have been a different syntax error that discarded a different
         initial context during error recovery, leaving behind the
         current lookahead.
    */

    if (!yyctx.lookahead ().empty ())
      {
        if (yyarg)
          yyarg[0] = yyctx.token ();
        int yyn = yyctx.expected_tokens (yyarg ? yyarg + 1 : yyarg, yyargn - 1);
        return yyn + 1;
      }
    return 0;
  }

  // Generate an error message.
  std::string
  parser::yysyntax_error_ (const context& yyctx) const
  {
    // Its maximum.
    enum { YYARGS_MAX = 5 };
    // Arguments of yyformat.
    symbol_kind_type yyarg[YYARGS_MAX];
    int yycount = yy_syntax_error_arguments_ (yyctx, yyarg, YYARGS_MAX);

    char const* yyformat = YY_NULLPTR;
    switch (yycount)
      {
#define YYCASE_(N, S)                         \
        case N:                               \
          yyformat = S;                       \
        break
      default: // Avoid compiler warnings.
        YYCASE_ (0, YY_("syntax error"));
        YYCASE_ (1, YY_("syntax error, unexpected %s"));
        YYCASE_ (2, YY_("syntax error, unexpected %s, expecting %s"));
        YYCASE_ (3, YY_("syntax error, unexpected %s, expecting %s or %s"));
        YYCASE_ (4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
        YYCASE_ (5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
#undef YYCASE_
      }

    std::string yyres;
    // Argument number.
    std::ptrdiff_t yyi = 0;
    for (char const* yyp = yyformat; *yyp; ++yyp)
      if (yyp[0] == '%' && yyp[1] == 's' && yyi < yycount)
        {
          yyres += symbol_name (yyarg[yyi++]);
          ++yyp;
        }
      else
        yyres += *yyp;
    return yyres;
  }


  const signed char parser::yypact_ninf_ = -105;

  const signed char parser::yytable_ninf_ = -86;

  const short
  parser::yypact_[] =
  {
     167,   194,  -105,  -105,     6,  -105,  -105,  -105,   194,   194,
     109,   120,    13,    13,     3,   297,    -8,  -105,    26,    60,
       9,  -105,    50,    50,    51,   282,    57,   265,    26,    26,
      86,   194,   194,   194,   194,   194,   194,   194,   194,   194,
     194,   194,   194,   194,    45,   155,    80,    89,   100,   104,
     108,  -105,  -105,  -105,  -105,    72,   119,    31,  -105,  -105,
    -105,  -105,  -105,  -105,  -105,  -105,  -105,    92,  -105,  -105,
     110,   105,   236,   236,    50,    50,    50,   210,   210,   210,
     210,   333,   333,   322,   310,  -105,   297,   116,   123,   124,
     225,  -105,   169,   115,   115,   115,   194,    13,    83,  -105,
     121,   113,   142,  -105,   160,  -105,  -105,   244,   262,   226,
     243,   245,   297,    32,   248,   194,   188,    13,   261,  -105,
    -105,  -105,   297,  -105,  -105,   237,    91,  -105,  -105,  -105,
     193,   193,   297,  -105,  -105,   -15,   248,   165,    58,  -105,
     253,   254,   298,    73,  -105,    17,    20,  -105,   248,    39,
     -11,  -105,   222,   324,   246,  -105,  -105,   205,   205,   317,
    -105,  -105,  -105,  -105,  -105,  -105,   247,  -105,  -105,  -105,
    -105,  -105,  -105,  -105,   201,  -105,  -105,  -105,  -105,   205,
    -105,   248,  -105,  -105,    52,  -105,  -105,  -105,   148,  -105,
    -105
  };

  const signed char
  parser::yydefact_[] =
  {
       0,     0,    73,   104,     0,    52,    20,    41,     0,     0,
       0,     0,     0,     0,     0,     2,    22,    21,    25,     3,
       0,     1,    18,    19,     0,     0,     0,     0,    23,    24,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    76,    74,    75,   108,    52,     0,     0,    64,   105,
     106,   107,    30,    29,    28,    27,    45,    49,    48,    44,
       0,    43,     5,     6,     7,     8,     9,    10,    11,    13,
      14,    12,    15,    16,    17,    38,    37,     0,    36,     0,
       0,    53,    66,    80,    80,    80,     0,     0,     0,    65,
       0,    50,     0,    26,     0,    32,    31,     0,     0,     0,
       0,     0,    62,    54,    61,     0,     0,     0,     0,    47,
      46,    40,    39,    67,    83,    81,     0,    77,   104,   104,
       0,     0,    63,    70,    69,     0,    51,     0,     0,    90,
       0,     0,     0,     0,    87,     0,     0,    58,    57,     0,
       0,    68,     0,     0,     0,    84,    82,     0,     0,     0,
      89,    86,    88,    78,    79,    55,     0,    56,    72,    71,
      34,    33,   101,   100,     0,   103,   102,    92,    91,     0,
      60,    59,    99,    95,     0,    96,    93,    94,     0,    98,
      97
  };

  const short
  parser::yypgoto_[] =
  {
    -105,    -6,  -105,  -105,  -105,  -105,   -12,   -75,   219,   -16,
    -105,   332,   250,  -105,  -105,   112,  -105,    49,  -105,  -105,
    -105,  -105,  -105,  -105,   249,  -105,   211,  -105,  -105,  -104,
    -105
  };

  const short
  parser::yydefgoto_[] =
  {
      -1,    15,    16,    87,    88,    17,    18,   148,   149,    58,
      59,    60,    69,    61,   135,   109,   125,    20,    19,    70,
      71,   118,    52,    53,   175,   143,   144,   176,   184,   177,
       4
  };

  const short
  parser::yytable_[] =
  {
      28,    29,    22,    23,    25,    27,    21,   151,    57,    -4,
      54,   167,    55,    44,    68,   152,     5,    45,    54,   166,
      55,    54,   114,    55,    30,    72,    73,    74,    75,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    86,    90,
      99,   163,   136,   130,   164,    47,    85,    56,     5,     6,
       7,   165,     8,    47,   178,    56,    47,    46,    56,   155,
      35,   156,    46,    46,     9,    10,    11,   -35,   100,   166,
     185,    62,    12,    13,   160,   186,   140,   141,   187,    64,
      14,   -85,   188,    91,   190,   113,    68,    66,   -85,    67,
     112,   181,   139,    92,   140,   141,    47,   161,   122,    48,
      49,    50,   142,    93,    96,   113,    97,    94,   -42,   132,
      24,    95,     5,     6,     7,   115,     8,    97,   113,   113,
     142,    26,    98,     5,     6,     7,    97,     8,     9,    10,
      11,   154,   101,    57,    57,   102,    12,    13,   103,     9,
      10,    11,   116,   119,    14,    67,   117,    12,    13,   189,
     105,   172,   173,   104,   113,    14,    89,   108,     5,     6,
       7,   121,     8,     5,     6,     7,   153,     8,     5,     6,
       7,   126,     8,   174,     9,    10,    11,   145,   146,     9,
      10,    11,    12,    13,     9,    10,    11,    12,    13,   133,
      14,   134,    12,    13,   147,    14,     5,     5,     6,     7,
      14,     8,   182,   107,   172,   173,   110,   111,   172,   173,
       1,     2,     3,     9,    10,    11,    31,    32,    33,    34,
      35,    12,    13,   168,   126,   169,   174,   183,   126,    14,
     174,    31,    32,    33,    34,    35,    36,    37,    38,    39,
      40,    41,    42,    43,    33,    34,    35,   123,   180,   126,
       5,   106,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    43,   124,   128,   138,   129,   131,
     171,    31,    32,    33,    34,    35,    36,    37,    38,    39,
      40,    41,    42,    43,   137,   157,   158,    65,    31,    32,
      33,    34,    35,    36,    37,    38,    39,    40,    41,    42,
      43,   159,    63,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    40,    41,    42,    43,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    31,    32,
      33,    34,    35,    36,    37,    38,    39,    40,    41,    31,
      32,    33,    34,    35,    36,    37,    38,    39,   170,   179,
     150,    51,   120,     0,   162,     0,     0,     0,   127
  };

  const short
  parser::yycheck_[] =
  {
      12,    13,     8,     9,    10,    11,     0,    22,    20,     0,
       1,    22,     3,    21,    30,    30,     3,    25,     1,    30,
       3,     1,    97,     3,    21,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      56,    24,   117,    11,    24,    36,     1,    38,     3,     4,
       5,    12,     7,    36,   158,    38,    36,    31,    38,     1,
      10,     3,    31,    31,    19,    20,    21,    22,    37,    30,
     174,    20,    27,    28,     1,   179,     3,     4,    26,    22,
      35,    23,    30,     3,   188,    97,   102,     1,    30,     3,
      96,   166,     1,     4,     3,     4,    36,    24,   104,    39,
      40,    41,    29,     3,    32,   117,    34,     3,    22,   115,
       1,     3,     3,     4,     5,    32,     7,    34,   130,   131,
      29,     1,     3,     3,     4,     5,    34,     7,    19,    20,
      21,   137,    22,   145,   146,    30,    27,    28,    22,    19,
      20,    21,    21,     1,    35,     3,    33,    27,    28,     1,
      26,     3,     4,    30,   166,    35,     1,    42,     3,     4,
       5,     1,     7,     3,     4,     5,     1,     7,     3,     4,
       5,    23,     7,    25,    19,    20,    21,   128,   129,    19,
      20,    21,    27,    28,    19,    20,    21,    27,    28,     1,
      35,     3,    27,    28,     1,    35,     3,     3,     4,     5,
      35,     7,     1,    34,     3,     4,    94,    95,     3,     4,
      43,    44,    45,    19,    20,    21,     6,     7,     8,     9,
      10,    27,    28,     1,    23,     3,    25,    26,    23,    35,
      25,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,     8,     9,    10,     3,     1,    23,
       3,    26,     6,     7,     8,     9,    10,    11,    12,    13,
      14,    15,    16,    17,    18,     3,    23,    30,    23,    21,
      24,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    23,    32,    32,    22,     6,     7,
       8,     9,    10,    11,    12,    13,    14,    15,    16,    17,
      18,     3,    20,     6,     7,     8,     9,    10,    11,    12,
      13,    14,    15,    16,    17,    18,     6,     7,     8,     9,
      10,    11,    12,    13,    14,    15,    16,    17,     6,     7,
       8,     9,    10,    11,    12,    13,    14,    15,    16,     6,
       7,     8,     9,    10,    11,    12,    13,    14,    24,    32,
     131,    19,   102,    -1,   143,    -1,    -1,    -1,   109
  };

  const signed char
  parser::yystos_[] =
  {
       0,    43,    44,    45,    77,     3,     4,     5,     7,    19,
      20,    21,    27,    28,    35,    48,    49,    52,    53,    65,
      64,     0,    48,    48,     1,    48,     1,    48,    53,    53,
      21,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    21,    25,    31,    36,    39,    40,
      41,    58,    69,    70,     1,     3,    38,    53,    56,    57,
      58,    60,    20,    20,    22,    22,     1,     3,    56,    59,
      66,    67,    48,    48,    48,    48,    48,    48,    48,    48,
      48,    48,    48,    48,    48,     1,    48,    50,    51,     1,
      48,     3,     4,     3,     3,     3,    32,    34,     3,    56,
      37,    22,    30,    22,    30,    26,    26,    34,    42,    62,
      62,    62,    48,    53,    54,    32,    21,    33,    68,     1,
      59,     1,    48,     3,     3,    63,    23,    71,    23,    23,
      11,    21,    48,     1,     3,    61,    54,    23,    30,     1,
       3,     4,    29,    72,    73,    64,    64,     1,    54,    55,
      55,    22,    30,     1,    48,     1,     3,    32,    32,     3,
       1,    24,    73,    24,    24,    12,    30,    22,     1,     3,
      24,    24,     3,     4,    25,    71,    74,    76,    76,    32,
       1,    54,     1,    26,    75,    76,    76,    26,    30,     1,
      76
  };

  const signed char
  parser::yyr1_[] =
  {
       0,    47,    77,    77,    77,    48,    48,    48,    48,    48,
      48,    48,    48,    48,    48,    48,    48,    48,    48,    48,
      48,    48,    48,    49,    49,    49,    49,    49,    49,    49,
      49,    49,    49,    49,    49,    50,    50,    51,    51,    51,
      51,    52,    66,    66,    67,    67,    67,    67,    59,    59,
      68,    68,    53,    53,    54,    54,    54,    55,    55,    55,
      55,    56,    57,    57,    57,    57,    58,    58,    60,    61,
      61,    61,    61,    65,    65,    65,    65,    69,    70,    70,
      62,    62,    63,    63,    63,    63,    71,    72,    72,    72,
      72,    73,    73,    73,    74,    74,    75,    75,    75,    75,
      76,    76,    76,    76,    64,    64,    64,    64,    64
  };

  const signed char
  parser::yyr2_[] =
  {
       0,     2,     2,     2,     2,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     2,     2,
       1,     1,     1,     2,     2,     1,     4,     3,     3,     3,
       3,     4,     4,     8,     8,     0,     1,     1,     1,     3,
       3,     1,     0,     1,     1,     1,     3,     3,     1,     1,
       0,     2,     1,     3,     1,     4,     4,     1,     1,     3,
       3,     3,     3,     4,     1,     2,     2,     4,     5,     1,
       1,     3,     3,     0,     2,     2,     2,     4,     6,     6,
       0,     2,     3,     1,     3,     2,     3,     1,     2,     2,
       1,     3,     3,     4,     3,     2,     1,     3,     3,     1,
       1,     1,     1,     1,     0,     2,     2,     2,     2
  };




#if YYDEBUG
  const short
  parser::yyrline_[] =
  {
       0,   261,   261,   262,   263,   267,   268,   269,   270,   271,
     272,   273,   274,   275,   276,   277,   278,   279,   280,   281,
     282,   283,   285,   289,   290,   291,   292,   293,   294,   295,
     296,   297,   298,   299,   301,   306,   307,   311,   312,   313,
     314,   317,   320,   321,   325,   326,   327,   328,   332,   333,
     337,   338,   354,   355,   359,   360,   361,   365,   366,   367,
     368,   371,   374,   375,   376,   377,   383,   384,   388,   393,
     394,   395,   396,   400,   401,   402,   403,   407,   412,   414,
     419,   420,   424,   425,   426,   427,   431,   435,   436,   437,
     438,   442,   443,   444,   448,   449,   453,   454,   455,   456,
     460,   461,   462,   463,   467,   468,   469,   470,   471
  };

  void
  parser::yy_stack_print_ () const
  {
    *yycdebug_ << "Stack now";
    for (stack_type::const_iterator
           i = yystack_.begin (),
           i_end = yystack_.end ();
         i != i_end; ++i)
      *yycdebug_ << ' ' << int (i->state);
    *yycdebug_ << '\n';
  }

  void
  parser::yy_reduce_print_ (int yyrule) const
  {
    int yylno = yyrline_[yyrule];
    int yynrhs = yyr2_[yyrule];
    // Print the symbols being reduced, and their result.
    *yycdebug_ << "Reducing stack by rule " << yyrule - 1
               << " (line " << yylno << "):\n";
    // The symbols being reduced.
    for (int yyi = 0; yyi < yynrhs; yyi++)
      YY_SYMBOL_PRINT ("   $" << yyi + 1 << " =",
                       yystack_[(yynrhs) - (yyi + 1)]);
  }
#endif // YYDEBUG


#line 157 "formula_parser.ypp"
} // lax
#line 2361 "formula_parser.tab.cpp"

#line 474 "formula_parser.ypp"

/* Epilogue */

void lax::parser::error(const location_type& loc, const std::string& msg)
{
    drv.registerError(loc, msg);
}

void lax::JSONObject::append(lax::JSONField&& f, const location& loc)
{
    auto val = std::make_unique<lax::JSONValue>(std::move(f.second));

    if (!objs.emplace(std::move(f.first), std::move(val)).second)
    {
        throw parser::syntax_error(loc, "duplication of key in JSON object");
    }
}
