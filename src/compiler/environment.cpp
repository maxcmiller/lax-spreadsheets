#include "environment.hpp"

#include <unordered_set>

namespace lax
{
    std::unordered_set<ValueObserver*> ValueObserver::ms_valList;

    void ValueObserver::clearDependencies()
    {
        const std::pair selfArray(getWeakRef(), true);
        const std::pair selfScalar(getWeakRef(), false);

        for (const auto& dep: m_deps)
        {
            dep.first->m_depOnMe.erase(selfArray);
            dep.first->m_depOnMe.erase(selfScalar);
        }

        m_deps.clear();
    }

    void ValueNode::setObject(Object::Ptr obj)
    {
        if (obj && obj->getType() != m_type &&
            obj->getType() != ErrorObject::theType())
        {
            throw std::runtime_error("mismatched type error");
        }

        m_obj = obj;
    }

    void ValueNode::doUpdate(int row)
    {
        if (!m_func)
        {
            // is data column so don't try to recompute
            return;
        }

        auto index = std::make_shared<NumberObject>(row);
        std::vector<Object::Ptr> args(1, index);

        if (m_type.isColumn())
        {
            auto arr = std::dynamic_pointer_cast<ArrayObject>(getObject());

            if (arr)
            {
                if (isModified() == ValueNode::kOneModified && row >= 0)
                {
                    (*arr)[row] = m_func->execute(args);
                }
                else
                {
                    for (std::size_t ctr = 0; ctr < arr->size(); ++ctr)
                    {
                        index->setValue(ctr);
                        (*arr)[ctr] = m_func->execute(args);
                    }
                }
            }
        }
        else
        {
            setObject(m_func->execute(args));
        }
    }

    std::string ValueObserver::getDepGraph()
    {
        std::string ret = "strict digraph {";

        for (auto ptr: ms_valList)
        {
            std::string name =
            std::to_string(reinterpret_cast<std::uintptr_t>(ptr));

            if (!ptr->m_name.empty())
            {
                ret += name + "[label=\"" + ptr->m_name + "\"]";
            }

            for (const auto& dep: ptr->m_deps)
            {
                if (dep.first.get() != ptr)
                {
                    std::string other =
                    std::to_string(reinterpret_cast<std::uintptr_t>(dep.first.get()));
                    ret += name + "->" + other + ' ';
                }
            }
        }

        ret += '}';
        return ret;
    }

    void ValueObserver::addDependency(const Ptr& dep, bool isArray)
    {
        if (&dep->m_sys != &m_sys && &dep->m_sys != &ValueSystem::staticSystem())
        {
            throw std::logic_error("tried to create dependency between values "
                    "in different systems");
        }

        m_deps.emplace(dep, isArray);
        dep->m_depOnMe.emplace(getWeakRef(), isArray);
    }

    Environment::Environment(ValueSystem& sys,
            std::initializer_list<std::pair<std::string, Object::Ptr> >&& il):
        m_sys(&sys), m_map()
    {
        m_map.reserve(il.size());

        for (auto&& p: il)
        {
            if (!p.second)
            {
                throw std::logic_error("object should not be null in Environment ctor");
            }

            auto n = ValueNode::make(p.second->getType(), sys);
            n->setObject(std::move(p.second));
            m_map.emplace(std::move(p.first), std::pair(std::move(n), false));
        }
    }

    bool Environment::addObject(std::string_view name, const TypeInfo& type,
            ValueNode::Ptr* res, bool isParam)
    {
        auto vn = ValueNode::make(type, *m_sys);
        auto success = m_map.emplace(name, std::pair(vn, isParam));

        if (res)
        {
            *res = success.first->second.first;
        }

        return success.second;
    }

    ValueNode& Environment::findObject(const ICaseString& name, bool* isParam) const
    {
        if (std::size_t index = name->find('.'); index != std::string::npos)
        {
            // TODO (Max#1#): Consider switching to std::map and string_view
            ICaseString inner(name->substr(index + 1));
            ICaseString outer(name->substr(0, index));

            if (auto itr = m_envs.find(outer); itr != m_envs.end())
            {
                return itr->second->findObject(inner, isParam);
            }
        }

        if (auto itr = m_map.find(name); itr != m_map.end())
        {
            if (isParam)
            {
                *isParam = itr->second.second;
            }

            return *itr->second.first;
        }
        else if (m_outer)
        {
            return m_outer->findObject(name, isParam);
        }
        else
        {
            throw std::out_of_range("could not find variable " + *name);
        }
    }
    void Environment::moveVarsFrom(Environment&& other)
    {
        m_map.merge(std::move(other.m_map));
        m_envs.merge(std::move(other.m_envs));
    }

    void Environment::copyVarsFrom(const Environment& other)
    {
        for (const auto& v: other.m_map)
        {
            m_map.insert(v);
        }

        for (const auto& e: other.m_envs)
        {
            m_envs.insert(e);
        }
    }
}
