#ifndef LAX_SRC_COMPILER_TYPE_INFO_HPP_
#define LAX_SRC_COMPILER_TYPE_INFO_HPP_

#include <map>
#include <memory>
#include <string>
#include <vector>

namespace lax
{
    class Object;

    class ObjectFactory
    {
    public:
        using Ptr = std::unique_ptr<ObjectFactory>;

        virtual ~ObjectFactory();
        virtual std::shared_ptr<Object> construct(std::string_view str) = 0;
    };

    class TypeInfoImpl
    {
    public:
        using Ptr = std::shared_ptr<TypeInfoImpl>;

        TypeInfoImpl(std::string_view name, ObjectFactory::Ptr&& f = nullptr,
                bool disp = false):
            m_name(name), m_f(std::move(f)), m_disp(disp) {}

        TypeInfoImpl(const TypeInfoImpl&) = delete;
        virtual ~TypeInfoImpl() = default;

        virtual bool isCompatible(const TypeInfoImpl&) const
        {
            return false;
        }

        virtual bool isArray() const noexcept
        {
            return false;
        }

        virtual bool isColumn() const noexcept
        {
            return false;
        }

        virtual bool shouldDisplay() const noexcept
        {
            return m_disp;
        }

        std::string getName() const
        {
            return m_name;
        }

        std::shared_ptr<Object> construct(std::string_view str)
        {
            if (m_f)
            {
                return m_f->construct(str);
            }
            else
            {
                return std::shared_ptr<Object>(nullptr);
            }
        }
    private:
        std::string m_name;
        ObjectFactory::Ptr m_f;
        bool m_disp;
    };

    class TypeInfo
    {
    public:
        TypeInfo():
            TypeInfo(nullType()) {}

        TypeInfo(std::string_view name, ObjectFactory::Ptr f = nullptr,
                bool disp = false):
            pimpl(new TypeInfoImpl(name, std::move(f), disp)) {}

        explicit TypeInfo(const TypeInfoImpl::Ptr& impl):
            pimpl(impl) {}

        TypeInfo(const TypeInfo&) = default;
        TypeInfo& operator=(const TypeInfo&) = default;
        ~TypeInfo() = default;

        std::string getName() const
        {
            return pimpl->getName();
        }

        std::shared_ptr<Object> construct(std::string_view str) const
        {
            return pimpl->construct(str);
        }

        auto getImpl() const noexcept
        {
            return pimpl;
        }

        template <typename T>
        auto castTo() const noexcept
        {
            return std::dynamic_pointer_cast<T>(pimpl);
        }

        bool isColumn() const
        {
            return pimpl->isColumn();
        }

        bool isArray() const
        {
            return pimpl->isArray();
        }

        bool shouldDisplay() const noexcept
        {
            return pimpl->shouldDisplay();
        }

        static TypeInfo nullType()
        {
            static TypeInfo null("Null");
            return null;
        }
    private:
        friend bool operator==(const TypeInfo& lhs, const TypeInfo& rhs)
        {
            return lhs.pimpl == rhs.pimpl ||
                   lhs.pimpl->isCompatible(*rhs.pimpl) ||
                   rhs.pimpl->isCompatible(*lhs.pimpl);
        }

        friend bool operator<(const TypeInfo& lhs, const TypeInfo& rhs)
        {
            return lhs.pimpl < rhs.pimpl;
        }

        TypeInfoImpl::Ptr pimpl;
    };

    inline bool operator!=(const TypeInfo& lhs, const TypeInfo& rhs)
    {
        return !(lhs == rhs);
    }

    class ArrayTypeInfo:
        public TypeInfoImpl
    {
    public:
        static std::shared_ptr<ArrayTypeInfo> getArrayInfo(const TypeInfo& under);

        TypeInfo getUnderlying() const noexcept
        {
            return m_under;
        }

        bool isArray() const noexcept override
        {
            return true;
        }
    protected:
        explicit ArrayTypeInfo(const TypeInfo& underlying):
            TypeInfoImpl("Array<" + underlying.getName() + '>'),
            m_under(underlying) {}
    private:
        TypeInfo m_under;
    };

    class ColumnTypeInfo:
        public ArrayTypeInfo
    {
    public:
        static std::shared_ptr<ColumnTypeInfo> getColumnInfo(const TypeInfo& under);

        bool isColumn() const noexcept override
        {
            return true;
        }

        bool isCompatible(const TypeInfoImpl& other) const;
    private:
        explicit ColumnTypeInfo(const TypeInfo& underlying):
            ArrayTypeInfo(underlying) {}
    };

    enum class ConvResult
    {
        kImpossible,
        kNoneNeeded,
        kNeedsConv,
    };

    class FunctionTypeInfo:
        public TypeInfoImpl
    {
    public:
        explicit FunctionTypeInfo(std::string_view name = "Function"):
            TypeInfoImpl(name) {}

        virtual TypeInfo getReturn(const std::vector<TypeInfo>& args) const = 0;
        virtual ConvResult needsConversion(const std::vector<TypeInfo>& args,
                                           TypeInfo& result,
                                           std::vector<TypeInfo>& to) const = 0;
    };

    class FixedFuncTypeInfo:
        public FunctionTypeInfo
    {
    public:
        using Ptr = std::shared_ptr<FixedFuncTypeInfo>;

        FixedFuncTypeInfo(const TypeInfo& ret, std::vector<TypeInfo>&& args):
            FunctionTypeInfo(makeName(ret, args)), m_type(ret),
            m_args(std::move(args)) {}

        template <typename ArgIter>
        static auto create(const TypeInfo& ret, ArgIter first, ArgIter last)
        {
            return create(ret, std::vector(first, last));
        }

        static Ptr create(const TypeInfo& ret, std::vector<TypeInfo>&& args);

        TypeInfo getReturn(const std::vector<TypeInfo>& args) const override;
        bool isCompatible(const TypeInfoImpl& other) const override;
        ConvResult needsConversion(const std::vector<TypeInfo>& args,
                                   TypeInfo& result,
                                   std::vector<TypeInfo>& to) const override;

        TypeInfo getReturn() const noexcept
        {
            return m_type;
        }

        auto begin() const noexcept
        {
            return m_args.begin();
        }

        auto end() const noexcept
        {
            return m_args.end();
        }
    private:
        static std::string makeName(const TypeInfo& res,
                const std::vector<TypeInfo>& args);

        TypeInfo m_type;
        std::vector<TypeInfo> m_args;
    };

    class TemplateTypeInfo
    {
    public:
        using Ptr = std::unique_ptr<TemplateTypeInfo>;

        virtual ~TemplateTypeInfo() = default;
        virtual TypeInfo getType(const std::vector<TypeInfo>&) = 0;
    };

    class TypeRegistry
    {
    public:
        TypeRegistry() = delete;
        TypeRegistry(const TypeRegistry&) = delete;

        static bool addType(const TypeInfo& t)
        {
            return theMap().emplace(t.getName(), t).second;
        }

        static bool addTemplate(std::string name, TemplateTypeInfo::Ptr&& t)
        {
            return theTemplMap().emplace(std::move(name), std::move(t)).second;
        }

        static bool removeType(const TypeInfo& t);
        static bool removeTemplate(std::string_view name);
        static TypeInfo parseType(std::string_view type);
        static TypeInfo getType(std::string_view name);

        static TypeInfo getType(std::string_view templ,
                const std::vector<TypeInfo>& params);

        static auto beginTypes() noexcept
        {
            return theMap().cbegin();
        }

        static auto endTypes() noexcept
        {
            return theMap().cend();
        }
    private:
        using TypeMap = std::map<std::string, TypeInfo, std::less<> >;
        using TemplMap = std::map<std::string, TemplateTypeInfo::Ptr, std::less<> >;

        static TypeInfo doParse(std::string_view str,
                std::string_view::const_iterator& itr);
        static std::vector<TypeInfo> parseArgs(std::string_view str,
                std::string_view::const_iterator& itr, char end);

        static TypeMap& theMap()
        {
            static TypeMap map;
            return map;
        }

        static TemplMap& theTemplMap()
        {
            static TemplMap map;
            return map;
        }
    };

    class RegisterType
    {
    public:
        explicit RegisterType(const TypeInfo& t):
            m_type(t)
        {
            if (!TypeRegistry::addType(m_type))
            {
                throw std::runtime_error("existing type with name " + t.getName());
            }
        }

        ~RegisterType()
        {
            TypeRegistry::removeType(m_type);
        }
    private:
        TypeInfo m_type;
    };

    class RegisterTemplate
    {
    public:
        RegisterTemplate(std::string name, TemplateTypeInfo::Ptr&& t):
            m_name(std::move(name))
        {
            TypeRegistry::addTemplate(m_name, std::move(t));
        }

        ~RegisterTemplate()
        {
            TypeRegistry::removeTemplate(m_name);
        }
    private:
        std::string m_name;
    };
}

namespace std
{
    template <>
    struct hash<lax::TypeInfo>:
        private hash<lax::TypeInfoImpl::Ptr>
    {
        using BaseType = hash<lax::TypeInfoImpl::Ptr>;

        size_t operator()(const lax::TypeInfo& ti) const noexcept
        {
            return static_cast<const BaseType&>(*this)(ti.getImpl());
        }
    };
}

#endif // LAX_SRC_COMPILER_TYPE_INFO_HPP_
