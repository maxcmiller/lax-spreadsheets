#include "importer.hpp"

#include "path_finder.hpp"

namespace lax
{
    Environment::ConstPtr ImportFinder::find(std::string_view name,
            std::string_view dir, bool useCache)
    {
        if (useCache)
        {
            if (auto env = searchCache(name))
            {
                return env;
            }
        }

        ImportLoader::Ptr l = doFind(name, dir);

        if (l)
        {
            // found
            Environment::ConstPtr env = l->load();

            if (env)
            {
                // successfully loaded
                addToCache(name, env);
                return env;
            }
        }

        return nullptr;
    }

    void CacheImportFinder::doClearCache()
    {
        m_cache.clear();
    }

    void CacheImportFinder::doPruneCache()
    {
        for (auto itr = m_cache.begin(); itr != m_cache.end(); )
        {
            if (itr->second.expired())
            {
                itr = m_cache.erase(itr);
            }
            else
            {
                ++itr;
            }
        }
    }

    void CacheImportFinder::addToCache(std::string_view name, Environment::ConstPtr env)
    {
        m_cache.emplace(name, env);
    }

    Environment::ConstPtr CacheImportFinder::searchCache(std::string_view name)
    {
        if (auto itr = m_cache.find(name); itr != m_cache.end())
        {
            return itr->second.lock();
        }
        else
        {
            return nullptr;
        }
    }

    Importer::Importer(const ImportFinder::Ptr& f, std::string_view name)
    {
        auto init = f;

        if (!init)
        {
            init = PathFinder::getFinder();
        }

        m_map[""] = init;
        m_map[std::string(name)] = init;
    }

    Environment::ConstPtr Importer::find(std::string_view name, std::string_view dir) const
    {
        auto colon = name.find(':');

        if (colon == std::string_view::npos)
        {
            return m_map.at("")->find(name, dir);
        }
        else if (auto itr = m_map.find(name.substr(0, colon)); itr != m_map.end())
        {
            return itr->second->find(name.substr(colon + 1), dir);
        }
        else
        {
            // just for you, Windows (in case someone does
            // import "C:/whatever/stuff.essc")
            return m_map.at("")->find(name, dir);
        }
    }

    bool Importer::removeImportType(std::string_view name)
    {
        auto itr = m_map.find(name);

        if (itr == m_map.end())
        {
            return false;
        }

        m_map.erase(itr);
        return true;
    }
}
