#ifndef LAX_SRC_COMPILER_IMPORTER_IMPORTER_HPP_
#define LAX_SRC_COMPILER_IMPORTER_IMPORTER_HPP_

#include <map>
#include <memory>
#include <string>
#include <string_view>
#include <vector>

#include "../environment.hpp"
#include "../location.hpp"

namespace lax
{
    class CompilationFailure:
        public std::runtime_error
    {
    public:
        struct ErrorInfo
        {
            location loc;
            std::string msg;
        };

        CompilationFailure():
            runtime_error("compilation failed"), pimpl(std::make_shared<Impl>()) {}

        void pushError(const location& loc, std::string_view msg)
        {
            pimpl->errors.emplace_back(ErrorInfo {loc, std::string(msg)});
        }

        void pushError(const location& loc, std::string&& msg)
        {
            pimpl->errors.emplace_back(ErrorInfo {loc, std::move(msg)});
        }

        void pushError(const location& loc, const char* msg)
        {
            pushError(loc, std::string_view(msg));
        }

        const char* what() const noexcept override
        {
            return "compilation failed";
        }

        auto begin() const noexcept
        {
            return pimpl->errors.cbegin();
        }

        auto end() const noexcept
        {
            return pimpl->errors.cend();
        }

        std::size_t size() const noexcept
        {
            return pimpl->errors.size();
        }

        void copyFrom(const CompilationFailure& other)
        {
            pimpl->errors.insert(pimpl->errors.end(), other.begin(), other.end());
        }
    private:
        struct Impl
        {
            std::vector<ErrorInfo> errors;
        };

        std::shared_ptr<Impl> pimpl;
    };

    class LoadError:
        public CompilationFailure
    {
    public:
        LoadError(const LoadError&) = default;
        LoadError(LoadError&&) = default;

        LoadError& operator=(const LoadError&) = default;
        LoadError& operator=(LoadError&&) = default;
    };

    class ImportLoader
    {
    public:
        using Ptr = std::shared_ptr<ImportLoader>;

        ImportLoader() = default;
        ImportLoader(const ImportLoader&) = delete;
        virtual ~ImportLoader() = default;

        Environment::ConstPtr load()
        {
            return doLoad();
        }
    private:
        virtual Environment::ConstPtr doLoad() = 0;
    };

    class ImportFinder
    {
    public:
        using Ptr = std::shared_ptr<ImportFinder>;

        ImportFinder() = default;
        ImportFinder(const ImportFinder&) = delete;
        virtual ~ImportFinder() = default;

        Environment::ConstPtr find(std::string_view name, std::string_view dir,
                bool searchCache = true);

        void clearCache()
        {
            return doClearCache();
        }

        void pruneCache()
        {
            return doPruneCache();
        }
    private:
        virtual ImportLoader::Ptr
        doFind(std::string_view name, std::string_view dir) = 0;

        virtual Environment::ConstPtr searchCache(std::string_view)
        {
            return nullptr;
        }

        virtual void doClearCache() {}
        virtual void doPruneCache() {}
        virtual void addToCache(std::string_view, Environment::ConstPtr) {}
    };

    class CacheImportFinder:
        public ImportFinder
    {
    public:
        CacheImportFinder() = default;
    private:
        using CacheType =
        std::map<std::string, std::weak_ptr<const Environment>, std::less<> >;

        Environment::ConstPtr searchCache(std::string_view name) override;
        void doClearCache() override;
        void doPruneCache() override;
        void addToCache(std::string_view name, Environment::ConstPtr env) override;

        CacheType m_cache;
    };

    class Importer
    {
    public:
        using Ptr = std::shared_ptr<Importer>;
        using ConstPtr = std::shared_ptr<const Importer>;

        explicit Importer(const ImportFinder::Ptr& dflt = nullptr,
                std::string_view dfltName = "file");
        Environment::ConstPtr find(std::string_view name, std::string_view dir) const;

        bool addImportType(std::string_view name, const ImportFinder::Ptr& f)
        {
            return m_map.emplace(name, f).second;
        }

        bool removeImportType(std::string_view name);
    private:
        using ImportTypeMap = std::map<std::string, ImportFinder::Ptr, std::less<> >;
        ImportTypeMap m_map;
    };
}

#endif // LAX_SRC_COMPILER_IMPORTER_IMPORTER_HPP_
