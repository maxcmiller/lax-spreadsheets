#ifndef LAX_SRC_COMPILER_IMPORTER_STDLIB_FINDER_HPP_
#define LAX_SRC_COMPILER_IMPORTER_STDLIB_FINDER_HPP_

#include "importer.hpp"

namespace lax
{
    class StdlibFinder:
        public ImportFinder
    {
    public:
        StdlibFinder() = default;

        static auto get()
        {
            static auto self = std::make_shared<StdlibFinder>();
            return self;
        }
    private:
        static Environment::ConstPtr makeMathLib();
        static Environment::ConstPtr makeDebugLib();
        static Environment::ConstPtr makeRegressionLib();

        ImportLoader::Ptr
        doFind(std::string_view name, std::string_view dir) override;
    };
}

#endif // LAX_SRC_COMPILER_IMPORTER_STDLIB_FINDER_HPP_
