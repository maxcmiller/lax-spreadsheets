#ifndef LAX_SRC_IMPORTER_PATH_FINDER_HPP_
#define LAX_SRC_IMPORTER_PATH_FINDER_HPP_

#include <filesystem>
#include <vector>

#include "importer.hpp"

namespace lax
{
    class PathLoader
    {
    public:
        using Ptr = std::shared_ptr<PathLoader>;

        virtual ~PathLoader();
        virtual Environment::ConstPtr load(const std::filesystem::path& path) = 0;
    };

    class PathFinder:
        public ImportFinder
    {
    public:
        PathFinder():
            m_loader(std::make_shared<RealLoader>(this)), m_path(1, "") {}

        template <typename Itr>
        PathFinder(Itr firstPath, Itr lastPath):
            PathFinder()
        {
            m_path.insert(m_path.end(), firstPath, lastPath);
        }

        static std::shared_ptr<PathFinder> getFinder();

        static void setFinder(const std::shared_ptr<PathFinder>& pf)
        {
            thePathFinder() = pf;
        }

        void prependPath(std::string_view p)
        {
            m_path.push_back(p);
        }

        void prependPath(std::string&& p)
        {
            m_path.push_back(std::move(p));
        }

        void addLoader(const PathLoader::Ptr& l)
        {
            m_loader->addLoader(l);
        }
    private:
        class RealLoader:
            public ImportLoader
        {
        public:
            RealLoader(PathFinder* pf):
                m_pf(pf), m_file(), m_loaders() {}

            void addLoader(const PathLoader::Ptr& l)
            {
                m_loaders.push_back(l);
            }

            void setPath(std::filesystem::path p)
            {
                m_file = std::move(p);
            }
        private:
            Environment::ConstPtr doLoad() override;

            PathFinder* m_pf;
            std::filesystem::path m_file;
            std::vector<PathLoader::Ptr> m_loaders;
        };

        struct HashPath
        {
            std::size_t operator()(const std::filesystem::path& p) const
            {
                return std::filesystem::hash_value(p);
            }
        };

        struct CacheData
        {
            std::weak_ptr<const Environment> env;
            std::filesystem::file_time_type time;
        };

        using CacheMap = std::unordered_map<
            std::filesystem::path,
            CacheData,
            HashPath
        >;

        ImportLoader::Ptr doFind(std::string_view name,
                std::string_view dir) override;

        Environment::ConstPtr searchCache(std::string_view name) override;
        void doClearCache() override;
        void doPruneCache() override;
        void addToCache(std::string_view name, Environment::ConstPtr env) override;
        static bool cacheExpired(CacheMap::const_iterator itr);

        static std::shared_ptr<PathFinder>& thePathFinder()
        {
            static std::shared_ptr<PathFinder> ptr;
            return ptr;
        }

        std::shared_ptr<RealLoader> m_loader;
        std::vector<std::filesystem::path> m_path;
        CacheMap m_cache;
        std::filesystem::path m_last;
    };
}

#endif // LAX_SRC_IMPORTER_PATH_FINDER_HPP_
