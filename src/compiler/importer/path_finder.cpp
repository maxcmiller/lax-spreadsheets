#include "path_finder.hpp"

#include <cstdlib>
#include <filesystem>
#include <fstream>
#include <sstream>

namespace lax
{
    PathLoader::~PathLoader() = default;

    std::shared_ptr<PathFinder> PathFinder::getFinder()
    {
        if (!thePathFinder())
        {
            thePathFinder() = std::make_shared<PathFinder>();

            if (const char* str = std::getenv("LAX_IMPORT_PATH"))
            {
#ifdef __WIN32
                constexpr char delim = ';';
#else
                constexpr char delim = ':';
#endif // __WIN32

                for (const char* ptr = str; ; ++ptr)
                {
                    if (*ptr == delim || *ptr == '\0')
                    {
                        thePathFinder()->m_path.emplace_back(str, ptr);
                        str = ptr + 1; // don't want delimiter
                    }

                    if (*ptr == '\0')
                    {
                        break;
                    }
                }
            }
        }

        return thePathFinder();
    }

    ImportLoader::Ptr PathFinder::doFind(std::string_view name,
            std::string_view d)
    {
        namespace fs = std::filesystem;

        for (const fs::path& dir: m_path)
        {
            fs::path p;

            if (dir.empty())
            {
                p = d.empty()? fs::path(".") / name : fs::path(d) / name;
            }
            else
            {
                p = dir / name;
            }

            if (fs::exists(p))
            {
                m_last = fs::canonical(p);
                m_loader->setPath(m_last);
                return m_loader;
            }
        }

        return nullptr;
    }

    void PathFinder::addToCache(std::string_view, Environment::ConstPtr env)
    {
        std::error_code ec;
        m_cache.emplace(m_last,
                CacheData {env, std::filesystem::last_write_time(m_last, ec)});
    }

    Environment::ConstPtr PathFinder::searchCache(std::string_view name)
    {
        std::filesystem::path p(name);

        if (p.is_absolute()) // if it's absolute, it's probably appropriate to
        {                    // use an older value if available
            std::error_code ec;
            p = std::filesystem::canonical(p, ec);

            if (ec.value() != 0) // failed to canonicalize
            {
                return nullptr;
            }

            if (auto itr = m_cache.find(p); itr != m_cache.end())
            {
                if (cacheExpired(itr))
                {
                    m_cache.erase(itr);
                }
                else
                {
                    return itr->second.env.lock();
                }
            }
        }

        return nullptr;
    }

    void PathFinder::doClearCache()
    {
        m_cache.clear();
    }

    void PathFinder::doPruneCache()
    {
        for (auto itr = m_cache.begin(); itr != m_cache.end(); )
        {
            if (cacheExpired(itr))
            {
                itr = m_cache.erase(itr);
            }
            else
            {
                ++itr;
            }
        }
    }

    bool PathFinder::cacheExpired(PathFinder::CacheMap::const_iterator itr)
    {
        namespace fs = std::filesystem;

        return itr->second.env.expired() ||
                fs::last_write_time(itr->first) > itr->second.time;
    }

    Environment::ConstPtr PathFinder::RealLoader::doLoad()
    {
        if (auto itr = m_pf->m_cache.find(m_file); itr != m_pf->m_cache.end())
        {
            if (cacheExpired(itr))
            {
                m_pf->m_cache.erase(itr);
            }
            else
            {
                return itr->second.env.lock();
            }
        }

        std::exception_ptr e;

        for (const auto& l: m_loaders)
        {
            try
            {
                Environment::ConstPtr env = l->load(m_file);

                if (env)
                {
                    return env;
                }
            }
            catch (LoadError&)
            {
                e = std::current_exception();
            }
        }

        if (e)
        {
            std::rethrow_exception(e);
        }

        return nullptr;
    }
}
