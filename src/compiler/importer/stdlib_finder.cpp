#include "stdlib_finder.hpp"

#include <cerrno>
#include <cmath>
#include <cstring>
#include <sstream>
#include <tuple>

#ifdef LAX_USE_MATH_FENV
# ifdef LAX_HAVE_PRAGMA_FENV
#  pragma STDC FENV_ACCESS ON
# endif // LAX_HAVE_PRAGMA_FENV

# include <cfenv>
#endif // LAX_USE_MATH_FENV

#include "math/exponent_regression.hpp"
#include "math/logarithm_regression.hpp"
#include "math/polynomial_regression.hpp"

namespace lax
{
    namespace
    {
        class PassthruLoader:
            public ImportLoader
        {
        public:
            explicit PassthruLoader(const Environment::ConstPtr& env):
                m_env(env) {}
        private:
            Environment::ConstPtr doLoad() override
            {
                return m_env;
            }

            Environment::ConstPtr m_env;
        };

        class MathFuncBase:
            public FunctionObject
        {
        protected:
            explicit MathFuncBase(const std::shared_ptr<FunctionTypeInfo>& t):
                FunctionObject(t) {}

            static void prepare() noexcept
            {
#ifdef LAX_USE_MATH_FENV
                std::feclearexcept(FE_ALL_EXCEPT);
#else
                errno = 0;
#endif // LAX_USE_MATH_FENV
            }

            static Ptr testError(double got)
            {
#ifdef LAX_USE_MATH_FENV
                if (std::fetestexcept(FE_DIVBYZERO))
                {
                    return std::make_shared<ErrorObject>("pole error",
                            ErrorObject::kRangeError);
                }
                else if (std::fetestexcept(FE_INVALID))
                {
                    return std::make_shared<ErrorObject>("domain error",
                            ErrorObject::kDomainError);
                }
                else if (std::fetestexcept(FE_OVERFLOW))
                {
                    return std::make_shared<ErrorObject>("numerical overflow",
                            ErrorObject::kRangeError);
                }
                else if (std::fetestexcept(FE_UNDERFLOW))
                {
                    return std::make_shared<ErrorObject>("numerical underflow",
                            ErrorObject::kRangeError);
                }
                else
                {
                    return std::make_shared<NumberObject>(got);
                }
#else
                ErrorObject::Error err;

                switch (errno)
                {
                case 0:
                    return std::make_shared<NumberObject>(got);
                case EDOM:
                    err = ErrorObject::kDomainError;
                    break;
                case ERANGE:
                    err = ErrorObject::kRangeError;
                    break;
                default:
                    err = ErrorObject::kMiscError;
                }

                return std::make_shared<ErrorObject>(std::strerror(errno), err);
#endif // LAX_USE_MATH_FENV
            }
        };

        template <std::size_t I>
        struct GetFuncType
        {
            template <std::size_t...Indices>
            static auto getTuple(std::index_sequence<Indices...>)
            {
                return std::make_tuple(static_cast<double>(Indices)...);
            }

            template <typename...Args>
            static auto getFunc(std::tuple<Args...>)
            {
                return static_cast<double(*)(Args...)>(nullptr);
            }

            using type = std::remove_pointer_t<
                decltype(getFunc(getTuple(std::make_index_sequence<I>())))
            >;
        };

        template <std::size_t I>
        using GetFuncType_t = typename GetFuncType<I>::type;

        template <std::size_t I>
        class MathFunc:
            public MathFuncBase
        {
        public:
            using Func = GetFuncType_t<I>;

            explicit MathFunc(Func* f):
                MathFuncBase(getFuncType()), m_func(f) {}

            Ptr execute(const std::vector<Ptr>& args) const override
            {
                try
                {
                    prepare();
                    return testError(doExecute(args, std::make_index_sequence<I>()));
                }
                catch (std::bad_cast&)
                {
                    return std::make_shared<ErrorObject>(
                            "arguments have incorrect type", ErrorObject::kBadArgs);
                }
                catch (std::out_of_range&)
                {
                    return std::make_shared<ErrorObject>(
                            "incorrect number of arguments", ErrorObject::kBadArgs);
                }
                catch (std::bad_alloc&)
                {
                    throw;
                }
                catch (std::exception& ex)
                {
                    return std::make_shared<ErrorObject>(ex.what(),
                            ErrorObject::kMiscError);
                }
            }

            std::string stringize() const
            {
                return "<math_func>";
            }
        private:
            static auto getFuncType()
            {
                static auto type = FixedFuncTypeInfo::create(NumberObject::theType(),
                        std::vector<TypeInfo>(I, NumberObject::theType()));
                return type;
            }

            template <std::size_t...Indices>
            auto doExecute(const std::vector<Ptr>& vec, std::index_sequence<Indices...>) const
            {
                if (!(vec.at(Indices) && ...))
                {
                    throw std::bad_cast();
                }

                return m_func(dynamic_cast<const NumberObject&>(*vec.at(Indices)).getValue()...);
            }

            Func* m_func;
        };

#define LAX_IMPL_MATH_FUNC(args, i)             \
        auto makeMathFunc(std::string_view name, double(*f)args) \
        {                                       \
            return std::pair(std::string(name), \
                    std::make_shared<MathFunc<i> >(f)); \
        }

        LAX_IMPL_MATH_FUNC((double), 1)
        LAX_IMPL_MATH_FUNC((double, double), 2)
#undef LAX_IMPL_MATH_FUNC

        class ReduceType:
            public FunctionTypeInfo
        {
        public:
            static auto get()
            {
                static const auto self = std::make_shared<ReduceType>();
                return self;
            }

            TypeInfo getReturn(const std::vector<TypeInfo>& args) const override
            {
                if (args.size() != 3)
                {
                    return TypeInfo::nullType();
                }

                auto f = args[0].castTo<FunctionTypeInfo>();
                auto array = args[2].castTo<ArrayTypeInfo>();

                if (!f || !array)
                {
                    return TypeInfo::nullType();
                }

                auto res = f->getReturn({args[1], array->getUnderlying()});

                if (res != args[1])
                {
                    return TypeInfo::nullType();
                }

                return res;
            }

            ConvResult needsConversion(const std::vector<TypeInfo>& args,
                            TypeInfo& result, std::vector<TypeInfo>& to) const override
            {
                if (args.size() != 3 || !args[2].isArray())
                {
                    return ConvResult::kImpossible;
                }

                ConvResult res = ConvResult::kNoneNeeded;
                std::vector<TypeInfo> newArgs(3);
                newArgs[2] = args[2];

                for (std::size_t i = 0; i < 2; ++i)
                {
                    if (args[i].isColumn())
                    {
                        res = ConvResult::kNeedsConv;
                        newArgs[i] = to[i] =
                        args[i].castTo<ColumnTypeInfo>()->getUnderlying();
                    }
                    else
                    {
                        newArgs[i] = args[i];
                    }
                }

                if ((result = getReturn(newArgs)) == TypeInfo::nullType())
                {
                    return ConvResult::kImpossible;
                }

                return res;
            }
        };

        class FuncReduce:
            public FunctionObject
        {
        public:
            FuncReduce():
                FunctionObject(ReduceType::get()) {}

            Ptr execute(const std::vector<Ptr>& args) const override
            {
                if (args.size() < 3)
                {
                    return std::make_shared<ErrorObject>(
                            "too few arguments to REDUCE", ErrorObject::kBadArgs);
                }

                const auto f = std::dynamic_pointer_cast<FunctionObject>(args.front());

                if (!f)
                {
                    return std::make_shared<ErrorObject>(
                            "first arg to REDUCE must be function",
                            ErrorObject::kBadArgs);
                }

                const auto array = std::dynamic_pointer_cast<ArrayObject>(args[2]);

                if (!array)
                {
                    return std::make_shared<ErrorObject>("third arg to REDUCE "
                            "must be array", ErrorObject::kBadArgs);
                }

                std::vector<Ptr> toPass(2);
                toPass[0] = args[1];

                for (const auto& elem: *array)
                {
                    toPass[1] = elem;
                    toPass[0] = f->execute(toPass);

                    if (typeid(*toPass[0]) == typeid(ErrorObject))
                    {
                        return toPass[0];
                    }
                }

                return toPass[0];
            }

            std::string stringize() const override
            {
                return "<reduce_func>";
            }
        };

        double sum(const std::shared_ptr<ArrayObject>& array)
        {
            double sum = 0.0;

            for (const auto& p: *array)
            {
                auto n = std::dynamic_pointer_cast<NumberObject>(p);

                if (!n)
                {
                    return std::numeric_limits<double>::quiet_NaN();
                }

                sum += n->getValue();
            }

            return sum;
        }

        double average(const std::shared_ptr<ArrayObject>& array)
        {
            return sum(array) / array->size();
        }

        double stddev(const std::shared_ptr<ArrayObject>& array, bool isSample)
        {
            const double avg = average(array);

            if (std::isnan(avg))
            {
                return avg;
            }

            double diffSum = 0.0;

            for (const auto& n: *array)
            {
                const auto num = std::static_pointer_cast<NumberObject>(n);
                const double diff = num->getValue() - avg;
                diffSum += diff * diff;
            }

            if (isSample)
            {
                diffSum /= array->size() - 1;
            }
            else
            {
                diffSum /= array->size();
            }

            return std::sqrt(diffSum);
        }

        double product(const std::shared_ptr<ArrayObject>& array)
        {
            double prod = 1.0;

            for (const auto& p: *array)
            {
                auto n = std::dynamic_pointer_cast<NumberObject>(p);

                if (!n)
                {
                    return std::numeric_limits<double>::quiet_NaN();
                }

                prod *= n->getValue();
            }

            return prod;
        }

        template <typename Func>
        class ArrayFunc:
            public FunctionObject
        {
        public:
            ArrayFunc(std::string&& name, Func&& f):
                FunctionObject(theType()), m_name(std::move(name)),
                m_func(std::forward<Func>(f)) {}

            Ptr execute(const std::vector<Ptr>& args) const override
            {
                if (args.size() < 1)
                {
                    return std::make_shared<ErrorObject>(
                            "too few arguments to " + m_name,
                            ErrorObject::kBadArgs);
                }

                auto array = std::dynamic_pointer_cast<ArrayObject>(args[0]);

                if (!array)
                {
                    return std::make_shared<ErrorObject>(
                            "first arg to " + m_name + " must be array",
                            ErrorObject::kBadArgs);
                }

                double got = m_func(array);

                if (std::isnan(got))
                {
                    return std::make_shared<ErrorObject>("bad array element",
                            ErrorObject::kBadArgs);
                }

                return std::make_shared<NumberObject>(got);
            }

            std::string stringize() const override
            {
                return '<' + m_name + '>';
            }

            static std::shared_ptr<FixedFuncTypeInfo> theType()
            {
                return FixedFuncTypeInfo::create(NumberObject::theType(),
                        {ArrayObject::theType(NumberObject::theType())});
            }
        private:
            std::string m_name;
            Func m_func;
        };

        template <typename Func>
        auto makeArrayFunc(std::string&& name, Func&& f)
        {
            using F = std::remove_reference_t<
                std::remove_cv_t<
                    std::decay_t<Func>
                >
            >;

            return std::make_shared<ArrayFunc<F> >(std::move(name),
                    std::forward<Func>(f));
        }

        class DebugType:
            public FunctionTypeInfo
        {
        public:
            static auto get()
            {
                static const auto self = std::make_shared<DebugType>();
                return self;
            }

            TypeInfo getReturn(const std::vector<TypeInfo>& args) const override
            {
                if (args.size() > 2 || args.size() == 0)
                {
                    return TypeInfo::nullType();
                }

                return args.back();
            }

            ConvResult needsConversion(const std::vector<TypeInfo>& args,
                                       TypeInfo& result,
                                       std::vector<TypeInfo>& to) const override;
        };

        ConvResult DebugType::needsConversion(const std::vector<TypeInfo>& args,
                        TypeInfo& result, std::vector<TypeInfo>& to) const
        {
            if (args.size() < 1 || args.size() > 2)
            {
                return ConvResult::kImpossible;
            }

            ConvResult conv = ConvResult::kNoneNeeded;

            for (std::size_t ctr = 0; ctr < args.size(); ++ctr)
            {
                if (args[ctr].isColumn())
                {
                    conv = ConvResult::kNeedsConv;
                    to[ctr] = args[ctr].castTo<ColumnTypeInfo>()->getUnderlying();
                }
            }

            result = args.back();
            return conv;
        }

        class FuncDebug:
            public FunctionObject
        {
        public:
            FuncDebug():
                FunctionObject(DebugType::get()) {}

            Ptr execute(const std::vector<Ptr>& args) const override
            {
                if (args.front())
                {
                    std::clog << args.front()->stringize() << '\n';

                    if (args.size() == 2)
                    {
                        return args[1];
                    }
                    else
                    {
                        return args[0];
                    }
                }
                else
                {
                    std::clog << "(null)\n";
                    return std::make_shared<ErrorObject>("null passed to function",
                            ErrorObject::kBadArgs);
                }
            }

            std::string stringize() const override
            {
                return "<func_debug>";
            }
        };

        class RegType:
            public FixedFuncTypeInfo
        {
        public:
            RegType():
                FixedFuncTypeInfo(NumberObject::theType(),
                        {NumberObject::theType()}) {}

            bool shouldDisplay() const noexcept
            {
                return true;
            }
        };

        class RegressionObject:
            public FunctionObject
        {
        public:
            using RegPtr = std::shared_ptr<math::Regression>;

            static auto theType()
            {
                static const auto type = std::make_shared<RegType>();
                return type;
            }

            RegressionObject(const RegPtr& reg):
                FunctionObject(theType()), m_reg(reg) {}

            Ptr execute(const std::vector<Ptr>& args) const override
            {
                auto arg = std::dynamic_pointer_cast<NumberObject>(args[0]);
                double res = m_reg->calculate(arg->getValue());
                return std::make_shared<NumberObject>(res);
            }

            std::string stringize() const override
            {
                std::ostringstream ostr;
                ostr << *m_reg << " (R^2=" << m_reg->getCoeffOfDetermination() << ')';
                return ostr.str();
            }
        private:
            RegPtr m_reg;
        };

        class RegressionFunc:
            public FunctionObject
        {
        public:
            RegressionFunc():
                FunctionObject(theType()) {}

            explicit RegressionFunc(const std::shared_ptr<FunctionTypeInfo>& type):
                FunctionObject(type) {}

            Ptr execute(const std::vector<Ptr>& args) const override
            {
                auto reg = makeRegression(args);
                std::vector<std::pair<double, double> > set;

                auto x = std::dynamic_pointer_cast<ArrayObject>(args[0]);
                auto y = std::dynamic_pointer_cast<ArrayObject>(args[1]);

                if (!x || !y)
                {
                    return std::make_shared<ErrorObject>("received null",
                            ErrorObject::kBadArgs);
                }

                set.resize(x->size());

                try
                {
                    std::transform(x->begin(), x->end(), y->begin(), set.begin(),
                            [](const auto& first, const auto& second)
                    {
                        auto x = std::dynamic_pointer_cast<NumberObject>(first);
                        auto y = std::dynamic_pointer_cast<NumberObject>(second);

                        if (!x || !y)
                        {
                            throw std::runtime_error("received null");
                        }

                        return std::pair(x->getValue(), y->getValue());
                    });
                }
                catch (std::runtime_error& e)
                {
                    return std::make_shared<ErrorObject>(e.what(),
                            ErrorObject::kBadArgs);
                }

                reg->reset(set);

                return std::make_shared<RegressionObject>(reg);
            }
        private:
            static std::shared_ptr<FixedFuncTypeInfo> theType()
            {
                const TypeInfo result(RegressionObject::theType());
                auto num = NumberObject::theType();
                auto array = ArrayObject::theType(num);
                return FixedFuncTypeInfo::create(result,
                        {array, array});
            }

            virtual std::shared_ptr<math::Regression>
            makeRegression(const std::vector<Ptr>& args) const = 0;
        };

        class PolyRegFunc:
            public RegressionFunc
        {
        public:
            explicit PolyRegFunc(int degree = -1):
                m_degree(degree) {}

            std::string stringize() const override
            {
                return "<polynomial_regression>";
            }
        private:
            std::shared_ptr<math::Regression>
            makeRegression(const std::vector<Ptr>& args) const override
            {
                int degree = m_degree;

                if (degree < 0)
                {
                    auto d = std::dynamic_pointer_cast<NumberObject>(args[2]);

                    if (!d)
                    {
                        throw std::logic_error("received null");
                    }

                    degree = d->getValue();
                }

                return std::make_shared<math::PolynomialRegression>(degree);
            }

            static std::shared_ptr<FunctionTypeInfo> polyType()
            {
                auto num = NumberObject::theType();
                auto array = ArrayObject::theType(num);
                auto result = TypeInfo(RegressionObject::theType());
                return FixedFuncTypeInfo::create(result,
                        {num, array, array});
            }

            int m_degree;
        };

        class ExpRegFunc:
            public RegressionFunc
        {
        public:
            std::string stringize() const override
            {
                return "<exponential_regression>";
            }
        private:
            std::shared_ptr<math::Regression>
            makeRegression(const std::vector<Ptr>&) const override
            {
                return std::make_shared<math::ExponentRegression>();
            }
        };

        class LogRegFunc:
            public RegressionFunc
        {
        public:
            std::string stringize() const override
            {
                return "<logarithmic_regression>";
            }
        private:
            std::shared_ptr<math::Regression>
            makeRegression(const std::vector<Ptr>&) const override
            {
                return std::make_shared<math::LogarithmRegression>();
            }
        };
    }

    ImportLoader::Ptr
    StdlibFinder::doFind(std::string_view name, std::string_view)
    {
        if (name == "math")
        {
            static auto lib = std::make_shared<PassthruLoader>(makeMathLib());
            return lib;
        }
        else if (name == "debug")
        {
            static auto lib = std::make_shared<PassthruLoader>(makeDebugLib());
            return lib;
        }
        else if (name == "regression")
        {
            static auto lib = std::make_shared<PassthruLoader>(makeRegressionLib());
            return lib;
        }
        else
        {
            return nullptr;
        }
    }

    Environment::ConstPtr StdlibFinder::makeMathLib()
    {
        // TODO (Max#1#): Define pi and e better?
        constexpr double pi = 3.1415926535897932384626;
        constexpr double e = 2.71828182845904523536;

        static Environment env(ValueSystem::staticSystem(), {
            makeMathFunc("SQRT", std::sqrt),
            makeMathFunc("CBRT", std::cbrt),
            makeMathFunc("POW", std::pow),
            makeMathFunc("SIN", std::sin),
            makeMathFunc("COS", std::cos),
            makeMathFunc("TAN", std::tan),
            makeMathFunc("ASIN", std::asin),
            makeMathFunc("ACOS", std::acos),
            makeMathFunc("ATAN", std::atan),
            makeMathFunc("SINH", std::sinh),
            makeMathFunc("COSH", std::cosh),
            makeMathFunc("TANH", std::tanh),
            makeMathFunc("ASINH", std::asinh),
            makeMathFunc("ACOSH", std::acosh),
            makeMathFunc("ATANH", std::atanh),
            makeMathFunc("LOG", std::log),
            makeMathFunc("LOG10", std::log10),
            makeMathFunc("LOG2", std::log2),
            makeMathFunc("EXP", std::exp),
            makeMathFunc("EXP2", std::exp2),
            makeMathFunc("ABS", std::abs),

            std::pair("REDUCE", std::make_shared<FuncReduce>()),
            std::pair("AVERAGE", makeArrayFunc("AVERAGE", average)),
            std::pair("STDDEVP", makeArrayFunc("STDDEVP",
                    [](const auto& p) {return stddev(p, false);})),
            std::pair("STDDEVS", makeArrayFunc("STDDEVS",
                    [](const auto& p) {return stddev(p, true);})),
            std::pair("SUM", makeArrayFunc("SUM", sum)),
            std::pair("PRODUCT", makeArrayFunc("PRODUCT", product)),

            std::pair("PI", std::make_shared<NumberObject>(pi)),
            std::pair("E", std::make_shared<NumberObject>(e)),
        });

        return Environment::ConstPtr(&env, [](Environment*) {});
    }

    Environment::ConstPtr StdlibFinder::makeDebugLib()
    {
        static Environment env(ValueSystem::staticSystem(), {
            std::pair("OUTPUT", std::make_shared<FuncDebug>())
        });

        return Environment::ConstPtr(&env, [](Environment*) {});
    }

    Environment::ConstPtr StdlibFinder::makeRegressionLib()
    {
        static Environment env(ValueSystem::staticSystem(), {
            std::pair("LINEAR", std::make_shared<PolyRegFunc>(1)),
            std::pair("QUADRATIC", std::make_shared<PolyRegFunc>(2)),
            std::pair("CUBIC", std::make_shared<PolyRegFunc>(3)),
            std::pair("QUARTIC", std::make_shared<PolyRegFunc>(4)),
            std::pair("POLYNOMIAL", std::make_shared<PolyRegFunc>()),
            std::pair("EXPONENTIAL", std::make_shared<ExpRegFunc>()),
            std::pair("LOGARITHMIC", std::make_shared<LogRegFunc>()),
        });

        return Environment::ConstPtr(&env, [](Environment*) {});
    }
}
