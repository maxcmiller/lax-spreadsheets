#ifndef LAX_SRC_FORMULA_DRIVER_HPP_
#define LAX_SRC_FORMULA_DRIVER_HPP_

#include <iosfwd>
#include <string>
#include <string_view>
#include <variant>
#include <vector>

#include "formula_ast.hpp"
#include "formula_parser.tab.hpp"

namespace lax
{
    class FormulaDriver
    {
    public:
        using AssignContainer = std::vector<TopAssign::Ptr>;

        struct ErrorData
        {
            location loc;
            std::string msg;
        };

        explicit FormulaDriver(std::istream& str, std::string_view filename):
            m_src(str), m_loc(std::make_shared<std::string>(filename)),
            m_errors(), m_value(), m_first(true) {}

        FormulaExpr::Ptr parseExpr(bool debug = false);
        bool parseAssigns(AssignContainer& res, bool debug = false);
        std::shared_ptr<TableAssign> parseTable(bool debug = false);
        parser::symbol_type lex();

        location& getLoc() noexcept
        {
            return m_loc;
        }

        void registerError(const location& loc, const std::string& msg)
        {
            m_errors.push_back({loc, msg});
        }

        void setFormula(const FormulaExpr::Ptr& expr)
        {
            m_value = expr;
        }

        void setAssign(std::vector<std::shared_ptr<TopAssign> >&& assign)
        {
            m_value = std::move(assign);
        }

        void setTable(const std::shared_ptr<TableAssign>& tbl)
        {
            m_value = tbl;
        }

        auto beginErrors() const noexcept
        {
            return m_errors.begin();
        }

        auto endErrors() const noexcept
        {
            return m_errors.end();
        }
    private:
        using ValueType = std::variant<
            FormulaExpr::Ptr,
            AssignContainer,
            std::shared_ptr<TableAssign>
        >;

        enum Variants
        {
            kFormula,
            kAssigns,
            kTable,
            kNumVariants
        };

        static_assert(std::variant_size_v<ValueType> == kNumVariants);

        std::istream& m_src;
        location m_loc;
        std::vector<ErrorData> m_errors;
        ValueType m_value;
        bool m_first;
    };

    inline parser::symbol_type yylex(FormulaDriver& drv)
    {
        return drv.lex();
    }
}

#endif // LAX_SRC_FORMULA_DRIVER_HPP_
