#ifndef LAX_SRC_FORMULA_AST_HPP_
#define LAX_SRC_FORMULA_AST_HPP_

#include <memory>
#include <string>
#include <vector>

#include "../util/icase_string.hpp"
#include "formula_parser.tab.hpp"
#include "location.hpp"

namespace lax
{
    class FormulaVisitor;

    class FormulaNode
    {
    public:
        explicit FormulaNode(const location& loc):
            m_loc(loc) {}

        virtual ~FormulaNode() = default;
        virtual const char* getType() const noexcept = 0;
        virtual std::string stringize() const = 0;

        location getLoc() const noexcept
        {
            return m_loc;
        }
    private:
        location m_loc;
    };

    class FormulaExpr:
        public FormulaNode
    {
    public:
        using Ptr = std::shared_ptr<FormulaExpr>;

        FormulaExpr(const location& loc):
            FormulaNode(loc) {}

        virtual ~FormulaExpr();

        virtual void accept(FormulaVisitor&) const = 0;

        static Ptr makeBin(const location& loc, parser::token_type op, Ptr lhs,
                Ptr rhs);
        static Ptr makeUnary(const location& loc, parser::token_type op, Ptr arg);
        static Ptr makeCall(const location& loc, Ptr name, std::vector<Ptr> args);
        static Ptr makeCall(const location& loc, std::string name,
                std::vector<Ptr> args);
        static Ptr makeVar(const location& loc, std::string name);
        static Ptr makeString(const location& loc, std::string str);
        static Ptr makeNumberData(const location& loc, double d);
        static Ptr makeArrayVar(const location& loc, std::string name);
        static Ptr makeFunc(const location& loc, std::string ret,
                std::vector<FormulaAssign> args, Ptr exp);
    };

    class BinaryExpr:
        public FormulaExpr
    {
    public:
        enum Operation
        {
            kSum,
            kDiff,
            kProduct,
            kQuotient,
            kLess,
            kLessEqual,
            kGreater,
            kGreaterEqual,
            kEqual,
            kNotEqual,
            kAnd,
            kOr,
            kPower,

            kNumOps
        };

        static auto make(const location& loc, parser::token_type op, Ptr lhs, Ptr rhs)
        {
            return std::make_shared<BinaryExpr>(loc, op, lhs, rhs);
        }

        BinaryExpr(const location& loc, parser::token_type op, Ptr lhs, Ptr rhs);

        virtual const char* getType() const noexcept override;
        virtual std::string stringize() const override;
        virtual void accept(FormulaVisitor&) const override;

        Operation getOp() const noexcept
        {
            return m_op;
        }

        Ptr getLeft() const noexcept
        {
            return m_lhs;
        }

        Ptr getRight() const noexcept
        {
            return m_rhs;
        }
    private:
        Operation m_op;
        Ptr m_lhs;
        Ptr m_rhs;
    };

    class UnaryExpr:
        public FormulaExpr
    {
    public:
        enum Operations
        {
            kNegate,
            kNot,
            kAbs,
            kNumOps
        };

        static auto make(const location& loc, parser::token_type op, Ptr arg)
        {
            return std::make_shared<UnaryExpr>(loc, op, arg);
        }

        UnaryExpr(const location& loc, parser::token_type op, Ptr arg);

        virtual const char* getType() const noexcept override;
        virtual std::string stringize() const override;
        virtual void accept(FormulaVisitor&) const override;

        Operations getOp() const noexcept
        {
            return m_op;
        }

        Ptr getArgument() const noexcept
        {
            return m_arg;
        }
    private:
        Operations m_op;
        Ptr m_arg;
    };

    class ElemExpr:
        public FormulaExpr
    {
    public:
        static auto make(const location& loc, const Ptr& lhs, const Ptr& rhs)
        {
            return std::make_shared<ElemExpr>(loc, lhs, rhs);
        }

        ElemExpr(const location& loc, const Ptr& lhs, const Ptr& rhs):
            FormulaExpr(loc), m_lhs(lhs), m_rhs(rhs) {}

        virtual const char* getType() const noexcept override;
        virtual std::string stringize() const override;
        virtual void accept(FormulaVisitor&) const override;

        Ptr getLeft() const noexcept
        {
            return m_lhs;
        }

        Ptr getRight() const noexcept
        {
            return m_rhs;
        }
    private:
        Ptr m_lhs;
        Ptr m_rhs;
    };

    namespace tags
    {
        struct SimpleType {};
        struct FlatVarType {};
        struct ArrayVarType {};

        struct ImportType {};
        struct ScalarType {};
    }

    template <typename T, typename Tag = tags::SimpleType>
    class SimpleExpr:
        public FormulaExpr
    {
    public:
        template <typename U>
        static auto make(const location& loc, U&& u)
        {
            return std::make_shared<SimpleExpr>(loc, std::forward<U>(u));
        }

        template <typename U>
        SimpleExpr(const location& loc, U&& u):
            FormulaExpr(loc), m_data(std::forward<U>(u)) {}

        virtual const char* getType() const noexcept override;
        virtual std::string stringize() const override;
        virtual void accept(FormulaVisitor&) const override;

        T getValue() const noexcept(std::is_nothrow_copy_constructible_v<T>)
        {
            return m_data;
        }
    private:
        T m_data;
    };

    enum class VarExprType
    {
        Exact,
        ToScalar,
        Inferred,
    };

    using VarExpr = SimpleExpr<std::pair<ICaseString, VarExprType>, tags::FlatVarType>;
    using NumExpr = SimpleExpr<double>;
    using StringExpr = SimpleExpr<std::string>;

    class CallExpr:
        public FormulaExpr
    {
    public:
        static auto make(const location& loc, Ptr expr, std::vector<Ptr> args)
        {
            return std::make_shared<CallExpr>(loc, expr, std::move(args));
        }

        static auto make(const location& loc, std::string name, std::vector<Ptr> args)
        {
            auto var = std::make_shared<VarExpr>(loc,
                            std::pair(std::move(name), VarExprType::Inferred));
            return std::make_shared<CallExpr>(loc, std::move(var), std::move(args));
        }

        CallExpr(const location& loc, Ptr expr, std::vector<Ptr> args):
            FormulaExpr(loc), m_expr(std::move(expr)), m_args(std::move(args)) {}

        virtual const char* getType() const noexcept override;
        virtual std::string stringize() const override;
        virtual void accept(FormulaVisitor&) const override;

        Ptr getExpr() const
        {
            return m_expr;
        }

        auto begin() const noexcept
        {
            return m_args.begin();
        }

        auto end() const noexcept
        {
            return m_args.end();
        }

        std::size_t size() const noexcept
        {
            return m_args.size();
        }
    private:
        Ptr m_expr;
        std::vector<Ptr> m_args;
    };

    class FuncLiteral;

    class FormulaVisitor
    {
    public:
        virtual ~FormulaVisitor();

        virtual void visit(const BinaryExpr&) = 0;
        virtual void visit(const UnaryExpr&) = 0;
        virtual void visit(const CallExpr&) = 0;
        virtual void visit(const VarExpr&) = 0;
        virtual void visit(const NumExpr&) = 0;
        virtual void visit(const StringExpr&) = 0;
//        virtual void visit(const ArrayVarExpr&) = 0;
        virtual void visit(const FuncLiteral&) = 0;
        virtual void visit(const ElemExpr&) = 0;
    };

    class AssignVisitor;

    class FormulaAssign:
        public FormulaNode
    {
    public:
        using Ptr = std::shared_ptr<FormulaAssign>;

        FormulaAssign(const location& loc, std::string_view name):
            FormulaNode(loc), m_name(name) {}

        FormulaAssign(const location& loc, std::string&& name):
            FormulaNode(loc), m_name(std::move(name)) {}

        virtual ~FormulaAssign() = default;

        std::string_view getName() const noexcept
        {
            return m_name;
        }

        location getLocation() const noexcept
        {
            return getLoc();
        }

        virtual void accept(AssignVisitor& v) const = 0;
    private:
        std::string m_name;
    };

    template <typename T, typename Tag = tags::SimpleType>
    class SimpleAssign:
        public FormulaAssign
    {
    public:
        template <typename U>
        static auto make(const location& loc, const std::string& name, U&& u)
        {
            return std::make_shared<SimpleAssign>(loc, name, std::forward<U>(u));
        }

        template <typename U>
        static auto make(const location& loc, std::string&& name, U&& u)
        {
            return std::make_shared<SimpleAssign>(loc, std::move(name), std::forward<U>(u));
        }

        template <typename U>
        SimpleAssign(const location& loc, std::string_view name, U&& u):
            FormulaAssign(loc, name), m_val(std::forward<U>(u)) {}

        template <typename U>
        SimpleAssign(const location& loc, std::string&& name, U&& u):
            FormulaAssign(loc, std::move(name)), m_val(std::forward<U>(u)) {}

        const T& getValue() const noexcept
        {
            return m_val;
        }

        const char* getType() const noexcept override;
        std::string stringize() const override;
        void accept(AssignVisitor& v) const override;
    private:
        T m_val;
    };

    using TypeAssign = SimpleAssign<TypeData>;
    using ScalarTypeAssign = SimpleAssign<TypeData, tags::ScalarType>;
    using ExprAssign = SimpleAssign<std::pair<FormulaExpr::Ptr, bool> >;
    using ImportAssign = SimpleAssign<std::string, tags::ImportType>;

    class ReduceAssign:
        public FormulaAssign
    {
    public:
        static auto make(const location& loc, std::string name,
                std::vector<std::string>&& idents)
        {
            return std::make_shared<ReduceAssign>(loc, std::move(name),
                    std::move(idents));
        }

        ReduceAssign(const location& loc, std::string name,
                std::vector<std::string>&& idents):
            FormulaAssign(loc, std::move(name)), m_idents(std::move(idents)) {}

        auto begin() const noexcept
        {
            return m_idents.begin();
        }

        auto end() const noexcept
        {
            return m_idents.end();
        }

        const char* getType() const noexcept override;
        std::string stringize() const override;
        void accept(AssignVisitor& v) const override;
    private:
        std::vector<std::string> m_idents;
    };

    struct JSONObject;

    class AssignVisitor
    {
    public:
        virtual ~AssignVisitor() = default;

        virtual void visit(const TypeAssign&) = 0;
        virtual void visit(const ScalarTypeAssign&) = 0;
        virtual void visit(const ExprAssign&) = 0;
        virtual void visit(const ImportAssign&) = 0;
        virtual void visit(const ReduceAssign&) = 0;
    };

    class FuncLiteral:
        public FormulaExpr
    {
    public:
        static auto make(const location& loc, TypeData ret,
                std::vector<std::shared_ptr<TypeAssign> > args, Ptr exp)
        {
            return std::make_shared<FuncLiteral>(loc, std::move(ret),
                    std::move(args), std::move(exp));
        }

        FuncLiteral(const location& loc, TypeData ret,
                std::vector<std::shared_ptr<TypeAssign> > args, Ptr exp):
            FormulaExpr(loc), m_ret(std::move(ret)), m_args(std::move(args)),
            m_exp(std::move(exp)) {}

        virtual const char* getType() const noexcept override;
        virtual std::string stringize() const override;
        virtual void accept(FormulaVisitor&) const override;

        Ptr getExpr() const noexcept
        {
            return m_exp;
        }

        auto begin() const noexcept
        {
            return m_args.begin();
        }

        auto end() const noexcept
        {
            return m_args.end();
        }

        const TypeData& getReturn() const
        {
            return m_ret;
        }
    private:
        TypeData m_ret;
        std::vector<std::shared_ptr<TypeAssign> > m_args;
        Ptr m_exp;
    };

    class TopVisitor;

    class TopAssign:
        public FormulaNode
    {
    public:
        using Ptr = std::shared_ptr<TopAssign>;

        TopAssign(const location& loc, std::string&& name,
                std::vector<std::string>&& extends):
            FormulaNode(loc), m_name(std::move(name)),
            m_extends(std::move(extends)) {}

        auto beginExtends() const noexcept
        {
            return m_extends.begin();
        }

        auto endExtends() const noexcept
        {
            return m_extends.end();
        }

        std::size_t numExtends() const noexcept
        {
            return m_extends.size();
        }

        std::string getName() const noexcept
        {
            return m_name;
        }

        virtual void accept(TopVisitor&) const = 0;
    private:
        std::string m_name;
        std::vector<std::string> m_extends;
    };

    class ChartAssign:
        public TopAssign
    {
    public:
        struct Data
        {
            std::string index;
            std::string type;
            std::vector<std::string> series;
        };

        static auto make(const location& loc, std::string&& name,
                std::vector<std::string>&& ext, const JSONObject& obj)
        {
            return std::make_shared<ChartAssign>(loc, std::move(name),
                    std::move(ext), obj);
        }

        ChartAssign(const location& loc, std::string&& name,
                std::vector<std::string>&& ext, const JSONObject& obj);

        const Data& getData() const noexcept
        {
            return m_data;
        }

        const char* getType() const noexcept override;
        std::string stringize() const override;
        void accept(TopVisitor& v) const override;
    private:
        Data m_data;
    };

    class TableAssign:
        public TopAssign
    {
    public:
        using FormulaContainer = std::vector<FormulaAssign::Ptr>;
        using iterator = FormulaContainer::const_iterator;

        enum class TableType
        {
            Const,
            Columns,
        };

        static auto make(const location& loc, std::string&& name,
                std::vector<std::string>&& ext,
                FormulaContainer&& vec)
        {
            return std::make_shared<TableAssign>(loc, std::move(name),
                    std::move(ext), std::move(vec), TableType::Columns);
        }

        static auto makeConst(const location& loc, std::string&& name,
                std::vector<std::string>&& ext,
                FormulaContainer&& vec)
        {
            return std::make_shared<TableAssign>(loc, std::move(name),
                    std::move(ext), std::move(vec), TableType::Const);
        }

        TableAssign(const location& loc, std::string&& name,
                std::vector<std::string>&& ext,
                FormulaContainer&& vec, TableType tt);

        auto begin() const noexcept
        {
            return m_vec.begin();
        }

        auto end() const noexcept
        {
            return m_vec.end();
        }

        const char* getType() const noexcept override;
        std::string stringize() const override;
        void accept(TopVisitor& v) const override;

        bool isConst() const noexcept
        {
            return m_type == TableType::Const;
        }
    private:
        std::string m_title;
        FormulaContainer m_vec;
        TableType m_type;
    };

    class TopImportAssign:
        public TopAssign
    {
    public:
        TopImportAssign(const location& loc, std::string&& name, std::string&& val):
            TopAssign(loc, std::move(name), {}), m_val(std::move(val)) {}

        explicit TopImportAssign(const std::shared_ptr<ImportAssign>& a):
            TopAssign(a->getLocation(), std::string(a->getName()),
                    std::vector<std::string>()),
            m_val(a->getValue()) {}

        std::string getValue() const noexcept
        {
            return m_val;
        }

        const char* getType() const noexcept override;
        std::string stringize() const override;
        void accept(TopVisitor& v) const override;
    private:
        std::string m_val;
    };

    class TopVisitor
    {
    public:
        virtual ~TopVisitor() = default;
        virtual void visit(const TableAssign&) = 0;
        virtual void visit(const ChartAssign&) = 0;
        virtual void visit(const TopImportAssign&) = 0;
    };
}

#endif // LAX_SRC_FORMULA_AST_HPP_
