#ifndef LAX_TABLE_COMPILER_HPP_
#define LAX_TABLE_COMPILER_HPP_

#include <any>
#include <cassert>
#include <iosfwd>
#include <map>
#include <memory>
#include <string>
#include <string_view>

#include "code_object.hpp"
#include "environment.hpp"
#include "formula_ast.hpp"
#include "importer/path_finder.hpp"

namespace lax
{
    struct CompileFlags;

    class Table:
        public CodeObject
    {
    public:
        struct CopyCompileInfo {};
        using AssignIter = TableAssign::iterator;

        struct ScalarData:
            CodeObject
        {
            ScalarData(const ValueNode::Ptr& n, std::string_view nm, bool ro,
                    PDataManager* mgr):
                ScalarData(n, ICaseString(nm), ro, mgr) {}

            ScalarData(const ValueNode::Ptr& n, ICaseString&& nm, bool ro,
                    PDataManager* mgr):
                node(n), name(std::move(nm)), data(getData(mgr)), readOnly(ro) {}

            ValueNode::Ptr node;
            ICaseString name;
            ICaseString m_pdataName;
            std::any* data;
            bool readOnly;
        private:
            ICaseStringView getName() const noexcept override
            {
                return m_pdataName;
            }

            std::string_view getType() const noexcept override
            {
                return "scalar";
            }

            std::any makePData() const override;
            std::any* getData(PDataManager* mgr);
        };

        struct ReduceData:
            CodeObject
        {
            ReduceData(ICaseString&& n, PDataManager* mgr):
                name(std::move(n)), data(getData(mgr)) {}

            ReduceData(std::string_view n, PDataManager* mgr):
                name(n), data(getData(mgr)) {}

            ICaseString name;
            std::any* data;
        private:
            ICaseStringView getName() const noexcept override
            {
                return name;
            }

            std::string_view getType() const noexcept override
            {
                return "reduce";
            }

            std::any makePData() const override
            {
                return std::any();
            }

            std::any* getData(PDataManager* mgr)
            {
                return mgr? &getPData(*mgr):nullptr;
            }
        };

        enum class Kind
        {
            Column,
            Const,
            Import,
        };

        Table(const Importer::ConstPtr& f, const ICaseString& name);
        Table(const Table& tbl, CopyCompileInfo);

        Table(const Table&) = delete;
        Table(Table&&) = default;

        Table& operator=(Table&&) = default;

        Environment::Ptr recompile(const CompileFlags& flags);

        void forceRecompute();

        Object::Ptr getObject(std::size_t col, std::size_t row) const
        {
            return (*m_data.cols.at(col).obj)[row];
        }

        void setObject(std::size_t col, std::size_t row, const Object::Ptr& obj);

        auto getArray(std::size_t col) const
        {
            return m_data.cols.at(col).obj;
        }

        void setArray(std::size_t col, const std::shared_ptr<ColumnObject>& arr);

        TypeInfo getType(std::size_t col) const
        {
            return m_data.cols.at(col).type;
        }

        std::size_t getNumRows() const noexcept
        {
            return m_nonCode->numRows;
        }

        std::size_t getNumCols() const noexcept
        {
            return m_data.cols.size();
        }

        std::size_t getNumReduces() const noexcept
        {
            return m_data.reduces.size();
        }

        std::size_t getNumScalars() const noexcept
        {
            return m_data.scalars.size();
        }

        ValueNode::Ptr getNode(std::size_t col) const
        {
            return m_data.cols.at(col).node;
        }

        ICaseStringView getName(std::size_t col) const
        {
            return m_data.cols.at(col).name;
        }

        std::any& getColData(std::size_t col)
        {
            return *m_data.cols.at(col).data;
        }

        const std::any& getColData(std::size_t col) const
        {
            return *m_data.cols.at(col).data;
        }

        ICaseStringView getReduceName(std::size_t row) const
        {
            return m_data.reduces.at(row).name;
        }

        ICaseString getTableName() const
        {
            return m_name;
        }

        ValueNode::Ptr getReduce(std::size_t col, std::size_t r) const
        {
            return m_data.cols.at(col).reduces.at(r);
        }

        const ScalarData& getScalar(std::size_t n) const
        {
            return m_data.scalars.at(n);
        }

        std::any& getScalarData(std::size_t n)
        {
            return *m_data.scalars.at(n).data;
        }

        const std::any& getScalarData(std::size_t n) const
        {
            return *getScalar(n).data;
        }

        const ReduceData& getReduce(std::size_t n) const
        {
            return m_data.reduces.at(n);
        }

        std::any& getReduceData(std::size_t n)
        {
            return *m_data.reduces.at(n).data;
        }

        const std::any& getReduceData(std::size_t n) const
        {
            return *getReduce(n).data;
        }

        void setScalar(std::size_t n, const Object::Ptr& obj);
        bool isReadOnly(std::size_t row, std::size_t col) const;

        void setNumRows(int n);
        void insert(int firstIdx, int n = 1);
        void erase(int firstIdx, int n = 1);
    private:
        using VarName = std::pair<ICaseString, TypeInfo>;

        class CompileAssign;

        struct Column:
            CodeObject
        {
            Column(std::string_view name, bool isReadOnly, std::size_t nReduces,
                    const ValueNode::Ptr& n, const TypeInfo& type,
                    PDataManager* mgr):
                node(n), type(type), name(name), reduces(nReduces),
                data(getData(mgr)), readOnly(isReadOnly) {}

            std::shared_ptr<ColumnObject> obj;
            ValueNode::Ptr node;
            TypeInfo type;
            ICaseString name;
            ICaseString m_pdataName;
            std::vector<ValueNode::Ptr> reduces;
            std::any* data;
            bool readOnly;
        private:
            ICaseStringView getName() const noexcept override
            {
                return m_pdataName;
            }

            std::string_view getType() const noexcept override
            {
                return "column";
            }

            std::any makePData() const override;
            std::any* getData(PDataManager* mgr);
        };

        struct TableData
        {
            std::vector<Column> cols;
            std::vector<ReduceData> reduces;
            std::vector<ScalarData> scalars;

            friend void swap(TableData& lhs, TableData& rhs)
            {
                swap(lhs.cols, rhs.cols);
                swap(lhs.reduces, rhs.reduces);
                swap(lhs.scalars, rhs.scalars);
            }
        };

        struct NonCodeData
        {
            std::size_t numRows;
        };

        void doRecalc(const ValueNode::Ptr& n, int index);

        std::any makePData() const override
        {
            return std::any(NonCodeData {0});
        }

        ICaseStringView getName() const noexcept override
        {
            return m_name;
        }

        std::string_view getType() const noexcept override
        {
            return "table";
        }

        Importer::ConstPtr m_finder;
        TableData m_data;
        ICaseString m_name;
        NonCodeData* m_nonCode;
    };

    class CompileLoader:
        public PathLoader
    {
    public:
        explicit CompileLoader(const Importer::ConstPtr& f):
            m_tbl(f, ICaseString("<import>")) {}

        Environment::ConstPtr load(const std::filesystem::path& p);
    private:
        Table m_tbl;
    };

    struct CompileFlags
    {
        using AssignIter = Table::AssignIter;
        using Kind = Table::Kind;

        CompileFlags& setAssigns(AssignIter f, AssignIter l) noexcept
        {
            first = f;
            last = l;
            return *this;
        }

        CompileFlags& setEnv(const Environment::ConstPtr& e) noexcept
        {
            env = e;
            return *this;
        }

        CompileFlags& setKind(Kind k) noexcept
        {
            kind = k;
            return *this;
        }

        CompileFlags& setDir(std::string_view s) noexcept
        {
            dir = s;
            return *this;
        }

        CompileFlags& setMgr(PDataManager& m) noexcept
        {
            mgr = &m;
            return *this;
        }

        AssignIter first;
        AssignIter last;
        Environment::ConstPtr env;
        Kind kind;
        std::string_view dir;
        PDataManager* mgr;
    };
}

#endif // LAX_TABLE_COMPILER_HPP_
