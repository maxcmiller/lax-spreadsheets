#include "top_compiler.hpp"

#include <unordered_set>

#include "formula_driver.hpp"
#include "table_compiler.hpp"
#include "util/scope_exit.hpp"

namespace lax
{
    class TopCompiler::TCVisitor:
        public TopVisitor
    {
    public:
        TCVisitor(TopCompiler* tc, std::string_view dir, CompilationFailure& cf):
            m_tc(tc), m_dir(dir), m_cf(cf),
            m_env(std::make_shared<Environment>(*tc->m_sys)) {}

        void visit(const TableAssign& t);
        void visit(const ChartAssign& c);
        void visit(const TopImportAssign& tia);
    private:
        int findColumn(const Table& tbl, ICaseStringView name);
        int findScalar(const Table& tbl, ICaseStringView name);

        TopCompiler* m_tc;
        std::string_view m_dir;
        CompilationFailure& m_cf;
        Environment::Ptr m_env;
    };

    int TopCompiler::TCVisitor::findColumn(const Table& tbl, ICaseStringView name)
    {
        for (std::size_t i = 0; i < tbl.getNumCols(); ++i)
        {
            if (tbl.getName(i) == name)
            {
                return i;
            }
        }

        return -1;
    }

    int TopCompiler::TCVisitor::findScalar(const Table& tbl, ICaseStringView name)
    {
        for (std::size_t i = 0; i < tbl.getNumScalars(); ++i)
        {
            if (tbl.getScalar(i).name == name)
            {
                return i;
            }
        }

        return -1;
    }

    void TopCompiler::TCVisitor::visit(const TableAssign& t)
    {
        if (t.numExtends() > 0)
        {
            m_cf.pushError(t.getLoc(), "tables cannot yet extend other objects");
        }

        const ICaseString name(t.getName());

        if (m_tc->m_objMap.count(name))
        {
            m_cf.pushError(t.getLoc(), t.getName() + " already exists");
        }

        Importer::ConstPtr imp(m_tc->m_info, &m_tc->m_info->imp);
        TablePtr tbl = std::make_shared<Table>(imp, name);

        try
        {
            Table::Kind kind;

            if (t.isConst())
            {
                kind = Table::Kind::Const;
            }
            else
            {
                kind = Table::Kind::Column;
            }

            auto env = tbl->recompile(
                    CompileFlags()
                        .setAssigns(t.begin(), t.end())
                        .setEnv(m_env)
                        .setKind(kind)
                        .setDir(m_dir)
                        .setMgr(m_tc->m_mgr));
            // env's outer environment should be m_env, but to avoid a reference
            // cycle, we remove its outer environment
            env->setOuter(nullptr);

            if (!m_env->addEnvironment(t.getName(), env))
            {
                m_cf.pushError(t.getLoc(), "couldn't add constants from " +
                        t.getName() + ": name is already used");
            }
        }
        catch (CompilationFailure& ex)
        {
            m_cf.copyFrom(ex);
            return;
        }

        m_tc->m_tblMap[name] = tbl;
        m_tc->m_tbls.push_back(tbl);
        m_tc->m_objMap[name] = tbl;
    }

    void TopCompiler::TCVisitor::visit(const ChartAssign& c)
    {
        if (c.numExtends() != 1)
        {
            m_cf.pushError(c.getLoc(), "charts must extend exactly one table");
            return;
        }

        TablePtr tbl;

        if (auto itr = m_tc->m_objMap.find(static_cast<ICaseString>(*c.beginExtends()));
            itr != m_tc->m_objMap.end())
        {
            TablePtr* ptbl = std::get_if<TablePtr>(&itr->second);

            if (ptbl)
            {
                tbl = *ptbl;
            }
            else
            {
                m_cf.pushError(c.getLoc(), "charts may only extend tables");
                return;
            }
        }
        else
        {
            m_cf.pushError(c.getLoc(), "object " + *c.beginExtends() + " not found");
            return;
        }

        bool success = true;
        const auto& d = c.getData();
        const ICaseString key(c.getName() + m_typeSep + d.type);
        std::shared_ptr<ChartBase> chart = nullptr;
        ChartBuilder::Ptr b;

        if (auto itr = m_tc->m_info->builders.find(static_cast<ICaseString>(d.type));
            itr != m_tc->m_info->builders.end())
        {
            b = itr->second->clone(tbl, ICaseStringView(c.getName()),
                    m_tc->m_mgr, m_env->getSystem());
        }
        else
        {
            m_cf.pushError(c.getLoc(), "no chart type matches " + c.getData().type);
            return;
        }

        if (int index = findColumn(*tbl, static_cast<ICaseStringView>(d.index));
            index >= 0)
        {
            try
            {
                b->setIndex(*tbl, index);
            }
            catch (ChartBuilder::Failure& ex)
            {
                m_cf.pushError(c.getLoc(), ex.what());
                success = false;
            }
        }
        else
        {
            m_cf.pushError(c.getLoc(), "could not find column " + d.index);
            success = false;
        }

        for (const auto& s: d.series)
        {
            try
            {
                ICaseStringView sview(s);

                if (int ser = findColumn(*tbl, sview); ser >= 0)
                {
                    b->addSeriesArray(*tbl, ser);
                }
                else if (int ser = findScalar(*tbl, sview); ser >= 0)
                {
                    b->addSeriesFunc(*tbl, ser);
                }
                else
                {
                    m_cf.pushError(c.getLoc(), "could not find " + s);
                    success = false;
                }
            }
            catch (ChartBuilder::Failure& ex)
            {
                m_cf.pushError(c.getLoc(), ex.what());
                success = false;
            }
        }

        if (success)
        {
            auto chart = b->build();
            m_tc->m_chartMap[key] = chart;
            m_tc->m_objMap[static_cast<ICaseString>(c.getName())] = chart;
            m_tc->m_charts.push_back(chart);
        }
    }

    void TopCompiler::TCVisitor::visit(const TopImportAssign& tia)
    {
        try
        {
            auto env =
            m_tc->m_info->imp.find(tia.getValue(), m_dir);

            if (tia.getName().empty())
            {
                m_env->copyVarsFrom(*env);
            }
            else if (!m_env->addEnvironment(tia.getName(), env))
            {
                m_cf.pushError(tia.getLoc(), "import name " + tia.getName() + " exists");
            }
        }
        catch (CompilationFailure& ex)
        {
            m_cf.pushError(tia.getLoc(), "In import " + tia.getValue());
            m_cf.copyFrom(ex);
        }
        catch (std::bad_alloc&)
        {
            throw;
        }
        catch (std::exception& ex)
        {
            using namespace std::literals::string_literals;
            m_cf.pushError(tia.getLoc(), "import error: "s + ex.what());
        }
    }

    ValueSystem& TopCompiler::recompile(std::istream& istr)
    {
        FormulaDriver drv(istr, m_file);
        std::vector<TopAssign::Ptr> vec;

        CompilationFailure cf;

        if (!drv.parseAssigns(vec))
        {
            for (auto itr = drv.beginErrors(); itr != drv.endErrors(); ++itr)
            {
                cf.pushError(itr->loc, itr->msg);
            }

            throw cf;
        }

        ScopeExit raii([this, oldTables = std::move(m_tbls),
                oldCharts = std::move(m_charts),
                oldObjs = std::move(m_objMap),
                oldSys = std::move(m_sys)]() mutable
        {
            using std::swap;
            swap(oldTables, m_tbls);
            swap(oldCharts, m_charts);
            swap(oldObjs, m_objMap);
            swap(oldSys, m_sys);
        });

        m_sys.reset(new ValueSystem);
        TCVisitor tcv(this, m_dir, cf);

        for (const auto& a: vec)
        {
            a->accept(tcv);
        }

        if (cf.size() != 0)
        {
            throw cf;
        }

        raii.release();
        return *m_sys;
    }

    TopCompiler::TablePtr TopCompiler::findTable(const std::string& name)
    {
        return m_tblMap.at(static_cast<ICaseString>(name));
    }

    TopCompiler::ChartPtr TopCompiler::findChart(const std::string& name,
            const std::string& type)
    {
        ICaseString chartName(name + m_typeSep + type);
        return m_chartMap.at(chartName);
    }
}
