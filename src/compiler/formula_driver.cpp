#include "formula_driver.hpp"

#include <cstdlib>
#include <unordered_map>

namespace lax
{
    namespace
    {
        // pulls out the entire UTF-8 Unicode character from the stream istr
        // with first as the first byte
        std::string extractChar(char first, std::istream& istr)
        {
            constexpr int highBit       = 0b10000000;
            constexpr int twoByteMask   = 0b11100000;
            constexpr int is2Bytes      = 0b11000000;
            constexpr int threeByteMask = 0b11110000;
            constexpr int is3Bytes      = 0b11100000;
            constexpr int fourByteMask  = 0b11111000;
            constexpr int is4Bytes      = 0b11110000;

            if ((first & highBit) == 0) // in ASCII range
            {
                return std::string(1, first);
            }
            else if ((first & twoByteMask) == is2Bytes)
            {
                std::string str(2, '\0');
                str[0] = first;
                str[1] = istr.get();
                return str;
            }
            else if ((first & threeByteMask) == is3Bytes)
            {
                std::string str(3, '\0');
                str[0] = first;
                str[1] = istr.get();
                str[2] = istr.get();
                return str;
            }
            else if ((first & fourByteMask) == is4Bytes)
            {
                std::string str(4, '\0');
                str[0] = first;
                str[1] = istr.get();
                str[2] = istr.get();
                str[3] = istr.get();
                return str;
            }

            throw std::runtime_error("bad UTF-8 sequence");
        }
    }

    FormulaExpr::Ptr FormulaDriver::parseExpr(bool debug)
    {
        m_value.emplace<FormulaExpr::Ptr>();
        parser p(*this);
        p.set_debug_level(debug);
        p();

        if (!m_errors.empty())
        {
            return nullptr;
        }
        else
        {
            return std::get<FormulaExpr::Ptr>(m_value);
        }
    }

    bool FormulaDriver::parseAssigns(AssignContainer& res, bool debug)
    {
        m_value.emplace<AssignContainer>();
        parser p(*this);
        p.set_debug_level(debug);
        p();

        if (m_errors.empty())
        {
            res = std::move(std::get<AssignContainer>(m_value));
        }

        return m_errors.empty();
    }

    std::shared_ptr<TableAssign> FormulaDriver::parseTable(bool debug)
    {
        m_value.emplace<std::shared_ptr<TableAssign> >();
        parser p(*this);
        p.set_debug_level(debug);
        p();

        if (m_errors.empty())
        {
            return std::get<std::shared_ptr<TableAssign> >(m_value);
        }
        else
        {
            return nullptr;
        }
    }

    parser::symbol_type FormulaDriver::lex()
    {
        if (m_first)
        {
            m_first = false;

            switch (m_value.index())
            {
            case kFormula:
                return parser::make_START_EXPR(m_loc);
            case kAssigns:
                return parser::make_START_ASSIGN(m_loc);
            case kTable:
                return parser::make_START_TABLE(m_loc);
            default:
                assert(!"bad value for index in lexer");
                throw parser::syntax_error(m_loc, "bug in parser");
            }
        }

        m_loc.step();

        while (!m_src.eof() &&
            (std::isspace(m_src.peek()) || m_src.peek() == '#'))
        {
            if (m_src.peek() == '#')
            {
                m_src.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                m_loc.lines(1);
            }
            else
            {
                if (m_src.get() == '\n')
                {
                    m_loc.lines(1);
                }
                else
                {
                    m_loc.columns(1);
                }
            }
        }

        m_loc.step();

        if (m_src.eof())
        {
            return parser::make_EOF(m_loc);
        }

        char c = m_src.get();

        switch (c)
        {
#define LAX_CHAR_CASE(c, tok)                   \
        case c:                                 \
            m_loc.columns();                    \
            return parser::make_##tok(m_loc)
        LAX_CHAR_CASE('+', PLUS);
        LAX_CHAR_CASE('*', STAR);
        LAX_CHAR_CASE('/', SLASH);
        LAX_CHAR_CASE('=', EQUAL);
        LAX_CHAR_CASE('(', LPAREN);
        LAX_CHAR_CASE(')', RPAREN);
        LAX_CHAR_CASE('$', DOLLAR);
        LAX_CHAR_CASE(',', COMMA);
        LAX_CHAR_CASE('.', PERIOD);
        LAX_CHAR_CASE('@', AT_SIGN);
        LAX_CHAR_CASE(':', COLON);
        LAX_CHAR_CASE('{', LBRACE);
        LAX_CHAR_CASE('}', RBRACE);
        LAX_CHAR_CASE('[', LBRACKET);
        LAX_CHAR_CASE(']', RBRACKET);
        LAX_CHAR_CASE('|', VERTBAR);
        LAX_CHAR_CASE('^', EXP);
        LAX_CHAR_CASE('%', PERCENT);
#undef LAX_CHAR_CASE
#define LAX_DBL_CHAR_CASE(c, tok)                           \
        case c:                                             \
            if (!m_src.eof() && m_src.peek() == '=')        \
            {                                               \
                m_src.get();                                \
                m_loc.columns(2);                           \
                return parser::make_##tok##_EQUAL(m_loc);   \
            }                                               \
            else                                            \
            {                                               \
                m_loc.columns();                            \
                return parser::make_##tok(m_loc);           \
            }
        LAX_DBL_CHAR_CASE('<', LESS)
        LAX_DBL_CHAR_CASE('>', GREATER)
#undef LAX_DBL_CHAR_CASE
        case '!':
            if (!m_src.eof() && m_src.peek() == '=')
            {
                m_src.get();
                m_loc.columns(2);
                return parser::make_NOT_EQUAL(m_loc);
            }
            else
            {
                m_loc.columns();
                throw parser::syntax_error(m_loc, "invalid character !");
            }
        case '-':
            if (!m_src.eof() && m_src.peek() == '>')
            {
                m_src.get();
                m_loc.columns(2);
                return parser::make_RARROW(m_loc);
            }
            else
            {
                m_loc.columns();
                return parser::make_MINUS(m_loc);
            }

            break;
        case '"':
        {
            std::string got;
            bool good = false;
            m_loc.columns(1); // for first "

            while (!m_src.eof())
            {
                char c = m_src.get();

                if (c == '\\' && !m_src.eof() && m_src.peek() == '"')
                {
                    got += '"';
                    m_loc.columns(2);
                    m_src.get();
                    continue;
                }
                else if (c == '\n')
                {
                    got += '\n';
                    m_loc.lines(1);
                }
                else if (c == '"')
                {
                    good = true;
                    break;
                }
                else
                {
                    got += c;
                    m_loc.columns();
                }
            }

            m_loc.columns(1); // for last "

            if (!good)
            {
                throw parser::syntax_error(m_loc, "unterminated string");
            }

            return parser::make_STRING(std::move(got), m_loc);
        }
        default:
            if (std::isalpha(c) || c == '_')
            {
                ICaseString got(std::string(1, c));

                while (!m_src.eof() &&
                    (std::isalnum(m_src.peek()) ||
                    m_src.peek() == '_'))
                {
                    *got += m_src.get();
                }

                m_loc.columns(got->size());

                using TokenMaker = parser::symbol_type(*)(location);
                static std::unordered_map<ICaseStringView, TokenMaker> map
                {
                    std::pair("as"_isview,      &parser::make_AS),
                    std::pair("import"_isview,  &parser::make_IMPORT),
                    std::pair("fn"_isview,      &parser::make_FN),
                    std::pair("reduces"_isview, &parser::make_REDUCES),
                    std::pair("scalar"_isview,  &parser::make_SCALAR),
                    std::pair("and"_isview,     &parser::make_AND),
                    std::pair("or"_isview,      &parser::make_OR),
                    std::pair("not"_isview,     &parser::make_NOT),
                    std::pair("chart"_isview,   &parser::make_CHART),
                    std::pair("table"_isview,   &parser::make_TABLE),
                    std::pair("extends"_isview, &parser::make_EXTENDS),
                    std::pair("const"_isview,   &parser::make_CONST),
                };

                if (auto itr = map.find(got); itr != map.end())
                {
                    return (itr->second)(m_loc);
                }
                else
                {
                    return parser::make_IDENT(*std::move(got), m_loc);
                }
            }
            else if (std::isdigit(c))
            {
                double got;
                auto first = m_src.tellg();
                m_src.putback(c);
                m_src >> got;
                m_loc.columns(m_src.tellg() - first + 1);

                return parser::make_NUMBER(got, m_loc);
            }
        }

        m_loc.columns();
        throw parser::syntax_error(m_loc, "invalid character " + extractChar(c, m_src));
    }
}
