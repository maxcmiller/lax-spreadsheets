// A Bison parser, made by GNU Bison 3.7.

// Skeleton interface for Bison LALR(1) parsers in C++

// Copyright (C) 2002-2015, 2018-2020 Free Software Foundation, Inc.

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// As a special exception, you may create a larger work that contains
// part or all of the Bison parser skeleton and distribute that work
// under terms of your choice, so long as that work isn't itself a
// parser generator using the skeleton or a modified version thereof
// as a parser skeleton.  Alternatively, if you modify or redistribute
// the parser skeleton itself, you may (at your option) remove this
// special exception, which will cause the skeleton and the resulting
// Bison output files to be licensed under the GNU General Public
// License without this special exception.

// This special exception was added by the Free Software Foundation in
// version 2.2 of Bison.


/**
 ** \file formula_parser.tab.hpp
 ** Define the lax::parser class.
 */

// C++ LALR(1) parser skeleton written by Akim Demaille.

// DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
// especially those whose name start with YY_ or yy_.  They are
// private implementation details that can be changed or removed.

#ifndef YY_YY_FORMULA_PARSER_TAB_HPP_INCLUDED
# define YY_YY_FORMULA_PARSER_TAB_HPP_INCLUDED
// "%code requires" blocks.
#line 27 "formula_parser.ypp"

#include <memory>
#include <unordered_map>
#include <variant>

#include "location.hpp"

namespace lax
{
    class ChartAssign;
    class FormulaDriver;
    class FormulaExpr;
    class FormulaAssign;
    class TableAssign;
    class TopAssign;
    using ExprPtr = std::shared_ptr<FormulaExpr>;
    using AssignPtr = std::shared_ptr<FormulaAssign>;

    class TypeInfo;

    class TypeData
    {
    public:
        TypeData():
            m_name(""), m_params() {}

        template <typename...Params>
        TypeData(std::string name, Params&&...params):
            m_name(std::move(name)), m_params(std::forward<Params>(params)...) {}

        TypeData(const TypeData&) = default;
        TypeData(TypeData&&) = default;

        TypeData& operator=(const TypeData&) = default;
        TypeData& operator=(TypeData&&) = default;

        TypeInfo getType() const;

        std::string_view getName() const noexcept
        {
            return m_name;
        }

        auto begin() const noexcept
        {
            return m_params.begin();
        }

        auto end() const noexcept
        {
            return m_params.end();
        }

        std::string stringize() const;
    private:
        std::string m_name;
        std::vector<TypeData> m_params;
    };

    namespace tags
    {
        struct SimpleType;
    }

    template <typename, typename>
    class SimpleAssign;

    using TypeAssignPtr = std::shared_ptr<SimpleAssign<TypeData, tags::SimpleType> >;

    struct JSONObject;
    struct JSONArray;
    using JSONValue = std::variant<std::string, JSONArray, JSONObject>;

    struct JSONArray
    {
        JSONArray() = default;
        inline JSONArray(JSONValue&& v);
        inline void append(JSONValue&& val);

        std::vector<JSONValue> elems;
    };

    using JSONField = std::pair<ICaseString, JSONValue>;

    struct JSONObject
    {
        JSONObject() = default;
        inline JSONObject(JSONObject&&);
        inline JSONObject& operator=(JSONObject&&);
        inline ~JSONObject();

        JSONObject(JSONField&& f):
            objs()
        {
            auto obj = std::make_unique<JSONValue>(std::move(f.second));
            objs.emplace(std::move(f.first), std::move(obj));
        }

        void append(JSONField&& f, const location& loc);

        std::unordered_map<ICaseString, std::unique_ptr<JSONValue> > objs;
    };

    inline JSONArray::JSONArray(JSONValue&& v):
        elems()
    {
        elems.emplace_back(std::move(v));
    }

    inline void JSONArray::append(JSONValue&& val)
    {
        elems.push_back(std::move(val));
    }

    inline JSONObject::JSONObject(JSONObject&&) = default;
    inline JSONObject& JSONObject::operator=(JSONObject&&) = default;
    inline JSONObject::~JSONObject() = default;
}

#line 169 "formula_parser.tab.hpp"


# include <cstdlib> // std::abort
# include <iostream>
# include <stdexcept>
# include <string>
# include <vector>

#if defined __cplusplus
# define YY_CPLUSPLUS __cplusplus
#else
# define YY_CPLUSPLUS 199711L
#endif

// Support move semantics when possible.
#if 201103L <= YY_CPLUSPLUS
# define YY_MOVE           std::move
# define YY_MOVE_OR_COPY   move
# define YY_MOVE_REF(Type) Type&&
# define YY_RVREF(Type)    Type&&
# define YY_COPY(Type)     Type
#else
# define YY_MOVE
# define YY_MOVE_OR_COPY   copy
# define YY_MOVE_REF(Type) Type&
# define YY_RVREF(Type)    const Type&
# define YY_COPY(Type)     const Type&
#endif

// Support noexcept when possible.
#if 201103L <= YY_CPLUSPLUS
# define YY_NOEXCEPT noexcept
# define YY_NOTHROW
#else
# define YY_NOEXCEPT
# define YY_NOTHROW throw ()
#endif

// Support constexpr when possible.
#if 201703 <= YY_CPLUSPLUS
# define YY_CONSTEXPR constexpr
#else
# define YY_CONSTEXPR
#endif


#ifndef YY_ASSERT
# include <cassert>
# define YY_ASSERT assert
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && ! defined __ICC && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                            \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif

#line 157 "formula_parser.ypp"
namespace lax {
#line 304 "formula_parser.tab.hpp"




  /// A Bison parser.
  class parser
  {
  public:
#ifndef YYSTYPE
  /// A buffer to store and retrieve objects.
  ///
  /// Sort of a variant, but does not keep track of the nature
  /// of the stored data, since that knowledge is available
  /// via the current parser state.
  class semantic_type
  {
  public:
    /// Type of *this.
    typedef semantic_type self_type;

    /// Empty construction.
    semantic_type () YY_NOEXCEPT
      : yybuffer_ ()
    {}

    /// Construct and fill.
    template <typename T>
    semantic_type (YY_RVREF (T) t)
    {
      YY_ASSERT (sizeof (T) <= size);
      new (yyas_<T> ()) T (YY_MOVE (t));
    }

#if 201103L <= YY_CPLUSPLUS
    /// Non copyable.
    semantic_type (const self_type&) = delete;
    /// Non copyable.
    self_type& operator= (const self_type&) = delete;
#endif

    /// Destruction, allowed only if empty.
    ~semantic_type () YY_NOEXCEPT
    {}

# if 201103L <= YY_CPLUSPLUS
    /// Instantiate a \a T in here from \a t.
    template <typename T, typename... U>
    T&
    emplace (U&&... u)
    {
      return *new (yyas_<T> ()) T (std::forward <U>(u)...);
    }
# else
    /// Instantiate an empty \a T in here.
    template <typename T>
    T&
    emplace ()
    {
      return *new (yyas_<T> ()) T ();
    }

    /// Instantiate a \a T in here from \a t.
    template <typename T>
    T&
    emplace (const T& t)
    {
      return *new (yyas_<T> ()) T (t);
    }
# endif

    /// Instantiate an empty \a T in here.
    /// Obsolete, use emplace.
    template <typename T>
    T&
    build ()
    {
      return emplace<T> ();
    }

    /// Instantiate a \a T in here from \a t.
    /// Obsolete, use emplace.
    template <typename T>
    T&
    build (const T& t)
    {
      return emplace<T> (t);
    }

    /// Accessor to a built \a T.
    template <typename T>
    T&
    as () YY_NOEXCEPT
    {
      return *yyas_<T> ();
    }

    /// Const accessor to a built \a T (for %printer).
    template <typename T>
    const T&
    as () const YY_NOEXCEPT
    {
      return *yyas_<T> ();
    }

    /// Swap the content with \a that, of same type.
    ///
    /// Both variants must be built beforehand, because swapping the actual
    /// data requires reading it (with as()), and this is not possible on
    /// unconstructed variants: it would require some dynamic testing, which
    /// should not be the variant's responsibility.
    /// Swapping between built and (possibly) non-built is done with
    /// self_type::move ().
    template <typename T>
    void
    swap (self_type& that) YY_NOEXCEPT
    {
      std::swap (as<T> (), that.as<T> ());
    }

    /// Move the content of \a that to this.
    ///
    /// Destroys \a that.
    template <typename T>
    void
    move (self_type& that)
    {
# if 201103L <= YY_CPLUSPLUS
      emplace<T> (std::move (that.as<T> ()));
# else
      emplace<T> ();
      swap<T> (that);
# endif
      that.destroy<T> ();
    }

# if 201103L <= YY_CPLUSPLUS
    /// Move the content of \a that to this.
    template <typename T>
    void
    move (self_type&& that)
    {
      emplace<T> (std::move (that.as<T> ()));
      that.destroy<T> ();
    }
#endif

    /// Copy the content of \a that to this.
    template <typename T>
    void
    copy (const self_type& that)
    {
      emplace<T> (that.as<T> ());
    }

    /// Destroy the stored \a T.
    template <typename T>
    void
    destroy ()
    {
      as<T> ().~T ();
    }

  private:
#if YY_CPLUSPLUS < 201103L
    /// Non copyable.
    semantic_type (const self_type&);
    /// Non copyable.
    self_type& operator= (const self_type&);
#endif

    /// Accessor to raw memory as \a T.
    template <typename T>
    T*
    yyas_ () YY_NOEXCEPT
    {
      void *yyp = yybuffer_.yyraw;
      return static_cast<T*> (yyp);
     }

    /// Const accessor to raw memory as \a T.
    template <typename T>
    const T*
    yyas_ () const YY_NOEXCEPT
    {
      const void *yyp = yybuffer_.yyraw;
      return static_cast<const T*> (yyp);
     }

    /// An auxiliary type to compute the largest semantic type.
    union union_type
    {
      // assign
      // import
      // reduce
      char dummy1[sizeof (AssignPtr)];

      // expr
      // mono_expr
      char dummy2[sizeof (ExprPtr)];

      // json_array
      // json_elems
      char dummy3[sizeof (JSONArray)];

      // json_field
      char dummy4[sizeof (JSONField)];

      // json_object
      // json_fields
      char dummy5[sizeof (JSONObject)];

      // json_value
      char dummy6[sizeof (JSONValue)];

      // decl
      // arg_decl
      char dummy7[sizeof (TypeAssignPtr)];

      // type_name
      // ret_spec
      char dummy8[sizeof (TypeData)];

      // "number"
      // number
      char dummy9[sizeof (double)];

      // chart
      char dummy10[sizeof (std::shared_ptr<ChartAssign> )];

      // table
      char dummy11[sizeof (std::shared_ptr<TableAssign> )];

      // "identifier"
      // "string"
      // nest_name
      char dummy12[sizeof (std::string)];

      // table_assigns
      char dummy13[sizeof (std::vector<AssignPtr> )];

      // arg_list
      // args
      char dummy14[sizeof (std::vector<ExprPtr> )];

      // arg_decls
      // arg_decl_list
      char dummy15[sizeof (std::vector<TypeAssignPtr> )];

      // type_params
      char dummy16[sizeof (std::vector<TypeData> )];

      // assigns
      char dummy17[sizeof (std::vector<std::shared_ptr<TopAssign> > )];

      // reduce_args
      // extension
      // ext_list
      char dummy18[sizeof (std::vector<std::string> )];
    };

    /// The size of the largest semantic type.
    enum { size = sizeof (union_type) };

    /// A buffer to store semantic values.
    union
    {
      /// Strongest alignment constraints.
      long double yyalign_me;
      /// A buffer large enough to store any of the semantic values.
      char yyraw[size];
    } yybuffer_;
  };

#else
    typedef YYSTYPE semantic_type;
#endif
    /// Symbol locations.
    typedef lax::location location_type;

    /// Syntax errors thrown from user actions.
    struct syntax_error : std::runtime_error
    {
      syntax_error (const location_type& l, const std::string& m)
        : std::runtime_error (m)
        , location (l)
      {}

      syntax_error (const syntax_error& s)
        : std::runtime_error (s.what ())
        , location (s.location)
      {}

      ~syntax_error () YY_NOEXCEPT YY_NOTHROW;

      location_type location;
    };

    /// Token kinds.
    struct token
    {
      enum token_kind_type
      {
        kLAX_YYEMPTY = -2,
    kLAX_EOF = 0,                  // "end of input"
    kLAX_YYerror = 1,              // error
    kLAX_YYUNDEF = 2,              // "invalid token"
    kLAX_IDENT = 3,                // "identifier"
    kLAX_STRING = 4,               // "string"
    kLAX_NUMBER = 5,               // "number"
    kLAX_PLUS = 6,                 // "+"
    kLAX_MINUS = 7,                // "-"
    kLAX_STAR = 8,                 // "*"
    kLAX_SLASH = 9,                // "/"
    kLAX_EXP = 10,                 // "^"
    kLAX_LESS = 11,                // "<"
    kLAX_GREATER = 12,             // ">"
    kLAX_LESS_EQUAL = 13,          // "<="
    kLAX_GREATER_EQUAL = 14,       // ">="
    kLAX_EQUAL = 15,               // "="
    kLAX_NOT_EQUAL = 16,           // "!="
    kLAX_AND = 17,                 // "and"
    kLAX_OR = 18,                  // "or"
    kLAX_NOT = 19,                 // "not"
    kLAX_VERTBAR = 20,             // "|"
    kLAX_LPAREN = 21,              // "("
    kLAX_RPAREN = 22,              // ")"
    kLAX_LBRACE = 23,              // "{"
    kLAX_RBRACE = 24,              // "}"
    kLAX_LBRACKET = 25,            // "["
    kLAX_RBRACKET = 26,            // "]"
    kLAX_DOLLAR = 27,              // "$"
    kLAX_AT_SIGN = 28,             // "@"
    kLAX_PERCENT = 29,             // "%"
    kLAX_COMMA = 30,               // ","
    kLAX_PERIOD = 31,              // "."
    kLAX_COLON = 32,               // ":"
    kLAX_RARROW = 33,              // "->"
    kLAX_AS = 34,                  // "as"
    kLAX_FN = 35,                  // "fn"
    kLAX_IMPORT = 36,              // "import"
    kLAX_REDUCES = 37,             // "reduces"
    kLAX_SCALAR = 38,              // "scalar"
    kLAX_CHART = 39,               // "chart"
    kLAX_TABLE = 40,               // "table"
    kLAX_CONST = 41,               // "const"
    kLAX_EXTENDS = 42,             // "extends"
    kLAX_START_EXPR = 43,          // START_EXPR
    kLAX_START_ASSIGN = 44,        // START_ASSIGN
    kLAX_START_TABLE = 45,         // START_TABLE
    kLAX_UMINUS = 46               // UMINUS
      };
      /// Backward compatibility alias (Bison 3.6).
      typedef token_kind_type yytokentype;
    };

    /// Token kind, as returned by yylex.
    typedef token::yytokentype token_kind_type;

    /// Backward compatibility alias (Bison 3.6).
    typedef token_kind_type token_type;

    /// Symbol kinds.
    struct symbol_kind
    {
      enum symbol_kind_type
      {
        YYNTOKENS = 47, ///< Number of tokens.
        S_YYEMPTY = -2,
        S_YYEOF = 0,                             // "end of input"
        S_YYerror = 1,                           // error
        S_YYUNDEF = 2,                           // "invalid token"
        S_IDENT = 3,                             // "identifier"
        S_STRING = 4,                            // "string"
        S_NUMBER = 5,                            // "number"
        S_PLUS = 6,                              // "+"
        S_MINUS = 7,                             // "-"
        S_STAR = 8,                              // "*"
        S_SLASH = 9,                             // "/"
        S_EXP = 10,                              // "^"
        S_LESS = 11,                             // "<"
        S_GREATER = 12,                          // ">"
        S_LESS_EQUAL = 13,                       // "<="
        S_GREATER_EQUAL = 14,                    // ">="
        S_EQUAL = 15,                            // "="
        S_NOT_EQUAL = 16,                        // "!="
        S_AND = 17,                              // "and"
        S_OR = 18,                               // "or"
        S_NOT = 19,                              // "not"
        S_VERTBAR = 20,                          // "|"
        S_LPAREN = 21,                           // "("
        S_RPAREN = 22,                           // ")"
        S_LBRACE = 23,                           // "{"
        S_RBRACE = 24,                           // "}"
        S_LBRACKET = 25,                         // "["
        S_RBRACKET = 26,                         // "]"
        S_DOLLAR = 27,                           // "$"
        S_AT_SIGN = 28,                          // "@"
        S_PERCENT = 29,                          // "%"
        S_COMMA = 30,                            // ","
        S_PERIOD = 31,                           // "."
        S_COLON = 32,                            // ":"
        S_RARROW = 33,                           // "->"
        S_AS = 34,                               // "as"
        S_FN = 35,                               // "fn"
        S_IMPORT = 36,                           // "import"
        S_REDUCES = 37,                          // "reduces"
        S_SCALAR = 38,                           // "scalar"
        S_CHART = 39,                            // "chart"
        S_TABLE = 40,                            // "table"
        S_CONST = 41,                            // "const"
        S_EXTENDS = 42,                          // "extends"
        S_START_EXPR = 43,                       // START_EXPR
        S_START_ASSIGN = 44,                     // START_ASSIGN
        S_START_TABLE = 45,                      // START_TABLE
        S_UMINUS = 46,                           // UMINUS
        S_YYACCEPT = 47,                         // $accept
        S_expr = 48,                             // expr
        S_mono_expr = 49,                        // mono_expr
        S_arg_list = 50,                         // arg_list
        S_args = 51,                             // args
        S_number = 52,                           // number
        S_nest_name = 53,                        // nest_name
        S_type_name = 54,                        // type_name
        S_type_params = 55,                      // type_params
        S_decl = 56,                             // decl
        S_assign = 57,                           // assign
        S_import = 58,                           // import
        S_arg_decl = 59,                         // arg_decl
        S_reduce = 60,                           // reduce
        S_reduce_args = 61,                      // reduce_args
        S_extension = 62,                        // extension
        S_ext_list = 63,                         // ext_list
        S_table_assigns = 64,                    // table_assigns
        S_assigns = 65,                          // assigns
        S_arg_decls = 66,                        // arg_decls
        S_arg_decl_list = 67,                    // arg_decl_list
        S_ret_spec = 68,                         // ret_spec
        S_chart = 69,                            // chart
        S_table = 70,                            // table
        S_json_object = 71,                      // json_object
        S_json_fields = 72,                      // json_fields
        S_json_field = 73,                       // json_field
        S_json_array = 74,                       // json_array
        S_json_elems = 75,                       // json_elems
        S_json_value = 76,                       // json_value
        S_first = 77                             // first
      };
    };

    /// (Internal) symbol kind.
    typedef symbol_kind::symbol_kind_type symbol_kind_type;

    /// The number of tokens.
    static const symbol_kind_type YYNTOKENS = symbol_kind::YYNTOKENS;

    /// A complete symbol.
    ///
    /// Expects its Base type to provide access to the symbol kind
    /// via kind ().
    ///
    /// Provide access to semantic value and location.
    template <typename Base>
    struct basic_symbol : Base
    {
      /// Alias to Base.
      typedef Base super_type;

      /// Default constructor.
      basic_symbol ()
        : value ()
        , location ()
      {}

#if 201103L <= YY_CPLUSPLUS
      /// Move constructor.
      basic_symbol (basic_symbol&& that)
        : Base (std::move (that))
        , value ()
        , location (std::move (that.location))
      {
        switch (this->kind ())
    {
      case symbol_kind::S_assign: // assign
      case symbol_kind::S_import: // import
      case symbol_kind::S_reduce: // reduce
        value.move< AssignPtr > (std::move (that.value));
        break;

      case symbol_kind::S_expr: // expr
      case symbol_kind::S_mono_expr: // mono_expr
        value.move< ExprPtr > (std::move (that.value));
        break;

      case symbol_kind::S_json_array: // json_array
      case symbol_kind::S_json_elems: // json_elems
        value.move< JSONArray > (std::move (that.value));
        break;

      case symbol_kind::S_json_field: // json_field
        value.move< JSONField > (std::move (that.value));
        break;

      case symbol_kind::S_json_object: // json_object
      case symbol_kind::S_json_fields: // json_fields
        value.move< JSONObject > (std::move (that.value));
        break;

      case symbol_kind::S_json_value: // json_value
        value.move< JSONValue > (std::move (that.value));
        break;

      case symbol_kind::S_decl: // decl
      case symbol_kind::S_arg_decl: // arg_decl
        value.move< TypeAssignPtr > (std::move (that.value));
        break;

      case symbol_kind::S_type_name: // type_name
      case symbol_kind::S_ret_spec: // ret_spec
        value.move< TypeData > (std::move (that.value));
        break;

      case symbol_kind::S_NUMBER: // "number"
      case symbol_kind::S_number: // number
        value.move< double > (std::move (that.value));
        break;

      case symbol_kind::S_chart: // chart
        value.move< std::shared_ptr<ChartAssign>  > (std::move (that.value));
        break;

      case symbol_kind::S_table: // table
        value.move< std::shared_ptr<TableAssign>  > (std::move (that.value));
        break;

      case symbol_kind::S_IDENT: // "identifier"
      case symbol_kind::S_STRING: // "string"
      case symbol_kind::S_nest_name: // nest_name
        value.move< std::string > (std::move (that.value));
        break;

      case symbol_kind::S_table_assigns: // table_assigns
        value.move< std::vector<AssignPtr>  > (std::move (that.value));
        break;

      case symbol_kind::S_arg_list: // arg_list
      case symbol_kind::S_args: // args
        value.move< std::vector<ExprPtr>  > (std::move (that.value));
        break;

      case symbol_kind::S_arg_decls: // arg_decls
      case symbol_kind::S_arg_decl_list: // arg_decl_list
        value.move< std::vector<TypeAssignPtr>  > (std::move (that.value));
        break;

      case symbol_kind::S_type_params: // type_params
        value.move< std::vector<TypeData>  > (std::move (that.value));
        break;

      case symbol_kind::S_assigns: // assigns
        value.move< std::vector<std::shared_ptr<TopAssign> >  > (std::move (that.value));
        break;

      case symbol_kind::S_reduce_args: // reduce_args
      case symbol_kind::S_extension: // extension
      case symbol_kind::S_ext_list: // ext_list
        value.move< std::vector<std::string>  > (std::move (that.value));
        break;

      default:
        break;
    }

      }
#endif

      /// Copy constructor.
      basic_symbol (const basic_symbol& that);

      /// Constructor for valueless symbols, and symbols from each type.
#if 201103L <= YY_CPLUSPLUS
      basic_symbol (typename Base::kind_type t, location_type&& l)
        : Base (t)
        , location (std::move (l))
      {}
#else
      basic_symbol (typename Base::kind_type t, const location_type& l)
        : Base (t)
        , location (l)
      {}
#endif
#if 201103L <= YY_CPLUSPLUS
      basic_symbol (typename Base::kind_type t, AssignPtr&& v, location_type&& l)
        : Base (t)
        , value (std::move (v))
        , location (std::move (l))
      {}
#else
      basic_symbol (typename Base::kind_type t, const AssignPtr& v, const location_type& l)
        : Base (t)
        , value (v)
        , location (l)
      {}
#endif
#if 201103L <= YY_CPLUSPLUS
      basic_symbol (typename Base::kind_type t, ExprPtr&& v, location_type&& l)
        : Base (t)
        , value (std::move (v))
        , location (std::move (l))
      {}
#else
      basic_symbol (typename Base::kind_type t, const ExprPtr& v, const location_type& l)
        : Base (t)
        , value (v)
        , location (l)
      {}
#endif
#if 201103L <= YY_CPLUSPLUS
      basic_symbol (typename Base::kind_type t, JSONArray&& v, location_type&& l)
        : Base (t)
        , value (std::move (v))
        , location (std::move (l))
      {}
#else
      basic_symbol (typename Base::kind_type t, const JSONArray& v, const location_type& l)
        : Base (t)
        , value (v)
        , location (l)
      {}
#endif
#if 201103L <= YY_CPLUSPLUS
      basic_symbol (typename Base::kind_type t, JSONField&& v, location_type&& l)
        : Base (t)
        , value (std::move (v))
        , location (std::move (l))
      {}
#else
      basic_symbol (typename Base::kind_type t, const JSONField& v, const location_type& l)
        : Base (t)
        , value (v)
        , location (l)
      {}
#endif
#if 201103L <= YY_CPLUSPLUS
      basic_symbol (typename Base::kind_type t, JSONObject&& v, location_type&& l)
        : Base (t)
        , value (std::move (v))
        , location (std::move (l))
      {}
#else
      basic_symbol (typename Base::kind_type t, const JSONObject& v, const location_type& l)
        : Base (t)
        , value (v)
        , location (l)
      {}
#endif
#if 201103L <= YY_CPLUSPLUS
      basic_symbol (typename Base::kind_type t, JSONValue&& v, location_type&& l)
        : Base (t)
        , value (std::move (v))
        , location (std::move (l))
      {}
#else
      basic_symbol (typename Base::kind_type t, const JSONValue& v, const location_type& l)
        : Base (t)
        , value (v)
        , location (l)
      {}
#endif
#if 201103L <= YY_CPLUSPLUS
      basic_symbol (typename Base::kind_type t, TypeAssignPtr&& v, location_type&& l)
        : Base (t)
        , value (std::move (v))
        , location (std::move (l))
      {}
#else
      basic_symbol (typename Base::kind_type t, const TypeAssignPtr& v, const location_type& l)
        : Base (t)
        , value (v)
        , location (l)
      {}
#endif
#if 201103L <= YY_CPLUSPLUS
      basic_symbol (typename Base::kind_type t, TypeData&& v, location_type&& l)
        : Base (t)
        , value (std::move (v))
        , location (std::move (l))
      {}
#else
      basic_symbol (typename Base::kind_type t, const TypeData& v, const location_type& l)
        : Base (t)
        , value (v)
        , location (l)
      {}
#endif
#if 201103L <= YY_CPLUSPLUS
      basic_symbol (typename Base::kind_type t, double&& v, location_type&& l)
        : Base (t)
        , value (std::move (v))
        , location (std::move (l))
      {}
#else
      basic_symbol (typename Base::kind_type t, const double& v, const location_type& l)
        : Base (t)
        , value (v)
        , location (l)
      {}
#endif
#if 201103L <= YY_CPLUSPLUS
      basic_symbol (typename Base::kind_type t, std::shared_ptr<ChartAssign> && v, location_type&& l)
        : Base (t)
        , value (std::move (v))
        , location (std::move (l))
      {}
#else
      basic_symbol (typename Base::kind_type t, const std::shared_ptr<ChartAssign> & v, const location_type& l)
        : Base (t)
        , value (v)
        , location (l)
      {}
#endif
#if 201103L <= YY_CPLUSPLUS
      basic_symbol (typename Base::kind_type t, std::shared_ptr<TableAssign> && v, location_type&& l)
        : Base (t)
        , value (std::move (v))
        , location (std::move (l))
      {}
#else
      basic_symbol (typename Base::kind_type t, const std::shared_ptr<TableAssign> & v, const location_type& l)
        : Base (t)
        , value (v)
        , location (l)
      {}
#endif
#if 201103L <= YY_CPLUSPLUS
      basic_symbol (typename Base::kind_type t, std::string&& v, location_type&& l)
        : Base (t)
        , value (std::move (v))
        , location (std::move (l))
      {}
#else
      basic_symbol (typename Base::kind_type t, const std::string& v, const location_type& l)
        : Base (t)
        , value (v)
        , location (l)
      {}
#endif
#if 201103L <= YY_CPLUSPLUS
      basic_symbol (typename Base::kind_type t, std::vector<AssignPtr> && v, location_type&& l)
        : Base (t)
        , value (std::move (v))
        , location (std::move (l))
      {}
#else
      basic_symbol (typename Base::kind_type t, const std::vector<AssignPtr> & v, const location_type& l)
        : Base (t)
        , value (v)
        , location (l)
      {}
#endif
#if 201103L <= YY_CPLUSPLUS
      basic_symbol (typename Base::kind_type t, std::vector<ExprPtr> && v, location_type&& l)
        : Base (t)
        , value (std::move (v))
        , location (std::move (l))
      {}
#else
      basic_symbol (typename Base::kind_type t, const std::vector<ExprPtr> & v, const location_type& l)
        : Base (t)
        , value (v)
        , location (l)
      {}
#endif
#if 201103L <= YY_CPLUSPLUS
      basic_symbol (typename Base::kind_type t, std::vector<TypeAssignPtr> && v, location_type&& l)
        : Base (t)
        , value (std::move (v))
        , location (std::move (l))
      {}
#else
      basic_symbol (typename Base::kind_type t, const std::vector<TypeAssignPtr> & v, const location_type& l)
        : Base (t)
        , value (v)
        , location (l)
      {}
#endif
#if 201103L <= YY_CPLUSPLUS
      basic_symbol (typename Base::kind_type t, std::vector<TypeData> && v, location_type&& l)
        : Base (t)
        , value (std::move (v))
        , location (std::move (l))
      {}
#else
      basic_symbol (typename Base::kind_type t, const std::vector<TypeData> & v, const location_type& l)
        : Base (t)
        , value (v)
        , location (l)
      {}
#endif
#if 201103L <= YY_CPLUSPLUS
      basic_symbol (typename Base::kind_type t, std::vector<std::shared_ptr<TopAssign> > && v, location_type&& l)
        : Base (t)
        , value (std::move (v))
        , location (std::move (l))
      {}
#else
      basic_symbol (typename Base::kind_type t, const std::vector<std::shared_ptr<TopAssign> > & v, const location_type& l)
        : Base (t)
        , value (v)
        , location (l)
      {}
#endif
#if 201103L <= YY_CPLUSPLUS
      basic_symbol (typename Base::kind_type t, std::vector<std::string> && v, location_type&& l)
        : Base (t)
        , value (std::move (v))
        , location (std::move (l))
      {}
#else
      basic_symbol (typename Base::kind_type t, const std::vector<std::string> & v, const location_type& l)
        : Base (t)
        , value (v)
        , location (l)
      {}
#endif

      /// Destroy the symbol.
      ~basic_symbol ()
      {
        clear ();
      }

      /// Destroy contents, and record that is empty.
      void clear ()
      {
        // User destructor.
        symbol_kind_type yykind = this->kind ();
        basic_symbol<Base>& yysym = *this;
        (void) yysym;
        switch (yykind)
        {
       default:
          break;
        }

        // Value type destructor.
switch (yykind)
    {
      case symbol_kind::S_assign: // assign
      case symbol_kind::S_import: // import
      case symbol_kind::S_reduce: // reduce
        value.template destroy< AssignPtr > ();
        break;

      case symbol_kind::S_expr: // expr
      case symbol_kind::S_mono_expr: // mono_expr
        value.template destroy< ExprPtr > ();
        break;

      case symbol_kind::S_json_array: // json_array
      case symbol_kind::S_json_elems: // json_elems
        value.template destroy< JSONArray > ();
        break;

      case symbol_kind::S_json_field: // json_field
        value.template destroy< JSONField > ();
        break;

      case symbol_kind::S_json_object: // json_object
      case symbol_kind::S_json_fields: // json_fields
        value.template destroy< JSONObject > ();
        break;

      case symbol_kind::S_json_value: // json_value
        value.template destroy< JSONValue > ();
        break;

      case symbol_kind::S_decl: // decl
      case symbol_kind::S_arg_decl: // arg_decl
        value.template destroy< TypeAssignPtr > ();
        break;

      case symbol_kind::S_type_name: // type_name
      case symbol_kind::S_ret_spec: // ret_spec
        value.template destroy< TypeData > ();
        break;

      case symbol_kind::S_NUMBER: // "number"
      case symbol_kind::S_number: // number
        value.template destroy< double > ();
        break;

      case symbol_kind::S_chart: // chart
        value.template destroy< std::shared_ptr<ChartAssign>  > ();
        break;

      case symbol_kind::S_table: // table
        value.template destroy< std::shared_ptr<TableAssign>  > ();
        break;

      case symbol_kind::S_IDENT: // "identifier"
      case symbol_kind::S_STRING: // "string"
      case symbol_kind::S_nest_name: // nest_name
        value.template destroy< std::string > ();
        break;

      case symbol_kind::S_table_assigns: // table_assigns
        value.template destroy< std::vector<AssignPtr>  > ();
        break;

      case symbol_kind::S_arg_list: // arg_list
      case symbol_kind::S_args: // args
        value.template destroy< std::vector<ExprPtr>  > ();
        break;

      case symbol_kind::S_arg_decls: // arg_decls
      case symbol_kind::S_arg_decl_list: // arg_decl_list
        value.template destroy< std::vector<TypeAssignPtr>  > ();
        break;

      case symbol_kind::S_type_params: // type_params
        value.template destroy< std::vector<TypeData>  > ();
        break;

      case symbol_kind::S_assigns: // assigns
        value.template destroy< std::vector<std::shared_ptr<TopAssign> >  > ();
        break;

      case symbol_kind::S_reduce_args: // reduce_args
      case symbol_kind::S_extension: // extension
      case symbol_kind::S_ext_list: // ext_list
        value.template destroy< std::vector<std::string>  > ();
        break;

      default:
        break;
    }

        Base::clear ();
      }

      /// The user-facing name of this symbol.
      const char *name () const YY_NOEXCEPT
      {
        return parser::symbol_name (this->kind ());
      }

      /// Backward compatibility (Bison 3.6).
      symbol_kind_type type_get () const YY_NOEXCEPT;

      /// Whether empty.
      bool empty () const YY_NOEXCEPT;

      /// Destructive move, \a s is emptied into this.
      void move (basic_symbol& s);

      /// The semantic value.
      semantic_type value;

      /// The location.
      location_type location;

    private:
#if YY_CPLUSPLUS < 201103L
      /// Assignment operator.
      basic_symbol& operator= (const basic_symbol& that);
#endif
    };

    /// Type access provider for token (enum) based symbols.
    struct by_kind
    {
      /// Default constructor.
      by_kind ();

#if 201103L <= YY_CPLUSPLUS
      /// Move constructor.
      by_kind (by_kind&& that);
#endif

      /// Copy constructor.
      by_kind (const by_kind& that);

      /// The symbol kind as needed by the constructor.
      typedef token_kind_type kind_type;

      /// Constructor from (external) token numbers.
      by_kind (kind_type t);

      /// Record that this symbol is empty.
      void clear ();

      /// Steal the symbol kind from \a that.
      void move (by_kind& that);

      /// The (internal) type number (corresponding to \a type).
      /// \a empty when empty.
      symbol_kind_type kind () const YY_NOEXCEPT;

      /// Backward compatibility (Bison 3.6).
      symbol_kind_type type_get () const YY_NOEXCEPT;

      /// The symbol kind.
      /// \a S_YYEMPTY when empty.
      symbol_kind_type kind_;
    };

    /// Backward compatibility for a private implementation detail (Bison 3.6).
    typedef by_kind by_type;

    /// "External" symbols: returned by the scanner.
    struct symbol_type : basic_symbol<by_kind>
    {
      /// Superclass.
      typedef basic_symbol<by_kind> super_type;

      /// Empty symbol.
      symbol_type () {}

      /// Constructor for valueless symbols, and symbols from each type.
#if 201103L <= YY_CPLUSPLUS
      symbol_type (int tok, location_type l)
        : super_type(token_type (tok), std::move (l))
      {
        YY_ASSERT (tok == token::kLAX_EOF || tok == token::kLAX_YYerror || tok == token::kLAX_YYUNDEF || tok == token::kLAX_PLUS || tok == token::kLAX_MINUS || tok == token::kLAX_STAR || tok == token::kLAX_SLASH || tok == token::kLAX_EXP || tok == token::kLAX_LESS || tok == token::kLAX_GREATER || tok == token::kLAX_LESS_EQUAL || tok == token::kLAX_GREATER_EQUAL || tok == token::kLAX_EQUAL || tok == token::kLAX_NOT_EQUAL || tok == token::kLAX_AND || tok == token::kLAX_OR || tok == token::kLAX_NOT || tok == token::kLAX_VERTBAR || tok == token::kLAX_LPAREN || tok == token::kLAX_RPAREN || tok == token::kLAX_LBRACE || tok == token::kLAX_RBRACE || tok == token::kLAX_LBRACKET || tok == token::kLAX_RBRACKET || tok == token::kLAX_DOLLAR || tok == token::kLAX_AT_SIGN || tok == token::kLAX_PERCENT || tok == token::kLAX_COMMA || tok == token::kLAX_PERIOD || tok == token::kLAX_COLON || tok == token::kLAX_RARROW || tok == token::kLAX_AS || tok == token::kLAX_FN || tok == token::kLAX_IMPORT || tok == token::kLAX_REDUCES || tok == token::kLAX_SCALAR || tok == token::kLAX_CHART || tok == token::kLAX_TABLE || tok == token::kLAX_CONST || tok == token::kLAX_EXTENDS || tok == token::kLAX_START_EXPR || tok == token::kLAX_START_ASSIGN || tok == token::kLAX_START_TABLE || tok == token::kLAX_UMINUS);
      }
#else
      symbol_type (int tok, const location_type& l)
        : super_type(token_type (tok), l)
      {
        YY_ASSERT (tok == token::kLAX_EOF || tok == token::kLAX_YYerror || tok == token::kLAX_YYUNDEF || tok == token::kLAX_PLUS || tok == token::kLAX_MINUS || tok == token::kLAX_STAR || tok == token::kLAX_SLASH || tok == token::kLAX_EXP || tok == token::kLAX_LESS || tok == token::kLAX_GREATER || tok == token::kLAX_LESS_EQUAL || tok == token::kLAX_GREATER_EQUAL || tok == token::kLAX_EQUAL || tok == token::kLAX_NOT_EQUAL || tok == token::kLAX_AND || tok == token::kLAX_OR || tok == token::kLAX_NOT || tok == token::kLAX_VERTBAR || tok == token::kLAX_LPAREN || tok == token::kLAX_RPAREN || tok == token::kLAX_LBRACE || tok == token::kLAX_RBRACE || tok == token::kLAX_LBRACKET || tok == token::kLAX_RBRACKET || tok == token::kLAX_DOLLAR || tok == token::kLAX_AT_SIGN || tok == token::kLAX_PERCENT || tok == token::kLAX_COMMA || tok == token::kLAX_PERIOD || tok == token::kLAX_COLON || tok == token::kLAX_RARROW || tok == token::kLAX_AS || tok == token::kLAX_FN || tok == token::kLAX_IMPORT || tok == token::kLAX_REDUCES || tok == token::kLAX_SCALAR || tok == token::kLAX_CHART || tok == token::kLAX_TABLE || tok == token::kLAX_CONST || tok == token::kLAX_EXTENDS || tok == token::kLAX_START_EXPR || tok == token::kLAX_START_ASSIGN || tok == token::kLAX_START_TABLE || tok == token::kLAX_UMINUS);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      symbol_type (int tok, double v, location_type l)
        : super_type(token_type (tok), std::move (v), std::move (l))
      {
        YY_ASSERT (tok == token::kLAX_NUMBER);
      }
#else
      symbol_type (int tok, const double& v, const location_type& l)
        : super_type(token_type (tok), v, l)
      {
        YY_ASSERT (tok == token::kLAX_NUMBER);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      symbol_type (int tok, std::string v, location_type l)
        : super_type(token_type (tok), std::move (v), std::move (l))
      {
        YY_ASSERT (tok == token::kLAX_IDENT || tok == token::kLAX_STRING);
      }
#else
      symbol_type (int tok, const std::string& v, const location_type& l)
        : super_type(token_type (tok), v, l)
      {
        YY_ASSERT (tok == token::kLAX_IDENT || tok == token::kLAX_STRING);
      }
#endif
    };

    /// Build a parser object.
    parser (FormulaDriver& drv_yyarg);
    virtual ~parser ();

#if 201103L <= YY_CPLUSPLUS
    /// Non copyable.
    parser (const parser&) = delete;
    /// Non copyable.
    parser& operator= (const parser&) = delete;
#endif

    /// Parse.  An alias for parse ().
    /// \returns  0 iff parsing succeeded.
    int operator() ();

    /// Parse.
    /// \returns  0 iff parsing succeeded.
    virtual int parse ();

#if YYDEBUG
    /// The current debugging stream.
    std::ostream& debug_stream () const YY_ATTRIBUTE_PURE;
    /// Set the current debugging stream.
    void set_debug_stream (std::ostream &);

    /// Type for debugging levels.
    typedef int debug_level_type;
    /// The current debugging level.
    debug_level_type debug_level () const YY_ATTRIBUTE_PURE;
    /// Set the current debugging level.
    void set_debug_level (debug_level_type l);
#endif

    /// Report a syntax error.
    /// \param loc    where the syntax error is found.
    /// \param msg    a description of the syntax error.
    virtual void error (const location_type& loc, const std::string& msg);

    /// Report a syntax error.
    void error (const syntax_error& err);

    /// The user-facing name of the symbol whose (internal) number is
    /// YYSYMBOL.  No bounds checking.
    static const char *symbol_name (symbol_kind_type yysymbol);

    // Implementation of make_symbol for each symbol type.
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_EOF (location_type l)
      {
        return symbol_type (token::kLAX_EOF, std::move (l));
      }
#else
      static
      symbol_type
      make_EOF (const location_type& l)
      {
        return symbol_type (token::kLAX_EOF, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_YYerror (location_type l)
      {
        return symbol_type (token::kLAX_YYerror, std::move (l));
      }
#else
      static
      symbol_type
      make_YYerror (const location_type& l)
      {
        return symbol_type (token::kLAX_YYerror, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_YYUNDEF (location_type l)
      {
        return symbol_type (token::kLAX_YYUNDEF, std::move (l));
      }
#else
      static
      symbol_type
      make_YYUNDEF (const location_type& l)
      {
        return symbol_type (token::kLAX_YYUNDEF, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_IDENT (std::string v, location_type l)
      {
        return symbol_type (token::kLAX_IDENT, std::move (v), std::move (l));
      }
#else
      static
      symbol_type
      make_IDENT (const std::string& v, const location_type& l)
      {
        return symbol_type (token::kLAX_IDENT, v, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_STRING (std::string v, location_type l)
      {
        return symbol_type (token::kLAX_STRING, std::move (v), std::move (l));
      }
#else
      static
      symbol_type
      make_STRING (const std::string& v, const location_type& l)
      {
        return symbol_type (token::kLAX_STRING, v, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_NUMBER (double v, location_type l)
      {
        return symbol_type (token::kLAX_NUMBER, std::move (v), std::move (l));
      }
#else
      static
      symbol_type
      make_NUMBER (const double& v, const location_type& l)
      {
        return symbol_type (token::kLAX_NUMBER, v, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_PLUS (location_type l)
      {
        return symbol_type (token::kLAX_PLUS, std::move (l));
      }
#else
      static
      symbol_type
      make_PLUS (const location_type& l)
      {
        return symbol_type (token::kLAX_PLUS, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_MINUS (location_type l)
      {
        return symbol_type (token::kLAX_MINUS, std::move (l));
      }
#else
      static
      symbol_type
      make_MINUS (const location_type& l)
      {
        return symbol_type (token::kLAX_MINUS, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_STAR (location_type l)
      {
        return symbol_type (token::kLAX_STAR, std::move (l));
      }
#else
      static
      symbol_type
      make_STAR (const location_type& l)
      {
        return symbol_type (token::kLAX_STAR, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_SLASH (location_type l)
      {
        return symbol_type (token::kLAX_SLASH, std::move (l));
      }
#else
      static
      symbol_type
      make_SLASH (const location_type& l)
      {
        return symbol_type (token::kLAX_SLASH, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_EXP (location_type l)
      {
        return symbol_type (token::kLAX_EXP, std::move (l));
      }
#else
      static
      symbol_type
      make_EXP (const location_type& l)
      {
        return symbol_type (token::kLAX_EXP, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_LESS (location_type l)
      {
        return symbol_type (token::kLAX_LESS, std::move (l));
      }
#else
      static
      symbol_type
      make_LESS (const location_type& l)
      {
        return symbol_type (token::kLAX_LESS, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_GREATER (location_type l)
      {
        return symbol_type (token::kLAX_GREATER, std::move (l));
      }
#else
      static
      symbol_type
      make_GREATER (const location_type& l)
      {
        return symbol_type (token::kLAX_GREATER, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_LESS_EQUAL (location_type l)
      {
        return symbol_type (token::kLAX_LESS_EQUAL, std::move (l));
      }
#else
      static
      symbol_type
      make_LESS_EQUAL (const location_type& l)
      {
        return symbol_type (token::kLAX_LESS_EQUAL, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_GREATER_EQUAL (location_type l)
      {
        return symbol_type (token::kLAX_GREATER_EQUAL, std::move (l));
      }
#else
      static
      symbol_type
      make_GREATER_EQUAL (const location_type& l)
      {
        return symbol_type (token::kLAX_GREATER_EQUAL, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_EQUAL (location_type l)
      {
        return symbol_type (token::kLAX_EQUAL, std::move (l));
      }
#else
      static
      symbol_type
      make_EQUAL (const location_type& l)
      {
        return symbol_type (token::kLAX_EQUAL, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_NOT_EQUAL (location_type l)
      {
        return symbol_type (token::kLAX_NOT_EQUAL, std::move (l));
      }
#else
      static
      symbol_type
      make_NOT_EQUAL (const location_type& l)
      {
        return symbol_type (token::kLAX_NOT_EQUAL, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_AND (location_type l)
      {
        return symbol_type (token::kLAX_AND, std::move (l));
      }
#else
      static
      symbol_type
      make_AND (const location_type& l)
      {
        return symbol_type (token::kLAX_AND, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_OR (location_type l)
      {
        return symbol_type (token::kLAX_OR, std::move (l));
      }
#else
      static
      symbol_type
      make_OR (const location_type& l)
      {
        return symbol_type (token::kLAX_OR, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_NOT (location_type l)
      {
        return symbol_type (token::kLAX_NOT, std::move (l));
      }
#else
      static
      symbol_type
      make_NOT (const location_type& l)
      {
        return symbol_type (token::kLAX_NOT, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_VERTBAR (location_type l)
      {
        return symbol_type (token::kLAX_VERTBAR, std::move (l));
      }
#else
      static
      symbol_type
      make_VERTBAR (const location_type& l)
      {
        return symbol_type (token::kLAX_VERTBAR, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_LPAREN (location_type l)
      {
        return symbol_type (token::kLAX_LPAREN, std::move (l));
      }
#else
      static
      symbol_type
      make_LPAREN (const location_type& l)
      {
        return symbol_type (token::kLAX_LPAREN, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_RPAREN (location_type l)
      {
        return symbol_type (token::kLAX_RPAREN, std::move (l));
      }
#else
      static
      symbol_type
      make_RPAREN (const location_type& l)
      {
        return symbol_type (token::kLAX_RPAREN, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_LBRACE (location_type l)
      {
        return symbol_type (token::kLAX_LBRACE, std::move (l));
      }
#else
      static
      symbol_type
      make_LBRACE (const location_type& l)
      {
        return symbol_type (token::kLAX_LBRACE, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_RBRACE (location_type l)
      {
        return symbol_type (token::kLAX_RBRACE, std::move (l));
      }
#else
      static
      symbol_type
      make_RBRACE (const location_type& l)
      {
        return symbol_type (token::kLAX_RBRACE, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_LBRACKET (location_type l)
      {
        return symbol_type (token::kLAX_LBRACKET, std::move (l));
      }
#else
      static
      symbol_type
      make_LBRACKET (const location_type& l)
      {
        return symbol_type (token::kLAX_LBRACKET, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_RBRACKET (location_type l)
      {
        return symbol_type (token::kLAX_RBRACKET, std::move (l));
      }
#else
      static
      symbol_type
      make_RBRACKET (const location_type& l)
      {
        return symbol_type (token::kLAX_RBRACKET, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_DOLLAR (location_type l)
      {
        return symbol_type (token::kLAX_DOLLAR, std::move (l));
      }
#else
      static
      symbol_type
      make_DOLLAR (const location_type& l)
      {
        return symbol_type (token::kLAX_DOLLAR, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_AT_SIGN (location_type l)
      {
        return symbol_type (token::kLAX_AT_SIGN, std::move (l));
      }
#else
      static
      symbol_type
      make_AT_SIGN (const location_type& l)
      {
        return symbol_type (token::kLAX_AT_SIGN, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_PERCENT (location_type l)
      {
        return symbol_type (token::kLAX_PERCENT, std::move (l));
      }
#else
      static
      symbol_type
      make_PERCENT (const location_type& l)
      {
        return symbol_type (token::kLAX_PERCENT, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_COMMA (location_type l)
      {
        return symbol_type (token::kLAX_COMMA, std::move (l));
      }
#else
      static
      symbol_type
      make_COMMA (const location_type& l)
      {
        return symbol_type (token::kLAX_COMMA, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_PERIOD (location_type l)
      {
        return symbol_type (token::kLAX_PERIOD, std::move (l));
      }
#else
      static
      symbol_type
      make_PERIOD (const location_type& l)
      {
        return symbol_type (token::kLAX_PERIOD, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_COLON (location_type l)
      {
        return symbol_type (token::kLAX_COLON, std::move (l));
      }
#else
      static
      symbol_type
      make_COLON (const location_type& l)
      {
        return symbol_type (token::kLAX_COLON, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_RARROW (location_type l)
      {
        return symbol_type (token::kLAX_RARROW, std::move (l));
      }
#else
      static
      symbol_type
      make_RARROW (const location_type& l)
      {
        return symbol_type (token::kLAX_RARROW, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_AS (location_type l)
      {
        return symbol_type (token::kLAX_AS, std::move (l));
      }
#else
      static
      symbol_type
      make_AS (const location_type& l)
      {
        return symbol_type (token::kLAX_AS, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_FN (location_type l)
      {
        return symbol_type (token::kLAX_FN, std::move (l));
      }
#else
      static
      symbol_type
      make_FN (const location_type& l)
      {
        return symbol_type (token::kLAX_FN, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_IMPORT (location_type l)
      {
        return symbol_type (token::kLAX_IMPORT, std::move (l));
      }
#else
      static
      symbol_type
      make_IMPORT (const location_type& l)
      {
        return symbol_type (token::kLAX_IMPORT, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_REDUCES (location_type l)
      {
        return symbol_type (token::kLAX_REDUCES, std::move (l));
      }
#else
      static
      symbol_type
      make_REDUCES (const location_type& l)
      {
        return symbol_type (token::kLAX_REDUCES, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_SCALAR (location_type l)
      {
        return symbol_type (token::kLAX_SCALAR, std::move (l));
      }
#else
      static
      symbol_type
      make_SCALAR (const location_type& l)
      {
        return symbol_type (token::kLAX_SCALAR, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_CHART (location_type l)
      {
        return symbol_type (token::kLAX_CHART, std::move (l));
      }
#else
      static
      symbol_type
      make_CHART (const location_type& l)
      {
        return symbol_type (token::kLAX_CHART, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_TABLE (location_type l)
      {
        return symbol_type (token::kLAX_TABLE, std::move (l));
      }
#else
      static
      symbol_type
      make_TABLE (const location_type& l)
      {
        return symbol_type (token::kLAX_TABLE, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_CONST (location_type l)
      {
        return symbol_type (token::kLAX_CONST, std::move (l));
      }
#else
      static
      symbol_type
      make_CONST (const location_type& l)
      {
        return symbol_type (token::kLAX_CONST, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_EXTENDS (location_type l)
      {
        return symbol_type (token::kLAX_EXTENDS, std::move (l));
      }
#else
      static
      symbol_type
      make_EXTENDS (const location_type& l)
      {
        return symbol_type (token::kLAX_EXTENDS, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_START_EXPR (location_type l)
      {
        return symbol_type (token::kLAX_START_EXPR, std::move (l));
      }
#else
      static
      symbol_type
      make_START_EXPR (const location_type& l)
      {
        return symbol_type (token::kLAX_START_EXPR, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_START_ASSIGN (location_type l)
      {
        return symbol_type (token::kLAX_START_ASSIGN, std::move (l));
      }
#else
      static
      symbol_type
      make_START_ASSIGN (const location_type& l)
      {
        return symbol_type (token::kLAX_START_ASSIGN, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_START_TABLE (location_type l)
      {
        return symbol_type (token::kLAX_START_TABLE, std::move (l));
      }
#else
      static
      symbol_type
      make_START_TABLE (const location_type& l)
      {
        return symbol_type (token::kLAX_START_TABLE, l);
      }
#endif
#if 201103L <= YY_CPLUSPLUS
      static
      symbol_type
      make_UMINUS (location_type l)
      {
        return symbol_type (token::kLAX_UMINUS, std::move (l));
      }
#else
      static
      symbol_type
      make_UMINUS (const location_type& l)
      {
        return symbol_type (token::kLAX_UMINUS, l);
      }
#endif


    class context
    {
    public:
      context (const parser& yyparser, const symbol_type& yyla);
      const symbol_type& lookahead () const { return yyla_; }
      symbol_kind_type token () const { return yyla_.kind (); }
      const location_type& location () const { return yyla_.location; }

      /// Put in YYARG at most YYARGN of the expected tokens, and return the
      /// number of tokens stored in YYARG.  If YYARG is null, return the
      /// number of expected tokens (guaranteed to be less than YYNTOKENS).
      int expected_tokens (symbol_kind_type yyarg[], int yyargn) const;

    private:
      const parser& yyparser_;
      const symbol_type& yyla_;
    };

  private:
#if YY_CPLUSPLUS < 201103L
    /// Non copyable.
    parser (const parser&);
    /// Non copyable.
    parser& operator= (const parser&);
#endif

    /// Check the lookahead yytoken.
    /// \returns  true iff the token will be eventually shifted.
    bool yy_lac_check_ (symbol_kind_type yytoken) const;
    /// Establish the initial context if no initial context currently exists.
    /// \returns  true iff the token will be eventually shifted.
    bool yy_lac_establish_ (symbol_kind_type yytoken);
    /// Discard any previous initial lookahead context because of event.
    /// \param event  the event which caused the lookahead to be discarded.
    ///               Only used for debbuging output.
    void yy_lac_discard_ (const char* event);

    /// Stored state numbers (used for stacks).
    typedef unsigned char state_type;

    /// The arguments of the error message.
    int yy_syntax_error_arguments_ (const context& yyctx,
                                    symbol_kind_type yyarg[], int yyargn) const;

    /// Generate an error message.
    /// \param yyctx     the context in which the error occurred.
    virtual std::string yysyntax_error_ (const context& yyctx) const;
    /// Compute post-reduction state.
    /// \param yystate   the current state
    /// \param yysym     the nonterminal to push on the stack
    static state_type yy_lr_goto_state_ (state_type yystate, int yysym);

    /// Whether the given \c yypact_ value indicates a defaulted state.
    /// \param yyvalue   the value to check
    static bool yy_pact_value_is_default_ (int yyvalue);

    /// Whether the given \c yytable_ value indicates a syntax error.
    /// \param yyvalue   the value to check
    static bool yy_table_value_is_error_ (int yyvalue);

    static const signed char yypact_ninf_;
    static const signed char yytable_ninf_;

    /// Convert a scanner token kind \a t to a symbol kind.
    /// In theory \a t should be a token_kind_type, but character literals
    /// are valid, yet not members of the token_type enum.
    static symbol_kind_type yytranslate_ (int t);



    // Tables.
    // YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
    // STATE-NUM.
    static const short yypact_[];

    // YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
    // Performed when YYTABLE does not specify something else to do.  Zero
    // means the default is an error.
    static const signed char yydefact_[];

    // YYPGOTO[NTERM-NUM].
    static const short yypgoto_[];

    // YYDEFGOTO[NTERM-NUM].
    static const short yydefgoto_[];

    // YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
    // positive, shift that token.  If negative, reduce the rule whose
    // number is the opposite.  If YYTABLE_NINF, syntax error.
    static const short yytable_[];

    static const short yycheck_[];

    // YYSTOS[STATE-NUM] -- The (internal number of the) accessing
    // symbol of state STATE-NUM.
    static const signed char yystos_[];

    // YYR1[YYN] -- Symbol number of symbol that rule YYN derives.
    static const signed char yyr1_[];

    // YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.
    static const signed char yyr2_[];


#if YYDEBUG
    // YYRLINE[YYN] -- Source line where rule number YYN was defined.
    static const short yyrline_[];
    /// Report on the debug stream that the rule \a r is going to be reduced.
    virtual void yy_reduce_print_ (int r) const;
    /// Print the state stack on the debug stream.
    virtual void yy_stack_print_ () const;

    /// Debugging level.
    int yydebug_;
    /// Debug stream.
    std::ostream* yycdebug_;

    /// \brief Display a symbol kind, value and location.
    /// \param yyo    The output stream.
    /// \param yysym  The symbol.
    template <typename Base>
    void yy_print_ (std::ostream& yyo, const basic_symbol<Base>& yysym) const;
#endif

    /// \brief Reclaim the memory associated to a symbol.
    /// \param yymsg     Why this token is reclaimed.
    ///                  If null, print nothing.
    /// \param yysym     The symbol.
    template <typename Base>
    void yy_destroy_ (const char* yymsg, basic_symbol<Base>& yysym) const;

  private:
    /// Type access provider for state based symbols.
    struct by_state
    {
      /// Default constructor.
      by_state () YY_NOEXCEPT;

      /// The symbol kind as needed by the constructor.
      typedef state_type kind_type;

      /// Constructor.
      by_state (kind_type s) YY_NOEXCEPT;

      /// Copy constructor.
      by_state (const by_state& that) YY_NOEXCEPT;

      /// Record that this symbol is empty.
      void clear () YY_NOEXCEPT;

      /// Steal the symbol kind from \a that.
      void move (by_state& that);

      /// The symbol kind (corresponding to \a state).
      /// \a symbol_kind::S_YYEMPTY when empty.
      symbol_kind_type kind () const YY_NOEXCEPT;

      /// The state number used to denote an empty symbol.
      /// We use the initial state, as it does not have a value.
      enum { empty_state = 0 };

      /// The state.
      /// \a empty when empty.
      state_type state;
    };

    /// "Internal" symbol: element of the stack.
    struct stack_symbol_type : basic_symbol<by_state>
    {
      /// Superclass.
      typedef basic_symbol<by_state> super_type;
      /// Construct an empty symbol.
      stack_symbol_type ();
      /// Move or copy construction.
      stack_symbol_type (YY_RVREF (stack_symbol_type) that);
      /// Steal the contents from \a sym to build this.
      stack_symbol_type (state_type s, YY_MOVE_REF (symbol_type) sym);
#if YY_CPLUSPLUS < 201103L
      /// Assignment, needed by push_back by some old implementations.
      /// Moves the contents of that.
      stack_symbol_type& operator= (stack_symbol_type& that);

      /// Assignment, needed by push_back by other implementations.
      /// Needed by some other old implementations.
      stack_symbol_type& operator= (const stack_symbol_type& that);
#endif
    };

    /// A stack with random access from its top.
    template <typename T, typename S = std::vector<T> >
    class stack
    {
    public:
      // Hide our reversed order.
      typedef typename S::iterator iterator;
      typedef typename S::const_iterator const_iterator;
      typedef typename S::size_type size_type;
      typedef typename std::ptrdiff_t index_type;

      stack (size_type n = 200)
        : seq_ (n)
      {}

#if 201103L <= YY_CPLUSPLUS
      /// Non copyable.
      stack (const stack&) = delete;
      /// Non copyable.
      stack& operator= (const stack&) = delete;
#endif

      /// Random access.
      ///
      /// Index 0 returns the topmost element.
      const T&
      operator[] (index_type i) const
      {
        return seq_[size_type (size () - 1 - i)];
      }

      /// Random access.
      ///
      /// Index 0 returns the topmost element.
      T&
      operator[] (index_type i)
      {
        return seq_[size_type (size () - 1 - i)];
      }

      /// Steal the contents of \a t.
      ///
      /// Close to move-semantics.
      void
      push (YY_MOVE_REF (T) t)
      {
        seq_.push_back (T ());
        operator[] (0).move (t);
      }

      /// Pop elements from the stack.
      void
      pop (std::ptrdiff_t n = 1) YY_NOEXCEPT
      {
        for (; 0 < n; --n)
          seq_.pop_back ();
      }

      /// Pop all elements from the stack.
      void
      clear () YY_NOEXCEPT
      {
        seq_.clear ();
      }

      /// Number of elements on the stack.
      index_type
      size () const YY_NOEXCEPT
      {
        return index_type (seq_.size ());
      }

      /// Iterator on top of the stack (going downwards).
      const_iterator
      begin () const YY_NOEXCEPT
      {
        return seq_.begin ();
      }

      /// Bottom of the stack.
      const_iterator
      end () const YY_NOEXCEPT
      {
        return seq_.end ();
      }

      /// Present a slice of the top of a stack.
      class slice
      {
      public:
        slice (const stack& stack, index_type range)
          : stack_ (stack)
          , range_ (range)
        {}

        const T&
        operator[] (index_type i) const
        {
          return stack_[range_ - i];
        }

      private:
        const stack& stack_;
        index_type range_;
      };

    private:
#if YY_CPLUSPLUS < 201103L
      /// Non copyable.
      stack (const stack&);
      /// Non copyable.
      stack& operator= (const stack&);
#endif
      /// The wrapped container.
      S seq_;
    };


    /// Stack type.
    typedef stack<stack_symbol_type> stack_type;

    /// The stack.
    stack_type yystack_;
    /// The stack for LAC.
    /// Logically, the yy_lac_stack's lifetime is confined to the function
    /// yy_lac_check_. We just store it as a member of this class to hold
    /// on to the memory and to avoid frequent reallocations.
    /// Since yy_lac_check_ is const, this member must be mutable.
    mutable std::vector<state_type> yylac_stack_;
    /// Whether an initial LAC context was established.
    bool yy_lac_established_;


    /// Push a new state on the stack.
    /// \param m    a debug message to display
    ///             if null, no trace is output.
    /// \param sym  the symbol
    /// \warning the contents of \a s.value is stolen.
    void yypush_ (const char* m, YY_MOVE_REF (stack_symbol_type) sym);

    /// Push a new look ahead token on the state on the stack.
    /// \param m    a debug message to display
    ///             if null, no trace is output.
    /// \param s    the state
    /// \param sym  the symbol (for its value and location).
    /// \warning the contents of \a sym.value is stolen.
    void yypush_ (const char* m, state_type s, YY_MOVE_REF (symbol_type) sym);

    /// Pop \a n symbols from the stack.
    void yypop_ (int n = 1);

    /// Constants.
    enum
    {
      yylast_ = 358,     ///< Last index in yytable_.
      yynnts_ = 31,  ///< Number of nonterminal symbols.
      yyfinal_ = 21 ///< Termination state number.
    };


    // User arguments.
    FormulaDriver& drv;

  };

  inline
  parser::symbol_kind_type
  parser::yytranslate_ (int t)
  {
    return static_cast<symbol_kind_type> (t);
  }

  // basic_symbol.
  template <typename Base>
  parser::basic_symbol<Base>::basic_symbol (const basic_symbol& that)
    : Base (that)
    , value ()
    , location (that.location)
  {
    switch (this->kind ())
    {
      case symbol_kind::S_assign: // assign
      case symbol_kind::S_import: // import
      case symbol_kind::S_reduce: // reduce
        value.copy< AssignPtr > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_expr: // expr
      case symbol_kind::S_mono_expr: // mono_expr
        value.copy< ExprPtr > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_json_array: // json_array
      case symbol_kind::S_json_elems: // json_elems
        value.copy< JSONArray > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_json_field: // json_field
        value.copy< JSONField > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_json_object: // json_object
      case symbol_kind::S_json_fields: // json_fields
        value.copy< JSONObject > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_json_value: // json_value
        value.copy< JSONValue > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_decl: // decl
      case symbol_kind::S_arg_decl: // arg_decl
        value.copy< TypeAssignPtr > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_type_name: // type_name
      case symbol_kind::S_ret_spec: // ret_spec
        value.copy< TypeData > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_NUMBER: // "number"
      case symbol_kind::S_number: // number
        value.copy< double > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_chart: // chart
        value.copy< std::shared_ptr<ChartAssign>  > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_table: // table
        value.copy< std::shared_ptr<TableAssign>  > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_IDENT: // "identifier"
      case symbol_kind::S_STRING: // "string"
      case symbol_kind::S_nest_name: // nest_name
        value.copy< std::string > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_table_assigns: // table_assigns
        value.copy< std::vector<AssignPtr>  > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_arg_list: // arg_list
      case symbol_kind::S_args: // args
        value.copy< std::vector<ExprPtr>  > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_arg_decls: // arg_decls
      case symbol_kind::S_arg_decl_list: // arg_decl_list
        value.copy< std::vector<TypeAssignPtr>  > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_type_params: // type_params
        value.copy< std::vector<TypeData>  > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_assigns: // assigns
        value.copy< std::vector<std::shared_ptr<TopAssign> >  > (YY_MOVE (that.value));
        break;

      case symbol_kind::S_reduce_args: // reduce_args
      case symbol_kind::S_extension: // extension
      case symbol_kind::S_ext_list: // ext_list
        value.copy< std::vector<std::string>  > (YY_MOVE (that.value));
        break;

      default:
        break;
    }

  }



  template <typename Base>
  parser::symbol_kind_type
  parser::basic_symbol<Base>::type_get () const YY_NOEXCEPT
  {
    return this->kind ();
  }

  template <typename Base>
  bool
  parser::basic_symbol<Base>::empty () const YY_NOEXCEPT
  {
    return this->kind () == symbol_kind::S_YYEMPTY;
  }

  template <typename Base>
  void
  parser::basic_symbol<Base>::move (basic_symbol& s)
  {
    super_type::move (s);
    switch (this->kind ())
    {
      case symbol_kind::S_assign: // assign
      case symbol_kind::S_import: // import
      case symbol_kind::S_reduce: // reduce
        value.move< AssignPtr > (YY_MOVE (s.value));
        break;

      case symbol_kind::S_expr: // expr
      case symbol_kind::S_mono_expr: // mono_expr
        value.move< ExprPtr > (YY_MOVE (s.value));
        break;

      case symbol_kind::S_json_array: // json_array
      case symbol_kind::S_json_elems: // json_elems
        value.move< JSONArray > (YY_MOVE (s.value));
        break;

      case symbol_kind::S_json_field: // json_field
        value.move< JSONField > (YY_MOVE (s.value));
        break;

      case symbol_kind::S_json_object: // json_object
      case symbol_kind::S_json_fields: // json_fields
        value.move< JSONObject > (YY_MOVE (s.value));
        break;

      case symbol_kind::S_json_value: // json_value
        value.move< JSONValue > (YY_MOVE (s.value));
        break;

      case symbol_kind::S_decl: // decl
      case symbol_kind::S_arg_decl: // arg_decl
        value.move< TypeAssignPtr > (YY_MOVE (s.value));
        break;

      case symbol_kind::S_type_name: // type_name
      case symbol_kind::S_ret_spec: // ret_spec
        value.move< TypeData > (YY_MOVE (s.value));
        break;

      case symbol_kind::S_NUMBER: // "number"
      case symbol_kind::S_number: // number
        value.move< double > (YY_MOVE (s.value));
        break;

      case symbol_kind::S_chart: // chart
        value.move< std::shared_ptr<ChartAssign>  > (YY_MOVE (s.value));
        break;

      case symbol_kind::S_table: // table
        value.move< std::shared_ptr<TableAssign>  > (YY_MOVE (s.value));
        break;

      case symbol_kind::S_IDENT: // "identifier"
      case symbol_kind::S_STRING: // "string"
      case symbol_kind::S_nest_name: // nest_name
        value.move< std::string > (YY_MOVE (s.value));
        break;

      case symbol_kind::S_table_assigns: // table_assigns
        value.move< std::vector<AssignPtr>  > (YY_MOVE (s.value));
        break;

      case symbol_kind::S_arg_list: // arg_list
      case symbol_kind::S_args: // args
        value.move< std::vector<ExprPtr>  > (YY_MOVE (s.value));
        break;

      case symbol_kind::S_arg_decls: // arg_decls
      case symbol_kind::S_arg_decl_list: // arg_decl_list
        value.move< std::vector<TypeAssignPtr>  > (YY_MOVE (s.value));
        break;

      case symbol_kind::S_type_params: // type_params
        value.move< std::vector<TypeData>  > (YY_MOVE (s.value));
        break;

      case symbol_kind::S_assigns: // assigns
        value.move< std::vector<std::shared_ptr<TopAssign> >  > (YY_MOVE (s.value));
        break;

      case symbol_kind::S_reduce_args: // reduce_args
      case symbol_kind::S_extension: // extension
      case symbol_kind::S_ext_list: // ext_list
        value.move< std::vector<std::string>  > (YY_MOVE (s.value));
        break;

      default:
        break;
    }

    location = YY_MOVE (s.location);
  }

  // by_kind.
  inline
  parser::by_kind::by_kind ()
    : kind_ (symbol_kind::S_YYEMPTY)
  {}

#if 201103L <= YY_CPLUSPLUS
  inline
  parser::by_kind::by_kind (by_kind&& that)
    : kind_ (that.kind_)
  {
    that.clear ();
  }
#endif

  inline
  parser::by_kind::by_kind (const by_kind& that)
    : kind_ (that.kind_)
  {}

  inline
  parser::by_kind::by_kind (token_kind_type t)
    : kind_ (yytranslate_ (t))
  {}

  inline
  void
  parser::by_kind::clear ()
  {
    kind_ = symbol_kind::S_YYEMPTY;
  }

  inline
  void
  parser::by_kind::move (by_kind& that)
  {
    kind_ = that.kind_;
    that.clear ();
  }

  inline
  parser::symbol_kind_type
  parser::by_kind::kind () const YY_NOEXCEPT
  {
    return kind_;
  }

  inline
  parser::symbol_kind_type
  parser::by_kind::type_get () const YY_NOEXCEPT
  {
    return this->kind ();
  }

#line 157 "formula_parser.ypp"
} // lax
#line 2751 "formula_parser.tab.hpp"




#endif // !YY_YY_FORMULA_PARSER_TAB_HPP_INCLUDED
