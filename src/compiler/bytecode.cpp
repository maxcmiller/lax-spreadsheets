#include "bytecode.hpp"

#include <cassert>
#include <cmath>
#include <map>

#include "compiler_visitor.hpp"
#include "formula_ast.hpp"
#include "../util/scope_exit.hpp"

namespace lax
{
    unsigned BytecodeFunction::m_maxStack = 512;

    CompilationError::CompilationError(const location& loc, ErrorType t,
            std::string_view extra):
        CompilationError(loc, t)
    {
        m_msg.reserve(m_msg.size() + extra.size() + 2);
        m_msg += ": ";

        auto pos = m_msg.size();
        m_msg += extra;
        m_extra = std::string_view(m_msg.c_str() + pos, m_msg.size() - pos);
    }

    const char* CompilationError::findMsg(ErrorType t) noexcept
    {
        constexpr static const char* msgs[]
        {
            "mismatched types to binary operation",
            "invalid binary operation",
            "a function must explicitly specify its return type to be recursive",
            "need a function object to call",
            "bad arguments to function",
            "invalid unary operation",
            "unknown variable",
            "array reference outside of array formula",
            "array reference on non-array variable",
            "size of arrays do not match",
            "if expression must have three arguments",
            "condition of if expression must be boolean",
            "results of if must have the same type",
            "declared return type of function and inferred return type do not "
                "match",
            "reduce reference to non-reduce variable",
            "unknown type"
        };

        int idx = static_cast<int>(t);
        assert(idx < static_cast<int>(sizeof(msgs)/sizeof(msgs[0])));
        return msgs[idx];
    }

    const char* stringize(OpCode op)
    {
        constexpr static const char* istrs[static_cast<int>(OpCode::kNumOps)] = {
            "OpConst",
            "OpLoad",
            "OpLoadClosure",
            "OpArray",
            "OpCall",
            "OpAdd",
            "OpSubtract",
            "OpMult",
            "OpDiv",
            "OpPower",
            "OpNegate",
            "OpEqualBool",
            "OpNotEqualBool",
            "OpAndBool",
            "OpOrBool",
            "OpNotBool",
            "OpEqualNum",
            "OpNotEqualNum",
            "OpLessNum",
            "OpGreaterNum",
            "OpLessEqualNum",
            "OpGreaterEqualNum",
            "OpEqualString",
            "OpNotEqualString",
            "OpLessString",
            "OpGreaterString",
            "OpLessEqualString",
            "OpGreaterEqualString",
            "OpConcat",
            "OpIf",
            "OpGoto",
            "OpArrayElem",
            "OpArraySize",
            "OpCounter",
        };

        return istrs[static_cast<int>(op)];
    }

    Instruction::Instruction(OpCode op):
        m_op(op), m_obj(), m_nargs(-1)
    {
        switch (m_op)
        {
        case OpCode::kOpCall:
            throw std::logic_error("OpCall requires a function argument");
        case OpCode::kOpConst:
            throw std::logic_error("OpConst requires an object argument");
//        case OpCode::kOpArray:
//            throw std::logic_error("OpArray requires an object argument");
        case OpCode::kOpLoad:
            throw std::logic_error("OpLoad requires an object argument");
        case OpCode::kOpLoadClosure:
            throw std::logic_error("OpLoadClosure requires an object argument");
//        case OpCode::kOpIf:
//            throw std::logic_error("OpIf requires an offset argument");
//        case OpCode::kOpGoto:
//            throw std::logic_error("OpGoto requires an offset argument");
        default:
            break;
        }
    }

    Instruction::Instruction(OpCode op, Object::Ptr obj):
        m_op(op), m_obj(obj), m_nargs(-1)
    {
        if (m_op == OpCode::kOpLoadClosure &&
            !std::dynamic_pointer_cast<BytecodeArgFunction>(obj))
        {
            throw std::logic_error("cannot use OpLoadClosure with no closure");
        }
    }

    Object::Ptr Instruction::getObject() const noexcept
    {
        struct ObjVisitor
        {
            Object::Ptr operator()(const Object::Ptr& obj) const
            {
                return obj;
            }

            Object::Ptr operator()(const WeakRef<const ValueNode>& node) const
            {
                return node->getObject();
            }
        };

        return std::visit(ObjVisitor(), m_obj);
    }

    std::string Instruction::stringize() const
    {
        std::string instr = lax::stringize(m_op);

        switch (m_op)
        {
        case OpCode::kOpConst:
        {
            auto obj = getObject();
            assert(obj);
            return instr + ' ' + obj->stringize();
        }
        case OpCode::kOpLoad:
        case OpCode::kOpArray:
        {
            return instr + getObject()->getType().getName();
        }
        case OpCode::kOpCall:
        {
            auto obj = getObject();
            assert(obj);
            return instr + ' ' + obj->stringize() + " taking " +
                    std::to_string(m_nargs) + " arguments";
        }
        case OpCode::kOpIf:
        case OpCode::kOpGoto:
        {
            return instr + ' ' + std::to_string(m_nargs);
        }
        default:
            return instr;
        }
    }

    int BytecodeFunction::pushInstruct(const Instruction& i)
    {
        // TODO (Max#1#): Add constexpr optimization?
        int ret = m_instr.size();
        m_instr.push_back(i);
        return ret;

//        if (i.getOp() == OpCode::kOpCall)
//        {
//            auto f = std::dynamic_pointer_cast<FunctionObject>(i.getObject());
//            assert(f);
//
//            if (!f->isPure())
//            {
//                m_isPure = false;
//            }
//        }
    }

    std::string BytecodeFunction::stringize() const
    {
        return "<bytecode>";
    }

    Object::Ptr BytecodeFunction::execute(const std::vector<Ptr>& args) const
    {
        int index = -1;

        if (args.size() >= 1)
        {
            if (auto n = std::dynamic_pointer_cast<NumberObject>(args[0]))
            {
                index = n->getValue();
            }
        }

        return doExecute(index);
    }

    Object::Ptr BytecodeFunction::doExecute(int index) const
    {
        thread_local unsigned stackNo = 0;

        if (stackNo > m_maxStack && m_maxStack > 0)
        {
            return std::make_shared<ErrorObject>("stack overflow",
                    ErrorObject::kStackOverflow);
        }

        ++stackNo;
        ScopeExit decStackNo([] {--stackNo;});
        std::vector<Ptr> stack;

        auto get2Args = [&stack](auto ptrType)
        {
            if (stack.size() < 2)
            {
                throw std::logic_error("not enough elements on stack to evaluate");
            }

            using Type = typename decltype(ptrType)::element_type;
            auto rhs = std::dynamic_pointer_cast<Type>(stack.back());
            stack.pop_back();
            auto lhs = std::dynamic_pointer_cast<Type>(stack.back());
            stack.pop_back();

            if (!lhs || !rhs)
            {
                auto error = std::make_shared<ErrorObject>("bad type combination",
                        ErrorObject::kBadArgs);
                throw FuncError(error);
            }

            return std::make_pair(lhs, rhs);
        };

        try
        {
            for (auto i = m_instr.begin(); i != m_instr.end(); ++i)
            {
                switch (i->getOp())
                {
                case OpCode::kOpConst:
                case OpCode::kOpLoad:
                    stack.push_back(i->getObject());
                    break;
                case OpCode::kOpLoadClosure:
                {
                    auto func =
                    std::static_pointer_cast<BytecodeArgFunction>(i->getObject());
                    func->setIndex(index);
                    stack.push_back(func->getClosure());
                    break;
                }
                case OpCode::kOpArray:
                {
                    if (stack.size() < 1)
                    {
                        throw std::logic_error("not enough elements on stack to "
                                "evaluate");
                    }

                    auto arr =
                    std::dynamic_pointer_cast<ArrayObject>(stack.back());
                    stack.pop_back();

                    if (!arr)
                    {
                        return std::make_shared<ErrorObject>("operand to OpArray "
                                "is not an array", ErrorObject::kBadArgs);
                    }

                    if (arr->size() <= static_cast<std::size_t>(index))
                    {
                        auto error =
                        std::make_shared<ErrorObject>("out of range on array",
                                ErrorObject::kDomainError);
                        throw FuncError(error);
                    }

                    stack.push_back((*arr)[index]);
                    break;
                }
                case OpCode::kOpCall:
                {
                    // +1 for the function itself
                    if (static_cast<std::size_t>(i->getNumArgs()) + 1 > stack.size())
                    {
                        throw std::logic_error("not enough elements on stack to"
                                " make call");
                    }

                    auto first = stack.end() - i->getNumArgs();
                    std::vector<Ptr> pass(first, stack.end());
                    stack.erase(first, stack.end());

                    auto f =
                    std::dynamic_pointer_cast<FunctionObject>(stack.back());

                    if (!f)
                    {
                        return std::make_shared<ErrorObject>("operand to call "
                                "is not function", ErrorObject::kBadArgs);
                    }

                    stack.pop_back();
                    stack.push_back(f->execute(pass));

                    if (!stack.back() || // safety check
                        stack.back()->getType() == ErrorObject::theType())
                    {
                        return stack.back(); // propagate the error
                    }

                    break;
                }
                case OpCode::kOpNegate:
                {
                    if (stack.size() < 1)
                    {
                        throw std::logic_error("not enough elements on stack to "
                                "evaluate");
                    }

                    auto arg =
                    std::dynamic_pointer_cast<NumberObject>(stack.back());
                    stack.pop_back();

                    if (!arg)
                    {
                        return std::make_shared<ErrorObject>("operand to negate "
                                "is not number", ErrorObject::kBadArgs);
                    }

                    stack.push_back(
                            std::make_shared<NumberObject>(-arg->getValue()));
                    break;
                }
                case OpCode::kOpDiv:
                {
                    auto [lhs, rhs] = get2Args(std::shared_ptr<NumberObject>());

                    if (rhs->getValue() == 0.0)
                    {
                        return std::make_shared<ErrorObject>("division by zero",
                                ErrorObject::kDomainError, "#DIVBY0");
                    }

                    stack.push_back(std::make_shared<NumberObject>(
                            lhs->getValue() / rhs->getValue()));

                    break;
                }
                case OpCode::kOpEqualBool:
                {
                    auto [lhs, rhs] = get2Args(std::shared_ptr<BoolObject>());
                    stack.push_back(BoolObject::get(lhs == rhs));
                    break;
                }
                case OpCode::kOpNotEqualBool:
                {
                    auto [lhs, rhs] = get2Args(std::shared_ptr<BoolObject>());
                    stack.push_back(BoolObject::get(lhs != rhs));
                    break;
                }
                case OpCode::kOpIf:
                {
                    if (stack.size() < 1)
                    {
                        throw std::logic_error("not enough elements on stack to"
                                " evaluate");
                    }

                    auto b = std::dynamic_pointer_cast<BoolObject>(stack.back());
                    stack.pop_back();

                    if (!b)
                    {
                        return std::make_shared<ErrorObject>("operand to if is "
                                "not boolean", ErrorObject::kBadArgs);
                    }

                    if (b == BoolObject::getFalse())
                    {
                        i += i->getNumArgs();
                    }

                    break;
                }
                case OpCode::kOpGoto:
                {
                    i += i->getNumArgs();
                    break;
                }
                case OpCode::kOpArrayElem:
                {
                    if (stack.size() < 2)
                    {
                        throw std::logic_error("not enough elements on stack to"
                                " evaluate");
                    }

                    auto rhs = std::dynamic_pointer_cast<NumberObject>(stack.back());
                    stack.pop_back();
                    auto lhs = std::dynamic_pointer_cast<ArrayObject>(stack.back());
                    stack.pop_back();

                    if (!lhs || !rhs)
                    {
                        return std::make_shared<ErrorObject>("incorrect "
                                "operands to array elements",
                                ErrorObject::kBadArgs);
                    }

                    auto val = rhs->getValue();

                    if (val < 0 || val >= lhs->size())
                    {
                        return std::make_shared<ErrorObject>("index out of bounds",
                                ErrorObject::kRangeError);
                    }

                    stack.push_back((*lhs)[rhs->getValue()]);
                    break;
                }
#define LAX_BIN_OP(code, type, op, ret)                                         \
                case OpCode::kOp##code:                                         \
                {                                                               \
                    auto [lhs, rhs] = get2Args(std::shared_ptr<type>());        \
                    stack.push_back(                                            \
                        std::make_shared<ret>(lhs->getValue() op rhs->getValue())); \
                    break;                                                      \
                }

                LAX_BIN_OP(Add, NumberObject, +, NumberObject)
                LAX_BIN_OP(Subtract, NumberObject, -, NumberObject)
                LAX_BIN_OP(Mult, NumberObject, *, NumberObject)
                LAX_BIN_OP(Concat, StringObject, +, StringObject)

#define LAX_COMPARE(code, type, op)     \
                case OpCode::kOp##code: \
                {                       \
                    auto [lhs, rhs] = get2Args(std::shared_ptr<type>()); \
                    stack.push_back(BoolObject::get(                     \
                            lhs->getValue() op rhs->getValue()));        \
                    break;              \
                }

#define LAX_COMP_OP(abbr, type)                                     \
                LAX_COMPARE(Equal##abbr, type, ==)           \
                LAX_COMPARE(NotEqual##abbr, type, !=)        \
                LAX_COMPARE(Less##abbr, type, <)             \
                LAX_COMPARE(Greater##abbr, type, >)          \
                LAX_COMPARE(LessEqual##abbr, type, <=)       \
                LAX_COMPARE(GreaterEqual##abbr, type, >=)

                LAX_COMP_OP(Num, NumberObject)
                LAX_COMP_OP(String, StringObject)
#undef LAX_COMP_OP
#undef LAX_COMPARE
#undef LAX_BIN_OP

                case OpCode::kOpAndBool:
                {
                    auto [lhs, rhs] = get2Args(std::shared_ptr<BoolObject>());
                    stack.push_back(BoolObject::get(lhs->getValue() && rhs->getValue()));
                    break;
                }
                case OpCode::kOpOrBool:
                {
                    auto [lhs, rhs] = get2Args(std::shared_ptr<BoolObject>());
                    stack.push_back(BoolObject::get(lhs->getValue() || rhs->getValue()));
                    break;
                }
                case OpCode::kOpNotBool:
                {
                    if (stack.size() < 1)
                    {
                        throw std::logic_error("not enough arguments for not");
                    }

                    auto lhs = std::dynamic_pointer_cast<BoolObject>(stack.back());

                    if (!lhs)
                    {
                        return std::make_shared<ErrorObject>("operand to not "
                                "is not boolean", ErrorObject::kBadArgs);
                    }

                    stack.push_back(BoolObject::get(lhs == BoolObject::getFalse()));
                    break;
                }
                case OpCode::kOpArraySize:
                {
                    if (stack.size() < 1)
                    {
                        throw std::logic_error("not enough arguments for array size");
                    }

                    auto lhs = std::dynamic_pointer_cast<ArrayObject>(stack.back());
                    stack.pop_back();

                    if (!lhs)
                    {
                        return std::make_shared<ErrorObject>("operand to array "
                                "size is not array", ErrorObject::kBadArgs);
                    }

                    stack.push_back(std::make_shared<NumberObject>(lhs->size()));
                    break;
                }
                case OpCode::kOpCounter:
                {
                    stack.push_back(std::make_shared<NumberObject>(index));
                    break;
                }
                case OpCode::kOpPower:
                {
                    auto [lhs, rhs] = get2Args(std::shared_ptr<NumberObject>());
                    auto got = std::pow(lhs->getValue(), rhs->getValue());
                    stack.push_back(std::make_shared<NumberObject>(got));
                    break;
                }
                case OpCode::kOpAbs:
                {
                    auto lhs = std::dynamic_pointer_cast<NumberObject>(stack.back());
                    stack.pop_back();

                    if (!lhs)
                    {
                        return std::make_shared<ErrorObject>("operand to abs is"
                                " not number", ErrorObject::kBadArgs);
                    }

                    auto got = std::abs(lhs->getValue());
                    stack.push_back(std::make_shared<NumberObject>(got));
                    break;
                }
                case OpCode::kNumOps:
                    assert(false && "invalid OpCode");
                } // switch on opcode
            } // for instruction in bytecode
        } // try block
        catch (FunctionObject::FuncError& ex)
        {
            return ex.getError();
        }

        if (stack.empty())
        {
            throw std::logic_error("nothing to return from stack");
        }

        return stack.back();
    }

    std::string BytecodeFunction::dumpBytecode() const
    {
        std::string got;

        for (const auto& i: m_instr)
        {
            got += i.stringize() + '\n';
        }

        return got;
    }

    Object::Ptr BytecodeArgFunction::execute(const std::vector<Ptr>& args) const
    {
        if (args.size() != m_args.size())
        {
            throw std::logic_error("bytecode function called with wrong number "
                                   "of arguments");
        }

        std::vector<Ptr> other;
        other.reserve(m_args.size());

        auto nodeItr = m_args.begin();
        auto argItr = args.begin();

        for (; nodeItr != m_args.end(); ++nodeItr, ++argItr)
        {
            other.push_back((*nodeItr)->getObject());

            if (*argItr)
            {
                (*nodeItr)->setObject(*argItr);
            }
        }

        auto got = doExecute(m_idx);

        nodeItr = m_args.begin();
        argItr = other.begin();

        for (; nodeItr != m_args.end(); ++nodeItr, ++argItr)
        {
            if (*argItr)
            {
                (*nodeItr)->setObject(*argItr);
            }
        }

        return got;
    }

    void BytecodeArgFunction::setReturn(const TypeInfo& res)
    {
        std::vector<TypeInfo> args(m_args.size());
        std::transform(m_args.cbegin(), m_args.cend(), args.begin(),
                [](const auto& n) {return n->getType();});
        setFuncTypeInfo(FixedFuncTypeInfo::create(res, std::move(args)));
    }

    void BytecodeArgFunction::addOuter(ValueNode& n)
    {
        m_outer.insert(n.getWeakRef());
    }

    class BytecodeArgFunction::Closure:
        public FunctionObject
    {
    public:
        Closure(const BytecodeArgFunction* f):
            FunctionObject(f->getFuncTypeInfo()), m_parent(f),
            m_vals()
        {
            m_vals.reserve(f->m_outer.size());

            for (const auto& n: f->m_outer)
            {
                m_vals.emplace_back(n, n->getObject());
            }
        }

        std::string stringize() const override
        {
            return "<closure>";
        }

        Ptr execute(const std::vector<Ptr>& args) const override
        {
            // TODO (ljcas#1#): Improve VM
            std::vector<Object::Ptr> old;
            old.reserve(m_vals.size());

            for (const auto& p: m_vals)
            {
                old.push_back(p.first->getObject());
                p.first->setObject(p.second);
            }

            auto got = m_parent->execute(args);

            auto objItr = old.begin();
            auto valItr = m_vals.begin();

            for (; objItr != old.end(); ++objItr, ++valItr)
            {
                valItr->first->setObject(*objItr);
            }

            return got;
        }
    private:
        const BytecodeArgFunction* m_parent;
        std::vector<std::pair<ValueNode::Ptr, Object::Ptr> > m_vals;
    };

    std::shared_ptr<FunctionObject> BytecodeArgFunction::getClosure() const
    {
        return std::make_shared<Closure>(this);
    }

    ValueNode::Ptr compile(FormulaExpr::Ptr exp, const Environment::Ptr& env, ExprType type)
    {
        auto f = std::make_shared<BytecodeFunction>();
        auto n = ValueNode::make(TypeInfo::nullType(), env->getSystem());

        CompileVisitor cv(*f, *n, env, type);
        cv.compile(*exp);
        f->setReturn(cv.getType());

        n->setFormula(f);
        n->setType(cv.getType());
        return n;
    }
}
