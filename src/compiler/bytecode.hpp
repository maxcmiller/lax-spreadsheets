#ifndef LAX_BYTECODE_HPP_
#define LAX_BYTECODE_HPP_

#include <unordered_set>
#include <variant>

#include "environment.hpp"
#include "location.hpp"
#include "object.hpp"

namespace lax
{
    enum class OpCode
    {
        kOpConst,
        kOpLoad,
        kOpLoadClosure,
        kOpArray,
        kOpCall,

        kOpAdd,
        kOpSubtract,
        kOpMult,
        kOpDiv,
        kOpPower,
        kOpNegate,
        kOpAbs,

        kOpEqualBool,
        kOpNotEqualBool,
        kOpAndBool,
        kOpOrBool,
        kOpNotBool,

        kOpEqualNum,
        kOpNotEqualNum,
        kOpLessNum,
        kOpGreaterNum,
        kOpLessEqualNum,
        kOpGreaterEqualNum,

        kOpEqualString,
        kOpNotEqualString,
        kOpLessString,
        kOpGreaterString,
        kOpLessEqualString,
        kOpGreaterEqualString,

        kOpConcat,

        kOpIf,
        kOpGoto,

        kOpArrayElem,
        kOpArraySize,

        kOpCounter,

        kNumOps
    };

    const char* stringize(OpCode op);

    class Instruction
    {
    public:
        explicit Instruction(OpCode op);

        Instruction(OpCode op, WeakRef<const ValueNode> obj):
            m_op(op), m_obj(obj), m_nargs(-1) {}

        explicit Instruction(int nargs):
            m_op(OpCode::kOpCall), m_obj(), m_nargs(nargs) {}

        Instruction(OpCode op, Object::Ptr obj);

        Instruction(const Instruction&) = default;
        Instruction(Instruction&&) = default;

        Instruction& operator=(const Instruction&) = default;
        Instruction& operator=(Instruction&&) = default;

        OpCode getOp() const noexcept
        {
            return m_op;
        }

        int getNumArgs() const noexcept
        {
            return m_nargs;
        }

        void setJump(int n) noexcept
        {
            m_nargs = n;
        }

        Object::Ptr getObject() const noexcept;
        std::string stringize() const;
    private:
        OpCode m_op;
        std::variant<Object::Ptr, WeakRef<const ValueNode> > m_obj;
        int m_nargs;
    };

    class BytecodeFunction:
        public FunctionObject
    {
    public:
        BytecodeFunction():
            FunctionObject(nullptr), m_instr(), m_iterNum(-1) {}

        BytecodeFunction(std::initializer_list<lax::Instruction>&& instrs,
                const TypeInfo& result, int iterNum):
            FunctionObject(FixedFuncTypeInfo::create(result, {})),
            m_instr(std::move(instrs)), m_iterNum(iterNum) {}

        BytecodeFunction(const BytecodeFunction&) = default;
        BytecodeFunction(BytecodeFunction&&) = default;

        int pushInstruct(const Instruction& i);

        void setJumpOn(int index, int jmp)
        {
            m_instr.at(index).setJump(jmp);
        }

        void setIterNum(int i)
        {
            m_iterNum = i;
        }

        int getIterNum() const noexcept
        {
            return m_iterNum;
        }

        auto begin() const noexcept
        {
            return m_instr.begin();
        }

        auto end() const noexcept
        {
            return m_instr.end();
        }

        std::size_t size() const noexcept
        {
            return m_instr.size();
        }

        int getNumIterations() const noexcept
        {
            return m_iterNum;
        }

        std::string stringize() const override;
        Ptr execute(const std::vector<Ptr>& args) const override;
//        bool isPure() const noexcept override;

        virtual void setReturn(const TypeInfo& res)
        {
            setFuncTypeInfo(FixedFuncTypeInfo::create(res, {}));
        }

        virtual void addOuter(ValueNode&) {}

        std::string dumpBytecode() const;

        static void setMaxStack(unsigned n)
        {
            m_maxStack = n;
        }

        static unsigned getMaxStack()
        {
            return m_maxStack;
        }
    protected:
        Ptr doExecute(int idx) const;
    private:
        std::vector<Instruction> m_instr;
        int m_iterNum;
//        bool m_isPure;

        static unsigned m_maxStack;
    };

    class BytecodeArgFunction:
        public BytecodeFunction
    {
    public:
        BytecodeArgFunction() = default;

        BytecodeArgFunction(std::initializer_list<lax::Instruction>&& instrs,
                const TypeInfo& result, int iterNum):
            BytecodeFunction(std::move(instrs), result, iterNum), m_idx(-1) {}

        BytecodeArgFunction(const BytecodeArgFunction&) = default;
        BytecodeArgFunction(BytecodeArgFunction&&) = default;

        void addArg(const ValueNode::Ptr& vn)
        {
            m_args.push_back(vn);
        }

        void setIndex(int idx)
        {
            m_idx = idx;
        }

        std::shared_ptr<FunctionObject> getClosure() const;

        Ptr execute(const std::vector<Ptr>& args) const override;
        void setReturn(const TypeInfo& res) override;
        void addOuter(ValueNode& n) override;
    private:
        class Closure;

        std::vector<ValueNode::Ptr> m_args;
        std::unordered_set<ValueNode::Ptr, hash<ValueNode::Ptr> > m_outer;
        int m_idx;
    };

    enum class ErrorType
    {
        kMismatchedBinType,
        kInvalidBinOp,
        kFuncExplicitReturn,
        kNeedFunc,
        kBadFuncArgs,
        kInvalidUnaryOp,
        kUnknownVar,
        kNotInArray,
        kNonArrayVar,
        kBadArraySize,
        kBadIfNum,
        kBadIfCond,
        kBadIfResult,
        kMismatchedFuncRet,
        kUnknownType,
    };

    class CompilationError:
        public std::exception
    {
    public:
        CompilationError(const location& loc, ErrorType t):
            m_loc(loc), m_type(t), m_msg(findMsg(t)), m_extra() {}

        CompilationError(const location& loc, ErrorType t, std::string_view extra);
        CompilationError(const CompilationError&) = default;
        CompilationError(CompilationError&&) = default;

        const char* what() const noexcept override
        {
            return m_msg.c_str();
        }

        location getLoc() const noexcept
        {
            return m_loc;
        }

        ErrorType getType() const noexcept
        {
            return m_type;
        }

        std::string_view getExtra() const noexcept
        {
            return m_extra;
        }
    private:
        static const char* findMsg(ErrorType t) noexcept;

        location m_loc;
        ErrorType m_type;
        std::string m_msg;
        std::string_view m_extra;
    };

    class FormulaExpr;

    enum class ExprType
    {
        kScalar,
        kArray,
    };

    ValueNode::Ptr compile(std::shared_ptr<FormulaExpr> exp,
            const Environment::Ptr& env, ExprType type);
}

#endif // LAX_BYTECODE_HPP_
