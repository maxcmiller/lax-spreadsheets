#include "object.hpp"

#include <cstdio>
#include <map>

namespace lax
{
    Object::~Object() = default;
    ObjectFactory::~ObjectFactory() = default;

    namespace
    {
        RegisterType registerNum(NumberObject::theType());
        RegisterType registerStr(StringObject::theType());
        RegisterType registerBool(BoolObject::theType());
    }

    TypeInfo NumberObject::theType()
    {
        class NumFactory:
            public ObjectFactory
        {
        public:
            Object::Ptr construct(std::string_view str)
            {
                double d;

                if (str.back() == '\0')
                {
                    sscanf(str.data(), "%lf", &d);
                }
                else
                {
                    d = std::stod(std::string(str.begin(), str.end()));
                }

                return std::make_shared<NumberObject>(d);
            }
        };

        static TypeInfo type("Number", std::make_unique<NumFactory>(), true);
        return type;
    }

    std::string NumberObject::stringize() const
    {
        auto n = snprintf(nullptr, 0, "%g", m_val);
        std::string buf(n, '\0');
        snprintf(buf.data(), buf.size() + 1, "%g", m_val);
        return buf;
    }

    std::string NumberObject::serialize() const
    {
        auto n = snprintf(nullptr, 0, "%a", m_val);
        std::string buf(n, '\0');
        snprintf(buf.data(), buf.size() + 1, "%a", m_val);
        return buf;
    }

    TypeInfo StringObject::theType()
    {
        class StrFactory:
            public ObjectFactory
        {
        public:
            Object::Ptr construct(std::string_view str)
            {
                return std::make_shared<StringObject>(
                        std::string(str.data(), str.size()));
            }
        };

        static TypeInfo type("String", std::make_unique<StrFactory>(), true);
        return type;
    }

    std::string StringObject::stringize() const
    {
        return m_val;
    }

    TypeInfo BoolObject::theType()
    {
        class BoolFactory:
            public ObjectFactory
        {
        public:
            std::shared_ptr<Object> construct(std::string_view str)
            {
                return BoolObject::get(str == "TRUE");
            }
        };

        static TypeInfo type("Boolean", std::make_unique<BoolFactory>(), true);
        return type;
    }

    std::string BoolObject::stringize() const
    {
        if (m_val)
        {
            return "TRUE";
        }
        else
        {
            return "FALSE";
        }
    }

    const char* FunctionObject::FuncError::what() const noexcept
    {
        return m_obj->getMessage().c_str();
    }

    TypeInfo ErrorObject::theType()
    {
        static TypeInfo type("Error", nullptr, true);
        return type;
    }

    std::string ErrorObject::stringize() const
    {
        return m_abbr;
    }

    const char* ErrorObject::abbrString(Error e)
    {
        switch (e)
        {
        case kBadArgs:
            return "#BADTYPE";
        case kDomainError:
            return "#DOMAIN";
        case kRangeError:
            return "#RANGE";
        case kStackOverflow:
            return "#STACK";
        case kMiscError:
            return "#ERROR";
        }

        return "#ERROR";
    }

    std::string ArrayObject::stringize() const
    {
        std::string str(1, '{');

        for (const auto& obj: m_data)
        {
            str += obj->stringize();
            str += ", ";
        }

        if (!m_data.empty())
        {
            str.pop_back();
            str.pop_back();
        }

        str += '}';

        return str;
    }
}
