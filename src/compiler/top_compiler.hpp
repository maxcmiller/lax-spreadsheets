#ifndef LAX_SRC_COMPILER_TOP_COMPILER_HPP_
#define LAX_SRC_COMPILER_TOP_COMPILER_HPP_

#include <iosfwd>
#include <memory>
#include <string_view>
#include <unordered_map>
#include <variant>
#include <vector>

#include "importer/importer.hpp"

namespace lax
{
    class ChartBase;
    class PDataManager;
    class Table;

    class ChartBuilder
    {
    public:
        class Failure:
            public std::runtime_error
        {
        public:
            explicit Failure(const std::string& str):
                runtime_error(str) {}

            explicit Failure(const char* str):
                runtime_error(str) {}

            Failure(const Failure&) = default;
            Failure(Failure&&) = default;
        };

        using Ptr = std::unique_ptr<ChartBuilder>;

        virtual ~ChartBuilder() = default;

        virtual Ptr clone(const std::shared_ptr<Table>&, ICaseStringView name,
                PDataManager&, ValueSystem&) = 0;
        virtual WeakRef<ChartBase> build() = 0;
        virtual void setIndex(const Table& tbl, int idx) = 0;
        virtual void addSeriesArray(const Table& tbl, int arrIndex) = 0;
        virtual void addSeriesFunc(const Table& tbl, int scalarIndex) = 0;
    };

    struct CompileInfo
    {
        using Ptr = std::shared_ptr<CompileInfo>;
        using ConstPtr = std::shared_ptr<const CompileInfo>;
        using BuilderMap = NameMap<ChartBuilder::Ptr>;

        explicit CompileInfo(const ImportFinder::Ptr& dflt = nullptr,
                std::string_view dfltName = ""):
            imp(dflt, dfltName) {}

        Importer imp;
        BuilderMap builders;
    };

    class TopCompiler
    {
    public:
        using TablePtr = std::shared_ptr<Table>;
        using ChartPtr = WeakRef<ChartBase>;

        TopCompiler(const CompileInfo::ConstPtr& ci, std::string_view filename,
                std::string_view dir, PDataManager& mgr):
            m_info(ci), m_file(filename), m_dir(dir), m_sys(), m_mgr(mgr) {}

        TopCompiler(const TopCompiler&) = delete;

        ValueSystem& recompile(std::istream& istr);

        const auto& getTables() const noexcept
        {
            return m_tbls;
        }

        const auto& getCharts() const noexcept
        {
            return m_charts;
        }

        const auto& getTableMap() const noexcept
        {
            return m_tblMap;
        }

        const auto& getChartMap() const noexcept
        {
            return m_chartMap;
        }

        TablePtr findTable(const std::string& name);
        ChartPtr findChart(const std::string& name, const std::string& type);

        void setFilename(std::string_view f)
        {
            m_file = f;
        }

        void setDir(std::string_view dir)
        {
            m_dir = dir;
        }
    private:
        class TCVisitor;
        using ObjectType = std::variant<TablePtr, ChartPtr>;
        using TableMap = NameMap<TablePtr>;
        using ChartMap = NameMap<ChartPtr>;

        static constexpr const char m_typeSep = '\x1f';

        CompileInfo::ConstPtr m_info;
        std::string_view m_file;
        std::string_view m_dir;
        std::vector<TablePtr> m_tbls;
        std::vector<ChartPtr> m_charts;
        TableMap m_tblMap;
        ChartMap m_chartMap;
        NameMap<ObjectType> m_objMap;
        std::unique_ptr<ValueSystem> m_sys;
        PDataManager& m_mgr;
    };
}

#endif // LAX_SRC_COMPILER_TOP_COMPILER_HPP_
