#ifndef LAX_SRC_AUTOCOMPLETE_HPP_
#define LAX_SRC_AUTOCOMPLETE_HPP_

#include "disable_warnings.h"

#include <unordered_map>

LAX_DISABLE_WARNINGS()
#include <wx/dataview.h>
#include <wx/stc/stc.h>
#include <wx/timer.h>
LAX_REENABLE_WARNINGS()

#include "compiler/importer/importer.hpp"
#include "compiler/formula_ast.hpp"

namespace lax
{
    class AutocompleteCtrl:
        public wxWindow
    {
    public:
        AutocompleteCtrl(wxStyledTextCtrl* ctrl, wxWindow* parent,
                wxWindowID id = wxID_ANY);

        void setSymbolList(const std::vector<TopAssign::Ptr>& comp);
    private:
        void OnDblClick(wxDataViewEvent& evt);
        void OnTextChanged(wxStyledTextEvent& evt);
        void OnEditLabel(wxDataViewEvent& evt);
        void OnMenu(wxDataViewEvent& evt);

        wxString getInsertText(const wxDataViewItem& item, int line, int col);

        std::unordered_map<std::string, wxDataViewItem> m_map;
        wxDataViewTreeCtrl* m_tree;
        wxStyledTextCtrl* m_code;

        wxDECLARE_EVENT_TABLE();
    };
}

#endif // LAX_SRC_AUTOCOMPLETE_HPP_
