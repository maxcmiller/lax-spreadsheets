#ifndef LAX_SRC_SHEET_VIEW_HPP_
#define LAX_SRC_SHEET_VIEW_HPP_

#include "disable_warnings.h"

LAX_DISABLE_WARNINGS()
#include <wx/button.h>
#include <wx/docview.h>
#include <wx/grid.h>
#include <wx/settings.h>
LAX_REENABLE_WARNINGS()

#include "sheet_doc.hpp"
#include "table_code.hpp"

class wxPropertyGridEvent;

namespace lax
{
    class SheetView;
    class TableBase;

    class TableCtrlData
    {
    public:
        enum Bits:
            std::uintptr_t
        {
            kTable = 0,
            kChart = static_cast<Bits>(1) << (sizeof(Bits) * 8 - 1),

            kTypeBits = kChart,
            kIndexBits = ~kTypeBits,
        };

        explicit TableCtrlData(void* ptr):
            m_bits(reinterpret_cast<std::intptr_t>(ptr)) {}

        TableCtrlData(Bits type, std::size_t index):
            m_bits(type | index) {}

        operator void*() const noexcept
        {
            return reinterpret_cast<void*>(m_bits);
        }

        TableBase* getTable(SheetView* view, int pg) const;
    private:
        std::intptr_t m_bits;

        friend bool operator==(const TableCtrlData& lhs, const TableCtrlData& rhs) noexcept
        {
            return lhs.m_bits == rhs.m_bits;
        }
    };

    inline bool operator!=(const TableCtrlData& lhs, const TableCtrlData& rhs) noexcept
    {
        return !(lhs == rhs);
    }

    class TableCtrlBase:
        public wxPanel
    {
    public:
        TableCtrlBase(TableBase* tb, TableCtrlData data, wxWindow* parent,
                wxWindowID id = wxID_ANY,
                const wxPoint& pos = wxDefaultPosition,
                const wxSize& size = wxDefaultSize);

        void Fit() override;
        void SetLabel(const wxString& lbl) override;

        TableBase* getTable() const noexcept
        {
            return m_tb;
        }
    private:
        void OnPopupMenu(wxCommandEvent& evt);
        virtual void doPopupMenu() = 0;

        TableBase* m_tb;

        wxDECLARE_EVENT_TABLE();
    };

    class TableCtrl:
        public TableCtrlBase
    {
    public:
        TableCtrl(SheetDataTable* tbl, TableCtrlData data, wxWindow* parent,
                wxWindowID id = wxID_ANY);

        SheetDataTable* getData() const noexcept
        {
            return m_table;
        }
    private:
        void doPopupMenu() override;
        void OnResize(wxGridSizeEvent& evt);
        void OnCopyPaste(wxKeyEvent& evt);
        void doCopy(bool isCut);
        void doPaste();

        SheetDataTable* m_table;
        wxGrid* m_grid;

        wxDECLARE_EVENT_TABLE();
    };

    class ChartCtrl:
        public TableCtrlBase
    {
    public:
        ChartCtrl(ChartBase* cd, TableCtrlData data, wxWindow* parent,
                wxWindowID id = wxID_ANY);

        ChartBase* getData() const noexcept
        {
            return m_data;
        }
    private:
        void doPopupMenu() override;

        ChartBase* m_data;
        wxWindow* m_ctrl;
    };

    class SheetView:
        public wxView
    {
    public:
        SheetView() = default;

        SheetDoc* GetDocument()
        {
            return m_doc;
        }

        bool OnClose(bool deleteWindow) override;
        void OnUpdate(wxView* sender, wxObject* hint) override;
        wxPrintout* OnCreatePrintout() override;
        void OnDraw(wxDC*) override {}
        void SetDocument(wxDocument* doc) override;

        wxCommandProcessor* GetCommandProcessor() const noexcept
        {
            return m_doc->GetCommandProcessor();
        }
    private:
        struct SheetData
        {
            SheetData(wxPanel* p, TableCodeCtrl::CodeSet::Ptr cs):
                panel(p), codeSet(cs) {}

            SheetData(const SheetData&) = default;
            SheetData& operator=(const SheetData&) = default;

            wxPanel* panel;
            TableCodeCtrl::CodeSet::Ptr codeSet;
        };

        void OnChangePage(wxAuiNotebookEvent& evt);
        void OnNewPage(wxAuiNotebookEvent& evt);
        void OnTabPopup(wxAuiNotebookEvent& evt);
        void OnRecompileTbl(wxCommandEvent& evt);
        void OnMoveTable(wxCommandEvent& evt);
        void OnRenameTable(wxCommandEvent& evt);
        void OnChangeChartProp(wxPropertyGridEvent& evt);
        void OnTableResize(wxGridSizeEvent& evt);
        void OnCopy(wxCommandEvent& evt);
        void OnPaste(wxCommandEvent& evt);
        void OnLabelMenu(wxGridEvent& evt);
        void OnEditLabel(wxGridEvent& evt);
        void OnTableAppend(wxCommandEvent& evt);

        void OnMoveRow(int row, int n, SheetDataTable* tbl);
        void OnInsertRow(int row, int n, SheetDataTable* tbl);
        void OnDeleteRows(const wxArrayInt& arr, SheetDataTable* tbl);
        void OnDeleteRow(int row, SheetDataTable* tbl);

        std::pair<std::function<bool()>, std::function<bool()> >
        getDeleteRow(int row, SheetDataTable* tbl);

        void updateAll();
        void updateTable(int pgNo, SheetDataTable* tbl);
        void addPage(int pgNo, PageData* pg);
        void addTable(int pgNo, SheetDataTable* table, int index);
        void showChart(const WeakRef<ChartBase>& cd, int index);
        bool doRecompile(PageData& pg);
        wxWindow* findWithData(const TableBase* table, int pg);
        wxWindow* findWithData(TableCtrlData data, int pg);
        wxWindow* findWithData(bool(*func)(void*, TableCtrlData), void* search,
                int pg);
        bool getSelection(wxGrid* grid, wxGridCellCoords& topLeft,
                wxGridCellCoords& bottomRight) const;
        TableBase* getTableBase(const TableCtrlData& data, int pg);
        void placeWidget(TableCtrlBase* w);

        wxWindow* findWithData(const UpdateInfo& info)
        {
            return findWithData(info.getTable(), info.getPageNumber());
        }

        SheetDoc* m_doc = nullptr;
        std::vector<SheetData> m_sheets;
        int m_active = -1;
        int m_ignorePropChange = 0;
        bool m_ignoreChange = false;

        wxDECLARE_DYNAMIC_CLASS(SheetView);
        wxDECLARE_EVENT_TABLE();

        friend class TableCtrlData;
    };
}

#endif // LAX_SRC_SHEET_VIEW_HPP_
