#include "sheet_view.hpp"

#include <vector>

LAX_DISABLE_WARNINGS()
#include <wx/choicdlg.h>
#include <wx/clipbrd.h>
#include <wx/dcclient.h>
#include <wx/log.h>
#include <wx/msgdlg.h>
#include <wx/numdlg.h>
#include <wx/panel.h>
#include <wx/propgrid/propgrid.h>
#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/stopwatch.h>
#include <wx/textdlg.h>

#include "util/splitter.hpp"
LAX_REENABLE_WARNINGS()

#include "charts/barchart.hpp"
#include "charts/chart_setup.hpp"
#include "compiler/type_info.hpp"
#include "main.hpp"
#include "main_window.hpp"
#include "util/scope_exit.hpp"

// TODO (Max#1#): Move event functionality to sheet_view.cpp?

namespace lax
{
    namespace
    {
        wxDEFINE_EVENT(LAX_TABLE_CTRL_MENU, wxCommandEvent);

        class FloatValidator:
            public wxTextValidator
        {
        public:
            FloatValidator(double& d, wxString& str):
                wxTextValidator(wxFILTER_INCLUDE_CHAR_LIST, &str), m_val(d),
                m_str(str)
            {
                SetCharIncludes("0123456789.e+-");
                m_str.Printf("%g", d);
            }

            bool TransferFromWindow() override
            {
                return wxTextValidator::TransferFromWindow() &&
                        m_str.ToDouble(&m_val);
            }

            bool TransferToWindow() override
            {
                return m_str.Printf("%g", m_val) > 0 &&
                        wxTextValidator::TransferToWindow();
            }

            FloatValidator* Clone() const override
            {
                return new FloatValidator(*this);
            }
        private:
            double& m_val;
            wxString& m_str;
        };

        class FloatEditor:
            public wxGridCellTextEditor
        {
        public:
            FloatEditor():
                m_val(0.0), m_str("0.0")
            {
                SetValidator(FloatValidator(m_val, m_str));
            }

            void BeginEdit(int row, int col, wxGrid* grid) override
            {
                m_val = grid->GetTable()->GetValueAsDouble(row, col);
                wxGridCellTextEditor::BeginEdit(row, col, grid);
            }

            bool EndEdit(int row, int col, const wxGrid* grid,
                    const wxString&, wxString* newVal) override
            {
                if (!GetControl()->GetValidator()->TransferFromWindow() ||
                    (grid->GetTable()->GetValueAsCustom(row, col, "") &&
                    m_val == grid->GetTable()->GetValueAsDouble(row, col)))
                {
                    return false;
                }

                newVal->Printf("%g", m_val);
                return true;
            }

            void ApplyEdit(int row, int col, wxGrid* grid) override
            {
                grid->GetTable()->SetValueAsDouble(row, col, m_val);
            }
        private:
            double m_val;
            wxString m_str;
        };

        class TableTab:
            public wxPanel
        {
        public:
            TableTab(wxWindow* parent, wxWindowID id = wxID_ANY):
                wxPanel(parent, id, wxPoint(-1, -1), wxSize(-1, 20)),
                m_click(wxDefaultPosition)
            {
                // TODO (Max#1#): Add some buttons to TableCtrl
                constexpr const char* error = "TableTab must have grandparent";
                wxCHECK2_MSG(GetGrandParent(), throw std::logic_error(error), error);
                wxWindow::SetBackgroundColour(wxColour(0x6D6D6D)); // dark grey
                Layout();
            }
        private:
            void OnMouseDown(wxMouseEvent& evt);
            void OnMouseUp(wxMouseEvent& evt);
            void OnMouseDrag(wxMouseEvent& evt);
            void OnCaptureLost(wxMouseCaptureLostEvent& evt);
            void OnPaint(wxPaintEvent& evt);
            void popupMenu();

            wxRect getButtonRect() const noexcept
            {
                return wxRect(GetSize().x - 19, 1, 18, 18);
            }

            bool clickButton(const wxPoint& pt) const noexcept
            {
                return getButtonRect().Contains(pt);
            }

            static wxFont& getFont()
            {
                static wxFont font = []
                {
                    wxFont f(wxFontInfo().Bold().Family(wxFONTFAMILY_TELETYPE));
                    f.SetPixelSize(wxSize(fontPxSize, fontPxSize));
                    return f;
                }();

                return font;
            }

            wxPoint m_click;
            bool m_isActive = false;
            static constexpr int fontPxSize = 16;

            wxDECLARE_EVENT_TABLE();
        };

    LAX_DISABLE_WARNINGS()
        wxBEGIN_EVENT_TABLE(TableTab, wxPanel)
            EVT_LEFT_DOWN(TableTab::OnMouseDown)
            EVT_LEFT_UP(TableTab::OnMouseUp)
            EVT_MOTION(TableTab::OnMouseDrag)
            EVT_MOUSE_CAPTURE_LOST(TableTab::OnCaptureLost)
            EVT_PAINT(TableTab::OnPaint)
        wxEND_EVENT_TABLE()
    LAX_REENABLE_WARNINGS()

        void TableTab::OnMouseDown(wxMouseEvent& evt)
        {
            if (clickButton(evt.GetPosition()))
            {
                // this is a button press
                m_isActive = true;
                RefreshRect(getButtonRect());
            }
            else
            {
                m_click = evt.GetPosition();
                CaptureMouse();
            }

            evt.Skip();
        }

        void TableTab::OnMouseUp(wxMouseEvent& event)
        {
            if (m_isActive)
            {
                if (clickButton(event.GetPosition()))
                {
                    popupMenu();
                }

                m_isActive = false;
                RefreshRect(getButtonRect());
                event.Skip();
                return;
            }

            ReleaseMouse();
            m_click = wxDefaultPosition;

            // we're concerned about where the parent is; not where we are
            wxPoint pos = GetParent()->GetPosition();

            if (auto sgp = dynamic_cast<wxScrolledWindow*>(GetGrandParent()))
            {
                pos = sgp->CalcUnscrolledPosition(pos);
            }

            wxCommandEvent evt(LAX_TABLE_MOVE, GetId());
            evt.SetInt(pos.x);
            evt.SetExtraLong(pos.y);
            evt.SetEventObject(GetParent());
            wxPostEvent(this, evt); // this ensures it will propagate to doc

            event.Skip();
        }

        void TableTab::OnMouseDrag(wxMouseEvent& evt)
        {
            if (m_click.IsFullySpecified()) // is dragging
            {
                wxWindow* gp = GetGrandParent();
                wxWindow* p = GetParent();

                wxPoint newPos =
                gp->ScreenToClient(wxGetMousePosition() - m_click);

                if (auto sgp = dynamic_cast<wxScrolledWindow*>(gp))
                {
                    wxSize minSize = p->GetSize();
                    minSize.x += p->GetPosition().x;
                    minSize.y += p->GetPosition().y;
                    wxSize oldSize = sgp->GetVirtualSize();
                    sgp->SetVirtualSize(std::max(minSize.x, oldSize.x),
                            std::max(minSize.y, oldSize.y));

                    static wxStopWatch t;
                    constexpr long period = 250; // 250 ms = 0.25 s
                    wxPoint mouse = gp->ScreenToClient(wxGetMousePosition());
                    wxSize gpSize = gp->GetSize();

                    if (t.Time() >= period)
                    {
                        int xRate, yRate;
                        int xPos, yPos;

                        sgp->GetScrollPixelsPerUnit(&xRate, &yRate);
                        sgp->GetViewStart(&xPos, &yPos);

                        if (mouse.x >= gpSize.x)
                        {
                            xPos += xRate;
                        }

                        if (mouse.y >= gpSize.y)
                        {
                            yPos += yRate;
                        }

                        sgp->Scroll(xPos, yPos);

                        t.Start();
                    }
                }
                else
                {
                    wxSize max = gp->GetSize() - p->GetSize();
                    newPos.x = std::min(newPos.x, max.x);
                    newPos.y = std::min(newPos.y, max.y);
                }

                newPos.x = std::max(0, newPos.x);
                newPos.y = std::max(0, newPos.y);

                p->Move(newPos);
            }

            evt.Skip();
        }

        void TableTab::OnCaptureLost(wxMouseCaptureLostEvent&)
        {
            m_click = wxDefaultPosition;
        }

        void TableTab::OnPaint(wxPaintEvent& evt)
        {
            wxColour backgrnd;

            if (m_isActive)
            {
                backgrnd.Set(0x6d6d6d);
            }
            else
            {
                backgrnd.Set(0x7f7f7f);
            }

            wxPaintDC dc(this);
            dc.Clear();
            dc.SetPen(*wxBLACK_PEN);
            dc.SetBrush(wxBrush(backgrnd));
            dc.DrawRoundedRectangle(getButtonRect(), -0.1);

            dc.SetBrush(*wxBLACK_BRUSH);
            int beginX = getButtonRect().x + 2;

            for (int y = 5; y <= 13; y += 4)
            {
                dc.DrawRectangle(beginX, y, 14, 2);
            }

            wxRect text(2, 2, beginX - 4, fontPxSize);
            wxString lbl = GetParent()->GetLabel();
            dc.SetTextForeground(*wxWHITE);
            dc.SetFont(getFont());

            if (dc.GetTextExtent(lbl).x > text.width)
            {
                lbl.resize(text.width / fontPxSize - 3);
                lbl += "...";
            }

            dc.DrawLabel(lbl, text, wxALIGN_CENTER);

            evt.Skip();
        }

        void TableTab::popupMenu()
        {
            wxCommandEvent evt(LAX_TABLE_CTRL_MENU, GetId());
            wxPostEvent(this, evt);
        }
    }

    TableBase* TableCtrlData::getTable(SheetView* view, int pg) const
    {
        const int index = m_bits & kIndexBits;
        const PageData::Ptr page = view->GetDocument()->begin()[pg];

        switch (m_bits & kTypeBits)
        {
        case kTable:
            return page->tables.at(index).get();
        case kChart:
            return page->comp.getCharts().at(index).get();
        default:
            wxFAIL;
            return nullptr;
        }
    }

LAX_DISABLE_WARNINGS()
    wxBEGIN_EVENT_TABLE(TableCtrlBase, wxPanel)
        EVT_COMMAND(wxID_ANY, LAX_TABLE_CTRL_MENU, TableCtrlBase::OnPopupMenu)
    wxEND_EVENT_TABLE()
LAX_REENABLE_WARNINGS()

    TableCtrlBase::TableCtrlBase(TableBase* tb, TableCtrlData data,
            wxWindow* parent, wxWindowID id, const wxPoint& pos,
            const wxSize& size):
        wxPanel(parent, id, pos, size), m_tb(tb)
    {
        std::unique_ptr<wxBoxSizer> sizer(new wxBoxSizer(wxVERTICAL));
        sizer->Add(new TableTab(this), wxSizerFlags().Expand());
        SetSizer(sizer.release());
        SetClientData(data);
    }

    void TableCtrlBase::Fit()
    {
        wxPanel::Fit();

        wxSize current = wxDefaultSize;

        for (wxWindow* w: GetParent()->GetChildren())
        {
            wxRect r(w->GetRect());

            // If x or y < 0, increase it to 0. This way, the window will
            // always be big enough to contain the tables, even if it is scrolled
            // and the position is < 0
            r.x = std::max(0, r.x);
            r.y = std::max(0, r.y);

            current.x = std::max(current.x, r.x + r.width);
            current.y = std::max(current.y, r.y + r.height);
        }

        GetParent()->SetVirtualSize(current);
    }

    void TableCtrlBase::SetLabel(const wxString& lbl)
    {
        wxPanel::SetLabel(lbl);
        wxRect r = GetChildren().front()->GetRect();
        Refresh(true, &r);
    }

    void TableCtrlBase::OnPopupMenu(wxCommandEvent&)
    {
        doPopupMenu();
    }

LAX_DISABLE_WARNINGS()
    wxBEGIN_EVENT_TABLE(TableCtrl, TableCtrlBase)
        EVT_GRID_COL_SIZE(TableCtrl::OnResize)
        EVT_GRID_ROW_SIZE(TableCtrl::OnResize)
    wxEND_EVENT_TABLE()
LAX_REENABLE_WARNINGS()

    TableCtrl::TableCtrl(SheetDataTable* tbl, TableCtrlData data,
            wxWindow* parent, wxWindowID id):
        TableCtrlBase(tbl, data, parent, id, tbl->getPosition()), m_table(tbl),
        m_grid()
    {
        // TODO (Max#1#): Add table resize
        wxSizer* const sizer = GetSizer();

        m_grid = new wxGrid(this, wxID_ANY);
        sizer->Add(m_grid, wxSizerFlags(1).Expand());

        auto strRender = new wxGridCellStringRenderer;
        auto strEditor = new wxGridCellTextEditor;
        strRender->IncRef();
        strRender->IncRef();
        strEditor->IncRef();
        strEditor->IncRef();

        m_grid->SetDefaultRenderer(strRender);
        m_grid->SetDefaultEditor(strEditor);
        m_grid->RegisterDataType(StringObject::theType().getName(), strRender,
                strEditor);
        m_grid->RegisterDataType(ErrorObject::theType().getName(), strRender,
                strEditor);
        m_grid->RegisterDataType(NumberObject::theType().getName(),
                new wxGridCellFloatRenderer(-1, -1, wxGRID_FLOAT_FORMAT_COMPACT),
                new FloatEditor);
        m_grid->RegisterDataType(BoolObject::theType().getName(),
                new wxGridCellBoolRenderer, new wxGridCellBoolEditor);
        m_grid->SetTable(m_table);

        SetLabel(tbl->getTitle());
        Fit();

        m_grid->Bind(wxEVT_KEY_DOWN, &TableCtrl::OnCopyPaste, this);
    }

    void TableCtrl::OnResize(wxGridSizeEvent& evt)
    {
        Fit();
        evt.Skip();
    }

    void TableCtrl::doPopupMenu()
    {
        wxMenu menu;
        menu.Append(MainWindow::kRenameTable, _("Rename table..."));
        menu.Append(MainWindow::kAppendRow, _("Append row"));
        menu.Append(MainWindow::kAppendNRows, _("Append rows..."));
        menu.Append(MainWindow::kSaveTable, _("Save to file..."));

        int sel = GetPopupMenuSelectionFromUser(menu);

        if (sel != wxID_NONE)
        {
            wxCommandEvent evt(wxEVT_MENU, sel);
            evt.SetClientData(GetClientData());
            wxPostEvent(this, evt);
        }
    }

    void TableCtrl::OnCopyPaste(wxKeyEvent& evt)
    {
        if (evt.GetModifiers() != wxMOD_CONTROL)
        {
            evt.Skip();
            return;
        }

        int id;

        switch (evt.GetUnicodeKey())
        {
        case 'C':
            id = wxID_COPY;
            break;
        case 'X':
            id = wxID_CUT;
            break;
        case 'V':
            id = wxID_PASTE;
            break;
        default:
            evt.Skip();
            return;
        }

        wxCommandEvent event(wxEVT_MENU, id);
        event.SetEventObject(m_grid);
        wxPostEvent(m_grid, event);
    }

    namespace
    {
        class TableDataObject:
            public wxDataObject
        {
            [[nodiscard]] auto setUTF16Locale()
            {
                const char* old = setlocale(LC_ALL, "en_US.UTF-16");
                return ScopeExit([old]
                {
                    setlocale(LC_ALL, old);
                });
            }
        public:
            TableDataObject() = default;

            explicit TableDataObject(const wxString& buf):
                m_buf(buf) {}

            const wxString& getBuffer() const noexcept
            {
                return m_buf;
            }

            void GetAllFormats(wxDataFormat* formats, Direction dir = Get) const override
            {
                const int maxFormat = GetFormatCount(dir);

                for (int i = 0; i < maxFormat; ++i)
                {
                    formats[i].SetId(m_formats[i]);
                }
            }

            bool GetDataHere(const wxDataFormat&, void* buf) const override
            {
                auto utf8 = m_buf.ToUTF8();
                memcpy(buf, utf8.data(), utf8.length());
                return true;
            }

            size_t GetDataSize(const wxDataFormat&) const override
            {
                return m_buf.ToUTF8().length();
            }

            size_t GetFormatCount(Direction dir = Get) const override
            {
                return dir == Set? kNumFormats:kUTF16Start;
            }

            wxDataFormat GetPreferredFormat(Direction = Get) const override
            {
                return wxDataFormat(m_formats[0]);
            }

            bool SetData(const wxDataFormat& format, size_t len, const void* buf)
            {
                if (!IsSupported(format))
                {
                    return false;
                }

                if (getFormats().at(format.GetId().ToStdString()) >= kUTF16Start)
                {
                    if constexpr (std::is_same_v<char16_t, wchar_t>)
                    {
                        m_buf = wxString(static_cast<const wchar_t*>(buf), len / 2);
                    }
                    else
                    {
                        auto raii = setUTF16Locale();
                        m_buf = std::string(static_cast<const char*>(buf), len);
                    }
                }
                else
                {
                    m_buf = wxString::FromUTF8(static_cast<const char*>(buf), len);
                }

                return true;
            }
        private:
            enum Format
            {
                kPlainUTF8,
                kTSVUTF8,
                kPlain,
                kTSV,
                kPlainUTF16,
                kTSVUTF16,
                kLibreOfficeTSV,
                kNumFormats,

                kUTF16Start = kPlainUTF16,
            };

            constexpr static const char* const m_formats[] = {
                "text/plain;charset=utf-8",
                "text/tab-separated-values;charset=utf-8",
                "text/plain",
                "text/tab-separated-values",
                "text/plain;charset=utf-16",
                "text/tab-separated-values;charset=utf-16",
                "application/x-libreoffice-tsvc",
            };

            static const std::unordered_map<std::string, Format>& getFormats()
            {
                static const std::unordered_map<std::string, Format> fmts {
#define FORMAT(n)   std::pair(m_formats[n], n)
#define ENCODED(n)  FORMAT(n),      \
                    FORMAT(n##UTF8),\
                    FORMAT(n##UTF16)
                    ENCODED(kPlain),
                    ENCODED(kTSV),
                    FORMAT(kLibreOfficeTSV),
#undef FORMAT
#undef ENCODED
                };

                return fmts;
            }

            wxString m_buf;
        };
    }

    ChartCtrl::ChartCtrl(ChartBase* cd, TableCtrlData data, wxWindow* parent,
            wxWindowID id):
        TableCtrlBase(cd, data, parent, id, cd->getPosition()), m_data(cd),
        m_ctrl(m_data->makeChart(this))
    {
        GetSizer()->Add(m_ctrl, wxSizerFlags(1).Expand());
        SetLabel(m_data->getChartData().title);
        Fit();
    }

    void ChartCtrl::doPopupMenu()
    {
        wxMessageBox(_("Popup menu not implemented yet."));
    }

    wxIMPLEMENT_DYNAMIC_CLASS(SheetView, wxView)

LAX_DISABLE_WARNINGS()
    wxBEGIN_EVENT_TABLE(SheetView, wxView)
        EVT_AUINOTEBOOK_PAGE_CHANGED(MainWindow::kSheetNotebook, SheetView::OnChangePage)
        EVT_AUINOTEBOOK_PAGE_CHANGING(MainWindow::kSheetNotebook, SheetView::OnNewPage)
        EVT_AUINOTEBOOK_TAB_RIGHT_UP(MainWindow::kSheetNotebook, SheetView::OnTabPopup)
        EVT_MENU(MainWindow::kRecompileTbl, SheetView::OnRecompileTbl)
        EVT_MENU(MainWindow::kRenameTable, SheetView::OnRenameTable)
        EVT_COMMAND(wxID_ANY, LAX_TABLE_MOVE, SheetView::OnMoveTable)
        EVT_PG_CHANGING(wxID_ANY, SheetView::OnChangeChartProp)
        EVT_GRID_CMD_COL_SIZE(wxID_ANY, SheetView::OnTableResize)
        EVT_GRID_CMD_ROW_SIZE(wxID_ANY, SheetView::OnTableResize)
        EVT_MENU(wxID_COPY, SheetView::OnCopy)
        EVT_MENU(wxID_CUT, SheetView::OnCopy)
        EVT_MENU(wxID_PASTE, SheetView::OnPaste)
        EVT_MENU(MainWindow::kAppendRow, SheetView::OnTableAppend)
        EVT_MENU(MainWindow::kAppendNRows, SheetView::OnTableAppend)
        EVT_GRID_LABEL_RIGHT_CLICK(SheetView::OnLabelMenu)
        EVT_GRID_LABEL_LEFT_CLICK(SheetView::OnEditLabel)
//        EVT_MENU(SheetView::kSwapTabLoc, SheetView::OnSwapTabLoc)
    wxEND_EVENT_TABLE()
LAX_REENABLE_WARNINGS()

    bool SheetView::OnClose(bool deleteWindow)
    {
        if (!wxView::OnClose(deleteWindow))
        {
            return false;
        }

        if (deleteWindow)
        {
            // If the application is being quit, we don't actually care about
            // resetting the window and in fact this causes a crash. So we just
            // ignore it entirely.
            return true;
        }

        m_ignoreChange = true;
        wxGetApp().getWindow().resetToStart();
        m_ignoreChange = false;

        return true;
    }

    void SheetView::OnUpdate(wxView*, wxObject* hint)
    {
        UpdateInfo* info;

        if (!hint || !(info = dynamic_cast<UpdateInfo*>(hint)) ||
            info->getType() == UpdateInfo::kReloadAll)
        {
            updateAll();
        }
        else
        {
            switch (info->getType())
            {
            case UpdateInfo::kUpdateTable:
            {
                updateTable(info->getPageNumber(), info->getTable());
                break;
            }
            case UpdateInfo::kAddPage:
            {
                m_ignoreChange = true; // ignore potential page add requests
                PageData* pg = info->getPage();
                int pgNo = info->getPageNumber();
                wxCHECK2(pg && pgNo >= 0, break);
                addPage(pgNo, pg);
                m_ignoreChange = false;
                break;
            }
            case UpdateInfo::kRemovePage:
            {
                int pg = info->getPageNumber();
                wxCHECK2(pg >= 0, break);

                m_sheets.erase(m_sheets.begin() + pg);
                wxGetApp().getWindow().removePage(pg);

                break;
            }
            case UpdateInfo::kRenamePage:
            {
                int pg = info->getPageNumber();
                PageData* data = info->getPage();
                wxCHECK2(pg >= 0 && data, break);

                wxGetApp().getWindow().renamePage(pg, data->name);
                break;
            }
            default:
                wxFAIL_MSG(wxS("unhandled update event"));
            }
        }
    }

    void SheetView::addPage(int pgNo, PageData* pg)
    {
        TableCodeCtrl::CodeSet::Ptr cs;
        wxPanel* p = wxGetApp().getWindow().addPage(pg->name, *pg, pgNo, &cs);
        p->SetClientData(pg);
        m_sheets.emplace(m_sheets.begin() + pgNo, p, cs);

        m_active = m_sheets.size() - 1;
        m_sheets.back().panel->SetClientData(pg);

        int index = 0;

        // could need to re-add tables
        for (const auto& pair: pg->tables)
        {
            addTable(pgNo, pair.get(), index++);
        }

        index = 0;

        for (const auto& chart: pg->comp.getCharts())
        {
            showChart(chart, index++);
        }
    }

    void SheetView::addTable(int pgNo, SheetDataTable* table, int index)
    {
        wxGetApp().getWindow().switchTo(pgNo);
        const auto& s = m_sheets.at(m_active);
        TableCtrlData data(TableCtrlData::kTable, index);
        new TableCtrl(table, data, s.panel, wxID_ANY);
    }

    void SheetView::showChart(const ChartBase::Ptr& chart, int index)
    {
        SheetData& sd = m_sheets.at(m_active);
        const auto& cd = chart->getChartData();
        TableCtrlData data(TableCtrlData::kChart, index);
        wxASSERT(sd.codeSet->addChart(cd.title, chart));

        ChartSetup* setup = chart->getSetup();
        wxASSERT(setup);
        wxEvtHandler* grid = dynamic_cast<wxEvtHandler*>(setup->getPropGrid());
        wxASSERT(grid);
        grid->SetClientData(data);

        new ChartCtrl(chart.get(), data, sd.panel);
    }

    void SheetView::placeWidget(TableCtrlBase* w)
    {
        if (!w->getTable()->getPosition().IsFullySpecified())
        {
            wxRegion region;

            for (wxWindow* other: w->GetParent()->GetChildren())
            {
                if (other != w && other->IsShown())
                {
                    region.Union(other->GetRect());
                }
            }

            int yPos = 10;
            const wxSize parentSize = w->GetParent()->GetSize();
            const wxSize size = w->GetSize();
            const int addX = size.x + 10;
            const int addY = size.y + 10;

            while (true)
            {
                int xPos = 10;
                int i = 0;

                while (true)
                {
                    const int next = xPos + addX;

                    // if the widget is large (i.e. wider than the parent window),
                    // keep going
                    if (i++ > 0 && next > parentSize.x)
                    {
                        break;
                    }

                    if (region.Contains(xPos, yPos, size.x, size.y) == wxOutRegion)
                    {
                        const wxPoint pt(xPos, yPos);
                        w->SetPosition(pt);
                        w->getTable()->setPosition(pt);
                        return;
                    }

                    xPos = next;
                }

                yPos += addY;
            }
        }
    }

    void SheetView::updateAll()
    {
        wxCHECK_RET(m_doc, wxS("m_doc must be non-null"));
        m_ignoreChange = true;

        m_sheets.clear();
        m_active = -1;
        wxGetApp().getWindow().resetToStart();

        int ctr = 0;

        for (const PageData::Ptr& page: *m_doc)
        {
            addPage(ctr++, page.get());
        }

        if (!m_sheets.empty())
        {
            m_active = m_sheets.size() - 1;
        }

        m_ignoreChange = false;
    }

    wxWindow* SheetView::findWithData(bool(*func)(void*, TableCtrlData),
            void* search, int pg)
    {
        wxCHECK(pg >= 0, nullptr);

        wxWindow* page = m_sheets.at(pg).panel;

        for (wxWindow* child: page->GetChildren())
        {
            TableCtrlData data(child->GetClientData());

            if (func(search, data))
            {
                return child;
            }
        }

        wxFAIL_MSG(wxS("table not found"));
        return nullptr;
    }

    wxWindow* SheetView::findWithData(TableCtrlData data, int pg)
    {
        return findWithData([](void* search, TableCtrlData got)
        {
            return got == search;
        }, data, pg);
    }

    wxWindow* SheetView::findWithData(const TableBase* table, int pg)
    {
        struct Data
        {
            const TableBase* table;
            SheetView* view;
            int page;
        };

        wxCHECK(table, nullptr);
        Data data {table, this, pg};

        return findWithData([](void* search, TableCtrlData got)
        {
            Data* info = static_cast<Data*>(search);
            return got.getTable(info->view, info->page) == info->table;
        }, &data, pg);
    }

    wxPrintout* SheetView::OnCreatePrintout()
    {
        // TODO (Max#1#): Implement printing.
        wxMessageBox(_("Sorry, this feature isn't implemented yet."));
        return nullptr;
    }

    void SheetView::SetDocument(wxDocument* doc)
    {
        m_doc = dynamic_cast<SheetDoc*>(doc);
        wxCHECK2_MSG(m_doc, , wxS("SheetView::SetDocument requires a SheetDoc arg"));
        wxView::SetDocument(doc);
    }

    void SheetView::OnChangePage(wxAuiNotebookEvent& evt)
    {
        if (!m_ignoreChange && !m_sheets.empty())
        {
            m_active = evt.GetSelection();
            m_sheets.at(m_active).codeSet->switchToMe();
        }

        evt.Skip();
    }

    void SheetView::OnNewPage(wxAuiNotebookEvent& evt)
    {
        if (m_ignoreChange)
        {
            evt.Skip();
            return;
        }

        wxAuiNotebook* nb = static_cast<wxAuiNotebook*>(evt.GetEventObject());

        if (evt.GetSelection() != 0 && // is only tab (or is another one)
            evt.GetSelection() == static_cast<int>(nb->GetPageCount()) - 1)
        {
            wxPostEvent(GetDocument(),
                    wxCommandEvent(wxEVT_MENU, MainWindow::kNewPage));
            evt.Veto();
        }
        else
        {
            evt.Skip();
        }
    }

    void SheetView::OnTabPopup(wxAuiNotebookEvent& evt)
    {
        wxMenu popup;

        if (m_sheets.size() > 1)
        {
            popup.Append(MainWindow::kDeletePage, _("Delete page"));
        }

        popup.Append(MainWindow::kRenamePage, _("Rename page"));

        MainWindow& win = wxGetApp().getWindow();
        win.switchTo(evt.GetSelection());
        win.PopupMenu(&popup);
    }

    void SheetView::OnRecompileTbl(wxCommandEvent& evt)
    {
        PageData* pg = static_cast<PageData*>(evt.GetClientData());

//        if (tbl->getDir().empty() &&
//            wxMessageBox(_("This spreadsheet has not yet been given a "
//                           "filename, so imports may not work correctly. "
//                           "Do you want to save first?"),
//                wxMessageBoxCaptionStr, wxYES_NO | wxCENTER) == wxYES)
//        {
//            GetDocument()->SaveAs();
//        }

        wxGetApp().getWindow().clearLog();

        if (pg->history.atLastCompilePoint())
        {
            // don't bother recompiling; no changes have been made
            return;
        }

        if (!doRecompile(*pg))
        {
            return;
        }

        auto undoRecomp = [this, pgNo = m_active, pg]
        {
            const SheetData& sd = m_sheets.at(pgNo);
            sd.codeSet->prevCompile();
            wxGetApp().getWindow().switchTo(pgNo);

            if (!doRecompile(*pg))
            {
                sd.codeSet->nextCompile();
                return false;
            }

            return true;
        };

        auto redoRecomp = [this, pgNo = m_active, pg]
        {
            const SheetData& sd = m_sheets.at(pgNo);
            sd.codeSet->nextCompile();
            wxGetApp().getWindow().switchTo(pgNo);

            if (!doRecompile(*pg))
            {
                sd.codeSet->prevCompile();
                return false;
            }

            return true;
        };

        GetDocument()->GetCommandProcessor()->Store(new SheetCommand(redoRecomp,
                undoRecomp, _("Recompile")));

        GetDocument()->Modify(true);
    }

    bool SheetView::doRecompile(PageData& pg)
    {
        std::vector<wxWindow*> oldCharts;
        std::vector<wxWindow*> oldTables;

        for (const auto& chart: pg.comp.getCharts())
        {
            oldCharts.push_back(findWithData(chart.get(), m_active));
        }

        for (const auto& tbl: pg.tables)
        {
            oldTables.push_back(findWithData(tbl.get(), m_active));
        }

        if (!pg.recompile())
        {
            return false;
        }

        SheetData& sd = m_sheets.at(m_active);

        // we set this *after* compilation has succeeded to prevent funky
        // strangeness with the edit history
        sd.codeSet->setModified(TableCodeCtrl::CodeSet::ModifyType::kCompiled);

        for (wxWindow* chart: oldCharts)
        {
            sd.codeSet->removeChart(chart->GetLabel());
            chart->Destroy();
        }

        for (wxWindow* tbl: oldTables)
        {
            tbl->Destroy();
        }

        int index = 0;

        for (const auto& chart: pg.comp.getCharts())
        {
            showChart(chart, index++);
        }

        index = 0;

        for (const auto& tbl: pg.tables)
        {
            addTable(m_active, tbl.get(), index++);
        }

        wxWindowList& children = sd.panel->GetChildren();

        for (wxWindow* win: children)
        {
            TableCtrlBase* ctrl = dynamic_cast<TableCtrlBase*>(win);

            if (!ctrl)
            {
                continue;
            }

            const auto pos = ctrl->getTable()->getPosition();
            ctrl->Show(pos.IsFullySpecified());
        }

        for (wxWindow* win: children)
        {
            TableCtrlBase* ctrl = dynamic_cast<TableCtrlBase*>(win);

            if (!ctrl)
            {
                continue;
            }

            placeWidget(ctrl);
            ctrl->Show();
        }

        return true;
    }

    void SheetView::updateTable(int pgNo, SheetDataTable* tbl)
    {
        wxWindow* win = findWithData(UpdateInfo(UpdateInfo::kUpdateTable,
                            nullptr, pgNo, tbl));

        if (win)
        {
            win->Fit();
            wxGetApp().getWindow().switchTo(pgNo);
        }
    }

    void SheetView::OnMoveTable(wxCommandEvent& evt)
    {
        TableCtrlBase* ctrl = dynamic_cast<TableCtrlBase*>(evt.GetEventObject());
        wxCHECK_RET(ctrl, wxS("OnTableMove event object should be TableCtrlBase"));

        TableBase* tbl = ctrl->getTable();
        wxCHECK_RET(tbl, wxS("TableCtrlBase must have an associated TableBase"));

        TableCtrlData data(ctrl->GetClientData());

        auto moveTbl = [this, active = m_active, data](const wxPoint& pt)
        {
            TableBase* tbl = data.getTable(this, active);
            wxWindow* win = findWithData(data, active);

            tbl->setPosition(pt);
            wxPoint pos = pt;

            if (auto scr = dynamic_cast<wxScrolledWindow*>(win->GetParent()))
            {
                pos = scr->CalcScrolledPosition(pos);
            }

            win->Move(pos);
            wxGetApp().getWindow().switchTo(active);

            return true;
        };

        wxPoint to(evt.GetInt(), evt.GetExtraLong());

        GetDocument()->GetCommandProcessor()->Submit(new SheetCommand(
                std::bind(moveTbl, to), std::bind(moveTbl, tbl->getPosition()),
                _("Table move")));
        GetDocument()->Modify(true);

        evt.Skip();
    }

    void SheetView::OnRenameTable(wxCommandEvent& evt)
    {
        TableCtrlData data(evt.GetClientData());
        TableBase* const base = data.getTable(this, m_active);
        SheetDataTable* const ptbl = static_cast<SheetDataTable*>(base);
        auto tbl = ptbl->shared_from_this();

        wxString newName = wxGetTextFromUser(_("New name for table:"),
                wxGetTextFromUserPromptStr, ptbl->getTitle());

        if (newName == ptbl->getTitle())
        {
            return;
        }

        auto renameTbl = [this, tbl, data, pgNo = m_active](const wxString& str)
        {
            const SheetData& sd = m_sheets.at(pgNo);

            tbl->setTitle(str);
            wxWindow* toUpdate = nullptr;

            for (wxWindow* child: sd.panel->GetChildren())
            {
                if (child->GetClientData() == data)
                {
                    toUpdate = child;
                }
            }

            if (!toUpdate)
            {
                return false;
            }

            toUpdate->SetLabel(str);
            wxGetApp().getWindow().switchTo(pgNo);

            return true;
        };

        GetDocument()->GetCommandProcessor()->Submit(new SheetCommand(
                std::bind(renameTbl, newName),
                std::bind(renameTbl, ptbl->getTitle())));
    }

    void SheetView::OnChangeChartProp(wxPropertyGridEvent& evt)
    {
        if (m_ignorePropChange > 0)
        {
            --m_ignorePropChange;
            evt.Skip();
            return;
        }

        wxPGProperty* prop = evt.GetProperty();
        wxString name = evt.GetPropertyName();
        wxPropertyGrid* grid = prop->GetGrid();
        TableCtrlData id(grid->GetClientData());
        ChartBase* data = static_cast<ChartBase*>(id.getTable(this, m_active));

        auto changeProp = [this, id, name, pgNo = m_active](const wxVariant& v)
        {
            ++m_ignorePropChange;
            ChartBase* data = static_cast<ChartBase*>(id.getTable(this, pgNo));
            ChartSetup* setup = data->getSetup();
            wxCHECK(setup, false);
            setup->getPropGrid()->ChangePropertyValue(name, v);

            if (name == wxS("title"))
            {
                wxWindow* win = findWithData(data, pgNo);
                wxCHECK(win, false);
                win->SetLabel(v.GetString());
            }

            return true;
        };

        if (name == wxS("title"))
        {
            wxWindow* win = findWithData(data, m_active);
            wxASSERT(win);
            win->SetLabel(evt.GetValue().GetString());
        }

        GetDocument()->GetCommandProcessor()->Store(new SheetCommand(
                std::bind(changeProp, evt.GetValue()),
                std::bind(changeProp, prop->GetValue())));
        GetDocument()->Modify(true);

        evt.Skip();
    }

    void SheetView::OnTableResize(wxGridSizeEvent& evt)
    {
        wxGrid* grid = dynamic_cast<wxGrid*>(evt.GetEventObject());
        SheetDataTable* tbl = dynamic_cast<SheetDataTable*>(grid->GetTable());
        wxEventType type = evt.GetEventType();
        int idx = evt.GetRowOrCol();
        int newSize;

        if (type == wxEVT_GRID_COL_SIZE)
        {
            newSize = grid->GetColSize(idx);
        }
        else if (type == wxEVT_GRID_ROW_SIZE)
        {
            newSize = grid->GetRowSize(idx);
        }
        else
        {
            wxFAIL_MSG(wxS("unknown event type"));
        }

        int old = tbl->setSize(type, idx, newSize);

        if (old < 0)
        {
            return;
        }

        auto setSize = [grid, tbl, type, idx](int size)
        {
            tbl->setSize(type, idx, size);

            if (type == wxEVT_GRID_COL_SIZE)
            {
                grid->SetColSize(idx, size);
            }
            else if (type == wxEVT_GRID_ROW_SIZE)
            {
                grid->SetRowSize(idx, size);
            }

            return true;
        };

        GetDocument()->GetCommandProcessor()->Store(new SheetCommand(
                std::bind(setSize, newSize), std::bind(setSize, old),
                _("Resize table")));
    }

    void SheetView::OnCopy(wxCommandEvent& evt)
    {
        wxGrid& grid = dynamic_cast<wxGrid&>(*evt.GetEventObject());
        wxGridTableBase* tbl = grid.GetTable();

        wxGridCellCoords topLeft, bottomRight;

        if (!getSelection(&grid, topLeft, bottomRight))
        {
            return;
        }

        wxString result;
        const int lastRow = bottomRight.GetRow();
        const int lastCol = bottomRight.GetCol();

        for (int row = topLeft.GetRow(); row <= lastRow; ++row)
        {
            for (int col = topLeft.GetCol(); col <= lastCol; ++col)
            {
                result += tbl->GetValue(row, col) + '\t';
            }

            result.erase(result.end() - 1);
            result += '\n';
        }

        std::unique_ptr<TableDataObject> obj(new TableDataObject(result));

        wxASSERT(wxTheClipboard->Open());
        wxASSERT(wxTheClipboard->SetData(obj.release()));
        wxTheClipboard->Close();
    }

    bool SheetView::getSelection(wxGrid* grid, wxGridCellCoords& topLeft,
            wxGridCellCoords& bottomRight) const
    {
        enum Type
        {
            None,
            Rows,
            Cols,
            Blocks,
            Cells,
        };

        auto rows = grid->GetSelectedRows();
        auto cols = grid->GetSelectedCols();
        auto topLefts = grid->GetSelectionBlockTopLeft();
        auto bottomRights = grid->GetSelectionBlockBottomRight();
        auto cells = grid->GetSelectedCells();
        Type type = None;
        bool problem = false;

#define TEST_EMPTY(var, t)      \
        if (!var.empty())       \
        {                       \
            if (type != None)   \
            {                   \
                problem = true; \
            }                   \
                                \
            type = t;           \
        }

        TEST_EMPTY(rows, Rows)
        TEST_EMPTY(cols, Cols)
        TEST_EMPTY(topLefts, Blocks)
        TEST_EMPTY(cells, Cells)
#undef TEST_EMPTY

        wxString error = _("Cannot copy with multiple selection.");

        if (problem)
        {
            wxLogError(error);
            return false;
        }

        switch (type)
        {
        case None:
            return false;
        case Rows:
        {
            int prev = rows[0] - 1;

            for (int index: rows)
            {
                if (index != prev + 1)
                {
                    wxLogError(error);
                    return false;
                }
            }

            const int lastCol = grid->GetNumberCols() - 1;
            topLeft = wxGridCellCoords(rows[0], 0);
            bottomRight = wxGridCellCoords(rows.back(), lastCol);
            break;
        }
        case Cols:
        {
            int prev = cols[0] - 1;

            for (int index: cols)
            {
                if (index != prev + 1)
                {
                    wxLogError(error);
                    return false;
                }
            }

            const int lastRow = grid->GetNumberRows() - 1;
            topLeft = wxGridCellCoords(0, cols[0]);
            bottomRight = wxGridCellCoords(lastRow, cols.back());
            break;
        }
        case Cells:
        {
            if (cells.size() > 1)
            {
                wxLogError(error);
                return false;
            }

            topLeft = cells[0];
            bottomRight = cells[0];
            break;
        }
        case Blocks:
            if (topLefts.size() > 1)
            {
                wxLogError(error);
                return false;
            }

            topLeft = topLefts[0];
            bottomRight = bottomRights[0];

            break;
        }

        return true;
    }

    void SheetView::OnPaste(wxCommandEvent& evt)
    {
        wxGrid& grid = dynamic_cast<wxGrid&>(*evt.GetEventObject());
        SheetDataTable& tbl = dynamic_cast<SheetDataTable&>(*grid.GetTable());

        TableDataObject obj;

        wxTheClipboard->Open();

        if (!wxTheClipboard->GetData(obj))
        {
            wxTheClipboard->Close();
            return;
        }

        wxTheClipboard->Close();

        auto [firstLine, lastLine] = splitterFor(obj.getBuffer(), '\n');
        int row = grid.GetCursorRow();
        const int firstCol = grid.GetCursorColumn();
        int col = firstCol;

        std::vector<Object::Ptr> old, pasted;
        const Table& impl = tbl.getImpl();

        int cols = -1;
        int rows = 0;

        for (auto line = firstLine; line != lastLine; ++line)
        {
            int count = std::count(line->begin(), line->end(), '\t');
            ++rows;

            if (cols < 0)
            {
                cols = count;
            }
            else if (cols != count)
            {
                wxLogError(_("Uneven number of columns in table from clipboard."));
                return;
            }
        }

        // since we count separators, we need to add one
        ++cols;

        if (firstCol + cols > tbl.GetNumberCols())
        {
            wxLogError(_("Columns go past the end of the table."));
            return;
        }

        const int append = std::max(0, row + rows - tbl.GetNumberRows());

        if (append > 0)
        {
            tbl.InsertRows(tbl.GetNumberRows(), append);
        }

        for (; firstLine != lastLine; ++firstLine)
        {
            if (tbl.GetNumberRows() <= row)
            {
                tbl.AppendRows();
            }

            auto [firstCell, lastCell] = splitterFor(*firstLine, '\t');

            for (; firstCell != lastCell; ++firstCell)
            {
                old.push_back(impl.getObject(col, row));
                tbl.SetValue(row, col, *firstCell);
                pasted.push_back(impl.getObject(col, row));
                ++col;
            }

            col = firstCol;
            ++row;
        }

        int firstRow = grid.GetCursorRow();
        auto paste = [this, &tbl, pgNo = m_active, firstCol, cols, firstRow](
                const std::vector<Object::Ptr>& objs, int append)
        {
            Table& impl = tbl.getImpl();
            int row = firstRow;

            if (append > 0)
            {
                tbl.InsertRows(tbl.GetNumberRows(), append);
            }

            for (auto itr = objs.begin(); itr != objs.end(); ++row)
            {
                for (int i = 0; i < cols; ++i)
                {
                    impl.setObject(firstCol + i, row, *itr);
                    ++itr;
                }
            }

            if (append < 0)
            {
                tbl.DeleteRows(tbl.GetNumberRows() + append, -append);
            }

            return true;
        };

        GetDocument()->GetCommandProcessor()->Store(new SheetCommand(
                std::bind(paste, std::move(pasted), append),
                std::bind(paste, std::move(old), -append), _("Paste")));
    }

    void SheetView::OnTableAppend(wxCommandEvent& evt)
    {
        TableCtrlData data(evt.GetClientData());
        TableBase* const base = data.getTable(this, m_active);
        SheetDataTable* const tbl = static_cast<SheetDataTable*>(base);

        switch (evt.GetId())
        {
        case MainWindow::kAppendRow:
            OnInsertRow(tbl->GetNumberRows(), 1, tbl);
            break;
        case MainWindow::kAppendNRows:
        {
            long n = wxGetNumberFromUser(_("Append rows"),
                    _("Number of rows to append:"), wxMessageBoxCaptionStr);
            OnInsertRow(tbl->GetNumberRows(), n, tbl);
            break;
        }
        default:
            wxFAIL_MSG(wxS("unhandled event identifier"));
        }
    }

    void SheetView::OnLabelMenu(wxGridEvent& evt)
    {
        wxGrid* grid = static_cast<wxGrid*>(evt.GetEventObject());
        SheetDataTable* tbl = dynamic_cast<SheetDataTable*>(grid->GetTable());

        wxCHECK_RET(tbl, wxS("grid must have SheetDataTable data"));

        enum Options
        {
            kEditCol,
            kDelete,
            kDeleteRows,
            kMoveNext,
            kMovePrev,
            kInsertBefore,
            kInsertAfter,
            kInsertNBefore,
            kInsertNAfter,
        };

        wxMenu menu;
        wxArrayInt rows = tbl->GetView()->GetSelectedRows();

        if (evt.GetRow() < 0)
        {
            if (evt.GetCol() < 0)
            {
                // it's a corner label; just skip
                evt.Skip();
                return;
            }

            // it's a column
            menu.Append(kEditCol, _("Edit column"));
        }
        else if (evt.GetRow() < static_cast<int>(tbl->getImpl().getNumRows()))
        {
            // it's a row (not a reduce)
            menu.Append(kInsertBefore, _("Insert row before"));
            menu.Append(kInsertNBefore, _("Insert rows before..."));
            menu.Append(kInsertAfter, _("Insert row after"));
            menu.Append(kInsertNAfter, _("Insert rows after..."));

            if (evt.GetRow() + 1 < tbl->GetNumberRows())
            {
                menu.Append(kMoveNext, _("Move row down"));
            }

            if (evt.GetRow() > 0)
            {
                menu.Append(kMovePrev, _("Move row up"));
            }

            if (tbl->GetNumberRows() > 1)
            {
                menu.Append(kDelete, _("Delete row"));
            }

            if (!rows.empty())
            {
                const wxString& str =
                wxPLURAL("Delete selected row", "Delete selected rows", rows.size());
                menu.Append(kDeleteRows, str);
            }
        }
        else
        {
            // it's a reduce
            menu.Append(kEditCol, _("Rename reduction"));
        }

        int sel = wxGetApp().getWindow().GetPopupMenuSelectionFromUser(menu);

        switch (sel)
        {
        case kEditCol:
        {
            wxPoint pt = evt.GetPosition();
            wxGridEvent event(evt.GetId(), wxEVT_GRID_LABEL_LEFT_CLICK,
                    evt.GetEventObject(), evt.GetRow(), evt.GetCol(), pt.x, pt.y);
            OnEditLabel(event);
            break;
        }
        case kDelete:
            OnDeleteRow(evt.GetRow(), tbl);
            break;
        case kDeleteRows:
            std::sort(rows.begin(), rows.end());
            OnDeleteRows(rows, tbl);
            break;
        case kMoveNext:
            OnMoveRow(evt.GetRow(), 1, tbl);
            break;
        case kMovePrev:
            OnMoveRow(evt.GetRow(), -1, tbl);
            break;
        case kInsertBefore:
            OnInsertRow(evt.GetRow(), 1, tbl);
            break;
        case kInsertAfter:
            OnInsertRow(evt.GetRow() + 1, 1, tbl);
            break;
        case kInsertNAfter:
        case kInsertNBefore:
        {
            long n = wxGetNumberFromUser(_("Row insertion"),
                    _("Number of rows to insert:"), wxMessageBoxCaptionStr);
            OnInsertRow(sel == kInsertNBefore? evt.GetRow():evt.GetRow() + 1, n, tbl);
            break;
        }
        case wxID_NONE:
            break;
        default:
            wxFAIL_MSG(wxS("missed case in row/column menu switch"));
        }
    }

    void SheetView::OnDeleteRows(const wxArrayInt& rows, SheetDataTable* tbl)
    {
        // assume that rows is sorted in ascending order
        std::vector<std::function<bool()> > delRows, insRows;

        for (int r: rows)
        {
            auto [del, ins] = getDeleteRow(r, tbl);
            delRows.push_back(std::move(del));
            insRows.push_back(std::move(ins));
        }

        auto del = [delRows = std::move(delRows)]
        {
            // we iterate backwards (from the bottom row up) because the row
            // indices change as we delete rows
            for (auto itr = delRows.rbegin(); itr != delRows.rend(); ++itr)
            {
                if (!(*itr)())
                {
                    return false;
                }
            }

            return true;
        };

        auto ins = [insRows = std::move(insRows)]
        {
            for (const auto& i: insRows)
            {
                if (!i())
                {
                    return false;
                }
            }

            return true;
        };

        GetCommandProcessor()->Submit(new SheetCommand(del, ins, _("Delete rows")));
    }

    void SheetView::OnDeleteRow(int row, SheetDataTable* tbl)
    {
        auto [del, reinsert] = getDeleteRow(row, tbl);
        GetCommandProcessor()->Submit(new SheetCommand(del, reinsert,
                _("Delete row")));
    }

    std::pair<std::function<bool()>, std::function<bool()> >
    SheetView::getDeleteRow(int row, SheetDataTable* tbl)
    {
        auto pg = m_doc->begin()[m_active];

        // is row
        auto delRow = [this, row, tbl, pg, pgNo = m_active]
        {
            tbl->DeleteRows(row);
            return true;
        };

        std::vector<Object::Ptr> r(tbl->GetNumberCols());
        int ctr = 0;

        std::generate(r.begin(), r.end(), [&ctr, tbl, row]() mutable
        {
            return (*tbl->getImpl().getArray(ctr++))[row];
        });

        auto reinsert = [this, row, tbl, pg, pgNo = m_active, r = std::move(r)]
        {
            tbl->InsertRows(row);

            for (int ctr = 0; ctr < tbl->GetNumberCols(); ++ctr)
            {
                (*tbl->getImpl().getArray(ctr))[row] = r.at(ctr);
            }

            return true;
        };

        return std::pair(delRow, reinsert);
    }

    void SheetView::OnMoveRow(int row, int n, SheetDataTable* tbl)
    {
        auto pg = m_doc->begin()[m_active];

        auto swapRows = [this, row, other = row + n, tbl, pg, pgNo = m_active]
        {
            const Table& t = tbl->getImpl();

            for (int ctr = 0; ctr < tbl->GetNumberCols(); ++ctr)
            {
                using std::swap;
                auto array = t.getArray(ctr);
                swap((*array)[row], (*array)[other]);
            }

            tbl->GetView()->ForceRefresh();

            return true;
        };

        GetCommandProcessor()->Submit(new SheetCommand(swapRows, swapRows,
                _("Move row")));
    }

    void SheetView::OnInsertRow(int r, int n, SheetDataTable* tbl)
    {
        auto pg = m_doc->begin()[m_active];

        auto appendRow = [this, pgNo = m_active, pg, tbl, r, n]
        {
            tbl->InsertRows(r, n);
            return true;
        };

        auto popRow = [this, pgNo = m_active, pg, tbl, r, n]
        {
            tbl->DeleteRows(r, n);
            return true;
        };

        GetCommandProcessor()->Submit(new SheetCommand(appendRow, popRow,
                _("Insert rows")));
    }

    void SheetView::OnEditLabel(wxGridEvent& evt)
    {
        bool isReduce;

        wxGrid* grid = static_cast<wxGrid*>(evt.GetEventObject());
        SheetDataTable* tbl = dynamic_cast<SheetDataTable*>(grid->GetTable());
        wxCHECK_RET(tbl, wxS("grid must have SheetDataTable data"));

        if (evt.GetCol() >= 0 && tbl->getColumnSize() > 0)
        {
            // clicked on column and column's name may be changed
            isReduce = false;
        }
        else if (evt.GetRow() >= tbl->getColumnSize())
        {
             // clicked on reduce or scalar
            isReduce = true;
        }
        else // clicked on row or corner label
        {
            evt.Skip();
            return; // don't care
        }

        wxString str = wxGetTextFromUser(_("What is the new name?"));

        if (str.empty())
        {
            return;
        }

        static auto updateGui = [](SheetDataTable* tbl)
        {
            wxCHECK_RET(tbl->GetView(), "table missing view");
            tbl->GetView()->ForceRefresh();
        };

        if (isReduce)
        {
            auto changeLabel = [row = evt.GetRow(), tbl](const wxString& str)
            {
                tbl->SetRowLabelValue(row, str);
                updateGui(tbl);
                return true;
            };

            GetCommandProcessor()->Submit(new SheetCommand(
                    std::bind(changeLabel, str),
                    std::bind(changeLabel, tbl->GetRowLabelValue(evt.GetRow()))));
        }
        else
        {
            auto changeLabel = [row = evt.GetCol(), tbl](const wxString& str)
            {
                tbl->SetColLabelValue(row, str);
                updateGui(tbl);
                return true;
            };

            GetCommandProcessor()->Submit(new SheetCommand(
                    std::bind(changeLabel, str),
                    std::bind(changeLabel, tbl->GetColLabelValue(evt.GetCol()))));
        }
    }
}
