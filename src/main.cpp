#include "main.hpp"

#include <clocale>
#include <exception>

LAX_DISABLE_WARNINGS()
#include <wx/aboutdlg.h>
#include <wx/cmdline.h>
#include <wx/config.h>
#include <wx/dir.h>
#include <wx/fs_arc.h>
#include <wx/process.h>
#include <wx/stdpaths.h>
LAX_REENABLE_WARNINGS()

#include "compiler/importer/importer.hpp"
#include "sheet_doc.hpp"
#include "sheet_view.hpp"
#include "util/exception_backtrace.h"

namespace lax
{
    class SheetManager:
        public wxDocManager
    {
    public:
        wxDocument* CreateDocument(const wxString& path, long flags = 0);
    };

    // We override this because wxDocManager doesn't do anything if the
    // document fails to open. This can cause crashes when the user tries
    // to modify anything.
    wxDocument* SheetManager::CreateDocument(const wxString& path, long flags)
    {
        if (wxDocument* doc = wxDocManager::CreateDocument(path, flags))
        {
            return doc;
        }
        else
        {
            return CreateNewDocument();
        }
    }

LAX_DISABLE_WARNINGS()
    wxBEGIN_EVENT_TABLE(SheetsGuiApp, wxApp)
        EVT_MENU(wxID_ABOUT, SheetsGuiApp::OnAbout)
        EVT_MENU(wxID_HELP, SheetsGuiApp::OnHelp)
    wxEND_EVENT_TABLE()
LAX_REENABLE_WARNINGS()

    wxString SheetsGuiApp::unknownError = _("unknown error");

    bool SheetsGuiApp::OnInit()
    {
        setlocale(LC_CTYPE, "en_US.UTF-8");

        try
        {
            if (!wxApp::OnInit() ||
                !createConfig())
            {
                return false;
            }

            wxFileSystem::AddHandler(new wxArchiveFSHandler);
            wxImage::AddHandler(new wxPNGHandler);

            SetAppDisplayName(wxS("Lax"));
            SetAppName(wxS("lax"));

            m_mgr = new SheetManager;
            new wxDocTemplate(m_mgr, _("Spreadsheet"), wxS("*.lax"), "", wxS("lax"),
                    wxS("Spreadsheet Doc"), wxS("Spreadsheet View"),
                    wxCLASSINFO(SheetDoc), wxCLASSINFO(SheetView));

            m_mgr->SetMaxDocsOpen(1);
            m_mgr->FileHistoryLoad(*wxConfig::Get());
            m_win = new MainWindow(m_mgr, wxS("Lax 0.1.0"));
            openDocument();
            return m_win->Show();
        }
        catch (std::exception& ex)
        {
            fatalError(ex.what());
        }
        catch (...)
        {
            fatalError(unknownError);
        }

        return false;
    }

    int SheetsGuiApp::OnExit()
    {
        try
        {
            m_mgr->FileHistorySave(*wxConfig::Get());
            wxDELETE(m_mgr);
            return wxApp::OnExit();
        }
        catch (std::exception& ex)
        {
            fatalError(ex.what());
        }
        catch (...)
        {
            fatalError(unknownError);
        }

        return -1;
    }

    bool SheetsGuiApp::OnExceptionInMainLoop()
    {
        std::exception_ptr p = std::current_exception();

        if (!p)
        {
            wxFAIL_MSG(wxS("could not get exception"));
            return false;
        }

        try
        {
            std::rethrow_exception(p);
        }
        catch (std::exception& ex)
        {
            fatalError(ex.what());
        }
        catch (...)
        {
            fatalError(wxS("unknown exception"));
        }

        return true;
    }

    void SheetsGuiApp::OnUnhandledException()
    {
        OnExceptionInMainLoop();
    }

    void SheetsGuiApp::fatalError(const wxString& msg)
    {
        wxLogDebug(wxS("Uncaught exception:\n%s"), msg);
        lax_print_exbt();
        std::terminate();
    }

    void SheetsGuiApp::OnAbout(wxCommandEvent&)
    {
        wxAboutDialogInfo info;
        info.SetName("Lax");
        info.SetCopyright("Copyright (C) Max Miller 2020-2021.");
        info.SetVersion("0.1.0");
        info.AddDeveloper("Max Miller");
        info.SetDescription("This spreadsheet program improves the "
                            "maintainability and readability of spreadsheets "
                            "by adding named ranges and other features");
        wxAboutBox(info, m_win);
    }

    void SheetsGuiApp::OnHelp(wxCommandEvent&)
    {
        wxLogError(_("This feature is not yet implemented"));
    }

    void SheetsGuiApp::OnInitCmdLine(wxCmdLineParser& p)
    {
        wxApp::OnInitCmdLine(p);

        p.AddParam(_("File to open"), wxCMD_LINE_VAL_STRING,
                wxCMD_LINE_PARAM_OPTIONAL);
    }

    bool SheetsGuiApp::OnCmdLineParsed(wxCmdLineParser& p)
    {
        if (p.GetParamCount() > 0)
        {
            m_file = p.GetParam(0);
        }

        return true;
    }

    bool SheetsGuiApp::createConfig()
    {
        wxStandardPaths& stdp = wxStandardPaths::Get();
        wxString confPath = stdp.GetUserDataDir();

        if (!wxDirExists(confPath) && !wxMkdir(confPath))
        {
            wxLogError(_("Couldn't create configuration directory."));
            return false;
        }

        confPath += wxS("/config.txt");
        wxConfigBase::Set(new wxFileConfig("", "", confPath));

        return true;
    }

    void SheetsGuiApp::openDocument()
    {
        if (m_file.empty())
        {
            m_mgr->CreateNewDocument();
        }
        else
        {
            m_mgr->CreateDocument(m_file);
        }
    }
}

wxIMPLEMENT_APP(lax::SheetsGuiApp);
