#ifndef LAX_SRC_TABLE_HPP_
#define LAX_SRC_TABLE_HPP_

#include "disable_warnings.h"

#include <map>

LAX_DISABLE_WARNINGS()
#include <wx/grid.h>
LAX_REENABLE_WARNINGS()

#include <nlohmann/json_fwd.hpp>

#include "compiler/bytecode.hpp"
#include "compiler/object.hpp"
#include "compiler/table_compiler.hpp"

void to_json(nlohmann::json& j, const wxString& str);
void from_json(const nlohmann::json& j, wxString& str);

namespace lax
{
    class TableBase
    {
    public:
        void setPosition(const wxPoint& pt) noexcept
        {
            m_pos = pt;
        }

        wxPoint getPosition() const noexcept
        {
            return m_pos;
        }
    protected:
        TableBase() = default;
        TableBase(const TableBase&) = default;
        TableBase(TableBase&&) = default;

        TableBase(const wxPoint& pt):
            m_pos(pt) {}

        TableBase(int x, int y):
            m_pos(x, y) {}

        ~TableBase() = default;

        TableBase& operator=(const TableBase&) = default;
        TableBase& operator=(TableBase&&) = default;
    private:
        wxPoint m_pos;
    };

    class SheetDataTable;

    class TableObserver:
        public ValueObserver
    {
    public:
        using Ptr = WeakRef<TableObserver>;

        explicit TableObserver(ValueSystem& sys):
            ValueObserver(sys) {}

        void columnRename(const std::shared_ptr<SheetDataTable>& tbl, int col)
        {
            onColRename(tbl, col);
        }

        void scalarRename(const std::shared_ptr<SheetDataTable>& tbl, int scalar)
        {
            onScalarRename(tbl, scalar);
        }
    private:
        virtual void onColRename(const std::shared_ptr<SheetDataTable>& tbl, int col) = 0;
        virtual void onScalarRename(const std::shared_ptr<SheetDataTable>& tbl, int col) = 0;
    };

    class SheetDataTable:
        public wxGridTableBase,
        public std::enable_shared_from_this<SheetDataTable>,
        public TableBase
    {
    public:
        using Ptr = std::shared_ptr<SheetDataTable>;

        struct ColumnInfo
        {
            wxString label;
            std::shared_ptr<ColumnObject> data;
            int width;
            int index;
        };

        struct ScalarInfo
        {
            wxString label;
            ValueNode::Ptr node;
            int height;
            int index;
        };

        struct ReduceInfo
        {
            wxString label;
            int height;
        };

        SheetDataTable(const std::shared_ptr<Table>& tbl, ValueSystem& sys):
            wxGridTableBase(), TableBase(-1, -1), m_impl(tbl),
            m_guiObs(makeValue<GuiObserver>(sys, this)),
            m_scalarAttr(new wxGridCellAttr), m_numRows(0) {}

        ~SheetDataTable();

        const Table& getImpl() const noexcept
        {
            return *m_impl;
        }

        Table& getImpl() noexcept
        {
            return *m_impl;
        }

        Object::Ptr getObject(int row, int col) const;
        void setObject(int row, int col, const Object::Ptr& obj);
        bool isReadOnly(int row, int col) const;

        int GetNumberRows() override;
        int GetNumberCols() override;

        int getNumScalars() const noexcept
        {
            return m_scalars.size();
        }

        bool IsEmptyCell(int row, int col) override
        {
            return getObject(row, col).get();
        }

        wxString GetValue(int row, int col) override;
        void SetValue(int row, int col, const wxString& val) override;
        wxString GetTypeName(int, int col) override;

        wxString GetTypeName(int row, int col) const
        {
            return const_cast<SheetDataTable*>(this)->GetTypeName(row, col);
        }

        bool CanGetValueAs(int row, int col, const wxString& type) override;
        bool CanSetValueAs(int row, int col, const wxString& type) override;

        long GetValueAsLong(int row, int col) override
        {
            return GetValueAsDouble(row, col);
        }

        double GetValueAsDouble(int row, int col) override;
        bool GetValueAsBool(int row, int col) override;
        void* GetValueAsCustom(int row, int col, const wxString& type) override;

        void SetValueAsLong(int row, int col, long val) override
        {
            SetValueAsDouble(row, col, val);
        }

        void SetValueAsDouble(int row, int col, double) override;
        void SetValueAsBool(int row, int col, bool) override;

        wxString GetColLabelValue(int col) override;

        wxString GetColLabelValue(int col) const
        {
            return const_cast<SheetDataTable*>(this)->GetColLabelValue(col);
        }

        void SetColLabelValue(int col, const wxString& str) override;
        wxString GetRowLabelValue(int row) override;

        wxString GetRowLabelValue(int row) const
        {
            return const_cast<SheetDataTable*>(this)->GetRowLabelValue(row);
        }

        void SetRowLabelValue(int row, const wxString& lbl) override;

        wxString getScalarLabel(int scalar) const
        {
            return m_scalars.at(scalar)->label;
        }

        wxString getTitle() const
        {
            return m_title;
        }

        void setTitle(const wxString& t)
        {
            m_title = t;
        }

        void Clear() override
        {
            DeleteRows(0, m_numRows);
        }

        bool InsertRows(std::size_t pos = 0, std::size_t numRows = 1) override;

        bool AppendRows(std::size_t numRows = 1) override
        {
            return InsertRows(m_numRows, numRows);
        }

        bool DeleteRows(std::size_t pos = 0, std::size_t numRows = 1) override;
        void swapRows(std::size_t first, std::size_t second);
        wxGridCellAttr* GetAttr(int row, int col, wxGridCellAttr::wxAttrKind kind) override;
        void SetView(wxGrid* grid) override;

        void update(const std::shared_ptr<Table>& tbl, ValueSystem& sys);
        void recalc();
        void saveTable(const wxString& file, int index);

        enum class FileTypes
        {
            CSVFile,
        };

        static wxString getWildcard()
        {
            return _("CSV files (*.csv)|*.csv");
        }

        void addObserver(const TableObserver::Ptr& o)
        {
            m_observers.push_back(o);
        }

        int setSize(wxEventType evt, int idx, int size);

        int getColWidth(int i) const noexcept
        {
            return m_cols.at(i)->width;
        }

        int getReduceHeight(int i) const noexcept
        {
            return m_reduces.at(i)->height;
        }

        int getScalarHeight(int i) const noexcept
        {
            return m_scalars.at(i)->height;
        }

        void setColWidth(int i, int w) noexcept
        {
            m_cols.at(i)->width = w;
        }

        void setReduceHeight(int i, int h) noexcept
        {
            m_reduces.at(i)->height = h;
        }

        void setScalarHeight(int i, int h) noexcept
        {
            m_scalars.at(i)->height = h;
        }

        int getColumnSize() const noexcept;
    private:
        class GuiObserver:
            public ValueObserver
        {
        public:
            explicit GuiObserver(ValueSystem& sys, SheetDataTable* tbl):
                ValueObserver(sys), m_tbl(tbl) {}
        private:
            void doUpdate(int row) override;
            SheetDataTable* m_tbl;
        };

        wxString& getLabel(int row);
        static bool hasType(const TypeInfo& type);

        wxString m_title;
        std::shared_ptr<Table> m_impl;
        std::vector<ColumnInfo*> m_cols;
        std::vector<ScalarInfo*> m_scalars;
        std::vector<ReduceInfo*> m_reduces;
        std::vector<TableObserver::Ptr> m_observers;
        WeakRef<GuiObserver> m_guiObs;
        wxGridCellAttr* m_scalarAttr;
        int m_numRows;
    };

    void to_json(nlohmann::json& j, const SheetDataTable& tbl);
    void from_json(const nlohmann::json& j, SheetDataTable& tbl);
}

namespace nlohmann
{
    template <>
    struct adl_serializer<wxString>
    {
        static void to_json(json& j, const wxString& str)
        {
            ::to_json(j, str);
        }

        static void from_json(const json& j, wxString& str)
        {
            ::from_json(j, str);
        }
    };
}

#endif // LAX_SRC_TABLE_HPP_
