#include <cerrno>
#include <cmath>
#include <cstdio>

#if __has_include(<cfenv>)
# include <cfenv>
# define HAS_FENV

# ifdef HAS_PRAGMA_FENV
#  pragma STDC FENV_ACCESS ON
# endif
#endif

#define TRY_ERRNO(exp, err) \
{                           \
    errno = 0;              \
    [[maybe_unused]] double d = exp; \
    if (errno != err)       \
        return false;       \
}

bool use_errno()
{
    TRY_ERRNO(asin(2), EDOM)
    TRY_ERRNO(log(0), ERANGE)
    TRY_ERRNO(log(-1), EDOM)
    TRY_ERRNO(exp(1000000000), ERANGE)
    return true;
}

#ifdef HAS_FENV
# define TRY_FENV(exp, err) \
{                           \
    std::feclearexcept(FE_ALL_EXCEPT); \
    [[maybe_unused]] double d = exp;   \
    if (!std::fetestexcept(err))       \
        return false;                  \
}

bool use_fenv()
{
    TRY_FENV(asin(2), FE_INVALID)
    TRY_FENV(log(0), FE_DIVBYZERO)
    TRY_FENV(log(-1), FE_INVALID)
    TRY_FENV(exp(1000000000), FE_OVERFLOW)
    return true;
}
#endif // HAS_FENV

int main()
{
    if (use_errno())
        puts("errno\n");
#ifdef HAS_FENV
    else if (use_fenv())
        puts("fenv\n");
#endif // HAS_FENV
    else
        return -1;

    return 0;
}
