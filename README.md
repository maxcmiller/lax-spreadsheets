Lax
===
Lax (Lax Ain't Excel) is a spreadsheet program that aims to simplify spreadsheet
programming by enforcing the use of named columns. Doing so prevents obscure,
unreadable cell references such as "A15" or "K95" and eliminates the inpenetrable
"absolute references" syntax. Because of its restrictions, Lax is very different
from other spreadsheet programs. For more details, see the documentation, which
is in the [Texinfo](https://www.gnu.org/software/texinfo) format and found in
"doc/lax.texi". See also the HTML and PDF versions of the documentation.

This program is distributed under the terms of the GNU General Public License.
See the COPYING file for details or
[visit the GNU website](https://www.gnu.org/licenses/gpl-3.0.html).

Building Lax
============
To build Lax from source, you will need [Meson](https://mesonbuild.com) version
0.28.0 or later, [wxWidgets](https://wxwidgets.org) version 3.0.0 or later and
a C++17 capable compiler. Lax is known to build with g++, but other compilers
may work as well. Lax may be built using several backends, including ninja,
Visual Studio, or XCode.

To build Lax using [ninja](https://ninja-build.org), navigate to the source root
and run:

    $ meson setup --backend ninja build
    $ ninja -C build

This will create a directory called `build` in the source root and compile the
code. To install, run:

    # ninja -C build install

Contacting the author
=====================
The code is on GitLab [here](https://gitlab.com/maxcmiller/lax-spreadsheets).
Bugs and feature requests may be filed there as an issue.
