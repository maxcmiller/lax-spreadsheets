#ifndef LAX_TEST_AST_SUGAR_HPP_
#define LAX_TEST_AST_SUGAR_HPP_

#include "compiler/formula_ast.hpp"

#ifdef LAX_BINARY_EXP
#undef LAX_BINARY_EXP
#endif // LAX_BINARY_EXP

#ifdef LAX_UNARY_EXP
#undef LAX_UNARY_EXP
#endif // LAX_UNARY_EXP

#define LAX_BINARY_EXP(tok, left, right)                      \
lax::BinaryExpr::make(lax::parser::location_type({}, {}), \
        lax::parser::token::kLAX_##tok, left, right)

#define LAX_UNARY_EXP(tok, exp)                                 \
lax::UnaryExpr::make(lax::parser::location_type({}, {}), \
        lax::parser::token::kLAX_##tok, exp)

#define LAX_CALL_EXP(str,...) \
lax::CallExpr::make(lax::parser::location_type({}, {}), str, \
        std::vector<lax::FormulaExpr::Ptr> {__VA_ARGS__})

#define LAX_FUNC_LITERAL(exp,...) \
makeFuncLiteral("", exp, std::make_tuple(__VA_ARGS__))

#define LAX_FUNC_LIT_RET(ret, exp,...) \
makeFuncLiteral(ret, exp, std::make_tuple(__VA_ARGS__))

#define LAX_ELEM_EXP(lhs, rhs) \
lax::ElemExpr::make(lax::parser::location_type({}, {}), lhs, rhs)

template <typename...Args, std::size_t...I>
inline auto
makeFuncLiteral_helper(const std::string& ret, const lax::FormulaExpr::Ptr& expr,
        const std::tuple<Args...>& args, std::index_sequence<I...>)
{
    return lax::FuncLiteral::make(lax::location({}, {}), ret,
            std::vector<std::shared_ptr<lax::TypeAssign> >
                {lax::TypeAssign::make(lax::location({}, {}),
                    std::get<I>(args).first,
                    lax::TypeData(std::get<I>(args).second))...}, expr);
}

template <typename...Args>
inline auto
makeFuncLiteral(const std::string& ret, const lax::FormulaExpr::Ptr& expr,
        const std::tuple<Args...>& args)
{
    return makeFuncLiteral_helper(ret, expr, args,
            std::index_sequence_for<Args...>());
}

inline lax::FormulaExpr::Ptr operator""_num(long double d)
{
    return lax::NumExpr::make(lax::parser::location_type(), d);
}

inline lax::FormulaExpr::Ptr operator""_var(const char* str, std::size_t)
{
    return lax::VarExpr::make(lax::parser::location_type(),
                    std::pair(str, lax::VarExprType::Inferred));
}

inline lax::FormulaExpr::Ptr operator""_evar(const char* str, std::size_t)
{
    return lax::VarExpr::make(lax::parser::location_type(),
                    std::pair(str, lax::VarExprType::Exact));
}

inline lax::FormulaExpr::Ptr operator""_tsvar(const char* str, std::size_t)
{
    return lax::VarExpr::make(lax::parser::location_type(),
                    std::pair(str, lax::VarExprType::ToScalar));
}

inline lax::FormulaExpr::Ptr operator""_str(const char* str, std::size_t)
{
    return lax::StringExpr::make(lax::parser::location_type(), str);
}

//inline lax::FormulaExpr::Ptr operator""_array(const char* str, std::size_t)
//{
//    return lax::ArrayVarExpr::make(lax::location(), str);
//}

#endif // LAX_TEST_AST_SUGAR_HPP_
