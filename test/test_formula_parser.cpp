#include "compiler/formula_driver.hpp"

#ifdef __GNUG__
# pragma GCC diagnostic push
# pragma GCC diagnostic ignored "-Wcast-function-type"
# pragma GCC diagnostic ignored "-Wunused-parameter"
#endif // __GNUG__

#define BOOST_TEST_MODULE Lax Test
#include <boost/test/included/unit_test.hpp>
#include <boost/test/data/test_case.hpp>

#ifdef __GNUG__
# pragma GCC diagnostic pop
#endif // __GNUG__

#include "ast_sugar.hpp"

namespace bdata = boost::unit_test::data;

namespace lax
{
    bool operator==(const lax::location& lhs, const lax::location& rhs)
    {
        return lhs.begin.line == rhs.begin.line &&
               lhs.begin.column == rhs.begin.column &&
               lhs.end.column == rhs.end.column &&
               lhs.end.line == rhs.end.line;
    }

    bool operator==(const TypeData& lhs, const TypeData& rhs)
    {
        if (lhs.getName() != rhs.getName())
        {
            return false;
        }

        return std::equal(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
    }

    std::ostream& operator<<(std::ostream& lhs, const TypeData& rhs)
    {
        return lhs << rhs.stringize();
    }
}

BOOST_AUTO_TEST_SUITE(testFormulaParser)

struct VarExprWrapper
{
    const lax::VarExpr& inner;
};

bool operator==(const VarExprWrapper& lhs, const VarExprWrapper& rhs)
{
    return lhs.inner.getValue() == rhs.inner.getValue();
}

std::ostream& operator<<(std::ostream& ostr, const VarExprWrapper& rhs)
{
    return ostr << rhs.inner.stringize();
}

class ComparisonVisitor:
    public lax::FormulaVisitor
{
public:
    void compare(lax::FormulaExpr::Ptr lhs, lax::FormulaExpr::Ptr rhs)
    {
        m_left = lhs;
        rhs->accept(*this);
    }

#define LAX_TEST_TYPE(type)                                     \
        auto lhs = std::dynamic_pointer_cast<lax::type>(m_left);\
                                                                \
        if (!lhs)                                               \
        {                                                       \
            BOOST_ERROR("left is not of type " #type ", got "   \
                     << m_left->getType() << " instead");       \
            return;                                             \
        }

    void visit(const lax::BinaryExpr& rhs) override
    {
        LAX_TEST_TYPE(BinaryExpr)

        BOOST_TEST(lhs->getOp() == rhs.getOp());

        ComparisonVisitor other;
        other.compare(lhs->getLeft(), rhs.getLeft());
        other.compare(lhs->getRight(), rhs.getRight());
    }

    void visit(const lax::CallExpr& rhs) override
    {
        LAX_TEST_TYPE(CallExpr)

        ComparisonVisitor other;

        auto litr = lhs->begin(), ritr = rhs.begin();

        for (; litr != lhs->end() && ritr != rhs.end(); ++litr, ++ritr)
        {
            other.compare(*litr, *ritr);
        }

        BOOST_TEST((litr == lhs->end() && ritr == rhs.end()),
            "left argument list not of same size as right");

        other.compare(lhs->getExpr(), rhs.getExpr());
    }

    void visit(const lax::NumExpr& rhs) override
    {
        LAX_TEST_TYPE(NumExpr)
        BOOST_TEST(lhs->getValue() == rhs.getValue());
    }

    void visit(const lax::StringExpr& rhs) override
    {
        LAX_TEST_TYPE(StringExpr)
        BOOST_TEST(lhs->getValue() == rhs.getValue());
    }

    void visit(const lax::UnaryExpr& rhs) override
    {
        LAX_TEST_TYPE(UnaryExpr)
        BOOST_TEST(lhs->getOp() == rhs.getOp());

        ComparisonVisitor other;
        other.compare(lhs->getArgument(), rhs.getArgument());
    }

    void visit(const lax::VarExpr& rhs) override
    {
        LAX_TEST_TYPE(VarExpr)
        VarExprWrapper left{*lhs}, right{rhs};
        BOOST_TEST(left == right);
    }

//    void visit(const lax::ArrayVarExpr& rhs) override
//    {
//        LAX_TEST_TYPE(ArrayVarExpr)
//        BOOST_TEST(lhs->getValue() == rhs.getValue());
//    }

    void visit(const lax::FuncLiteral& rhs) override
    {
        LAX_TEST_TYPE(FuncLiteral)

        auto litr = lhs->begin();
        auto ritr = rhs.begin();

        for (; litr != lhs->end() && ritr != rhs.end(); ++litr, ++ritr)
        {
            BOOST_TEST((*litr)->getName() == (*ritr)->getName());
            BOOST_TEST((*litr)->getValue() == (*ritr)->getValue());
        }

        BOOST_TEST((litr == lhs->end() && ritr == rhs.end()));
        BOOST_TEST(lhs->getReturn() == rhs.getReturn());

        ComparisonVisitor other;
        other.compare(lhs->getExpr(), rhs.getExpr());
    }

    void visit(const lax::ElemExpr& rhs) override
    {
        LAX_TEST_TYPE(ElemExpr)

        ComparisonVisitor other;
        other.compare(lhs->getLeft(), rhs.getLeft());
        other.compare(lhs->getRight(), rhs.getRight());
    }
private:
    lax::FormulaExpr::Ptr m_left;
};

BOOST_DATA_TEST_CASE(testParser,
bdata::make({
    "1 + 2",
    "5 + 6 * 7",
    "(5 + 6) * 7",
    "pi*SQUARE(r)",
    "6.022e23*moles",
    "POW(5, 7)",
    "1 + 2 = 1.5 * 2",
    R"("Hello world\"\blah" + test)",
    "6.67e-11*M*m/r/r != g",
    "PHYS.G < CHEM.AVOGADRO",
    "TEST.ASSERT(-1 < 2.3)",
    "_1 = __one_ONE() - _(1.0)",
    "SCI.PHYS.G",
    "MATH.TRIG.SIN(MATH.PI*x)",
    "fn() {12 + 3}",
    "fn(n as Number) {n * n}",
    "fn(x as Number, y as Number) {x / y}",
    "ACCUMULATE(data, fn(current as Number, x as Number) {IF(x > current, x, current)})",
    "fn(n as Number) {fn(m as Number) {m / n}}",
    "fn(n as Number)->Number {IF(n > 0, n * self(n - 1), 1)}",
    "fn(m, n) {m / n}(12, 3)",
    "multBy(2)(3)",
    "1/moles[0]",
    "not (a and not b or c)",
    "|-2^3^4|",
    "fn(n as Number, f as String(Number)) {f(n)}",
    "array",
    "$array",
    "@array",
}) ^
bdata::make(std::vector<lax::FormulaExpr::Ptr> {
    LAX_BINARY_EXP(PLUS, 1.0_num, 2.0_num),
    LAX_BINARY_EXP(PLUS, 5.0_num, LAX_BINARY_EXP(STAR, 6.0_num, 7.0_num)),
    LAX_BINARY_EXP(STAR, LAX_BINARY_EXP(PLUS, 5.0_num, 6.0_num), 7.0_num),
    LAX_BINARY_EXP(STAR, "pi"_var, LAX_CALL_EXP("SQUARE", "r"_var)),
    LAX_BINARY_EXP(STAR, 6.022e23_num, "moles"_var),
    LAX_CALL_EXP("POW", 5.0_num, 7.0_num),
    LAX_BINARY_EXP(EQUAL, LAX_BINARY_EXP(PLUS, 1.0_num, 2.0_num),
        LAX_BINARY_EXP(STAR, 1.5_num, 2.0_num)),
    LAX_BINARY_EXP(PLUS, "Hello world\"\\blah"_str, "test"_var),
    LAX_BINARY_EXP(NOT_EQUAL, LAX_BINARY_EXP(SLASH,
        LAX_BINARY_EXP(SLASH, LAX_BINARY_EXP(STAR,
            LAX_BINARY_EXP(STAR, 6.67e-11_num, "M"_var), "m"_var), "r"_var), "r"_var),
        "g"_var),
    LAX_BINARY_EXP(LESS, "PHYS.G"_var, "CHEM.AVOGADRO"_var),
    LAX_CALL_EXP("TEST.ASSERT",
        LAX_BINARY_EXP(LESS, LAX_UNARY_EXP(MINUS, 1.0_num), 2.3_num)),
    LAX_BINARY_EXP(EQUAL, "_1"_var, LAX_BINARY_EXP(MINUS,
        LAX_CALL_EXP("__one_ONE",), LAX_CALL_EXP("_", 1.0_num))),
    "SCI.PHYS.G"_var,
    LAX_CALL_EXP("MATH.TRIG.SIN", LAX_BINARY_EXP(STAR, "MATH.PI"_var, "x"_var)),
    LAX_FUNC_LITERAL(LAX_BINARY_EXP(PLUS, 12.0_num, 3.0_num), ),
    LAX_FUNC_LITERAL(LAX_BINARY_EXP(STAR, "n"_var, "n"_var),
            std::pair("n", "Number")),
    LAX_FUNC_LITERAL(LAX_BINARY_EXP(SLASH, "x"_var, "y"_var),
            std::pair("x", "Number"),
            std::pair("y", "Number")),
    LAX_CALL_EXP("ACCUMULATE", "data"_var, LAX_FUNC_LITERAL(LAX_CALL_EXP("IF",
            LAX_BINARY_EXP(GREATER, "x"_var, "current"_var), "x"_var, "current"_var),
            std::pair("current", "Number"),
            std::pair("x", "Number"))),
    LAX_FUNC_LITERAL(LAX_FUNC_LITERAL(LAX_BINARY_EXP(SLASH, "m"_var, "n"_var),
            std::pair("m", "Number")),
            std::pair("n", "Number")),
    LAX_FUNC_LIT_RET("Number", LAX_CALL_EXP("IF",
            LAX_BINARY_EXP(GREATER, "n"_var, 0.0_num),
            LAX_BINARY_EXP(STAR, "n"_var, LAX_CALL_EXP("self",
                    LAX_BINARY_EXP(MINUS, "n"_var, 1.0_num))), 1.0_num),
            std::pair("n", "Number")),
    LAX_CALL_EXP(LAX_FUNC_LITERAL(LAX_BINARY_EXP(SLASH, "m"_var, "n"_var),
            std::pair("m", "Number"),
            std::pair("n", "Number")), 12.0_num, 3.0_num),
    LAX_CALL_EXP(LAX_CALL_EXP("multBy", 2.0_num), 3.0_num),
    LAX_BINARY_EXP(SLASH, 1.0_num, LAX_ELEM_EXP("moles"_var, 0.0_num)),
    LAX_UNARY_EXP(NOT, LAX_BINARY_EXP(OR, LAX_BINARY_EXP(AND, "a"_var,
            LAX_UNARY_EXP(NOT, "b"_var)), "c"_var)),
    LAX_UNARY_EXP(VERTBAR, LAX_UNARY_EXP(MINUS, LAX_BINARY_EXP(EXP, 2.0_num,
            LAX_BINARY_EXP(EXP, 3.0_num, 4.0_num)))),
    LAX_FUNC_LITERAL(LAX_CALL_EXP("f", "n"_var), std::pair("n", "Number"),
            std::pair("f", lax::TypeData("Function",
                    std::vector {lax::TypeData("String"), lax::TypeData("Number")}))),
    "array"_var,
    "array"_evar,
    "array"_tsvar,
}), str, expect)
{
    std::istringstream istr(str);
    lax::FormulaDriver drv(istr, "<test input>");
    ComparisonVisitor v;
    auto got = drv.parseExpr();

    for (auto itr = drv.beginErrors(); itr != drv.endErrors(); ++itr)
    {
        std::cerr << itr->loc << ':' << itr->msg << '\n';
    }

    if (drv.beginErrors() != drv.endErrors())
    {
        BOOST_FAIL("parsing failed");
    }

    v.compare(expect, got);
}

class AssignCompare:
    public lax::AssignVisitor
{
public:
    void compare(const lax::FormulaAssign::Ptr& lhs, const lax::FormulaAssign::Ptr& rhs)
    {
        BOOST_TEST(lhs->getName() == rhs->getName());
        m_left = lhs;
        rhs->accept(*this);
    }

    void visit(const lax::ExprAssign& rhs) override
    {
        LAX_TEST_TYPE(ExprAssign)

        testFormulaParser::ComparisonVisitor cv;
        cv.compare(lhs->getValue().first, rhs.getValue().first);
        BOOST_TEST(lhs->getValue().second == rhs.getValue().second);
    }

    void visit(const lax::ImportAssign& rhs) override
    {
        LAX_TEST_TYPE(ImportAssign)
        BOOST_TEST(lhs->getValue() == rhs.getValue());
    }

    void visit(const lax::TypeAssign& rhs) override
    {
        LAX_TEST_TYPE(TypeAssign)
        BOOST_TEST(lhs->getValue() == rhs.getValue());
    }

    void visit(const lax::ScalarTypeAssign& rhs) override
    {
        LAX_TEST_TYPE(ScalarTypeAssign)
        BOOST_TEST(lhs->getValue() == rhs.getValue());
    }

    void visit(const lax::ReduceAssign& rhs) override
    {
        LAX_TEST_TYPE(ReduceAssign)

        auto litr = lhs->begin(), ritr = rhs.begin();

        for (; litr != lhs->end() && ritr != rhs.end(); ++litr, ++ritr)
        {
            BOOST_TEST(*litr == *ritr);
        }

        BOOST_TEST((litr == lhs->end() && ritr == rhs.end()));
    }
private:
    lax::FormulaAssign::Ptr m_left;
};

class TopAssignCompare:
    public lax::TopVisitor
{
public:
    void compare(const lax::TopAssign::Ptr& lhs, const lax::TopAssign::Ptr& rhs)
    {
        BOOST_TEST(lhs->getName() == rhs->getName());
        BOOST_TEST(std::equal(lhs->beginExtends(), lhs->endExtends(),
                rhs->beginExtends(), rhs->endExtends()));
        m_left = lhs;
        rhs->accept(*this);
    }

    void visit(const lax::TableAssign& rhs) override
    {
        LAX_TEST_TYPE(TableAssign);

        AssignCompare comp;
        auto litr = lhs->begin(), ritr = rhs.begin();

        for (; litr != lhs->end() && ritr != rhs.end(); ++litr, ++ritr)
        {
            comp.compare(*litr, *ritr);
        }

        BOOST_TEST((litr == lhs->end() && ritr == rhs.end()),
                "assignment sets not of same size");
    }

    void visit(const lax::ChartAssign& rhs) override
    {
        LAX_TEST_TYPE(ChartAssign);

        const auto& ldata = lhs->getData();
        const auto& rdata = rhs.getData();

        BOOST_TEST(ldata.index == rdata.index);
        BOOST_TEST(ldata.type == rdata.type);
        BOOST_TEST(std::equal(ldata.series.begin(), ldata.series.end(),
                rdata.series.begin(), rdata.series.end()));
    }

    void visit(const lax::TopImportAssign& rhs) override
    {
        LAX_TEST_TYPE(TopImportAssign);
        BOOST_TEST(lhs->getValue() == rhs.getValue());
    }
private:
    lax::TopAssign::Ptr m_left;
};

BOOST_AUTO_TEST_CASE(testAssigns)
{
    std::istringstream test(R"(table t {
    import "math.lax"
    import "phys.lax" as PHYS
    x as Number
    y: x + 1
    z: x/y
    AVERAGE reduces (x, y, z)
    scalar pi: 3.14
    scalar size as Number
}

chart c extends t {
    type: line
    "index": "x"
    "series": [
        "y",
        "z"
    ]
})");

    lax::JSONArray chartSeries;
    chartSeries.append("y");
    chartSeries.append("z");

    lax::JSONObject chartObj;
    chartObj.append(lax::JSONField("type", "line"), lax::location());
    chartObj.append(lax::JSONField("index", "x"), lax::location());
    chartObj.append(lax::JSONField("series", std::move(chartSeries)), lax::location());

    lax::TableAssign::FormulaContainer tableObj
    {
        lax::ImportAssign::make(lax::location(), "", "math.lax"),
        lax::ImportAssign::make(lax::location(), "PHYS", "phys.lax"),
        lax::TypeAssign::make(lax::location(), "x", "Number"),
        lax::ExprAssign::make(lax::location(), "y",
                std::pair(LAX_BINARY_EXP(PLUS, "x"_var, 1.0_num), false)),
        lax::ExprAssign::make(lax::location(), "z",
                std::pair(LAX_BINARY_EXP(SLASH, "x"_var, "y"_var), false)),
        lax::ReduceAssign::make(lax::location(), "AVERAGE", {"x", "y", "z"}),
        lax::ExprAssign::make(lax::location(), "pi", std::pair(3.14_num, true)),
        lax::ScalarTypeAssign::make(lax::location(), "size", "Number"),
    };

    lax::TopAssign::Ptr expect[]
    {
        lax::TableAssign::make(lax::location(), "t", {}, std::move(tableObj)),
        lax::ChartAssign::make(lax::location(), "c", {"t"}, chartObj),
    };

    lax::FormulaDriver drv(test, "<test input>");
    TopAssignCompare ac;
    std::vector<lax::TopAssign::Ptr> vec;

    if (!drv.parseAssigns(vec))
    {
        for (auto itr = drv.beginErrors(); itr != drv.endErrors(); ++itr)
        {
            std::cerr << itr->loc << ':' << itr->msg << '\n';
        }

        BOOST_FAIL("parsing failed");
    }

    auto eitr = std::begin(expect);
    auto gitr = vec.begin();

    for (; eitr != std::end(expect) && gitr != vec.end(); ++eitr, ++gitr)
    {
        BOOST_TEST((*eitr)->getName() == (*gitr)->getName());
        ac.compare(*eitr, *gitr);
    }

    BOOST_TEST((eitr == std::end(expect) && gitr == vec.end()));
}

std::vector<lax::location> getLexLocations(const std::string& str)
{
    std::vector<lax::location> vec;
    std::istringstream istr(str);
    lax::FormulaDriver lexer(istr, "<test input>");
    lexer.lex(); // ignore start token

    while (true)
    {
        lax::parser::symbol_type tok = lexer.lex();
        vec.push_back(tok.location);

        if (tok.type_get() == lax::parser::symbol_kind::S_YYEOF)
        {
            break;
        }
    }

    return vec;
}

lax::location getLineLoc(int col1, int col2, int line1 = 1, int line2 = 1)
{
    static auto name = std::make_shared<std::string>("<test input>");
    return lax::location(lax::position(name, line1, col1),
            lax::position(name, line2, col2));
}

#define LAX_LOCATION(...) getLineLoc(__VA_ARGS__)

BOOST_DATA_TEST_CASE(testLocations,
getLexLocations(R"("testing...\"Hello", BLAH + FOO*BAR(1.3e-2) != (x < u) "blah
 blah #this is a comment"
other: stuff # then another comment
done )") ^
bdata::make({
    LAX_LOCATION(1, 20),        // "testing...\"Hello"
    LAX_LOCATION(20, 21),       // ,
//    LAX_LOCATION(22, 23),       // $
    LAX_LOCATION(22, 26),       // BLAH
    LAX_LOCATION(27, 28),       // +
//    LAX_LOCATION(30, 31),       // $
    LAX_LOCATION(29, 32),       // FOO
    LAX_LOCATION(32, 33),       // *
    LAX_LOCATION(33, 36),       // BAR
    LAX_LOCATION(36, 37),       // (
    LAX_LOCATION(37, 43),       // 1.3e-2
    LAX_LOCATION(43, 44),       // )
    LAX_LOCATION(45, 47),       // !=
    LAX_LOCATION(48, 49),       // (
    LAX_LOCATION(49, 50),       // x
    LAX_LOCATION(51, 52),       // <
    LAX_LOCATION(53, 54),       // u
    LAX_LOCATION(54, 55),       // )
    LAX_LOCATION(56, 26, 1, 2), // "blah\n blah #this is a comment"
    LAX_LOCATION(1, 6, 3, 3),   // other
    LAX_LOCATION(6, 7, 3, 3),   // :
//    LAX_LOCATION(8, 9, 3, 3),   // $
    LAX_LOCATION(8, 13, 3, 3),  // stuff
    LAX_LOCATION(1, 5, 4, 4),   // done
    LAX_LOCATION(6, 6, 4, 4)    // EOF
}), got, expected)
{
    BOOST_TEST(got == expected);
}

BOOST_AUTO_TEST_SUITE_END()
