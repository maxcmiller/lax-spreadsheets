#include "../src/math/matrix.hpp"

#include <boost/test/unit_test.hpp>

namespace lax
{
namespace math
{
    std::ostream& operator<<(std::ostream& ostr, GaussResult rhs)
    {
        switch (rhs)
        {
        case GaussResult::Success:
            ostr << "Success";
            break;
        case GaussResult::Dependent:
            ostr << "Dependent";
            break;
        }

        return ostr;
    }
}
}

BOOST_AUTO_TEST_CASE(testMatrix)
{
    using namespace lax::math;

    Matrix m(makeRow(1.0, 2.0, 3.0),
             makeRow(4.0, 5.0, 6.0));

    Matrix other(makeRow(1.0, 0.0, -1.0),
                 makeRow(0.0, 1.0, 2.0));

    BOOST_TEST(m == m);
    BOOST_TEST(m != other);
    BOOST_TEST(gaussElimination(m, true) == GaussResult::Success);
    BOOST_TEST(m == other);
}

BOOST_AUTO_TEST_CASE(testDependentMatrix)
{
    using namespace lax::math;

    Matrix m(makeRow(1.0, 2.0),
             makeRow(2.0, 4.0));

    Matrix other(makeRow(1.0, 2.0),
                 makeRow(0.0, 0.0));

    BOOST_TEST(gaussElimination(m, false) == GaussResult::Dependent);
    BOOST_TEST(m == other);
}
