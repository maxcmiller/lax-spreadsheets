#include "../src/util/splitter.hpp"

#include <boost/test/unit_test.hpp>

using StringSplitter = lax::Splitter<std::string::const_iterator, std::string>;
BOOST_TEST_DONT_PRINT_LOG_VALUE(StringSplitter)

BOOST_AUTO_TEST_SUITE(testSplitter)

BOOST_AUTO_TEST_CASE(testCompare)
{
    std::string test = "1";
    auto [first, last] = lax::splitterFor(test, ',');

    BOOST_TEST(first == first);
    BOOST_TEST(last == last);
    BOOST_TEST(first != last);

    ++first;

    BOOST_TEST(first == last);

    BOOST_CHECK_THROW(++first, std::logic_error);
    BOOST_CHECK_THROW(++last, std::logic_error);
}

BOOST_AUTO_TEST_CASE(testString)
{
    std::string str = "1,2,hello world,***,92";
    auto [first, last] = lax::splitterFor(str, ',');

    std::vector<std::string> expect = {
        "1",
        "2",
        "hello world",
        "***",
        "92",
    };

    std::vector<std::string> got(first, last);

    BOOST_TEST(expect == got);
}

BOOST_AUTO_TEST_CASE(testVector)
{
    std::vector<int> vec {0, -1, 2, 3, -1, 15, -1, 12};
    auto [first, last] = lax::splitterFor(vec, -1);

    std::vector<std::vector<int> > expect = {{0}, {2, 3}, {15}, {12}};
    std::vector<std::vector<int> > got(first, last);

    BOOST_TEST(expect == got);
}

BOOST_AUTO_TEST_SUITE_END()
