#include "compiler/bytecode.hpp"
#include "compiler/environment.hpp"

#include <functional>
#include <list>
#include <sstream>
#include <unordered_set>
#include <vector>

#include <boost/test/data/test_case.hpp>
#include <boost/test/unit_test.hpp>

#include "ast_sugar.hpp"

namespace bdata = boost::unit_test::data;

namespace lax
{
    std::ostream& operator<<(std::ostream& ostr, const TypeInfo& info)
    {
        return ostr << info.getName();
    }

    std::ostream& operator<<(std::ostream& ostr, OpCode op)
    {
        return ostr << stringize(op);
    }
}

namespace testBytecode
{
    constexpr double onePi = 3.14159265358979323846;

    std::shared_ptr<lax::NumberObject> operator""_nobj(long double d)
    {
        return std::make_shared<lax::NumberObject>(d);
    }

    std::shared_ptr<lax::StringObject> operator""_sobj(const char* str, std::size_t)
    {
        return std::make_shared<lax::StringObject>(str);
    }

    lax::Instruction operator""_ninstr(long double d)
    {
        return lax::Instruction(lax::OpCode::kOpConst,
                std::make_shared<lax::NumberObject>(d));
    }

    lax::Instruction operator""_sinstr(const char* str, std::size_t)
    {
        return lax::Instruction(lax::OpCode::kOpConst,
                std::make_shared<lax::StringObject>(str));
    }

    template <typename T, typename = void>
    struct GetObjectType
    {
        static_assert(!std::is_same_v<T, T>, "no object for given type");
    };

    template <>
    struct GetObjectType<bool, void>
    {
        using type = lax::BoolObject;
    };

    template <typename T>
    struct GetObjectType<
        T,
        std::enable_if_t<std::is_arithmetic_v<T> && !std::is_same_v<T, bool> >
    >
    {
        using type = lax::NumberObject;
    };

    template <typename T>
    struct GetObjectType<
        T, std::enable_if_t<std::is_constructible_v<std::string, T> >
    >
    {
        using type = lax::StringObject;
    };

    template <typename...Args>
    std::shared_ptr<lax::ColumnObject> makeArray(lax::Object::Ptr first, Args&&...args)
    {
        auto obj = std::make_shared<lax::ColumnObject>(first->getType());

        obj->push(first);
        (obj->push(std::forward<Args>(args)), ...);

        return obj;
    }

    template <typename Func, typename...Args>
    class FuncType:
        public lax::FunctionObject
    {
    public:
        static_assert(std::is_invocable_v<Func, Args...>,
                    "function must be invocable on given arguments");

        using Result = decltype(std::declval<Func>()(std::declval<Args>()...));
        using ObjType = typename GetObjectType<Result>::type;
        using IndexSeq = std::index_sequence_for<Args...>;

        FuncType(Func&& f):
            FunctionObject(getMyType(IndexSeq())), m_func(std::move(f)) {}

        std::string stringize() const override
        {
            return "<function>";
        }

        lax::Object::Ptr execute(const std::vector<Ptr>& args) const override
        {
            if (args.size() != sizeof...(Args))
            {
                return std::make_shared<lax::ErrorObject>(
                        "wrong number of arguments", lax::ErrorObject::kBadArgs);
            }

            try
            {
                return applyFunc(args, IndexSeq());
            }
            catch (FuncError& e)
            {
                return e.getError();
            }
        }
    private:
        template <std::size_t...I>
        Ptr applyFunc(const std::vector<Ptr>& args, std::index_sequence<I...>) const
        {
            return std::make_shared<ObjType>(m_func(extractObj<I>(args[I])...));
        }

        template <std::size_t I>
        auto extractObj(Ptr p) const
        {
            using Type = std::tuple_element_t<I, std::tuple<Args...> >;
            auto got =
            std::dynamic_pointer_cast<typename GetObjectType<Type>::type>(p);

            if (!got)
            {
                throw FuncError(std::make_shared<lax::ErrorObject>(
                        "mismatched argument type", lax::ErrorObject::kBadArgs));
            }
            else
            {
                return got->getValue();
            }
        }

        template <std::size_t...I>
        std::shared_ptr<lax::FunctionTypeInfo>
        getMyType(std::index_sequence<I...>) const
        {
            std::vector<lax::TypeInfo> args {getArgType<I>()...};
            return lax::FixedFuncTypeInfo::create(ObjType::theType(),
                    std::move(args));
        }

        template <std::size_t I>
        lax::TypeInfo getArgType() const
        {
            using Type = std::tuple_element_t<I, std::tuple<Args...> >;
            return GetObjectType<Type>::type::theType();
        }

        mutable Func m_func;
    };

    template <typename...Args, typename Func>
    auto makeFunc(Func&& f)
    {
        return std::make_shared<FuncType<Func, Args...> >(std::forward<Func>(f));
    }

    double accumulate(const std::shared_ptr<lax::ArrayObject>& arr,
            const std::shared_ptr<lax::FunctionObject>& f)
    {
        auto current = std::make_shared<lax::NumberObject>(0.0);

        for (const auto& obj: *arr)
        {
            current = std::dynamic_pointer_cast<lax::NumberObject>(
                    f->execute(std::vector<lax::Object::Ptr> {current, obj}));
        }

        return current->getValue();
    }

    struct EnvFixture
    {
        EnvFixture():
            env(std::make_shared<lax::Environment>(sys))
        {
            addObject("pi", pi, std::make_shared<lax::NumberObject>(onePi));
            addObject("SQUARE", square,
                    makeFunc<double>([](double d) {return d*d;}));
            addObject("r", r, makeArray(1.0_nobj, 2.0_nobj, 3.0_nobj));
            addObject("moles", moles, makeArray(0.5_nobj, 1.5_nobj, 3.0_nobj));
            addObject("POW", pow,
                    makeFunc<double, double>([](double base, double power)
                    {
                        return std::pow(base, power);
                    }));
            addObject("test", test, "test"_sobj);
            addObject("M", bigM, 1.0_nobj);
            addObject("m", m, 1.0_nobj);
            addObject("g", g, 9.81_nobj);
            addObject("_1", _1, 1.0_nobj);
            addObject("__one_ONE", _one_ONE, makeFunc<>([] {return 1.0;}));
            addObject("_", underscore,
                    makeFunc<double, double>([](double one, double two)
                    {
                        return one*two;
                    }));
            addObject("a", a, makeArray(0.0_nobj, 0.5_nobj, 0.25_nobj));
            addObject("isGood", isGood, lax::BoolObject::getTrue());
            addObject("bad", bad, lax::BoolObject::getFalse());
            addObject("failed", failed, lax::BoolObject::getFalse());
        }

        void addObject(const char* name, lax::ValueNode::Ptr& node,
                const lax::Object::Ptr& value)
        {
            env->addObject(name, value->getType(), &node);
            node->setObject(value);
        }

        lax::ValueSystem sys;
        lax::Environment::Ptr env;
        lax::ValueNode::Ptr pi,  square, r, moles, pow, test, bigM, m, g, _1;
        lax::ValueNode::Ptr _one_ONE, underscore, a, isGood, bad, failed;
    };

    static EnvFixture envFixture;
}

using DepType = std::pair<lax::ValueObserver::Ptr, bool>;
using DepArrayType = std::unordered_set<DepType, lax::hash<DepType> >;
BOOST_TEST_DONT_PRINT_LOG_VALUE(DepArrayType)
BOOST_TEST_DONT_PRINT_LOG_VALUE(lax::BytecodeFunction)

BOOST_AUTO_TEST_SUITE(testBytecode)

void compareObjects(lax::Object::Ptr lhs, lax::Object::Ptr rhs)
{
    if (lhs->getType() != rhs->getType())
    {
        BOOST_ERROR("types not the same [" << lhs->getType().getName() << " != "
                << rhs->getType().getName() << ']');
        return;
    }

    auto compare = [](lax::Object::Ptr lhs, lax::Object::Ptr rhs, auto type)
    {
        using DataType = typename decltype(type)::element_type;
        auto left = std::dynamic_pointer_cast<DataType>(lhs);
        auto right = std::dynamic_pointer_cast<DataType>(rhs);

        BOOST_TEST(left->getValue() == right->getValue());
    };

    using FuncData = std::function<void(lax::Object::Ptr lhs, lax::Object::Ptr rhs)>;

    namespace pl = std::placeholders;

    static std::map<lax::TypeInfo, FuncData> typeMap = {
#define LAX_ADD_OBJ_TYPE(type)               \
        std::make_pair(lax::type::theType(), \
                std::bind(compare, pl::_1, pl::_2, std::shared_ptr<lax::type>()))

        LAX_ADD_OBJ_TYPE(NumberObject),
        LAX_ADD_OBJ_TYPE(StringObject),
        LAX_ADD_OBJ_TYPE(BoolObject)
    };

    if (auto left = std::dynamic_pointer_cast<lax::ArrayObject>(lhs))
    {
        auto right = std::dynamic_pointer_cast<lax::ArrayObject>(rhs);

        for (auto litr = left->begin(), ritr = right->begin();
            litr != left->end() && ritr != right->end(); ++litr, ++ritr)
        {
            compareObjects(*litr, *ritr);
        }

        BOOST_TEST(left->size() == right->size());
    }
    else if (std::dynamic_pointer_cast<lax::FunctionObject>(lhs))
    {
        BOOST_TEST(lhs == rhs);
    }
    else if (auto itr = typeMap.find(lhs->getType()); itr != typeMap.end())
    {
        itr->second(lhs, rhs);
    }
    else
    {
        BOOST_ERROR("Could not find type " << lhs->getType() << " in type map.");
    }
}

template <typename...Args>
lax::BytecodeFunction
genInstrSet(const lax::TypeInfo& res, int iterNum, Args&&...args)
{
    return lax::BytecodeFunction({lax::Instruction(std::move(args))...}, res, iterNum);
}

template <typename...Args>
DepArrayType makeDepArray(Args&&...args)
{
    return DepArrayType {std::forward<Args>(args)...};
}

lax::Instruction makeJump(lax::OpCode op, int n)
{
    lax::Instruction i(op);
    i.setJump(n);
    return i;
}

template <typename...Args>
lax::Instruction makeClosure(const lax::TypeInfo& res, Args&&...args)
{
    auto f = std::make_shared<lax::BytecodeArgFunction>(
            {std::forward<Args>(args)...}, res, -1);
    return lax::Instruction(lax::OpCode::kOpLoadClosure, f);
}

#define LAX_ARRAY_OP(name) LAX_LOAD_OP(name), lax::Instruction(lax::OpCode::kOpArray)
#define LAX_LOAD_OP(name) lax::Instruction(lax::OpCode::kOpLoad, envFixture.name)
#define LAX_JUMP_OP(type, offset) makeJump(lax::OpCode::kOp##type, offset)
#define LAX_CALL_FUNC(nargs) lax::Instruction(nargs)

#define LAX_VAR_DEP(name) std::make_pair(envFixture.name, false)
#define LAX_ARRAY_DEP(name) std::make_pair(envFixture.name, true)

namespace
{
    std::vector<lax::BytecodeFunction> funcs
    {
        genInstrSet(lax::NumberObject::theType(), -1, 1.0_ninstr, 2.0_ninstr,
                lax::OpCode::kOpAdd),
        genInstrSet(lax::NumberObject::theType(), -1, 5.0_ninstr, 6.0_ninstr,
                7.0_ninstr, lax::OpCode::kOpMult, lax::OpCode::kOpAdd),
        genInstrSet(lax::NumberObject::theType(), -1, 5.0_ninstr, 6.0_ninstr,
                lax::OpCode::kOpAdd, 7.0_ninstr, lax::OpCode::kOpMult),
        genInstrSet(lax::NumberObject::theType(), 3, LAX_LOAD_OP(pi),
                LAX_LOAD_OP(square), LAX_ARRAY_OP(r), LAX_CALL_FUNC(1),
                lax::OpCode::kOpMult),
        genInstrSet(lax::NumberObject::theType(), 3, 6.022e23_ninstr,
                LAX_ARRAY_OP(moles), lax::OpCode::kOpMult),
        genInstrSet(lax::NumberObject::theType(), -1, LAX_LOAD_OP(pow), 5.0_ninstr,
                7.0_ninstr, LAX_CALL_FUNC(2)),
        genInstrSet(lax::BoolObject::theType(), -1, 1.0_ninstr, 2.0_ninstr,
                lax::OpCode::kOpAdd, 1.5_ninstr, 2.0_ninstr, lax::OpCode::kOpMult,
                lax::OpCode::kOpEqualNum),
        genInstrSet(lax::StringObject::theType(), -1, "Hello world\"\\blah"_sinstr,
                LAX_LOAD_OP(test), lax::OpCode::kOpConcat),
        genInstrSet(lax::BoolObject::theType(), 3, 6.67e-11_ninstr,
                LAX_LOAD_OP(bigM), lax::OpCode::kOpMult, LAX_LOAD_OP(m),
                lax::OpCode::kOpMult, LAX_ARRAY_OP(r), lax::OpCode::kOpDiv,
                LAX_ARRAY_OP(r), lax::OpCode::kOpDiv, LAX_LOAD_OP(g),
                lax::OpCode::kOpNotEqualNum),
        genInstrSet(lax::BoolObject::theType(), -1, LAX_LOAD_OP(_1),
                LAX_LOAD_OP(_one_ONE), LAX_CALL_FUNC(0), LAX_LOAD_OP(underscore),
                1.0_ninstr, 2.0_ninstr, lax::OpCode::kOpMult, 0.5_ninstr,
                LAX_LOAD_OP(pi), lax::OpCode::kOpMult, LAX_CALL_FUNC(2),
                lax::OpCode::kOpSubtract, lax::OpCode::kOpEqualNum),
        genInstrSet(lax::BoolObject::theType(), -1, "Hello "_sinstr, "world"_sinstr,
                lax::OpCode::kOpConcat, "Hello world"_sinstr,
                lax::OpCode::kOpEqualString),
        genInstrSet(lax::NumberObject::theType(), 3, LAX_ARRAY_OP(a), 0.0_ninstr,
                lax::OpCode::kOpEqualNum, LAX_JUMP_OP(If, 2), 0.0_ninstr,
                LAX_JUMP_OP(Goto, 4), 1.0_ninstr, LAX_ARRAY_OP(a),
                lax::OpCode::kOpDiv),
        genInstrSet(lax::NumberObject::theType(), -1, 1.0_ninstr, LAX_LOAD_OP(moles),
                0.0_ninstr, lax::OpCode::kOpArrayElem, lax::OpCode::kOpDiv),
        genInstrSet(lax::NumberObject::theType(), -1, LAX_LOAD_OP(a),
                lax::OpCode::kOpArraySize),
        genInstrSet(lax::BoolObject::theType(), -1, LAX_LOAD_OP(isGood),
                LAX_LOAD_OP(bad), LAX_LOAD_OP(failed), lax::OpCode::kOpOrBool,
                lax::OpCode::kOpNotBool, lax::OpCode::kOpAndBool,
                lax::OpCode::kOpNotBool),
        genInstrSet(lax::NumberObject::theType(), -1, 2.0_ninstr, 3.0_ninstr,
                4.0_ninstr, lax::OpCode::kOpPower, lax::OpCode::kOpPower,
                lax::OpCode::kOpNegate, lax::OpCode::kOpAbs),
    };
}

BOOST_DATA_TEST_CASE(testCompile,
bdata::make(std::vector<lax::FormulaExpr::Ptr> {
    LAX_BINARY_EXP(PLUS, 1.0_num, 2.0_num),
    LAX_BINARY_EXP(PLUS, 5.0_num, LAX_BINARY_EXP(STAR, 6.0_num, 7.0_num)),
    LAX_BINARY_EXP(STAR, LAX_BINARY_EXP(PLUS, 5.0_num, 6.0_num), 7.0_num),
    LAX_BINARY_EXP(STAR, "pi"_var, LAX_CALL_EXP("SQUARE", "r"_var)),
    LAX_BINARY_EXP(STAR, 6.022e23_num, "moles"_var),
    LAX_CALL_EXP("POW", 5.0_num, 7.0_num),
    LAX_BINARY_EXP(EQUAL, LAX_BINARY_EXP(PLUS, 1.0_num, 2.0_num),
        LAX_BINARY_EXP(STAR, 1.5_num, 2.0_num)),
    LAX_BINARY_EXP(PLUS, "Hello world\"\\blah"_str, "test"_var),
    LAX_BINARY_EXP(NOT_EQUAL, LAX_BINARY_EXP(SLASH,
        LAX_BINARY_EXP(SLASH, LAX_BINARY_EXP(STAR,
            LAX_BINARY_EXP(STAR, 6.67e-11_num, "M"_var), "m"_var), "r"_var), "r"_var),
        "g"_var),
    LAX_BINARY_EXP(EQUAL, "_1"_var, LAX_BINARY_EXP(MINUS,
        LAX_CALL_EXP("__one_ONE",),
        LAX_CALL_EXP("_", LAX_BINARY_EXP(STAR, 1.0_num, 2.0_num),
            LAX_BINARY_EXP(STAR, 0.5_num, "pi"_var)))),
    LAX_BINARY_EXP(EQUAL, LAX_BINARY_EXP(PLUS, "Hello "_str, "world"_str),
        "Hello world"_str),
    LAX_CALL_EXP("IF", LAX_BINARY_EXP(EQUAL, "a"_var, 0.0_num), 0.0_num,
            LAX_BINARY_EXP(SLASH, 1.0_num, "a"_var)),
    LAX_BINARY_EXP(SLASH, 1.0_num, LAX_ELEM_EXP("moles"_var, 0.0_num)),
    LAX_CALL_EXP("SIZE", "a"_var),
    LAX_UNARY_EXP(NOT, LAX_BINARY_EXP(AND, "isGood"_var, LAX_UNARY_EXP(NOT,
            LAX_BINARY_EXP(OR, "bad"_var, "failed"_var)))),
    LAX_UNARY_EXP(VERTBAR, LAX_UNARY_EXP(MINUS, LAX_BINARY_EXP(EXP, 2.0_num,
            LAX_BINARY_EXP(EXP, 3.0_num, 4.0_num)))),
}) ^ funcs ^
bdata::make({
    makeDepArray(),
    makeDepArray(),
    makeDepArray(),
    makeDepArray(LAX_VAR_DEP(pi), LAX_ARRAY_DEP(r), LAX_VAR_DEP(square)),
    makeDepArray(LAX_ARRAY_DEP(moles)),
    makeDepArray(LAX_VAR_DEP(pow)),
    makeDepArray(),
    makeDepArray(LAX_VAR_DEP(test)),
    makeDepArray(LAX_VAR_DEP(bigM), LAX_VAR_DEP(m), LAX_ARRAY_DEP(r), LAX_VAR_DEP(g)),
    makeDepArray(LAX_VAR_DEP(_1), LAX_VAR_DEP(pi), LAX_VAR_DEP(_one_ONE),
            LAX_VAR_DEP(underscore)),
    makeDepArray(),
    makeDepArray(LAX_ARRAY_DEP(a)),
    makeDepArray(LAX_VAR_DEP(moles)),
    makeDepArray(LAX_VAR_DEP(a)),
    makeDepArray(LAX_VAR_DEP(isGood), LAX_VAR_DEP(bad), LAX_VAR_DEP(failed)),
    makeDepArray(),
}), input, bytes, deps)
{
    auto node = lax::compile(input, envFixture.env, lax::ExprType::kArray);

    BOOST_TEST(std::equal(deps.begin(), deps.end(), node->beginDeps(), node->endDeps()));
    BOOST_TEST(node->getType() == bytes.getResult(std::vector<lax::TypeInfo>()));

    auto got = std::dynamic_pointer_cast<lax::BytecodeFunction>(node->getFunc());
    BOOST_REQUIRE(got);

    auto eitr = bytes.begin(), gitr = got->begin();

    for (; eitr != bytes.end() && gitr != got->end(); ++eitr, ++gitr)
    {
        BOOST_TEST(eitr->getOp() == gitr->getOp());

        if (eitr->getOp() != gitr->getOp())
        {
            continue;
        }

        switch (eitr->getOp())
        {
        case lax::OpCode::kOpLoad:
        case lax::OpCode::kOpArray:
            BOOST_TEST(eitr->getObject() == gitr->getObject());
            break;
        case lax::OpCode::kOpIf:
        case lax::OpCode::kOpGoto:
            BOOST_TEST(eitr->getNumArgs() == gitr->getNumArgs());
            break;
        case lax::OpCode::kOpCall:
            BOOST_TEST(eitr->getNumArgs() == gitr->getNumArgs());
            break;
        case lax::OpCode::kOpConst:
            compareObjects(eitr->getObject(), gitr->getObject());
            break;
        default:
            break;
        }
    }

    BOOST_TEST(bytes.size() == got->size());
}

BOOST_DATA_TEST_CASE(testExecute,
funcs ^
std::vector<lax::Object::Ptr>({
    3.0_nobj,
    47.0_nobj,
    77.0_nobj,
    makeArray(std::make_shared<lax::NumberObject>(onePi),
        std::make_shared<lax::NumberObject>(4*onePi),
        std::make_shared<lax::NumberObject>(9*onePi)),
    makeArray(3.011e23_nobj, 9.033e23_nobj, 1.8066e24_nobj),
    78125.0_nobj,
    lax::BoolObject::getTrue(),
    "Hello world\"\\blahtest"_sobj,
    makeArray(lax::BoolObject::getTrue(), lax::BoolObject::getTrue(),
        lax::BoolObject::getTrue()),
    lax::BoolObject::getFalse(),
    lax::BoolObject::getTrue(),
    makeArray(0.0_nobj, 2.0_nobj, 4.0_nobj),
    2.0_nobj,
    3.0_nobj,
    lax::BoolObject::getTrue(),
    std::make_shared<lax::NumberObject>(std::pow(2.0, std::pow(3.0, 4.0))),
}), func, expect)
{
    lax::Object::Ptr got;
    using ArrayArgType = std::vector<lax::Object::Ptr>;

    if (func.getIterNum() == -1)
    {
        got = func.execute(ArrayArgType(1, std::make_shared<lax::NumberObject>(0)));
    }
    else
    {
        auto arr = std::make_shared<lax::ArrayObject>(
                func.getResult(std::vector<lax::TypeInfo>()));

        for (int ctr = 0; ctr < func.getIterNum(); ++ctr)
        {
            arr->push(func.execute(ArrayArgType(1,
                    std::make_shared<lax::NumberObject>(ctr))));
        }

        got = arr;
    }

    compareObjects(got, expect);
}

// There was a bug where explicit return types caused a crash,
// so just make sure that it doesn't.
BOOST_AUTO_TEST_CASE(testExplicitReturn)
{
    auto exp = LAX_FUNC_LIT_RET("Number", 1.0_num,);
    auto node = lax::compile(exp, envFixture.env, lax::ExprType::kScalar);
    auto funcType = lax::FixedFuncTypeInfo::create(lax::NumberObject::theType(), {});
    BOOST_TEST(node->getType() == lax::TypeInfo(funcType));
}

#define LAX_FIXED_FUNC(ret,...) \
lax::FixedFuncTypeInfo::create(ret, std::vector{__VA_ARGS__})

BOOST_AUTO_TEST_CASE(testFixedFuncType)
{
    using namespace lax;

    auto numOfNum = LAX_FIXED_FUNC(NumberObject::theType(), NumberObject::theType());
    auto numOfNum2 = LAX_FIXED_FUNC(NumberObject::theType(), NumberObject::theType());
    auto numOfStr = LAX_FIXED_FUNC(NumberObject::theType(), StringObject::theType());
    auto numOfStrNum = LAX_FIXED_FUNC(NumberObject::theType(), StringObject::theType(),
            NumberObject::theType());

    BOOST_TEST(numOfNum == numOfNum2);
    BOOST_TEST(numOfNum != numOfStr);
    BOOST_TEST(numOfStrNum != numOfStr);
}

class PairTypeInfo:
    public lax::TypeInfoImpl
{
public:
    PairTypeInfo(const lax::TypeInfo& lhs, const lax::TypeInfo& rhs):
        TypeInfoImpl("Pair<" + lhs.getName() + ',' + rhs.getName() + '>') {}

    static auto get(const lax::TypeInfo& lhs, const lax::TypeInfo& rhs)
    {
        static std::map<std::pair<lax::TypeInfo, lax::TypeInfo>, lax::TypeInfo> map;

        auto pair = std::make_pair(lhs, rhs);

        if (auto itr = map.find(pair); itr != map.end())
        {
            return itr->second;
        }

        auto got = map.emplace(std::move(pair),
                lax::TypeInfo(std::make_shared<PairTypeInfo>(lhs, rhs)));
        return got.first->second;
    }
};

class PairTemplTypeInfo:
    public lax::TemplateTypeInfo
{
public:
    lax::TypeInfo getType(const std::vector<lax::TypeInfo>& args) override
    {
        if (args.size() != 2)
        {
            return lax::TypeInfo::nullType();
        }

        return PairTypeInfo::get(args[0], args[1]);
    }
};

lax::RegisterTemplate tmpl("Pair", std::make_unique<PairTemplTypeInfo>());

#define ARRAY_TYPE(member) lax::ArrayObject::theType(member)
#define SIMPLE_TYPE(name) lax::name ## Object::theType()
#define FUNC_TYPE(ret,...) \
lax::TypeInfo( \
    lax::FixedFuncTypeInfo::create( \
        ret, \
        std::vector<lax::TypeInfo>{__VA_ARGS__}))

BOOST_DATA_TEST_CASE(testTypeRegistryParse,
bdata::make({
    "Array<Number>",
    " Array < Number > ",
    "Array<Array<String> >",
    "Pair<String,Number>",
    "Array < Pair  <Number , String  > >",
    "Pair<Array<Pair<Array<Number>, Number>>, String>",
    "Number()",
    "Number(Number)",
    "String(String, Number)",
    "String(Array<Number(Number, String)>, String(String))",
    "Array<Number(Pair<String, Number>)>",
    "Array<Number>(Number)",
    "Number(Number)(Number)",
    "Array",
    "Array<>",
    "Array<",
    "<Array<Number>>",
    "Array<Number Number>",
    "*Array",
    ",Array",
    "Array<Number>,",
    "Number(",
    "Number(Number",
    "Number(Number String)",
    "Array<Number(String>",
    "Pair<Number(String, String>",
    "Array<Number><Number>",
    "Number(Number)<Number>"
}) ^
bdata::make({
    ARRAY_TYPE(SIMPLE_TYPE(Number)),
    ARRAY_TYPE(SIMPLE_TYPE(Number)),
    ARRAY_TYPE(ARRAY_TYPE(SIMPLE_TYPE(String))),
    PairTypeInfo::get(SIMPLE_TYPE(String), SIMPLE_TYPE(Number)),
    ARRAY_TYPE(PairTypeInfo::get(SIMPLE_TYPE(Number), SIMPLE_TYPE(String))),
    PairTypeInfo::get(ARRAY_TYPE(PairTypeInfo::get(ARRAY_TYPE(SIMPLE_TYPE(Number)),
                                                   SIMPLE_TYPE(Number))),
                      SIMPLE_TYPE(String)),
    FUNC_TYPE(SIMPLE_TYPE(Number),),
    FUNC_TYPE(SIMPLE_TYPE(Number), SIMPLE_TYPE(Number)),
    FUNC_TYPE(SIMPLE_TYPE(String), SIMPLE_TYPE(String), SIMPLE_TYPE(Number)),
    FUNC_TYPE(SIMPLE_TYPE(String), ARRAY_TYPE(
            FUNC_TYPE(SIMPLE_TYPE(Number), SIMPLE_TYPE(Number), SIMPLE_TYPE(String))),
            FUNC_TYPE(SIMPLE_TYPE(String), SIMPLE_TYPE(String))),
    ARRAY_TYPE(FUNC_TYPE(SIMPLE_TYPE(Number),
            PairTypeInfo::get(SIMPLE_TYPE(String), SIMPLE_TYPE(Number)))),
    FUNC_TYPE(ARRAY_TYPE(SIMPLE_TYPE(Number)), SIMPLE_TYPE(Number)),
    FUNC_TYPE(FUNC_TYPE(SIMPLE_TYPE(Number), SIMPLE_TYPE(Number)), SIMPLE_TYPE(Number)),
    lax::TypeInfo::nullType(),
    lax::TypeInfo::nullType(),
    lax::TypeInfo::nullType(),
    lax::TypeInfo::nullType(),
    lax::TypeInfo::nullType(),
    lax::TypeInfo::nullType(),
    lax::TypeInfo::nullType(),
    lax::TypeInfo::nullType(),
    lax::TypeInfo::nullType(),
    lax::TypeInfo::nullType(),
    lax::TypeInfo::nullType(),
    lax::TypeInfo::nullType(),
    lax::TypeInfo::nullType(),
    lax::TypeInfo::nullType(),
    lax::TypeInfo::nullType(),
}), str, type)
{
    BOOST_TEST(lax::TypeRegistry::parseType(str) == type);
}

BOOST_AUTO_TEST_SUITE_END()
