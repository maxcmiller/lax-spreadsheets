#include "../src/math/exponent_regression.hpp"
#include "../src/math/logarithm_regression.hpp"
#include "../src/math/polynomial_regression.hpp"

#include <ctime>
#include <fstream>
#include <random>

#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_SUITE(testRegression)

struct RandomFixture
{
    static double generate()
    {
        return distr(mt);
    }

    static std::uniform_real_distribution<> distr;
    static std::mt19937 mt;
};

std::uniform_real_distribution<> RandomFixture::distr(-1.0, 1.0);
std::mt19937 RandomFixture::mt(83226642);

double calcPolynom(const std::vector<double>& coeff, double x)
{
    double exp = 1.0;
    double res = 0.0;

    for (double c: coeff)
    {
        res += exp * c;
        exp *= x;
    }

    return res;
}

struct ApproxEqual
{
    constexpr bool operator()(double lhs, double rhs) noexcept
    {
        const double absDiff = std::abs(lhs - rhs);
        return absDiff <= std::max(std::abs(lhs), std::abs(rhs)) * eps;
    }

    const double eps;
};

struct VecPrinter
{
    const std::vector<double>& vec;
};

std::ostream& operator<<(std::ostream& ostr, const VecPrinter& vp)
{
    for (int i = vp.vec.size() - 1; i >= 0; --i)
    {
        ostr << vp.vec[i];

        if (i > 0)
        {
            ostr << ", ";
        }
    }

    return ostr;
}

BOOST_AUTO_TEST_CASE(testPolynomial)
{
    using namespace lax::math;
    constexpr double eps = 1e-7;
    ApproxEqual approxEqual{eps};

    for (int i = 1; i <= 6; ++i)
    {
        PolynomialRegression pr(i);

        for (int j = 1; j <= 100; ++j)
        {
            BOOST_TEST_CHECKPOINT("polynomial of degree " << i << " (try " << j
                               << " of 10)");

            std::vector<double> coeff(i + 1);
            std::generate(coeff.begin(), coeff.end(), RandomFixture::generate);

            std::vector<std::pair<double, double> > set(1000);

            std::generate(set.begin(), set.end(), [&coeff]
            {
                const double x = RandomFixture::generate();
                return std::pair(x, calcPolynom(coeff, x));
            });

            BOOST_TEST_REQUIRE(pr.reset(set));
            BOOST_TEST(std::equal(coeff.begin(), coeff.end(),
                            pr.begin(), pr.end(), approxEqual),
                    "coefficient comparison failed: [" << VecPrinter{coeff}
                 << " != " << pr << ']');
        }
    }
}

BOOST_AUTO_TEST_CASE(testPolynomialFail)
{
    using namespace lax::math;

    for (int i = 1; i <= 6; ++i)
    {
        PolynomialRegression pr(i);

        for (int j = 1; j <= i; ++j)
        {
            std::vector<std::pair<double, double> > set(j);
            std::fill(set.begin(), set.end(), std::pair(0.0, 0.0));
            BOOST_TEST(!pr.reset(set));
        }
    }
}

BOOST_AUTO_TEST_CASE(testExponent)
{
    using namespace lax::math;
    ExponentRegression reg;

    constexpr double eps = 1e-1;
    ApproxEqual approxEqual{eps};

    for (int i = 0; i < 100; ++i)
    {
        BOOST_TEST_CHECKPOINT("exponent try " << i << " of 100");
        const double a = RandomFixture::generate();
        const double b = RandomFixture::generate();

        std::vector<std::pair<double, double> > vec(1000);
        std::generate(vec.begin(), vec.end(), [a, b]
        {
            const double x = RandomFixture::generate();
            const double noise = RandomFixture::generate() / 10000;
            const double y = a * std::exp(b * x);
            return std::pair(x, (1 + noise) * y);
        });

        BOOST_TEST_REQUIRE(reg.reset(vec));
        BOOST_TEST(approxEqual(reg.getA(), a),
                "comparison of a failed: [" << reg.getA() << " != " << a << ']');
        BOOST_TEST(approxEqual(reg.getB(), b),
                "comparison of b failed: [" << reg.getB() << " != " << b << ']');
    }
}

BOOST_AUTO_TEST_CASE(testExponentFail)
{
    using namespace lax::math;
    ExponentRegression reg;

    std::vector<std::pair<double, double> > vec {{0.0, 0.0}, {1.0, 1.0}};
    BOOST_TEST(!reg.reset(vec));

    vec[0].second = -1.0;
    BOOST_TEST(!reg.reset(vec));

    vec[0].first = 1.0;
    vec[0].second = 2.0;
    BOOST_TEST(!reg.reset(vec));

    vec.pop_back();
    BOOST_TEST(!reg.reset(vec));
}

BOOST_AUTO_TEST_CASE(testLogarithm)
{
    using namespace lax::math;
    LogarithmRegression reg;

    constexpr double eps = 1e-1;
    ApproxEqual approxEqual{eps};

    for (int i = 0; i < 100; ++i)
    {
        BOOST_TEST_CHECKPOINT("logarithm try " << i << " of 100");
        const double a = RandomFixture::generate();
        const double b = RandomFixture::generate();

        std::vector<std::pair<double, double> > vec(1000);
        std::generate(vec.begin(), vec.end(), [a, b]
        {
            // +2.0 to avoid both negatives and huge y values
            const double x = RandomFixture::generate() + 2.0;
            const double noise = RandomFixture::generate() / 10000;
            const double y = a * std::log(x) + b;
            return std::pair(x, (1 + noise) * y);
        });

        BOOST_TEST_REQUIRE(reg.reset(vec));
        BOOST_TEST(approxEqual(reg.getA(), a),
                "comparison of a failed: [" << reg.getA() << " != " << a << ']');
        BOOST_TEST(approxEqual(reg.getB(), b),
                "comparison of b failed: [" << reg.getB() << " != " << b << ']');
    }
}

class CoeffOfDetTest:
    public lax::math::Regression
{
public:
    CoeffOfDetTest(double m, double b):
        m_slope(m), m_int(b) {}
private:
    bool doReset(const std::vector<PairedData>&) override
    {
        return true;
    }

    double doCalculate(double in) const override
    {
        return m_slope * in + m_int;
    }

    void print(std::ostream&) const override {}

    double m_slope;
    double m_int;
};

BOOST_AUTO_TEST_CASE(testCoeffOfDet)
{
    CoeffOfDetTest codt(1.0, 1.0);

    std::vector<std::pair<double, double> > set {
        {0.0, 1.0},
        {1.0, 2.0},
        {2.0, 3.0},
        {-1.0, 0.0},
    };

    codt.reset(set);
    BOOST_CHECK_CLOSE(codt.getCoeffOfDetermination(), 1.0, 1e-5);

    set[0].second = 0.5;
    set[1].second = 2.5;
    set[2].first = 1.75;

    codt.reset(set);
    BOOST_CHECK_CLOSE(codt.getCoeffOfDetermination(), 0.9134615385, 1e-5);
}

BOOST_AUTO_TEST_SUITE_END()
